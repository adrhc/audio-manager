package ro.go.adrhc.audiomanager.assertions;

import lombok.experimental.UtilityClass;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.FoundAudio;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.SearchedAudio;

import static org.assertj.core.api.Assertions.assertThat;

@UtilityClass
public class FoundAudioAssertions {
	public void assertEqual(FoundAudio<SearchedAudio, DiskLocation> expected,
			FoundAudio<SearchedAudio, DiskLocation> actual) {
		assertThat(actual.searchedAudio()).isEqualTo(expected.searchedAudio());
		assertThat(actual.location()).isEqualTo(expected.location());
		assertThat(actual.durations()).isEqualTo(expected.durations());
	}
}
