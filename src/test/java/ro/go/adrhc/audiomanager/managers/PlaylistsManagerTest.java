package ro.go.adrhc.audiomanager.managers;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.annotation.DirtiesContext;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocationAccessors;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.PlaylistStorageOutcome;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytMusicLocation;
import static ro.go.adrhc.audiomanager.util.FileTestUtils.copy;

/**
 * Take care of appPaths.setPlaylistsPath(tempDir) changes!
 */
@DirtiesContext
@Slf4j
class PlaylistsManagerTest extends AbstractManagersTest {
	@Autowired
	private PlaylistsManager playlistsManager;
	@Autowired
	private DiskEntriesFixManager diskEntriesHandlingManager;
	@Autowired
	private YouTubeLocationFactory ytLocationFactory;
	@Value("${:classpath:merge-from-win.m3u8}")
	private Resource mergeFromPlWin;
	@Value("${:classpath:merge-into-win.m3u8}")
	private Resource mergeIntoWin;
	@Value("${:classpath:merge-from-linux.m3u8}")
	private Resource mergeFromPlLinux;
	@Value("${:classpath:merge-into-linux.m3u8}")
	private Resource mergeIntoLinux;
	@Value("${:classpath:replaceTitleWithFileName.m3u8}")
	private Resource replaceTitleWithFileName;
	@Value("${:classpath:duplicates-win.m3u8}")
	private Resource duplicatesWin;
	@Value("${:classpath:duplicates-linux.m3u8}")
	private Resource duplicatesLinux;
	@Value("${:classpath:uniques-win.m3u8}")
	private Resource uniquesWin;
	@Value("${:classpath:uniques-linux.m3u8}")
	private Resource uniquesLinux;
	@Value("${:classpath:fixPathsFormat-win.m3u8}")
	private Resource fixPathsFormatWin;
	@Value("${:classpath:fixPathsFormat-linux.m3u8}")
	private Resource fixPathsFormatLinux;
	@Value("${:classpath:badPlaylist-win.m3u8}")
	private Resource badPlaylistWin;
	@Value("${:classpath:badPlaylist-linux.m3u8}")
	private Resource badPlaylistLinux;

	@Test
	void deduplicateByMediaId(@TempDir Path tempDir) {
		copy(duplicates(), tempDir);
		DiskLocation plLocation = DiskLocationFactory.createDiskLocation(tempDir
				.resolve(Objects.requireNonNull(duplicates().getFilename())));
		PlaylistEntries initEntries = diskPlTestRepository.loadEntries(plLocation);
		List<String> initLocations = initEntries.map(it -> it.location().toString()).toList();
		Set<String> initUniqueLocations = new HashSet<>(initLocations);
		assertThat(initLocations.size()).isNotEqualTo(initUniqueLocations.size());
		PlaylistStorageOutcome outcome = playlistsManager.deduplicateByMediaId(plLocation);
		assertTrue(outcome.stored());
		assertThat(outcome.location()).isEqualTo(plLocation);
		PlaylistEntries uniqueEntries = diskPlTestRepository.loadEntries(outcome.location());
		List<String> uniqueLocations = uniqueEntries.map(it -> it.location().toString()).toList();
		assertThat(uniqueLocations.size()).isEqualTo(initUniqueLocations.size());
		assertThat(uniqueLocations).containsAll(initUniqueLocations);
	}

	@Test
	void deduplicate(@TempDir Path tempDir) {
		copy(duplicates(), tempDir);
		DiskLocation plLocation = DiskLocationFactory.createDiskLocation(tempDir
				.resolve(Objects.requireNonNull(duplicates().getFilename())));
		PlaylistStorageOutcome outcome = playlistsManager.deduplicate(plLocation);
		assertTrue(outcome.stored());
		assertThat(outcome.location()).isEqualTo(plLocation);
		PlaylistEntries uniqueEntries = diskPlTestRepository.loadEntries(outcome.location());
		PlaylistEntries expectedUniques = diskPlTestRepository.loadEntries(uniques());
		assertThat(uniqueEntries).isEqualTo(expectedUniques);
	}

	@Test
	@SneakyThrows
	void sumDiffs(@TempDir Path tempDir) {
		DiskLocation fromLocation = DiskLocationFactory.createDiskLocation(mergeFromPl());
		DiskLocation intoLocation = DiskLocationFactory.createDiskLocation(mergeIntoPl());
		DiskLocation mergedPlLocation = DiskLocationFactory.createDiskLocation(
				tempDir.resolve("merged.m3u8"));
		playlistsManager.sumDiffs(mergedPlLocation, intoLocation, fromLocation);
		PlaylistEntries intoRecords = diskPlTestRepository.loadEntries(intoLocation);
		// Extinf title will exist in merged playlist even when missing in the original(s)
		PlaylistEntries mergedRecords = diskPlTestRepository.loadEntries(mergedPlLocation);
		assertThat(mergedRecords.entries().subList(0, intoRecords.size()))
				.containsExactlyElementsOf(intoRecords);
		assertThat(mergedRecords)
				.map(PlaylistEntry::location)
				.map(Location::toString)
				.containsSubsequence(someOtherMissingDiskSong(), "DISK: bad path3");
	}

	@Test
	@SneakyThrows
	void fixDiskLocations(@TempDir Path tempDir) {
		observableAppPaths.setPlaylistsPath(tempDir);
		DiskLocation badPathsPlLocation = copy(badPlaylist(), tempDir);
		String plName = badPathsPlLocation.name();
		DiskLocation foundLocation = DiskLocationFactory.createDiskLocation(
				tempDir.resolve(plName + " - found.m3u8"));
		DiskLocation notFoundLocation = DiskLocationFactory.createDiskLocation(
				tempDir.resolve(plName + " - NOT found.m3u8"));
		PlaylistEntries entries = diskPlTestRepository.loadEntries(badPathsPlLocation);
		log.debug("\n{}", entries);
		diskEntriesHandlingManager.fixDiskLocations(badPathsPlLocation);
		PlaylistEntries found = diskPlTestRepository.loadEntries(foundLocation);
		log.debug("\nfoundRecords:\n{}", found);
		log.debug("\n{} - found.m3u8:\n{}", plName, Files.readString(foundLocation.path()));
		PlaylistEntries notFound = diskPlTestRepository.loadEntries(notFoundLocation);
		log.debug("\nnotFoundRecords:\n{}", notFound);
		log.debug("\n{} - NOT found.m3u8:\n{}", plName, Files.readString(notFoundLocation.path()));
		// check that the number of the fixed playlist entries is overall preserved
		assertEquals(entries.size(), notFound.size() + found.size());
		// check that notFound contains only the "somemissingsong.mp3" entry
		assertThat(notFound)
				.map(DiskLocationAccessors::rawPath)
				.containsExactly(someMissingSong());
		// check that found contains the ytmusic entry
		assertThat(found)
				.map(PlaylistEntry::location)
				.contains(ytMusicLocation("SAsJ_n747T4"));
		// check that only 1 bad entry was fixed
		PlaylistEntries fixedBadEntries = found.minus(entries);
		assertThat(fixedBadEntries).hasSize(1);
		assertThat(fixedBadEntries).map(DiskLocationAccessors::rawPath)
				.filteredOn(raw -> raw.contains("Alabina") &&
						!raw.endsWith("Alabina - Alabina         Version).mp3"))
				.hasSize(1);
	}

	private Resource badPlaylist() {
		return OS.WINDOWS.isCurrentOs() ? badPlaylistWin : badPlaylistLinux;
	}

	private Resource duplicates() {
		return OS.WINDOWS.isCurrentOs() ? duplicatesWin : duplicatesLinux;
	}

	private Resource uniques() {
		return OS.WINDOWS.isCurrentOs() ? uniquesWin : uniquesLinux;
	}

	private Resource mergeFromPl() {
		return OS.WINDOWS.isCurrentOs() ? mergeFromPlWin : mergeFromPlLinux;
	}

	private Resource mergeIntoPl() {
		return OS.WINDOWS.isCurrentOs() ? mergeIntoWin : mergeIntoLinux;
	}

	private static String someMissingSong() {
		return OS.WINDOWS.isCurrentOs()
				? "C:\\some\\missing\\song\\somemissingsong.mp3"
				: "/home/gigi/some/missing/song/somemissingsong.mp3";
	}

	private static String someOtherMissingDiskSong() {
		return OS.WINDOWS.isCurrentOs()
				? "DISK: C:\\some\\missing\\song\\someothermissingsong.mp3"
				: "DISK: /home/gigi/some/missing/song/someothermissingsong.mp3";
	}
}