package ro.go.adrhc.audiomanager.managers;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.config.context.ObservableAppPaths;
import ro.go.adrhc.audiomanager.datasources.AbstractStubsBasedTest;
import ro.go.adrhc.audiomanager.datasources.disk.DiskPlTestRepository;

import java.io.IOException;

public abstract class AbstractManagersTest extends AbstractStubsBasedTest {
	@Autowired
	protected ObservableAppPaths observableAppPaths;
	protected AppPaths appPathsBackup;
	@Autowired
	protected DiskPlTestRepository diskPlTestRepository;

	@BeforeEach
	protected void beforeEach() throws IOException {
		super.beforeEach();
		appPathsBackup = observableAppPaths.cloneAppPaths();
	}

	@AfterEach
	protected void afterEach() {
		observableAppPaths.copy(appPathsBackup);
	}

	@Override
	protected BeforEachConfig beforEachConfig() {
		return new BeforEachConfig(5, 5);
	}
}
