package ro.go.adrhc.audiomanager.managers;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.annotation.DirtiesContext;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.PlaylistStorageOutcome;
import ro.go.adrhc.audiomanager.domain.location.LocationPredicates;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytVideoPlLocation;
import static ro.go.adrhc.audiomanager.stubs.TestData.*;

@DirtiesContext
class YtEntriesManagerTest extends AbstractManagersTest {
	@Autowired
	private YtEntriesManager ytEntriesHandlingManager;
	@Value("${:classpath:replaceYouTubeURIsWithLocalPaths.m3u8}")
	private Resource replaceYouTubeURIsWithLocalPaths;

	@Test
	@SneakyThrows
	void switchYtToDiskLocation(@TempDir Path tempDir) {
		observableAppPaths.setPlaylistsPath(tempDir);
		Playlist<DiskLocation> initial = diskPlTestRepository.loadPlaylist(
				replaceYouTubeURIsWithLocalPaths);
		PlaylistStorageOutcome storageOutcome = ytEntriesHandlingManager.switchYtToDiskLocation(
				initial.location());
		PlaylistEntries switched = diskPlTestRepository.loadEntries(storageOutcome.location());
		assertEquals(initial.size(), switched.size());
		assertThat(switched.filter(LocationPredicates::hasYtLocation))
				.map(PlaylistEntry::title)
				.containsExactly("O manea negăsibilă ww");
	}

	@Test
	void downloadYtPlaylistMissingTargetPath(@TempDir Path tempDir) {
		observableAppPaths.setPlaylistsPath(tempDir);
		// using relative to playlists path
		PlaylistStorageOutcome outcome = ytEntriesHandlingManager
				.downloadYtPlaylist(ytVideoPlLocation(YT_MALUKAH_PL_ID));
		DiskLocation downloadedLocation = outcome.location();
		assertEquals("%s.copy.%s".formatted(YT_MALUKAH_PL_TITLE, YT_MALUKAH_PL_ID),
				downloadedLocation.name());
		malukahPlDownloadAssertions(outcome);
	}

	@Test
	void downloadYtPlaylistWithTargetPath(@TempDir Path tempDir) {
		observableAppPaths.setPlaylistsPath(tempDir);
		DiskLocation plLocation = DiskLocationFactory.createDiskLocation("MALUKAH.copy.m3u8");
		// using absolute playlist path
		PlaylistStorageOutcome outcome = ytEntriesHandlingManager
				.downloadYtPlaylist(ytVideoPlLocation(YT_MALUKAH_PL_ID), plLocation);
		assertEquals(outcome.location().name(), "MALUKAH.copy", "Bad download location!");
		malukahPlDownloadAssertions(outcome);
	}

	@Test
	void downloadYtLikedMusicPlaylist(@TempDir Path tempDir) {
		observableAppPaths.setPlaylistsPath(tempDir);
		DiskLocation ytLikedLocation = DiskLocationFactory.createDiskLocation("local-liked.m3u8");
		PlaylistStorageOutcome outcome =
				ytEntriesHandlingManager.downloadYtLikedMusicPlaylist(ytLikedLocation);
		assertTrue(outcome.stored());
		PlaylistEntries m3u8Records = diskPlTestRepository.loadEntries(outcome.location());
		assertThat(m3u8Records).map(PlaylistEntry::title).containsOnly(
				YTM_LIKES_TITLE2, YTM_LIKES_TITLE1, YTM_LIKES_TITLE3, YTM_LIKES_TITLE4,
				YTM_LIKES_TITLE5);
	}

	private void malukahPlDownloadAssertions(PlaylistStorageOutcome outcome) {
		assertTrue(outcome.stored());
		PlaylistEntries entries = diskPlTestRepository.loadEntries(outcome.location());
		assertThat(entries.toString()).isEqualTo("""
				\040-1 seconds, Auld Lang Syne - Malukah                , youtube:video:0q5SSoho0cs
				\040-1 seconds, Malukah - Misty Mountains - The Hobbit Cover, youtube:video:0dEZMAeiAiY
				\040-1 seconds, Malukah - Three Hearts As One - Elder Scrolls Online Bard Song, youtube:video:2-lGevvO2vw
				\040-1 seconds, Malukah - Times Like These - Foo Fighters Cover, youtube:video:4SENkrissNw
				\040-1 seconds, Malukah - ‪The Dragonborn Comes - Skyrim Bard Song and Main Theme Female Cover‬, youtube:video:4z9TdDCWN7g""");
	}
}