package ro.go.adrhc.audiomanager.managers.diskplquery;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;

import java.util.ArrayList;
import java.util.List;

import static java.util.function.Predicate.not;
import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;

@ExtendWith(SpringExtension.class)
class UriSelectedLocationsTest {
	@Test
	void changes() {
		UriSelectedLocations uriSelectedLocations = UriSelectedLocations.of(
				"youtube:video:UTRSgkgKcxw", "Ibiza Summer Mix 2024");
		uriSelectedLocations.addSelected(createDiskLocation("pl11"));
		uriSelectedLocations.addSelected(createDiskLocation("pl12"));
		uriSelectedLocations.addSelected(createDiskLocation("pl21"));
		uriSelectedLocations.addSelected(createDiskLocation("pl22"));
		uriSelectedLocations.addSelected(createDiskLocation("pl23"));
		uriSelectedLocations.addDeselected(createDiskLocation("pl4"));

		List<DiskLocation> currentlySelected = new ArrayList<>();
		currentlySelected.add(createDiskLocation("pl21"));
		currentlySelected.add(createDiskLocation("pl22"));
		currentlySelected.add(createDiskLocation("pl23"));
		currentlySelected.add(createDiskLocation("pl31"));
		currentlySelected.add(createDiskLocation("pl32"));

		UriSelectedLocations changes = uriSelectedLocations.changes(currentlySelected);
		assertThat(changes).hasSize(4);

		assertThat(changes.rawFilter(LocationSelection::selected))
				.map(LocationSelection::location).map(DiskLocation::name)
				.containsOnly("pl11", "pl12");
		assertThat(changes.rawFilter(not(LocationSelection::selected)))
				.map(LocationSelection::location).map(DiskLocation::name)
				.containsOnly("pl31", "pl32");
	}
}