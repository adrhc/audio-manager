package ro.go.adrhc.audiomanager.managers;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.lang.NonNull;
import org.springframework.test.context.ContextConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.config.context.ObservableAppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory;
import ro.go.adrhc.audiomanager.managers.diskplquery.DiskPlQueryManager;
import ro.go.adrhc.audiomanager.managers.diskplquery.LocationSelections;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@EnableConfigurationProperties
@SpringBootTest(properties = {"lucene.read-only=true"},
		classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
				HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@ContextConfiguration(initializers = DiskPlQueryManagerIT.Config.class)
@Slf4j
class DiskPlQueryManagerIT {
	@TempDir
	static Path tmpDir;
	@Autowired
	private DiskPlQueryManager diskPlQueryManager;
	@Value("${:classpath:DiskPlQueryManagerIT.m3u8}")
	private Resource testPlaylist;
	@Autowired
	private ObservableAppPaths observableAppPaths;

	@ParameterizedTest
	@ValueSource(strings = {"youtube:video:T8UMtKw6Uho", "file:///C:/song1.mp3",
			"C:/song1.mp3", "/home/gigi/Music/MUZICA/Others/05%20Pretty%20Girl.mp3",
			"file:///home/gigi/Music/MUZICA/Others/05%20Pretty%20Girl.mp3"})
	void findPlLocationsByEntry(String uri) throws IOException {
		DiskLocation plLocation = DiskLocationFactory.createDiskLocation(
				observableAppPaths.getPlaylistsPath().resolve(
						Objects.requireNonNull(testPlaylist.getFilename())));
		FileUtils.copyInputStreamToFile(testPlaylist.getInputStream(), plLocation.toFile());
		LocationSelections playlists = diskPlQueryManager.findPlLocationsByEntry(uri);
		assertThat(playlists).isNotEmpty();
	}

	@EnabledOnOs(OS.LINUX)
	@Test
	void findPlsByLinuxEntry() throws IOException {
		findPlLocationsByEntry("file:///home/gigi/song2.mp3");
	}

	@EnabledOnOs(OS.WINDOWS)
	@Test
	void findPlsByWinEntry() throws IOException {
		findPlLocationsByEntry("file:///C:/song3.mp3");
	}

	static class Config
			implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		@Override
		public void initialize(@NonNull ConfigurableApplicationContext context) {
			TestPropertyValues.of(STR."context-paths.index-path=\{tmpDir}",
					STR."context-paths.playlists-path=\{tmpDir}").applyTo(context);
		}
	}
}