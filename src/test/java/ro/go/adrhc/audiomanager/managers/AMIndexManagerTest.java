package ro.go.adrhc.audiomanager.managers;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.lang.NonNull;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;

import java.io.IOException;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

@EnableConfigurationProperties
@ContextConfiguration(initializers = AMIndexManagerTest.IndexPathInitializer.class)
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@DirtiesContext
@Slf4j
class AMIndexManagerTest {
	@TempDir
	static Path tmpDir;
	@Autowired
	private AMIndexManager manager;

	@Test
	void reset() throws IOException {
		int count = manager.reset();
		assertThat(count).isZero();
	}

	static class IndexPathInitializer
			implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		@Override
		public void initialize(@NonNull ConfigurableApplicationContext context) {
			TestPropertyValues.of(STR."context-paths.index-path=\{tmpDir}",
					STR."context-paths.songs-path=\{tmpDir}").applyTo(context);
		}
	}
}