package ro.go.adrhc.audiomanager.managers;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.annotation.DirtiesContext;
import ro.go.adrhc.audiomanager.config.context.ObservableAppPaths;
import ro.go.adrhc.audiomanager.datasources.AbstractStubsBasedTest;
import ro.go.adrhc.audiomanager.datasources.disk.DiskPlTestRepository;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.stubs.AppPathsGenerator;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;

@DirtiesContext
@Slf4j
class YtDlBashScriptsManagerTest extends AbstractStubsBasedTest {
	@Autowired
	protected ObservableAppPaths observableAppPaths;
	@Autowired
	private DiskPlTestRepository diskPlRepoAccessors;
	@Autowired
	private YtDlBashScriptsManager ytDlBashScriptsManager;
	@Value("${:classpath:createDownloadScripts.m3u8}")
	private Resource createDownloadScripts;

	@BeforeAll
	void setup() {
		AppPathsGenerator.populateProdPaths(false, observableAppPaths);
		log.debug("\n{}", observableAppPaths);
	}

	@Test
	@SneakyThrows
	void createDownloadScripts(@TempDir Path tempDir) {
		observableAppPaths.setScriptsPath(tempDir);
		DiskLocation diskLocation = createDiskLocation(createDownloadScripts);
		ytDlBashScriptsManager.create(diskLocation, 10);

		Path toDownload = tempDir.resolve("createDownloadScripts-youtube-dl1.sh");
		assertTrue(Files.isRegularFile(toDownload));
		assertEquals("""
						# Clamavi De Profundis - Far Over the Misty Mountains Cold (reversed title)
						$HOME/x.sh ytxmy '-' 'http://www.youtube.com/watch?v=q-DvcEAO9xw' "Clamavi De Profundis - Far Over the Misty Mountains Cold (reversed title).%(ext)s"

						# O manea negăsibilă ww
						$HOME/x.sh ytxmy '-' 'http://www.youtube.com/watch?v=tJegIlnJNhc' "O manea negăsibilă ww.%(ext)s"
												
						# missingYtId
						$HOME/x.sh ytxmy '-' 'http://www.youtube.com/watch?v=missingYtId' "missingYtId.%(ext)s\"""",
				Files.readString(toDownload).indent(0).trim());

		Path onDisk = tempDir.resolve("createDownloadScripts-youtube-dl-on-disk.sh");
		assertTrue(Files.isRegularFile(onDisk));
		assertEquals("""
						# "Isaac Chambers - Moonlight Ride (ft. Blue Moon)" found at:
						# M:\\MUZICA\\Youtube_mp3\\Isaac Chambers - Moonlight Ride (ft. Blue Moon).mp3
						# $HOME/x.sh ytxmy '-' 'http://www.youtube.com/watch?v=SAsJ_n747T4' "Isaac Chambers - Moonlight Ride (ft. Blue Moon).%(ext)s"
						      
						# "Touch in the Night - Silent Circle" found at:
						# M:\\MUZICA\\Youtube_mp3\\Touch in the Night - Silent Circle.mp3
						# $HOME/x.sh ytxmy '-' 'http://www.youtube.com/watch?v=oDLU3e34G1o' "Touch in the Night - Silent Circle.%(ext)s\""""
						.replace("M:\\MUZICA\\Youtube_mp3\\", muzicaYoutubeMp3Dir()),
				Files.readString(onDisk).indent(0).trim());
	}

	private String muzicaYoutubeMp3Dir() {
		return "%s%s".formatted(observableAppPaths.resolveSongsPath(
				Path.of("MUZICA/Youtube_mp3")), File.separator);
	}
}