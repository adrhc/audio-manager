package ro.go.adrhc.audiomanager.managers.diskplupdate;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.managers.diskplupdate.SongSelection.ofExcluded;
import static ro.go.adrhc.audiomanager.managers.diskplupdate.SongSelection.ofIncluded;

class PlContentUpdateRequestTest {
	@Test
	void removeExcludedIfIncludedExists() {
		PlContentUpdateRequest request = PlContentUpdateRequest.of("music.m3u8");
		request.add(ofIncluded("file:///song1.mp3", "song1i"));
		request.add(ofExcluded("file:///song1.mp3", "song1e"));

		assertThat(request.removeExcludedIfIncludedExists().selections())
				.map(SongSelection::title).containsExactly("song1i");

		request.add(ofExcluded("file:///song1.mp3", "song1e2"));

		assertThat(request.removeExcludedIfIncludedExists().selections())
				.map(SongSelection::title).containsExactly("song1i");

		request.add(ofExcluded("file:///song2.mp3", "song2e"));

		assertThat(request.removeExcludedIfIncludedExists().selections())
				.map(SongSelection::title).containsExactly("song1i", "song2e");

		request.add(ofIncluded("file:///song3.mp3", "song3i"));

		assertThat(request.removeExcludedIfIncludedExists().selections())
				.map(SongSelection::title)
				.containsExactly("song1i", "song2e", "song3i");

		request.add(ofIncluded("youtube:video:HWjCStB6k4o", "duplicate"));
		request.add(ofIncluded("youtube:video:HWjCStB6k4o", "duplicate"));

		assertThat(request.removeExcludedIfIncludedExists().selections())
				.map(SongSelection::title)
				.containsExactly("song1i", "song2e", "song3i", "duplicate", "duplicate");
	}
}