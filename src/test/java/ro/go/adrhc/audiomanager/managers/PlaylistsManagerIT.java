package ro.go.adrhc.audiomanager.managers;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.config.MainProperties;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.config.context.ObservableAppPaths;
import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpec;
import ro.go.adrhc.audiomanager.datasources.disk.DiskPlTestRepository;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.PlaylistStorageOutcome;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ro.go.adrhc.audiomanager.stubs.TestData.YTM_LIKES_TITLE1;

@Disabled("Too much YouTube traffic, it loads \"lm\" PlaylistSpec!")
@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DirtiesContext
@Slf4j
public class PlaylistsManagerIT {
	@Autowired
	protected DiskPlTestRepository diskPlTestRepository;
	@Autowired
	private MainProperties appProperties;
	@Autowired
	private PlaylistsManager playlistsManager;
	@Autowired
	private PlSpecsHandlingManager plSpecsHandlingManager;
	@Autowired
	private ObservableAppPaths observableAppPaths;
	private AppPaths appPathsBackup;

	@BeforeEach
	protected void beforeEach() {
		appPathsBackup = observableAppPaths.cloneAppPaths();
	}

	@AfterEach
	protected void afterEach() {
		observableAppPaths.copy(appPathsBackup);
	}

	@Test
	void saveYtLikedPlaylist(@TempDir Path tempDir) {
		observableAppPaths.setPlaylistsPath(tempDir);
		PlaylistSpec plSpec = appProperties.getPlaylistSpec("lm");
		PlaylistStorageOutcome outcome = plSpecsHandlingManager.createPlFromSpec(plSpec);
		assertTrue(outcome.stored());
		PlaylistEntries entries = diskPlTestRepository.loadEntries(outcome.location());
		log.debug("\nlm:\n{}", entries);
		assertThat(entries).isNotEmpty();
		assertThat(entries).map(PlaylistEntry::title).contains(
				"Seven Nation Army (Live) - The White Stripes",
				"Bakerman - Laid Back",
				"Kaelyn - Look Around You",
				"Lisaya ft. Guido Staps Falling (Roan Portman Chillout Mix)",
				YTM_LIKES_TITLE1);
	}

	@Test
	void saveLmAlternativePlaylist(@TempDir Path tempDir) {
		observableAppPaths.setPlaylistsPath(tempDir);
		PlaylistSpec plSpec = appProperties.getPlaylistSpec("lm-alternative");
		PlaylistStorageOutcome outcome = plSpecsHandlingManager.createPlFromSpec(plSpec);
		assertTrue(outcome.stored());
		PlaylistEntries entries = diskPlTestRepository.loadEntries(outcome.location());
		log.debug("\nlm-alternative:\n{}", entries);
		assertThat(entries).isNotEmpty();
		assertThat(entries).map(PlaylistEntry::title).contains(
				"Destiny - Jennifer Rush",
				"Nightshift - Commodores",
				"Bakerman - Laid Back",
				"Kaelyn - Look Around You",
				"Lisaya ft. Guido Staps Falling (Roan Portman Chillout Mix)");
	}

	@Test
	void saveLmOnlyPlaylist(@TempDir Path tempDir) {
		observableAppPaths.setPlaylistsPath(tempDir);
		PlaylistSpec plSpec = appProperties.getPlaylistSpec("lm-only");
		PlaylistStorageOutcome outcome = plSpecsHandlingManager.createPlFromSpec(plSpec);
		assertTrue(outcome.stored());
		PlaylistEntries entries = diskPlTestRepository.loadEntries(outcome.location());
		log.debug("\nlm-alternative:\n{}", entries);
		assertThat(entries).isNotEmpty();
		assertThat(entries).map(PlaylistEntry::title).contains(
				"Destiny - Jennifer Rush",
				"Nightshift - Commodores",
				"Bakerman - Laid Back");
	}
}
