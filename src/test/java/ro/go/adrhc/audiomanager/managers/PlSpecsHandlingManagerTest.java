package ro.go.adrhc.audiomanager.managers;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import ro.go.adrhc.audiomanager.config.MainProperties;
import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpec;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.PlaylistStorageOutcome;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ro.go.adrhc.audiomanager.stubs.TestData.*;

@DirtiesContext
@Slf4j
class PlSpecsHandlingManagerTest extends AbstractManagersTest {
	@Autowired
	private MainProperties appProperties;
	@Autowired
	private PlSpecsHandlingManager plSpecsHandlingManager;

	@Test
	void createPlaylistBySpec(@TempDir Path tempDir) {
		observableAppPaths.setPlaylistsPath(tempDir);
		PlaylistSpec plSpec = appProperties.getPlaylistSpec("liked");
		PlaylistStorageOutcome outcome = plSpecsHandlingManager.createPlFromSpec(plSpec);
		assertTrue(outcome.stored());
		PlaylistEntries entries = diskPlTestRepository.loadEntries(outcome.location());
		log.debug("\n{}", entries);
		assertThat(entries).isNotEmpty();
		assertThat(entries).map(PlaylistEntry::title).contains(
				YTM_LIKES_TITLE3,
				YTM_LIKES_TITLE2,
				"T'he Engendrat amb Dolor - Picot",
				"The White Stripes - Seven Nation Army",
				YTM_LIKES_TITLE1);
	}

	@Test
	void createPlaylistsBySpecs(@TempDir Path tempDir) {
		observableAppPaths.setPlaylistsPath(tempDir);
		plSpecsHandlingManager.createPlFromSpecs();
		assertThat(appProperties.getPlaylistSpecs())
				.map(PlaylistSpec::filename)
				.map(Path::of)
				.map(tempDir::resolve)
				.allMatch(Files::isRegularFile);
	}
}