package ro.go.adrhc.audiomanager.managers;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ParseContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.YtMusicAuthFileRepository;

import java.util.Objects;
import java.util.stream.Stream;

import static com.jayway.jsonpath.Configuration.defaultConfiguration;
import static com.jayway.jsonpath.Option.SUPPRESS_EXCEPTIONS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtMusicAuthFileRepositoryIT {
	private final ParseContext PARSE_CONTEXT = JsonPath.using(defaultConfiguration().addOptions(SUPPRESS_EXCEPTIONS));

	@Autowired
	private YtMusicAuthFileRepository repository;

	@Test
	void getYouTubeMusicAuthJson() {
		String json = repository.getYouTubeMusicAuth();
		assertThat(firstNotNull(json, "authorization", "Authorization")).isNotBlank();
		assertThat(firstNotNull(json, "cookie", "Cookie")).isNotBlank();
		assertThat(firstNotNull(json, "x-goog-visitor-id")).isNotBlank();
		assertEquals("application/json", firstNotNull(json, "content-type", "Content-Type"));
		assertEquals("https://music.youtube.com", firstNotNull(json, "origin", "Origin"));
	}

	private String firstNotNull(String json, String... values) {
		DocumentContext documentContext = PARSE_CONTEXT.parse(json);
		return Stream.of(values)
				.map(s -> lcJsonPathRead(documentContext, STR."$.\{s}"))
				.filter(Objects::nonNull)
				.findFirst()
				.orElse(null);
	}

	private String lcJsonPathRead(DocumentContext documentContext, String jsonPath) {
		String value = documentContext.read(jsonPath);
		return value == null ? null : value.toLowerCase();
	}
}