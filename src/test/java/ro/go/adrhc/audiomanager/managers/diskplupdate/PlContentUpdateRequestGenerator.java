package ro.go.adrhc.audiomanager.managers.diskplupdate;

import static ro.go.adrhc.audiomanager.managers.diskplupdate.SongSelection.ofExcluded;
import static ro.go.adrhc.audiomanager.managers.diskplupdate.SongSelection.ofIncluded;

public class PlContentUpdateRequestGenerator {
	public static PlContentUpdateRequest create() {
		PlContentUpdateRequest request = PlContentUpdateRequest.of("music.m3u8");
		request.add(ofIncluded("file:///song1.mp3", "song1i"));
		request.add(ofExcluded("file:///song1.mp3", "song1e1"));
		request.add(ofExcluded("file:///song1.mp3", "song1e2"));
		request.add(ofExcluded("file:///song2.mp3", "song2e"));
		request.add(ofIncluded("file:///song3.mp3", "song3i"));
		return request;
	}
}
