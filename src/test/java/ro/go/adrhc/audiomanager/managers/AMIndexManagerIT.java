package ro.go.adrhc.audiomanager.managers;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.io.IOException;
import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled("It recreates or updates the index!")
@EnabledOnOs(value = OS.WINDOWS, disabledReason = "context-paths.songs-path is set for Windows")
@SpringBootTest(
		properties = "context-paths.songs-path=M:/Youtube_mp3",
		classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
				HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
public class AMIndexManagerIT {
	@Autowired
	private FileSystemIndex<URI, AudioMetadata> amIndexRepository;
	@Autowired
	private AMIndexManager manager;

	@Test
	void shallowDiskUpdate() throws IOException {
		log.debug("\nbefore update: {}", amIndexRepository.count());
		int count = manager.shallowDiskUpdate();
		log.debug("\ncount after update: {}", count);
		assertThat(count).isPositive();
	}

	@Test
	void reset() throws IOException {
		int count = manager.reset();
		log.debug("\ncount after reset: {}", count);
		assertThat(count).isPositive();
	}
}
