package ro.go.adrhc.audiomanager.infrastructure;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import ro.go.adrhc.audiomanager.infrastructure.backup.BackupUtils;
import ro.go.adrhc.audiomanager.stubs.AppPathsGenerator;

import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class BackupUtilsTest {
	private static final DateTimeFormatter DATE_PATTERN = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	private final BackupUtils backupUtils = new BackupUtils(AppPathsGenerator.create(false));

	@ParameterizedTest
	@ValueSource(strings = {"some-playlist.1900.01.01.m3u8:some-playlist.m3u8",
			"subdir/some-playlist.1900.01.01.m3u8:subdir/some-playlist.m3u8"})
	void relativePlaylistPathOf(String backupAndPlaylist) {
		String[] values = backupAndPlaylist.split(":");
		Optional<Path> relativePlaylistPathOptional =
				backupUtils.relativePlaylistPathOf(Path.of(values[0]));
		assertThat(relativePlaylistPathOptional).contains(Path.of(values[1]));
	}

	@ParameterizedTest
	@ValueSource(strings = {"some-playlist.m3u8:some-playlist.",
			"subdir/some-playlist.m3u8:subdir/some-playlist."})
	void relativeBackupPathOf(String backupAndPlaylist) {
		String[] values = backupAndPlaylist.split(":");
		Optional<Path> relativeBackupPathOptional =
				backupUtils.relativeBackupPathOf(Path.of(values[0]));
		assertThat(relativeBackupPathOptional).isNotEmpty();
		assertThat(relativeBackupPathOptional.get().toString())
				.startsWith(Path.of(values[1] +
						LocalDate.now().format(DATE_PATTERN) + ".m3u8").toString());
	}
}