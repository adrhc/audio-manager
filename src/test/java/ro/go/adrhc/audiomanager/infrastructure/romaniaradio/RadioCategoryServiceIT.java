package ro.go.adrhc.audiomanager.infrastructure.romaniaradio;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

@Disabled("DRAFT code")
@EnableConfigurationProperties
@SpringBootTest(classes = {RadioCategoryService.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class RadioCategoryServiceIT {
	@Autowired
	private RadioCategoryService service;

	@Test
	void extractCategoriesAndSubcategories() {
		Map<String, List<String>> radios = service.extractCategoriesAndSubcategories();
		log.debug("\nradios:\n{}", radios);
	}
}