package ro.go.adrhc.audiomanager.infrastructure;

import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.core.io.Resource;
import org.springframework.test.annotation.DirtiesContext;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.config.context.ObservableAppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.DiskPlTestRepository;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.audiomanager.infrastructure.backup.BackupResult;
import ro.go.adrhc.audiomanager.infrastructure.backup.BackupService;
import ro.go.adrhc.audiomanager.infrastructure.backup.RestorationResult;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DirtiesContext
class BackupServiceTest {
	@Autowired
	private ObservableAppPaths observableAppPaths;
	@Autowired
	private DiskPlTestRepository diskPlRepoAccessors;
	@Autowired
	private BackupService backupService;
	@Value("${:classpath:test.m3u8}")
	private Resource testPlaylist;

	@ParameterizedTest
	@ValueSource(strings = {"some-playlist.m3u8", "some-playlist.1900.01.01.m3u8"})
	void backup(String playlistFileName, @TempDir Path tempDir) {
		backupTest(playlistFileName, "some-playlist.", tempDir);
	}

	@Test
	void backup(@TempDir Path tempDir) {
		backupTest("some-playlist.local.m3u8", "some-playlist.local.", tempDir);
	}

	@SneakyThrows
	void backupTest(String playlistFileName, String expectedBackupPrefix, @TempDir Path tempDir) {
		setupContext(tempDir);
		// create the playlist
		DiskLocation plLocation = DiskLocationFactory.createDiskLocation(
				observableAppPaths.getPlaylistsPath().resolve(playlistFileName));
		FileUtils.copyInputStreamToFile(testPlaylist.getInputStream(), plLocation.toFile());
		// backing-up and checking the result
		Optional<BackupResult> optionalBackupResult = backupService.backup(plLocation);
		assertThat(optionalBackupResult).isNotEmpty();
		DiskLocation backup = optionalBackupResult.get().backup();
		assertThat(backup).isNotNull();
		assertThat(backup.path().toString())
				.startsWith(observableAppPaths.getBackupsPath().resolve(
						expectedBackupPrefix).toString());
		assertEqual(backup, plLocation);
	}

	@Test
	@SneakyThrows
	void restore(@TempDir Path tempDir) {
		setupContext(tempDir);
		// create testPlaylist's backup
		DiskLocation backupLocation = DiskLocationFactory.createDiskLocation(
				observableAppPaths.getBackupsPath().resolve("some-playlist.1900.01.01.m3u8"));
		FileUtils.copyInputStreamToFile(testPlaylist.getInputStream(), backupLocation.toFile());
		// restoring and checking the result
		Optional<RestorationResult> optionalRestorationResult = backupService.restore(
				backupLocation);
		assertThat(optionalRestorationResult).isNotEmpty();
		DiskLocation restored = optionalRestorationResult.get().restored();
		assertThat(restored).isNotNull();
		assertThat(restored.path()).isEqualTo(
				observableAppPaths.getPlaylistsPath().resolve("some-playlist.m3u8"));
		assertEqual(backupLocation, restored);
	}

	private void assertEqual(DiskLocation backupLocation, DiskLocation plLocation) {
		PlaylistEntries backup = diskPlRepoAccessors.loadEntries(backupLocation);
		PlaylistEntries original = diskPlRepoAccessors.loadEntries(plLocation);
		assertThat(original).containsExactly(
				backup.entries().toArray(PlaylistEntry[]::new));
	}

	@SneakyThrows
	private void setupContext(Path tempDir) {
		Path backupsPath = Files.createDirectory(tempDir.resolve("BACKUPS"));
		Path playlistsPath = Files.createDirectory(tempDir.resolve("PLAYLISTS"));
		observableAppPaths.setBackupsPath(backupsPath);
		observableAppPaths.setPlaylistsPath(playlistsPath);
	}
}