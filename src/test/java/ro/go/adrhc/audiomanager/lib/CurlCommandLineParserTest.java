package ro.go.adrhc.audiomanager.lib;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.config.YouTubeProperties;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CurlCommandLineParserTest {
	@Autowired
	private YouTubeProperties youTubeProperties;
	@Autowired
	private CurlCommandLineParser curlCommandLineParser;

	/**
	 * Copy "git bash" curl command for YouTube Music liked playlist into yt-music-liked-curl.txt.
	 */
	@Test
	void parseYtMusicCurl() throws ParseException {
		CommandLine commandLine = curlCommandLineParser
				.parse(youTubeProperties.getLikedMusicCurlCommand());
		assertEquals(2, commandLine.getArgs().length);
		assertThat(commandLine.getArgs()[0]).isEqualTo("curl");
		assertThat(commandLine.getArgs()[1])
				.containsPattern("https?://music.youtube.com/youtubei/v1/browse");
		assertThat(commandLine.getOptionValue('D'))
				.containsPattern("\\{\\s*\"context\"");
		assertThat(Stream.of(commandLine.getOptionValues('H')).map(String::toLowerCase))
				.contains("origin: https://music.youtube.com", "content-type: application/json");
	}
}
