package ro.go.adrhc.audiomanager.lib;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.core.io.Resource;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.lib.m3u8.M3u8DiskLoader;
import ro.go.adrhc.lib.m3u8.domain.M3u8Playlist;
import ro.go.adrhc.lib.m3u8.domain.M3u8Record;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.lib.m3u8.domain.Extinf.UNKNOWN_SECONDS;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class M3U8DiskLoaderTest {
	@Autowired
	private M3u8DiskLoader m3U8DiskLoader;
	@Value("${:classpath:playlist-parse.m3u8}")
	private Resource playlistParse;

	@Test
	@SneakyThrows
	void m3u8Load() {
		M3u8Playlist m3u8Playlist = m3U8DiskLoader.load(playlistParse.getFile().toPath());
		assertThat(m3u8Playlist.records()).containsExactly(
				M3u8Record.ofLocation("ytmusic:track:SAsJ_n747T4"),
				M3u8Record.of("ytmusic:track:withSeconds", 23),
				M3u8Record.of("ytmusic:track:withSecondsAndComma", UNKNOWN_SECONDS),
				M3u8Record.of("youtube:video:withSecondsAndComment", 675, "comment3"),
				M3u8Record.of("C:/Users/gigi/Temp/Muzica/test-dir/Alabina - Alabina  Version1.mp3",
						228, "Alabina - Alabina (Original Version)"),
				M3u8Record.ofLocation("M:\\MUZICA\\Youtube_mp3\\Alabina - Alabina  Version2.mp3"),
				M3u8Record.of("youtube:video:j91eq3O_R-o", UNKNOWN_SECONDS,
						"Scarborough Fair - Cover by Malukah (feat. Alexander Knutsen)"),
				M3u8Record.of("youtube:video:LQKspH34BkM", UNKNOWN_SECONDS,
						"Malukah - Fear Not This Night - Guild Wars 2 cover by Taylor, Lara, and Malukah"),
				M3u8Record.ofLocation("file:///dir1/3slash-file.txt"),
				M3u8Record.ofLocation("file:/C:/1slash-file.txt"),
				M3u8Record.ofLocation("unknown:/"),
				M3u8Record.ofLocation("yt:http://www.youtube.com/watch?v=111"),
				M3u8Record.ofLocation("youtube:http://www.youtube.com/watch?v=222"),
				M3u8Record.ofLocation("yt:https://www.youtube.com/watch?v=333"),
				M3u8Record.ofLocation("youtube:https://www.youtube.com/watch?v=444"),
				M3u8Record.ofLocation("yt:video:555"),
				M3u8Record.ofLocation("yt:playlist:777"),
				M3u8Record.ofLocation("ytmusic:playlist:ytmusic-playlist"),
				M3u8Record.ofLocation("youtube:playlist:SAsJ_n747T4")
		);
	}
}