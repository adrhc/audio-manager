package ro.go.adrhc.audiomanager.domain.playlist;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.condition.OS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.core.io.Resource;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.disk.DiskPlTestRepository;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.filters.UniquesPerTitleAndLocationTypeFilter;

import static org.assertj.core.api.Assertions.assertThat;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class UniquesPerTitleAndLocationTypeFilterTest {
	@Autowired
	private DiskPlTestRepository playlistHelper;
	@Value("${:classpath:duplicates-win.m3u8}")
	private Resource duplicatesWin;
	@Value("${:classpath:duplicates-linux.m3u8}")
	private Resource duplicatesLinux;
	@Value("${:classpath:uniques-win.m3u8}")
	private Resource uniquesWin;
	@Value("${:classpath:uniques-linux.m3u8}")
	private Resource uniquesLinux;

	@Test
	void getUniques() {
		PlaylistEntries duplicates = playlistHelper.loadEntries(duplicates());
		PlaylistEntries expected = playlistHelper.loadEntries(uniques());
		PlaylistEntries uniques = new UniquesPerTitleAndLocationTypeFilter().filter(duplicates);
		assertThat(uniques).isEqualTo(expected);
	}

	private Resource duplicates() {
		return OS.WINDOWS.isCurrentOs() ? duplicatesWin : duplicatesLinux;
	}

	private Resource uniques() {
		return OS.WINDOWS.isCurrentOs() ? uniquesWin : uniquesLinux;
	}
}