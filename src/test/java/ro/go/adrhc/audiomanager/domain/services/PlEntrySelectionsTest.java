package ro.go.adrhc.audiomanager.domain.services;

import org.junit.jupiter.api.Test;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntriesFactory.ofDiskLocations;
import static ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntryFactory.ofNamedLocation;
import static ro.go.adrhc.audiomanager.domain.services.PlEntrySelectionsGenerator.*;

class PlEntrySelectionsTest {
	@Test
	void isNotExcluded() {
		PlEntrySelections selections = PlEntrySelectionsGenerator.create();
		assertTrue(selections.isNotExcluded(ofNamedLocation(SONG1I)));
		assertTrue(selections.isNotExcluded(ofNamedLocation(URI_SONG1I)));
		assertFalse(selections.isNotExcluded(ofNamedLocation(SONG3E)));
		assertFalse(selections.isNotExcluded(ofNamedLocation(URI_SONG3E)));
	}

	@Test
	void addDuplicatedIfLessInPlaylist() {
		PlEntrySelections selections = PlEntrySelectionsGenerator.create();
		// SONG1I will be twice in selections
		selections.add(createPlEntrySelection(SONG1I, true));

		// SONG2I is only once present in entries
		PlaylistEntries entries = ofDiskLocations(SONG1I, SONG2I, SONG3E);

		// here is about adding only hence SONG3E isn't removed
		selections.addIncludedIfMissing(entries);
		assertThat(entries.map(PlaylistEntry::title))
				// SONG1I is added at the end of entries
				.containsExactly(SONG1I.name(), SONG2I.name(), SONG3E.name(), SONG1I.name());

		// no change on a 2nd add with the same already added entries
		selections.addIncludedIfMissing(entries);
		assertThat(entries.map(PlaylistEntry::title))
				.containsExactly(SONG1I.name(), SONG2I.name(), SONG3E.name(), SONG1I.name());
	}

	@Test
	void addIncludedIfMissing() {
		PlEntrySelections selections = PlEntrySelectionsGenerator.create();

		// SONG1I will be twice in selections but with different rawPath
		selections.add(createPlEntrySelection(URI_SONG1I, true));

		// SONG1I will be twice in entries (both have the same rawPath)
		PlaylistEntries entries = ofDiskLocations(SONG1I, SONG1I, SONG2I, SONG3E);

		// here is about adding only hence SONG3E isn't removed
		selections.addIncludedIfMissing(entries);

		// no change because SONG1I is present twice and is equivalent to URI_SONG1I
		assertThat(entries.map(PlaylistEntry::title))
				.containsExactly(SONG1I.name(), SONG1I.name(), SONG2I.name(), SONG3E.name());
	}
}