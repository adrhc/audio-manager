package ro.go.adrhc.audiomanager.domain.location;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;

import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocationAccessors.rawPath;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationType.MUSIC;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationType.VIDEO;
import static ro.go.adrhc.audiomanager.domain.location.LocationAccessors.locationName;
import static ro.go.adrhc.audiomanager.domain.location.LocationType.*;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class LocationParsersTest {
	@Autowired
	private LocationParsers locationParsers;

	@Test
	void parseHttpLocationOnWin() {
		String rawLocation = "http://dir/file.txt";
		Location location = locationParsers.parse(rawLocation);
		assertUriLocation(location, rawLocation);
	}

	/**
	 * On linux "unknown:/" is evaluated as DISK location!
	 */
	@EnabledOnOs(OS.WINDOWS)
	@Test
	void parseUriLocation() {
		String rawLocation = "unknown:/";
		Location location = locationParsers.parse(rawLocation);
		assertUriLocation(location, rawLocation);
	}

	@EnabledOnOs(OS.LINUX)
	@Test
	void parseLinuxLocationOnLinux() {
		String rawLocation = "/dir1/linux -   location.mp3";
		Location location = locationParsers.parse(rawLocation);
		assertDiskLocation(location, "linux - location", rawLocation, rawLocation);
	}

	@EnabledOnOs(OS.WINDOWS)
	@Test
	void parseLinuxLocationOnWin() {
		String rawLocation = "/dir1/linux -   location.mp3";
		Location location = locationParsers.parse(rawLocation);
		assertUnknownLocation(location, rawLocation);
	}

	@EnabledOnOs(OS.LINUX)
	@Test
	void parseRelativeLocationOnLinux() {
		String rawLocation = "dir1/relative -   location.mp3";
		Location location = locationParsers.parse(rawLocation);
		assertDiskLocation(location, "relative - location", rawLocation, rawLocation);
	}

	@EnabledOnOs(OS.WINDOWS)
	@Test
	void parseRelativeLocationOnWin() {
		String rawLocation = "dir1/relative -   location.mp3";
		Location location = locationParsers.parse(rawLocation);
		assertDiskLocation(location, "relative - location", "dir1\\relative -   location.mp3",
				rawLocation);
	}

	@ParameterizedTest
	@ValueSource(strings = {"yt:http://www.youtube.com/watch?v=123",
			"yt:https://www.youtube.com/watch?v=123",
			"youtube:http://www.youtube.com/watch?v=123",
			"youtube:https://www.youtube.com/watch?v=123",
			"yt:video:123", "youtube:video:123"})
	void parseUrnVideoLocation(String ytLocationCode) {
		YouTubeLocation ytLocation = createAndAssertYtLocation(ytLocationCode);
		assertThat(ytLocation.ytType()).isEqualTo(VIDEO);
		assertThat(ytLocation.code()).isEqualTo("123");
	}

	@ParameterizedTest
	@ValueSource(strings = {"ytmusic:track:123"})
	void parseUrnMusicLocation(String urnLocationCode) {
		YouTubeLocation ytLocation = createAndAssertYtLocation(urnLocationCode);
		assertThat(ytLocation.ytType()).isEqualTo(MUSIC);
		assertThat(ytLocation.code()).isEqualTo("123");
	}

	@ParameterizedTest
	@CsvSource({
			"MUSIC_PLAYLIST, ytmusic:playlist:plYtCode",
			"VIDEO_PLAYLIST, youtube:playlist:plYtCode"
	})
	void parseUrnPlaylistLocation(String ytType, String urnLocationCode) {
		YouTubeLocation ytLocation = createAndAssertYtLocation(urnLocationCode);
		assertThat(ytLocation.ytType().name()).isEqualTo(ytType);
		assertThat(ytLocation.code()).isEqualTo("plYtCode");
	}

	@EnabledOnOs(OS.WINDOWS)
	@Test
	void parseFileUriLocationWin() {
		Location location = locationParsers.parse("file:///dir/file.txt");
		assertDiskLocation(location, "file", "\\dir\\file.txt", "file:///dir/file.txt");
	}

	@EnabledOnOs(OS.LINUX)
	@Test
	void parseFileUriLocationLinux() {
		Location location = locationParsers.parse("file:///dir/file.txt");
		assertDiskLocation(location, "file", "/dir/file.txt", "file:///dir/file.txt");
	}

	@EnabledOnOs(OS.WINDOWS)
	@Test
	void parseShortFileUriLocationWin() {
		Location location = locationParsers.parse("file:/C:/1slash-file.txt");
		assertDiskLocation(location, "1slash-file", "C:\\1slash-file.txt",
				"file:/C:/1slash-file.txt");
	}

	@EnabledOnOs(OS.LINUX)
	@Test
	void parseShortFileUriLocationLinux() {
		Location location = locationParsers.parse("file:/C:/1slash-file.txt");
		assertDiskLocation(location, "1slash-file", "/C:/1slash-file.txt",
				"file:/C:/1slash-file.txt");
	}

	@EnabledOnOs(OS.WINDOWS)
	@ParameterizedTest
	@ValueSource(strings = {"C:\\dir\\file.txt", "C:/dir/file.txt"})
	void parseWinStylePathLocationOnWin(String path) {
		DiskLocation diskLocation = createAndAssertWinStylePathLocation(path);
		assertThat(diskLocation.path()).isEqualTo(Path.of("C:", "dir", "file.txt"));
	}

	@EnabledOnOs(OS.LINUX)
	@ParameterizedTest
	@ValueSource(strings = {"C:\\dir\\file.txt"})
	void parseUriWinStylePathLocationOnLinux(String path) {
		Location location = locationParsers.parse(path);
		assertUnknownLocation(location, path);
	}

	@EnabledOnOs(OS.LINUX)
	@ParameterizedTest
	@ValueSource(strings = {"C:/dir/file.txt"})
	void parseUnknownWinStylePathLocationOnLinux(String path) {
		Location location = locationParsers.parse(path);
		assertUriLocation(location, path);
	}

	public DiskLocation createAndAssertWinStylePathLocation(String path) {
		Location location = locationParsers.parse(path);
		assertThat(location.type()).isEqualTo(DISK);
		assertThat(rawPath(location)).isEqualTo(path);
		assertThat(locationName(location)).isEqualTo("file");
		return (DiskLocation) location;
	}

	private YouTubeLocation createAndAssertYtLocation(String ytLocationCode) {
		Location location = locationParsers.parse(ytLocationCode);
		assertThat(location.type()).isEqualTo(YOUTUBE);
		return (YouTubeLocation) location;
	}

	private static void assertDiskLocation(Location location, String locationName,
			String path, String rawPath) {
		assertThat(location.type()).isEqualTo(DISK);
		DiskLocation diskLocation = (DiskLocation) location;
		assertThat(diskLocation.path().toString()).isEqualTo(path);
		assertThat(diskLocation.rawPath()).isEqualTo(rawPath);
		assertThat(diskLocation.name()).isEqualTo(locationName);
	}

	private static void assertUnknownLocation(Location location, String rawLocation) {
		assertThat(location.type()).isEqualTo(UNKNOWN);
		assertThat(((UnknownLocation) location).raw()).isEqualTo(rawLocation);
	}

	private static void assertUriLocation(Location location, String rawLocation) {
		assertThat(location.type()).isEqualTo(URI);
		assertThat(location.uri().toString()).isEqualTo(rawLocation);
	}
}