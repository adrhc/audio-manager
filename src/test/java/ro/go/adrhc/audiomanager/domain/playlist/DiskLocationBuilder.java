package ro.go.adrhc.audiomanager.domain.playlist;

import lombok.Setter;
import lombok.experimental.Accessors;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.util.OSUtils;

import java.nio.file.Path;

import static org.springframework.util.StringUtils.hasText;

@Accessors(fluent = true)
@Setter
public class DiskLocationBuilder {
	private String winRawPath;
	private String linuxRawPath;
	private String winPath;
	private String linuxPath;

	public static DiskLocationBuilder ofRawPath(String rawPath) {
		return new DiskLocationBuilder().winRawPath(rawPath).linuxRawPath(rawPath);
	}

	public static DiskLocationBuilder ofLinuxRawPath(String linuxRawPath) {
		return new DiskLocationBuilder().linuxRawPath(linuxRawPath);
	}

	public DiskLocation build() {
		Path path = OSUtils.isLinux()
				? Path.of(hasText(linuxPath) ? linuxPath : linuxRawPath)
				: Path.of(hasText(winPath) ? winPath : winRawPath);
		String rawPath = OSUtils.isLinux() ? linuxRawPath : winRawPath;
		return new DiskLocation(path, rawPath);
	}
}
