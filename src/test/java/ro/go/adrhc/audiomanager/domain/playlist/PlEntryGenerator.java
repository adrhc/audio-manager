package ro.go.adrhc.audiomanager.domain.playlist;

import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import static ro.go.adrhc.audiomanager.domain.playlist.DiskLocationBuilder.ofRawPath;

public class PlEntryGenerator {
	public static PlaylistEntry createPlEntry(String rawAndWinPath, String linuxPath) {
		return createPlEntry(rawAndWinPath, rawAndWinPath, linuxPath);
	}

	public static PlaylistEntry createPlEntry(String rawPath, String winPath, String linuxPath) {
		DiskLocation diskLocation = ofRawPath(rawPath).winPath(winPath).linuxPath(linuxPath).build();
		return PlaylistEntry.of(diskLocation, diskLocation.name(), null);
	}
}
