package ro.go.adrhc.audiomanager.domain.services;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import ro.go.adrhc.audiomanager.datasources.AbstractStubsBasedTest;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.domain.playlist.PlaylistsGenerator;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.util.fn.PredicateUtils;
import ro.go.adrhc.util.text.StringUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.util.OSUtils.isLinux;

@Slf4j
class PlaylistsServiceTest extends AbstractStubsBasedTest {
	@Autowired
	private PlaylistsGenerator playlistsGenerator;
	@Autowired
	private PlaylistsService playlistsService;

	@ParameterizedTest
	@ValueSource(booleans = {true, false})
	void replaceTitleWithLocationName(boolean missingTitleOnly) {
		Playlist<?> playlist = playlistsGenerator.createDiskPlaylist();
		Playlist<?> newPlaylist = playlist.replaceTitleWithLocationName(missingTitleOnly);
		assertTitleChange(playlist, newPlaylist, missingTitleOnly ? (isLinux() ? 4 : 3) : 4);
	}

	@ParameterizedTest
	@ValueSource(booleans = {true, false})
	void updateYtEntries(boolean missingTitleOnly) {
		Playlist<?> playlist = playlistsGenerator.createDiskPlaylist();
		Playlist<?> newPlaylist = playlistsService.updateYtEntries(missingTitleOnly, playlist);
		assertTitleChange(playlist, newPlaylist, missingTitleOnly ? 2 : 5);
	}

	private void assertTitleChange(Playlist<?> playlist, Playlist<?> newPlaylist, int diffSize) {
		assertThat(newPlaylist.location()).isEqualTo(playlist.location());
		assertThat(newPlaylist.size()).isEqualTo(playlist.entries().size());
		assertThat(newPlaylist.entries().filter(PlaylistEntry::hasTitle).size())
				.isGreaterThan(playlist.entries().filter(PlaylistEntry::hasTitle).size());
		PlaylistEntries diff = newPlaylist.entries()
				.minus(PredicateUtils::eqInstance, playlist.entries());
		log.info("\nChanged titles (expected diff size is {}):\n{}", diffSize,
				StringUtils.concat(diff.map(PlaylistEntry::toString)));
		assertThat(diff.size()).isEqualTo(diffSize);
	}
}
