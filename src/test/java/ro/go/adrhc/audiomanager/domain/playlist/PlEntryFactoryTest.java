package ro.go.adrhc.audiomanager.domain.playlist;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntryFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytVideoLocation;
import static ro.go.adrhc.audiomanager.domain.playlist.DiskLocationBuilder.ofRawPath;

class PlEntryFactoryTest {
	@Test
	@DisplayName("PlaylistEntry should not have the title or duration set but only the YouTube location")
	void ofYtLocation() {
		PlaylistEntry entry = PlEntryFactory.of(ytVideoLocation("j91eq3O_R-o"));
		assertNotNullLocationNullTitleNullDuration(entry);
	}

	@Test
	@DisplayName("PlaylistEntry should not have the title or duration set but only the disk location")
	void ofLocation() {
		String rawAndWinPath = "M:\\MUZICA\\Youtube_mp3\\Alabina - Alabina  Version2.mp3";
		DiskLocation diskLocation = ofRawPath(rawAndWinPath).winPath(rawAndWinPath)
				.linuxPath("Alabina - Alabina  Version2.mp3").build();
		PlaylistEntry entry = PlEntryFactory.of(diskLocation);
		assertNotNullLocationNullTitleNullDuration(entry);
	}

	@Test
	@DisplayName("PlaylistEntry should have the title set to disk location name while duration not set")
	void ofNamedLocation() {
		String rawAndWinPath = "M:\\MUZICA\\Youtube_mp3\\Alabina - Alabina  Version2.mp3";
		DiskLocation diskLocation = ofRawPath(rawAndWinPath).winPath(rawAndWinPath)
				.linuxPath("Alabina - Alabina  Version2.mp3").build();
		PlaylistEntry entry = PlEntryFactory.ofNamedLocation(diskLocation);
		assertThat(entry.location()).isNotNull();
		assertThat(entry.title()).isNotBlank();
		assertThat(entry.duration()).isNull();
	}

	private static void assertNotNullLocationNullTitleNullDuration(PlaylistEntry entry) {
		assertThat(entry.location()).isNotNull();
		assertThat(entry.title()).isNull();
		assertThat(entry.duration()).isNull();
	}
}