package ro.go.adrhc.audiomanager.domain.playlist.entries;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.domain.playlist.PlEntryGenerator.createPlEntry;

@ExtendWith(MockitoExtension.class)
class PlaylistEntriesTest {
	@ParameterizedTest
	@ValueSource(booleans = {true, false})
	void replaceTitleWithLocationName(boolean missingTitleOnly) {
		PlaylistEntries entries = PlEntriesFactory.empty();
		entries.add(createPlEntry("file:/C:/1slash-file.txt",
				"C:/1slash-file.txt", "/C:/1slash-file.txt"));
		PlaylistEntry havingTitleEntry = createPlEntry("file:/C:/havingTitleEntry.txt",
				"C:/havingTitleEntry.txt", "/C:/havingTitleEntry.txt");
		entries.add(havingTitleEntry.title("title1"));
		entries = entries.replaceTitleWithLocationName(missingTitleOnly);
		assertThat(entries.entries()).extracting(PlaylistEntry::title)
				.containsExactly("1slash-file", missingTitleOnly ? "title1" : "havingTitleEntry");
	}
}