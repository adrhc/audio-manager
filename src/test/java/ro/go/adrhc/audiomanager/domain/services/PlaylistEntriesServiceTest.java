package ro.go.adrhc.audiomanager.domain.services;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import ro.go.adrhc.audiomanager.datasources.AbstractStubsBasedTest;
import ro.go.adrhc.audiomanager.datasources.disk.DiskPlTestRepository;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.audiomanager.managers.diskplupdate.PlContentUpdateRequest;
import ro.go.adrhc.audiomanager.managers.diskplupdate.PlContentUpdateRequestGenerator;

import static org.assertj.core.api.Assertions.assertThat;

class PlaylistEntriesServiceTest extends AbstractStubsBasedTest {
	@Autowired
	private PlaylistEntriesService plEntriesService;
	@Autowired
	private DiskPlTestRepository diskPlTestRepository;
	@Value("${:classpath:replaceTitleWithYtTitle.m3u8}")
	private Resource replaceTitleWithYtTitle;
	@Value("${:classpath:updatePlFromSelections.m3u8}")
	private Resource updatePlFromSelections;

	@Test
	void updatePlFromSelections() {
		PlaylistEntries entries = diskPlTestRepository.loadEntries(updatePlFromSelections);
		PlContentUpdateRequest request = PlContentUpdateRequestGenerator.create();
		entries = plEntriesService.update(request, entries);
		assertThat(entries).map(PlaylistEntry::title).containsExactly(
				"youtube-video", "song1-old", "song3i");
	}

	@Test
	@SneakyThrows
	void updateYtEntriesMissingTitleOnly() {
		PlaylistEntries entries = diskPlTestRepository.loadEntries(replaceTitleWithYtTitle);
		PlaylistEntries newEntries = plEntriesService.updateYtEntries(true, entries);
		PlaylistEntries diff = newEntries.minus(Record::equals, entries);
		assertThat(diff).map(PlaylistEntry::title).containsExactly(
				"Moonlight Ride - Bluey Moon, Isaac Chambers",
				"Deep Eyes - Kubix",
				"Likes Alternative",
				"Morrowind Soundtrack",
				"Touch in the Night",
				"Malukah - Video Game Covers & Originals",
				"Seven Nation Army (Live) - The White Stripes");
	}

	@Test
	@SneakyThrows
	void updateYtEntries() {
		PlaylistEntries entries = diskPlTestRepository.loadEntries(replaceTitleWithYtTitle);
		PlaylistEntries newEntries = plEntriesService.updateYtEntries(false, entries);
		PlaylistEntries diff = newEntries.minus(Record::equals, entries);
		assertThat(diff).map(PlaylistEntry::title).containsExactly(
				"Moonlight Ride - Bluey Moon, Isaac Chambers",
				"Deep Eyes - Kubix",
				"Moonlight Ride - Bluey Moon, Isaac Chambers",
				"Likes Alternative",
				"Morrowind Soundtrack",
				"Touch in the Night",
				"Malukah - Video Game Covers & Originals",
				"Seven Nation Army (Live) - The White Stripes");
	}
}