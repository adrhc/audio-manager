package ro.go.adrhc.audiomanager.domain.playlist;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory;
import ro.go.adrhc.audiomanager.domain.location.LocationParsers;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntriesFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntryFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.nio.file.Path;

import static org.junit.jupiter.api.condition.OS.WINDOWS;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.*;
import static ro.go.adrhc.lib.m3u8.domain.Extinf.UNKNOWN_SECONDS;

/**
 * Keep in sync with DiskPlaylistsRepositoryTest-win.m3u8
 * and DiskPlaylistsRepositoryTest-linux.m3u8.
 */
@RequiredArgsConstructor
@Component
public class PlaylistsGenerator {
	public static final String PLAYLIST_FILENAME = "songs.m3u8";
	private final AppPaths appPaths;
	private final LocationParsers locationParsers;

	public Playlist<DiskLocation> createDiskPlaylist() {
		PlaylistEntries entries = PlEntriesFactory.ofEntries(
				toPlEntry("yt:http://www.youtube.com/watch?v=111"),
				toPlEntry("youtube:http://www.youtube.com/watch?v=222"),
				toPlEntry("yt:https://www.youtube.com/watch?v=333"),
				toPlEntry("youtube:https://www.youtube.com/watch?v=444"),
				toPlEntry(
						WINDOWS.isCurrentOs() ? "dir1\\3slash-file.txt" : "file:///dir1/3slash-file.txt"),
				toPlEntry("file:/C:/1slash-file.txt"),
				toPlEntry("ytmusic:track:SAsJ_n747T4"),
				PlaylistEntry.of(locationParsers.parse(
								"C:/Users/gigi/Temp/Muzica/test-dir/Alabina - Alabina  Version).mp3"),
						"Alabina - Alabina (Original Version)", 228),
				toPlEntry("M:\\MUZICA\\Youtube_mp3\\Alabina - Alabina  Version).mp3"),
				toPlEntry("/MUZICA/Youtube_mp3/Alabina - Alabina  Version).mp3"),
				PlEntryFactory.of(ytVideoLocation("WlKhxulA4O8"), "Big in Japan", UNKNOWN_SECONDS),
				PlEntryFactory.of(ytVideoLocation("j91eq3O_R-o"),
						"Scarborough Fair - Cover by Malukah (feat. Alexander Knutsen)",
						UNKNOWN_SECONDS),
				PlEntryFactory.of(ytVideoLocation("LQKspH34BkM"),
						"Malukah - Fear Not This Night - Guild Wars 2 cover by Taylor, Lara, and Malukah",
						UNKNOWN_SECONDS),
				PlEntryFactory.of(ytMusicPlLocation("PLJw1EkQJl1znlNjw8B0uzGAeOEjpTh3Ot"),
						"Likes Alternative", UNKNOWN_SECONDS),
				PlEntryFactory.of(ytVideoPlLocation("PL0IYpdlafdv0gP13011Bap4N-45QkMd_Y"),
						"Nature Sounds for Sleep, Focus or Relaxation", UNKNOWN_SECONDS),
				PlEntryFactory.of(ytMusicPlLocation("PLJw1EkQJl1znlNjw8B0uzGAeOEjpTh3Ot"),
						"Likes Alternative", UNKNOWN_SECONDS),
				PlEntryFactory.of(ytVideoPlLocation("PL0IYpdlafdv0gP13011Bap4N-45QkMd_Y"),
						"Nature Sounds for Sleep, Focus or Relaxation", UNKNOWN_SECONDS),
				PlEntryFactory.of(locationParsers.parse("unknown:/")),
				PlEntryFactory.of(ytVideoLocation("-Zs1a4RhgcY"),
						"Deep Eyes - Kubix - suffix to make a wrong title", UNKNOWN_SECONDS),
				PlEntryFactory.of(ytVideoLocation("ytVideoLocation")),
				PlEntryFactory.of(ytMusicLocation("-MeC5dLat-A")),
				PlEntryFactory.of(ytMusicLocation("ytMusicLocation")),
				PlEntryFactory.of(ytVideoPlLocation("ytVideoPlLocation")),
				PlEntryFactory.of(ytMusicPlLocation("ytMusicPlLocation"))
		);

		Path plPath = appPaths.resolvePlaylistPath(PLAYLIST_FILENAME);
		DiskLocation plLocation = DiskLocationFactory.createDiskLocation(plPath);

		return new Playlist<>(plLocation, entries);
	}

	private PlaylistEntry toPlEntry(String rawLocation) {
		return PlEntryFactory.of(locationParsers.parse(rawLocation));
	}
}
