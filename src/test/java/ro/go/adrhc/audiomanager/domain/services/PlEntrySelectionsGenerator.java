package ro.go.adrhc.audiomanager.domain.services;

import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.domain.location.NamedLocation;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.audiomanager.managers.diskplupdate.SongSelection;
import ro.go.adrhc.util.pair.Pair;

import static ro.go.adrhc.audiomanager.domain.playlist.DiskLocationBuilder.ofLinuxRawPath;
import static ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntryFactory.ofNamedLocation;

public class PlEntrySelectionsGenerator {
	public static DiskLocation URI_SONG1I = ofLinuxRawPath("file:////song1i.mp3").linuxPath("/song1i.mp3")
			.winRawPath("file:///C:/song1i.mp3").winPath("C:\\song1i.mp3").build();
	public static DiskLocation URI_SONG3E = ofLinuxRawPath("file:////song3e.mp3").linuxPath("/song3e.mp3")
			.winRawPath("file:///C:/song3e.mp3").winPath("C:\\song3e.mp3").build();

	public static DiskLocation SONG1I = ofLinuxRawPath("/song1i.mp3").winRawPath("C:\\song1i.mp3").build();
	public static DiskLocation SONG2I = ofLinuxRawPath("/song2i.mp3").winRawPath("C:\\song2i.mp3").build();
	public static DiskLocation SONG3E = ofLinuxRawPath("/song3e.mp3").winRawPath("C:\\song3e.mp3").build();

	public static PlEntrySelections create() {
		PlEntrySelections selections = PlEntrySelections.of();
		selections.add(createPlEntrySelection(SONG1I, true));
		selections.add(createPlEntrySelection(SONG2I, true));
		selections.add(createPlEntrySelection(SONG3E, false));
		return selections;
	}

	public static Pair<SongSelection, PlaylistEntry>
	createPlEntrySelection(NamedLocation location, boolean include) {
		PlaylistEntry entry = ofNamedLocation(location);
		SongSelection selection = new SongSelection(entry.uri().toString(), entry.title(), include);
		return new Pair<>(selection, entry);
	}
}
