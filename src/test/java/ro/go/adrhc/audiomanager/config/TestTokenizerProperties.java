package ro.go.adrhc.audiomanager.config;

import lombok.experimental.UtilityClass;
import ro.go.adrhc.persistence.lucene.core.bare.analysis.PatternsAndReplacement;
import ro.go.adrhc.persistence.lucene.core.bare.analysis.TokenizerProperties;

import java.util.List;
import java.util.Map;

@UtilityClass
public class TestTokenizerProperties {
	public static TokenizerProperties create() {
		return new TokenizerProperties(
				2, List.of(),
				List.of("( *Official *)",
						"( *Original *)",
						"[( ]*(Audio|HD|Video) *(Version|Ve)[ )]*",
						"[( ]*(Official|Original) *((Music|Vinyl) +Video|Video +Music|Audio|HD|Music|Version|Ve|Video)[ )]*",
						"[( ]*Versiunea? *(Oficiala|Originala)[ )]*"),
				Map.of("_", " "),
				new PatternsAndReplacement("$1", List.of("([^\\s]*)\\.mp3"))
		);
	}
}
