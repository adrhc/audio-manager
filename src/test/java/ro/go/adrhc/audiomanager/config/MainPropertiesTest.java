package ro.go.adrhc.audiomanager.config;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpec;
import ro.go.adrhc.audiomanager.domain.location.Location;

import static org.assertj.core.api.Assertions.assertThat;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class MainPropertiesTest {
	@Autowired
	private MainProperties appProperties;

	@Test
	void getPlaylistSpec() {
		PlaylistSpec liked = appProperties.getPlaylistSpec("liked");
		assertThat(liked.filename()).isEqualTo("lm-all-and-local.copy.m3u8");
		assertThat(liked.locations()).map(Location::toString).contains("DISK: Youtube_mp3",
				"ytmusic:playlist:LM", "youtube:playlist:PLJw1EkQJl1znlNjw8B0uzGAeOEjpTh3Ot");
	}
}