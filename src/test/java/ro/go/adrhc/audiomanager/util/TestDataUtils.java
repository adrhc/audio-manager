package ro.go.adrhc.audiomanager.util;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntryFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.nio.file.Path;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytVideoLocation;
import static ro.go.adrhc.audiomanager.stubs.TestData.INDEX;
import static ro.go.adrhc.audiomanager.stubs.TestData.YT_VIDEOS;

@Component
@RequiredArgsConstructor
public class TestDataUtils {
	private final AppPaths appPaths;

	public DiskLocation indexDiskLocation(String title) {
		return createDiskLocation(appPaths.getSongsPath().resolve(Path.of(INDEX.get(title))));
	}

	public PlaylistEntry indexPlEntry(String title) {
		return PlEntryFactory.of(indexDiskLocation(title), title);
	}

	public PlaylistEntry indexPlEntry(String titleInIndex, String entryTitle) {
		return PlEntryFactory.of(indexDiskLocation(titleInIndex), entryTitle);
	}

	public PlaylistEntry ytVideoPlEntry(String ytCode) {
		return PlEntryFactory.of(ytVideoLocation(ytCode), YT_VIDEOS.get(ytCode));
	}
}
