package ro.go.adrhc.audiomanager.util;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.nio.file.Path;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class PathUtilsTest {
	/**
	 * On linux "unknown:/" is evaluated to a Path!
	 */
	@EnabledOnOs(OS.WINDOWS)
	@Test
	void parseLinuxPathReadInWin() {
		Optional<Path> pathOptional = PathUtils.rawToDiskPath("\\home\\gigi");
		assertThat(pathOptional).hasValue(Path.of("\\home\\gigi"));
	}

	/**
	 * On linux "unknown:/" is evaluated to a Path!
	 */
	@EnabledOnOs(OS.WINDOWS)
	@Test
	void parseUnknownLocation() {
		Optional<Path> pathOptional = PathUtils.rawToDiskPath("unknown:/");
		assertThat(pathOptional).isEmpty();
	}

	@Test
	void diskPathOfUrl() {
		Optional<Path> pathOptional = PathUtils.rawToDiskPath("http://dir/file.txt");
		assertThat(pathOptional).isEmpty();
	}

	@EnabledOnOs(OS.WINDOWS)
	@Test
	void diskPathOfFileUrlWin() {
		Path path = Path.of("C:\\Users\\gigi");
		String pathUri = path.toUri().toString();
		Optional<Path> pathOptional = PathUtils.rawToDiskPath(pathUri);
		assertThat(pathOptional).hasValue(path);
	}

	@EnabledOnOs(OS.WINDOWS)
	@Test
	void diskPathOfURIOnWin() {
		Optional<Path> pathOptional = PathUtils.rawToDiskPath(
				"file:///C:/Music/Michael%20Jackson%20-%20Liberian%20Girl.mp3");
		assertThat(pathOptional).map(Path::toString)
				.hasValue("C:\\Music\\Michael Jackson - Liberian Girl.mp3");
	}

	@EnabledOnOs(OS.LINUX)
	@Test
	void diskPathOfURIOnLinux() {
		Optional<Path> pathOptional = PathUtils.rawToDiskPath(
				"file:////Music/Michael%20Jackson%20-%20Liberian%20Girl.mp3");
		assertThat(pathOptional).map(Path::toString)
				.hasValue("/Music/Michael Jackson - Liberian Girl.mp3");
	}

	@EnabledOnOs(OS.WINDOWS)
	@Test
	void diskPathOfDecodedFileURIOnWin() {
		Optional<Path> pathOptional = PathUtils.rawToDiskPath(
				"file:///C:/Music/Michael Jackson - Liberian Girl.mp3");
		assertThat(pathOptional).map(Path::toString)
				.hasValue("C:\\Music\\Michael Jackson - Liberian Girl.mp3");
	}

	@EnabledOnOs(OS.LINUX)
	@Test
	void diskPathOfDecodedFileURIOnLinux() {
		Optional<Path> pathOptional = PathUtils.rawToDiskPath(
				"file:////Music/Michael Jackson - Liberian Girl.mp3");
		assertThat(pathOptional).map(Path::toString)
				.hasValue("/Music/Michael Jackson - Liberian Girl.mp3");
	}

	@EnabledOnOs(OS.WINDOWS)
	@ParameterizedTest
	@ValueSource(strings = {"C:\\dir\\file.txt", "C:/dir/file.txt"})
	void diskPathOfWinStylePathOnWin(String path) {
		Optional<Path> pathOptional = PathUtils.rawToDiskPath(path);
		assertThat(pathOptional).map(Path::toString).hasValue("C:\\dir\\file.txt");
	}

	@EnabledOnOs(OS.LINUX)
	@ParameterizedTest
	@ValueSource(strings = {"C:\\dir\\file.txt", "C:/dir/file.txt"})
	void diskPathOfWinStylePathOnLinux(String path) {
		Optional<Path> pathOptional = PathUtils.rawToDiskPath(path);
		assertThat(pathOptional).isEmpty();
	}

	@EnabledOnOs(OS.WINDOWS)
	@Test
	void fileURIOnWin() {
		Optional<Path> pathOptional = PathUtils.rawToDiskPath("file:/C:/file.txt");
		assertThat(pathOptional).map(Path::toString).hasValue("C:\\file.txt");
	}

	@EnabledOnOs(OS.LINUX)
	@Test
	void fileURIOnLinux() {
		Optional<Path> pathOptional = PathUtils.rawToDiskPath("file:/C:/file.txt");
		assertThat(pathOptional).map(Path::toString).hasValue("/C:/file.txt");
	}

	@ParameterizedTest
	@ValueSource(strings = {"yt:http://www.youtube.com/watch?v=SAsJ_n747T4",
			"youtube:http://www.youtube.com/watch?v=SAsJ_n747T4",
			"yt:https://www.youtube.com/watch?v=SAsJ_n747T4",
			"youtube:https://www.youtube.com/watch?v=SAsJ_n747T4"})
	void diskPathOfYtPrefixedURL(String path) {
		Optional<Path> pathOptional = PathUtils.rawToDiskPath(path);
		assertThat(pathOptional).isEmpty();
	}

	@ParameterizedTest
	@ValueSource(strings = {"ytmusic:track:SAsJ_n747T4", "yt:video:SAsJ_n747T4",
			"youtube:video:P-IHYOpI3gc", "yt:playlist:SAsJ_n747T4",
			"youtube:video:SAsJ_n747T4", "youtube:playlist:SAsJ_n747T4"})
	void diskPathOfYtURN(String path) {
		Optional<Path> pathOptional = PathUtils.rawToDiskPath(path);
		assertThat(pathOptional).isEmpty();
	}

	@ParameterizedTest
	@ValueSource(strings = {"youtube:video:P-IHYOpI3gc",
			"yt:123abc123abc-:()+,-.=@;$_!*'123abc123abc"})
	void isURN(String path) {
		assertThat(PathPredicates.isYouTubeURN(path)).isTrue().as(path + " must be an URN!");
	}

	@ParameterizedTest
	@ValueSource(strings = {"radio:abc:abc", "fakeurn:abc_1:P-IHYOpI3gc"})
	void isNotURN(String path) {
		assertThat(PathPredicates.isYouTubeURN(path))
				.isFalse().as(path + " should not be an URN!");
	}
}