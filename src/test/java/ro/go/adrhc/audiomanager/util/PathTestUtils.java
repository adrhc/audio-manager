package ro.go.adrhc.audiomanager.util;

import lombok.experimental.UtilityClass;
import org.apache.commons.io.FileUtils;

import java.nio.file.Path;

@UtilityClass
public class PathTestUtils {
	public static Path resolveToHome(String fileName) {
		Path userHome = FileUtils.getUserDirectory().toPath();
		return userHome.resolve(fileName);
	}
}
