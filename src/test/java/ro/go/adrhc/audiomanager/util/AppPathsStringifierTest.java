package ro.go.adrhc.audiomanager.util;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.condition.OS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.stubs.AppPathsGenerator;
import ro.go.adrhc.audiomanager.util.stringifier.AppPathsStringifier;
import ro.go.adrhc.util.io.FileSystemUtils;

import static org.assertj.core.api.Assertions.assertThat;

@SpringJUnitConfig(classes = {FileSystemUtils.class, AppPathsStringifier.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AppPathsStringifierTest {
	@Autowired
	private AppPathsStringifier appPathsStringifier;

	@Test
	void toStringTest() {
		if (OS.WINDOWS.isCurrentOs()) {
			assertContextPathsToString("""
					%s\\Music (index parent-path) exists:                      %b
					%s\\Music\\Index MUZICA (index path) exists:                %b
					M:\\ (songs path) exists:                                                %b
					%s\\Music\\Playlists (playlists path) exists:               %b
					%s\\Music\\Playlists-backup (playlist backups path) exists: %b
					%s\\Music\\Download-scripts (download scripts path) exists: %b""");
		} else {
			assertContextPathsToString("""
					%s/.backup/database (index parent-path) exists:                  %b
					%s/.backup/database/audio-db-web (index path) exists:            %b
					%s/Music/MUZICA (songs path) exists:                             %b
					/var/lib/mopidy/m3u (playlists path) exists:                            %b
					%s/Backups 6M/Playlists-backup (playlist backups path) exists:   %b
					%s (download scripts path) exists:                               %b""");
		}
	}

	private void assertContextPathsToString(String pattern) {
		AppPaths appPaths = AppPathsGenerator.create(false);
		String expectedContextPathsStringTrue = contextPathsToString(pattern, true);
		String expectedContextPathsStringFalse = contextPathsToString(pattern, false);
		assertThat(appPathsStringifier.toString(appPaths))
				.isIn(expectedContextPathsStringFalse, expectedContextPathsStringTrue);
	}

	/**
	 * see <a href="https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/util/Formatter.html">Formatter</a>
	 */
	private String contextPathsToString(String pattern, boolean pathsExist) {
		String userHome = FileUtils.getUserDirectory().getPath();
		if (OS.WINDOWS.isCurrentOs()) {
			return pattern.formatted(userHome, pathsExist, userHome, pathsExist,
					pathsExist, userHome, pathsExist, userHome, pathsExist, userHome, pathsExist);
		} else {
			return pattern.formatted(userHome, pathsExist, userHome, pathsExist,
					userHome, pathsExist, pathsExist, userHome, pathsExist, userHome, pathsExist);
		}
	}
}