package ro.go.adrhc.audiomanager.util;

import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationType.VIDEO;
import static ro.go.adrhc.audiomanager.util.YtLocationParser.parseOne;
import static ro.go.adrhc.audiomanager.util.YtLocationParser.parseVideoPl;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class YtLocationParserTest {
	@ParameterizedTest
	@ValueSource(strings = {"https://youtu.be/oDLU3e34G1o", "/oDLU3e34G1o",
			"http://www.youtube.com/watch?v=oDLU3e34G1o", "/watch?v=oDLU3e34G1o"})
	void parseYtVideoId(String ytUrlOrUriOrId) {
		YouTubeLocation ytLocation = parseOne(VIDEO, ytUrlOrUriOrId);
		assertEquals("oDLU3e34G1o", ytLocation.code());
	}

	@ParameterizedTest
	@ValueSource(strings = {"/playlist?list=PL6B14C3B2024B1CF9",
			"https://www.youtube.com/playlist?list=PL6B14C3B2024B1CF9"})
	void parseYtPlaylistId(String ytUrlOrUriOrId) {
		YouTubeLocation ytLocation = parseVideoPl(ytUrlOrUriOrId);
		assertEquals("PL6B14C3B2024B1CF9", ytLocation.code());
	}
}