package ro.go.adrhc.audiomanager.util;

import lombok.experimental.UtilityClass;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;

import java.io.IOException;

import static java.nio.charset.StandardCharsets.UTF_8;

@UtilityClass
public class ResourceUtils {
	public static String path(Resource resource) throws IOException {
		return resource.getFile().getPath();
	}

	public static String getText(Resource resource) throws IOException {
		return IOUtils.toString(resource.getInputStream(), UTF_8);
	}

	public static byte[] getBytes(Resource resource) throws IOException {
		return IOUtils.toByteArray(resource.getInputStream());
	}
}
