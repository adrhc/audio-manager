package ro.go.adrhc.audiomanager.util;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class PathPredicatesTest {

	@Test
	void isFileUri() {
		assertTrue(PathPredicates.isFileUri("file:///document.txt"));
	}
}