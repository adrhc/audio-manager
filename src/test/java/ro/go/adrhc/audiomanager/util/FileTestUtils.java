package ro.go.adrhc.audiomanager.util;

import lombok.SneakyThrows;
import org.springframework.core.io.Resource;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory;

import java.nio.file.Files;
import java.nio.file.Path;

public class FileTestUtils {
	@SneakyThrows
	public static DiskLocation copy(Resource resource, Path tempDir) {
		Path path = tempDir.resolve(resource.getFilename());
		Files.copy(resource.getInputStream(), path);
		return DiskLocationFactory.createDiskLocation(path);
	}
}
