package ro.go.adrhc.audiomanager;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class AppContextTest {
	@Test
	void contextLoads() {
		log.info("context loaded");
	}
}
