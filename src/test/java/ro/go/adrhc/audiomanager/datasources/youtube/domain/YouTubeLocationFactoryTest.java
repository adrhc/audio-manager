package ro.go.adrhc.audiomanager.datasources.youtube.domain;

import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationType.*;
import static ro.go.adrhc.audiomanager.domain.location.LocationType.YOUTUBE;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class YouTubeLocationFactoryTest {
	@Autowired
	private YouTubeLocationFactory youTubeLocationFactory;

	@ParameterizedTest
	@ValueSource(strings = {"ytmusic:playlist:123"})
	void ytMusicPlUrnLocationTest(String urn) {
		Optional<YouTubeLocation> locationOptional = youTubeLocationFactory.parse(urn);
		assertThat(locationOptional).isPresent();
		assertLocation(MUSIC_PLAYLIST, locationOptional.get());
	}

	@ParameterizedTest
	@ValueSource(strings = {"yt:playlist:123", "youtube:playlist:123"})
	void ytVideoPlUrnLocationTest(String urn) {
		Optional<YouTubeLocation> locationOptional = youTubeLocationFactory.parse(urn);
		assertThat(locationOptional).isPresent();
		assertLocation(VIDEO_PLAYLIST, locationOptional.get());
	}

	@ParameterizedTest
	@ValueSource(strings = {"ytmusic:track:123"})
	void youTubeUrnLocationTest(String urnLocation) {
		Optional<YouTubeLocation> locationOptional = youTubeLocationFactory.parse(urnLocation);
		assertThat(locationOptional).isPresent();
		assertLocation(MUSIC, locationOptional.get());
	}

	@ParameterizedTest
	@ValueSource(strings = {"yt:http://www.youtube.com/watch?v=123",
			"yt:https://www.youtube.com/watch?v=123",
			"youtube:http://www.youtube.com/watch?v=123",
			"youtube:https://www.youtube.com/watch?v=123"})
	void ytVideoLocationTest(String ytLocationId) {
		Optional<YouTubeLocation> locationOptional = youTubeLocationFactory.parse(ytLocationId);
		assertThat(locationOptional).isPresent();
		assertLocation(VIDEO, locationOptional.get());
	}

	private void assertLocation(YouTubeLocationType type, YouTubeLocation ytLocation) {
		assertThat(ytLocation.type()).isEqualTo(YOUTUBE);
		assertThat(ytLocation.ytType()).isEqualTo(type);
		assertThat(ytLocation.code()).isEqualTo("123");
	}
}