package ro.go.adrhc.audiomanager.datasources.disk;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.core.io.Resource;
import org.springframework.test.annotation.DirtiesContext;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.config.context.ObservableAppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.domain.playlist.PlaylistsGenerator;
import ro.go.adrhc.audiomanager.util.ResourceUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;
import static ro.go.adrhc.audiomanager.domain.playlist.PlaylistsGenerator.PLAYLIST_FILENAME;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DirtiesContext
class DiskPlaylistsRepositoryTest {
	@Autowired
	private ObservableAppPaths observableAppPaths;
	@Autowired
	private DiskPlaylistsRepository diskPlaylistsRepository;
	@Autowired
	private PlaylistsGenerator playlistsGenerator;
	@Value("${:classpath:DiskPlaylistsRepositoryTest-win.m3u8}")
	private Resource winPlaylist;
	@Value("${:classpath:DiskPlaylistsRepositoryTest-linux.m3u8}")
	private Resource linuxPlaylist;
	@Value("${:classpath:DiskPlaylistsRepositoryTest-stored-win.m3u8}")
	private Resource winPlaylistStored;
	@Value("${:classpath:DiskPlaylistsRepositoryTest-stored-linux.m3u8}")
	private Resource linuxPlaylistStored;

	@Test
	@SneakyThrows
	void load(@TempDir Path tempDir) {
		setupPlaylistsPath(tempDir);
		Playlist<DiskLocation> loaded = diskPlaylistsRepository.load(playlistToLoad());
		Playlist<DiskLocation> expected = playlistsGenerator.createDiskPlaylist();
		assertThat(loaded.entries()).isEqualTo(expected.entries());
	}

	@Test
	@SneakyThrows
	void store(@TempDir Path tempDir) {
		setupPlaylistsPath(tempDir);
		diskPlaylistsRepository.store(playlistsGenerator.createDiskPlaylist());
		assertThat(observableAppPaths.resolvePlaylistPath(PLAYLIST_FILENAME))
				.exists().hasContent(getExpectedStoredContent());
	}

	private void setupPlaylistsPath(Path tempDir) throws IOException {
		Path playlistsPath = Files.createDirectory(tempDir.resolve("PLAYLISTS"));
		observableAppPaths.setPlaylistsPath(playlistsPath);
	}

	private DiskLocation playlistToLoad() throws IOException {
		return createDiskLocation(ResourceUtils.path(getPlResourceToLoad()));
	}

	private Resource getPlResourceToLoad() {
		return OS.WINDOWS.isCurrentOs() ? winPlaylist : linuxPlaylist;
	}

	private String getExpectedStoredContent() throws IOException {
		return ResourceUtils.getText(getExpectedToStoredPlResource());
	}

	private Resource getExpectedToStoredPlResource() {
		return OS.WINDOWS.isCurrentOs() ? winPlaylistStored : linuxPlaylistStored;
	}
}