package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.core.io.Resource;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.util.ResourceUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * yt-dlp --dump-json http://music.youtube.com/watch?v=oDLU3e34G1o
 */
@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class JsonToYoutubeDlRecordConverterTest {
	@Autowired
	private ObjectMapper mapper;
	@Value("${:classpath:youtube-dl-SAsJ_n747T4.json}")
	private Resource ytDlJson;
	@Value("${:classpath:yt-dl-record-oDLU3e34G1o.json}")
	private Resource ytDlRecord;
	@Autowired
	private JsonToYoutubeDlRecordConverter converter;

	@SneakyThrows
	@Test
	void convert() {
		YoutubeDlRecord result = converter.convert(ResourceUtils.getText(ytDlJson));
		assertNotNull(result);
		YoutubeDlRecord expected = mapper.readValue(ResourceUtils.getText(ytDlRecord),
				YoutubeDlRecord.class);
		assertEquals(expected, result);
	}
}