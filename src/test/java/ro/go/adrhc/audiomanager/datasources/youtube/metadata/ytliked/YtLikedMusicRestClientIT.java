package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.ApplicationContext;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams.YouTubeApiParams;

import static org.junit.jupiter.api.Assertions.assertFalse;

@Disabled("YtLikedMusicRestClient is already tested with YtLikedMusicMetadataProviderIT!")
@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtLikedMusicRestClientIT {
	@Autowired
	private ApplicationContext ac;
	@Autowired
	private YtLikedMusicRestClient client;

	@Test
	void getLikedMusic() {
		YouTubeApiParams ytLikedMusicParams = ac.getBean(
				"ytLikedMusicParams", YouTubeApiParams.class);
		String rawYtPlaylist = client.getLiked(
				ytLikedMusicParams.headers(), ytLikedMusicParams.payload(),
				ytLikedMusicParams.queryParams(null));
		log.info("\nrawYtPlaylist:\n{}", rawYtPlaylist);
		assertFalse(rawYtPlaylist.isBlank());
		log.debug("\nrawYtPlaylist:\n{}", rawYtPlaylist);
	}
}