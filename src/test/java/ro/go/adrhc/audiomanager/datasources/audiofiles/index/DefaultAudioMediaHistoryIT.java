package ro.go.adrhc.audiomanager.datasources.audiofiles.index;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.audiofiles.history.AudioMediaHistory;
import ro.go.adrhc.audiomanager.datasources.audiofiles.history.HistoryPage;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@EnableConfigurationProperties
@SpringBootTest(properties = {"lucene.search.result-includes-missing-files=true"},
		classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
				HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@DirtiesContext
@Slf4j
class DefaultAudioMediaHistoryIT extends AbstractIndexRepositoryHolderAwareTest {
	@Autowired
	private AudioMediaHistory history;

	@Test
	void browse() throws IOException {
		HistoryPage<AudioMetadata> page1 = history.getFirst();
		assertThat(page1.scoreDocAndValues().values()).hasSize(5);

		HistoryPage<AudioMetadata> page2 = history
				.getNext(page1.lastPosition());
		assertThat(page1.scoreDocAndValues().values()).hasSize(5);

		HistoryPage<AudioMetadata> back = history
				.getPrevious(page2.firstPosition());
		assertThat(back.scoreDocAndValues().values()).hasSize(5);
	}
}