package ro.go.adrhc.audiomanager.datasources.youtube;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.util.text.StringUtils;

import static ro.go.adrhc.audiomanager.datasources.domain.SongQuery.ofFreeText;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtPlEntriesRepositoryIT {
	@Autowired
	private YtPlEntriesRepository entriesRepository;

	@Disabled("Used for debug only.")
	@Test
	void debugFindAllYtMatchesByTitle() {
		PlaylistEntries plEntries = entriesRepository.findAllVideoMatches(
				ofFreeText("Colinde Românești 2025 Colaj cu Cele Mai Iubite"));
//		log.debug("\n{}", StringUtils.concat(plEntries.map(PlaylistEntry::title)));
		log.debug("\n{}", StringUtils.concat(plEntries.map(it -> it.location().uri())));
	}
}