package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Strings.concat;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytMusicLocations;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytVideoLocations;
import static ro.go.adrhc.audiomanager.stubs.TestData.*;

/**
 * see https://github.com/ytdl-org/youtube-dl/issues/31533
 * youtube-dl --dump-json https://music.youtube.com/watch?v=SAsJ_n747T4
 */
@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtDlSongsAndVideosMetadataProviderIT {
	@Autowired
	private YtDlSongsAndVideosMetadataProvider ytDlSongsAndVideosMetadataProvider;

	@Test
	void getVideoTitles() {
		YtLocationMetadataPairs ytMetadata = ytDlSongsAndVideosMetadataProvider.loadMetadata(
				createYtLocations());
		log.debug("\n{}", concat(ytMetadata.metadataMap().values()));
		assertThat(ytMetadata.size()).isEqualTo(8);
		assertThat(ytMetadata.metadataMap().values())
				.extracting(YouTubeMetadata::title)
				.containsOnly("Touch in the Night - Silent Circle",
						YTM_LIKES_TITLE1, YTM_LIKES_TITLE2, YT_MALUKAH_PL_ITEM1_TITLE);
	}

	private static List<YouTubeLocation> createYtLocations() {
		List<YouTubeLocation> ytVideoLocations =
				ytVideoLocations("oDLU3e34G1o", YTM_LIKES_ID2, YTM_LIKES_ID1,
						YT_MALUKAH_PL_ITEM1_ID);
		List<YouTubeLocation> ytMusicLocations =
				ytMusicLocations("oDLU3e34G1o", YTM_LIKES_ID1, YTM_LIKES_ID2,
						YT_MALUKAH_PL_ITEM1_ID);
		return ListUtils.union(ytVideoLocations, ytMusicLocations);
	}
}