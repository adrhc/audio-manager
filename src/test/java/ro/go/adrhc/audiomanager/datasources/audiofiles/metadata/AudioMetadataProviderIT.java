package ro.go.adrhc.audiomanager.datasources.audiofiles.metadata;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.config.context.ObservableAppPaths;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.operations.restore.IndexDataSource;
import ro.go.adrhc.util.StopWatchUtils;
import ro.go.adrhc.util.stream.StreamCounter;

import java.io.IOException;
import java.net.URI;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled("Too much work, it loads all Youtube_mp3 metadata!")
@SpringBootTest(
//		properties = "context-paths.songs-path=${user.home}/Music/Youtube_mp3",
		properties = "context-paths.songs-path=M:/Youtube_mp3",
		classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
				HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DirtiesContext
@Slf4j
class AudioMetadataProviderIT {
	@Autowired
	private ObservableAppPaths appPaths;
	@Autowired
	private IndexDataSource<URI, AudioMetadata> indexDataSource;

	@Test
	void loadAll() throws IOException {
		StopWatch watch = StopWatchUtils.start();
		Stream<AudioMetadata> metadataStream = indexDataSource.loadAll();
		StreamCounter counter = new StreamCounter();
		counter.countedStream(metadataStream)
				.forEach(m -> log.info("\nprocessing done for: {}", m.fileNameNoExt()));
		watch.stop();
		log.debug("\ntime: {}", watch.formatTime());
		assertThat(counter.getCount()).isPositive();
	}
}