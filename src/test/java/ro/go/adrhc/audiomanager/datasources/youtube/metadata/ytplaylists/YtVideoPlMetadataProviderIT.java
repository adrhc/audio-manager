package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytVideoPlLocations;
import static ro.go.adrhc.audiomanager.stubs.TestData.*;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtVideoPlMetadataProviderIT {
	@Autowired
	private YtVideoPlMetadataProvider ytVideoPlMetadataProvider;

	@Test
	void getTitles() {
		YtLocationMetadataPairs ytLocations = ytVideoPlMetadataProvider.loadMetadata(
				ytVideoPlLocations(YT_MORROWIND_PL_ID, YT_MALUKAH_PL_ID));
		assertThat(ytLocations.size()).isEqualTo(2);
		assertThat(ytLocations.metadataMap().values())
				.map(YouTubeMetadata::title)
				.containsOnly(YT_MALUKAH_PL_TITLE, YT_MORROWIND_PL_TITLE);
	}
}
