package ro.go.adrhc.audiomanager.datasources.youtube.locations;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.domain.location.LocationsGenerator;

import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytVideoPlLocation;
import static ro.go.adrhc.audiomanager.stubs.TestData.YT_MALUKAH_PL_ID;

@SpringBootTest(properties = {"youtube-api-props.max-results=5"},
		classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
				HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtVideoPlContentLocationsProviderIT {
	@Autowired
	private YtVideoPlContentLocationsProvider ytVideoPlContentLocationsProvider;

	@Test
	void getVideoPlContent() {
		Collection<YouTubeLocation> ytLocations = ytVideoPlContentLocationsProvider
				.getPlContent(ytVideoPlLocation(YT_MALUKAH_PL_ID));

		log.debug("\nytPlIds.size = {}", ytLocations.size());
		assertFalse(ytLocations.isEmpty());

		Collection<YouTubeLocation> ytStubLocations = LocationsGenerator.createMalukahPlContent();
		log.debug("\nytPlIdsStub.size = {}", ytStubLocations.size());

		assertThat(CollectionUtils.intersection(ytLocations, ytStubLocations))
				.hasSizeGreaterThan((int) (ytStubLocations.size() * 0.8));
	}
}