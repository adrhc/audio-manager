package ro.go.adrhc.audiomanager.datasources.youtube.metadata;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ro.go.adrhc.audiomanager.datasources.AbstractStubsBasedTest;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytVideoLocation;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytVideoPlLocation;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YtMusicPlLocation.YT_LIKES_MUSIC;
import static ro.go.adrhc.audiomanager.stubs.TestData.*;

@Slf4j
class YtMetadataRepositoryTest extends AbstractStubsBasedTest {
	@Autowired
	private YtMetadataRepository ytMetadataRepository;

	@Test
	void getByLocation() {
		YouTubeMetadata metadata = ytMetadataRepository
				.loadFromLocation(ytVideoPlLocation(YT_MALUKAH_PL_ID));
		log.debug("\n{}", metadata);
		assertThat(metadata.location()).isEqualTo(ytVideoPlLocation(YT_MALUKAH_PL_ID));
		assertThat(metadata.title()).isEqualTo(YT_MALUKAH_PL_TITLE);
	}

	@Test
	void getByLocations() {
		YtLocationMetadataPairs metadata = ytMetadataRepository.loadFromLocations(
				List.of(ytVideoLocation(YTM_LIKES_ID2), ytVideoLocation(YTM_LIKES_ID1),
						ytVideoLocation(YTM_LIKES_ID2), ytVideoLocation(YT_MALUKAH_PL_ITEM1_ID),
						ytVideoLocation(YT_MALUKAH_PL_ITEM2_ID)));
		log.debug("\n{}", metadata);
		assertThat(metadata.metadataMap().values())
				.extracting(YouTubeMetadata::title)
				.containsOnly(YTM_LIKES_TITLE2, YTM_LIKES_TITLE1,
						YT_MALUKAH_PL_ITEM1_TITLE, YT_MALUKAH_PL_ITEM2_TITLE);
	}

	@Test
	void getPlContent() {
		YtLocationMetadataPairs metadata = ytMetadataRepository
				.loadFromPlaylist(ytVideoPlLocation(YT_MALUKAH_PL_ID));
		log.debug("\n{}", metadata);
		assertThat(metadata.metadataMap().values())
				.extracting(YouTubeMetadata::title)
				.containsOnly("Malukah - Times Like These - Foo Fighters Cover",
						"Malukah - ‪The Dragonborn Comes - Skyrim Bard Song and Main Theme Female Cover‬",
						"Auld Lang Syne - Malukah",
						"Malukah - Misty Mountains - The Hobbit Cover",
						"Malukah - Three Hearts As One - Elder Scrolls Online Bard Song");
	}

	@Test
	void getLikedMusic() {
		YtLocationMetadataPairs metadata = ytMetadataRepository.loadFromPlaylist(YT_LIKES_MUSIC);
		assertFalse(metadata.isEmpty());
		log.debug("\n{}", metadata);
		assertThat(metadata.metadataMap().values())
				.extracting(YouTubeMetadata::title)
				.containsOnly(
						"Absolutely Stunning The Sound of Silence - A Bass Duet with Ekaterina Shelehova",
						"Scala & Kolacny Brothers California Dreamin'",
						"Pirates of the Caribbean - 2CELLOS, Hans Zimmer",
						"Seven Nation Army (Live) - The White Stripes",
						"Deep Eyes - Kubix");
	}

	@Override
	protected BeforEachConfig beforEachConfig() {
		return new BeforEachConfig(5, 5);
	}
}