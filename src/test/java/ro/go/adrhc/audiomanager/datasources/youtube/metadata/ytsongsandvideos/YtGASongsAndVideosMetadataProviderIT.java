package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.ytdataapi.YtGASongsAndVideosMetadataProvider;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytMusicLocations;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytVideoLocations;
import static ro.go.adrhc.audiomanager.domain.location.LocationsGenerator.createYtLikedLocations;
import static ro.go.adrhc.util.text.StringUtils.concat;

@Disabled("Consumes YouTube API credits (1 unit)!")
@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtGASongsAndVideosMetadataProviderIT {
	@Autowired
	private YtGASongsAndVideosMetadataProvider ytGASongsAndVideosMetadataProvider;

	@Test
	void getMetadataChunks() {
		YtLocationMetadataPairs ytMetadata = ytGASongsAndVideosMetadataProvider
				.loadMetadata(createYtLikedLocations(51));
		log.debug("\n{}", concat(ytMetadata.metadataMap().values()));
		assertThat(ytMetadata.size()).isGreaterThanOrEqualTo(50);
		assertThat(ytMetadata.metadataMap().values())
				.extracting(YouTubeMetadata::title)
				.contains("Get Free", "Hozier - Take Me To Church",
						"Black Wolf's Inn", "Peter Belli - The house of the rising sun",
						"Isaac Chambers - Moonlight Ride (ft. Blue Moon)");
	}

	@Test
	void getMetadataForBothLocationTypesOnSameId() {
		YtLocationMetadataPairs ytMetadata = ytGASongsAndVideosMetadataProvider
				.loadMetadata(CollectionUtils.union(
						ytVideoLocations("SAsJ_n747T4", "oDLU3e34G1o"),
						ytMusicLocations("SAsJ_n747T4", "oDLU3e34G1o")));
		log.debug("\n{}", concat(ytMetadata.metadataMap().values()));
		assertThat(ytMetadata.size()).isEqualTo(4);
		assertThat(ytMetadata.metadataMap().values())
				.extracting(YouTubeMetadata::title)
				.containsOnly("Touch in the Night",
						"Isaac Chambers - Moonlight Ride (ft. Blue Moon)");
	}
}