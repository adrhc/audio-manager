package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.core.io.Resource;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.util.ResourceUtils;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtVideoSearchResultParserTest {
	@Value("${:classpath:ytVideoHotStuff.json}")
	private Resource jsonResource;

	@Test
	void extractYouTubeMetadata() throws IOException {
		YtVideoSearchResultParser parser = ytVideoSearchResultParser();
		List<YouTubeMetadata> metadata = parser.extractYouTubeMetadata();
		assertThat(metadata).isNotEmpty();
		assertThat(metadata).map(YouTubeMetadata::title).contains(
				"Donna Summer -- Hot Stuff Video HQ", "Hot Stuff", "Donna Summer- Hot Stuff");
	}

	private YtVideoSearchResultParser ytVideoSearchResultParser() throws IOException {
		String json = ResourceUtils.getText(jsonResource);
		DocumentContext context = JsonPath.parse(json);
		return new YtVideoSearchResultParser(context);
	}
}