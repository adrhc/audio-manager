package ro.go.adrhc.audiomanager.datasources.youtube.metadata.shazam;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.core.io.Resource;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.shazam.parser.ShazamCSVFileParser;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ShazamCSVFileParserTest {
	@Value("${:classpath:shazamlibrary.csv}")
	private Resource shazamlibrary;

	@Autowired
	private ShazamCSVFileParser parser;

	@Test
	void parse() throws IOException {
		List<SongQuery> records = parser.parse(shazamlibrary.getFile());
		assertThat(records).hasSize(118);
		assertThat(records).startsWith(SongQuery.ofTitleAndArtist("Hot Stuff", "Donna Summer"));
	}
}