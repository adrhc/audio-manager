package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.websearch;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.ApplicationContext;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.config.YouTubeProperties;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams.YouTubeApiParams;

import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

@Disabled("Used to debug YtLibraryProvider.")
@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtMusicLibraryIT {
	@Autowired
	private YouTubeProperties youTubeProperties;
	@Autowired
	private ApplicationContext ac;
	@Autowired
	private YtMusicLibrary client;

	@Test
	void getAll() {
		String json = getRawMusicPlaylists();
//		log.info("\nrawYtPlaylists:\n{}", json);
		assertFalse(json.isBlank());
		log.debug("\nrawYtPlaylists:\n{}", json);
	}

	@Test
	void getAllParse() {
		String json = getRawMusicPlaylists();
		YtMusicPlaylistsJsonParser parser = ytMusicPlaylistsJsonParser(json);
		Collection<YouTubeMetadata> metadata = parser.parse();
		assertThat(metadata).isNotEmpty();
	}

	private String getRawMusicPlaylists() {
		YouTubeApiParams ytMusicLibraryParams = ac.getBean(
				"ytMusicLibraryParams", YouTubeApiParams.class);
		return client.getAll(
				ytMusicLibraryParams.headers(),
				ytMusicLibraryParams.payload(),
				ytMusicLibraryParams.queryParams(null));
	}

	private YtMusicPlaylistsJsonParser ytMusicPlaylistsJsonParser(String json) {
		DocumentContext context = JsonPath.parse(json);
		return new YtMusicPlaylistsJsonParser(youTubeProperties::withoutYtMusicPrefix, context);
	}
}