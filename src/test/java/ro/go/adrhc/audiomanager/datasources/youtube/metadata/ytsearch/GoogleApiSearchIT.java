package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsearch;

import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static ro.go.adrhc.audiomanager.stubs.TestData.YTM_LIKES_ID6;
import static ro.go.adrhc.audiomanager.stubs.TestData.YTM_LIKES_TITLE6;
import static ro.go.adrhc.util.collection.CollectionUtils.firstValue;

@Disabled("Consumes too many (i.e. 100) YouTube API credits!")
@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class GoogleApiSearchIT {
	@Autowired
	private GoogleApiSearch client;

	@Test
	void list() {
		var json = client.list(1, "\"Hot Stuff\" \"Donna Summer\"");
		log.debug("\n{}", json);
		assertFalse(json.isEmpty());
		JSONArray nodes = JsonPath.read(json, "$.items");
		assertFalse(nodes.isEmpty());
		String videoId = firstValue(JsonPath.read(nodes, "$[*].id.videoId"));
		String title = firstValue(JsonPath.read(nodes, "$[*].snippet.title"));
		assertThat(videoId).isEqualTo(YTM_LIKES_ID6);
		assertThat(title).isEqualTo(YTM_LIKES_TITLE6);
	}
}