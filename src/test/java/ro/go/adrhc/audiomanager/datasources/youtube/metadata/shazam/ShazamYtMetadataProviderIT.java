package ro.go.adrhc.audiomanager.datasources.youtube.metadata.shazam;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.core.io.Resource;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class ShazamYtMetadataProviderIT {
	@Autowired
	private ShazamYtMetadataProvider ytProvider;
	@Value("${:classpath:shazamlibrary.csv}")
	private Resource shazamlibrary;
	@Value("${:classpath:shazamlibrary-3lines.csv}")
	private Resource shazamlibrary3;

	@Test
	void get3ForCsvShazam() throws IOException {
		List<YouTubeMetadata> result = ytProvider.loadMetadata(shazamlibrary3.getFile());
		assertThat(result).map(YouTubeMetadata::title)
				.containsExactly("Donna Summer -- Hot Stuff Video HQ",
						"Laid Back - Bakerman Original Video",
						"Medieval Music – Black Wolf's Inn");
	}

	@Disabled("Used for debug only!")
	@Test
	void getForCsvShazam() throws IOException {
		List<YouTubeMetadata> result = ytProvider.loadMetadata(shazamlibrary.getFile());
		assertThat(result).hasSize(117);
	}
}