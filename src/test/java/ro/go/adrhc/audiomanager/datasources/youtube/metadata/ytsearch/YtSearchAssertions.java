package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsearch;

import lombok.experimental.UtilityClass;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;

import static org.assertj.core.api.Assertions.assertThat;

@UtilityClass
public class YtSearchAssertions {
	public static void ytMetadataAssert(String ytCode, String title, YouTubeMetadata metadata) {
		assertThat(metadata.title()).isEqualTo(title);
		assertThat(metadata.location().code()).isEqualTo(ytCode);
	}
}
