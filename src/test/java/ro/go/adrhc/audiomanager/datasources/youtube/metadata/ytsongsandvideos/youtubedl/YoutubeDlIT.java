package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;

import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Strings.concat;

/**
 * https://github.com/ytdl-org/youtube-dl
 * Get cookies.txt LOCALLY (for Chrome) or cookies.txt (for Firefox)
 * <p>
 * Use -v for debugging.
 * youtube-dl --cookies %homedrive%%homepath%\Projects-adrhc\audio-manager\audio-manager\m3u8-playlists-manager\config\youtube.com_cookies.txt --dump-json http://music.youtube.com/watch?v=4zgsPO48wvw http://music.youtube.com/watch?v=eBo9IrJxQzw http://music.youtube.com/watch?v=n5scparvQWE http://music.youtube.com/watch?v=BwU7FR0uaaY http://music.youtube.com/watch?v=sFvkHaB0yfA http://music.youtube.com/watch?v=QzHXt6n1QhM http://music.youtube.com/watch?v=72oJGTPSWIM http://music.youtube.com/watch?v=zwp8UxIY_iM http://music.youtube.com/watch?v=4SENkrissNw http://music.youtube.com/watch?v=0dEZMAeiAiY http://music.youtube.com/watch?v=eOR2QYy670k http://music.youtube.com/watch?v=hbDx2FVd9e0 http://music.youtube.com/watch?v=Zqo3dCxv20U http://music.youtube.com/watch?v=0q5SSoho0cs http://music.youtube.com/watch?v=87obnKnpIds http://music.youtube.com/watch?v=Z3nzbRftpeU http://music.youtube.com/watch?v=KNDT7EInclo http://music.youtube.com/watch?v=uX1HnvlWfc4 http://music.youtube.com/watch?v=lc_3vcv8IDI http://music.youtube.com/watch?v=j91eq3O_R-o http://music.youtube.com/watch?v=8pO2KTnLPaI http://music.youtube.com/watch?v=W_3cjNiFvFE http://music.youtube.com/watch?v=Hem9ZE3U8O8 http://music.youtube.com/watch?v=VIqUuNiCG_s http://music.youtube.com/watch?v=UjEpw4O-WNI http://music.youtube.com/watch?v=re32xnyYP3A http://music.youtube.com/watch?v=LQKspH34BkM http://music.youtube.com/watch?v=MDSaYvs-LJI http://music.youtube.com/watch?v=4z9TdDCWN7g http://music.youtube.com/watch?v=2-lGevvO2vw http://music.youtube.com/watch?v=VOvadjUtE0s > videos.txt
 * <p>
 * https://github.com/ytdl-org/youtube-dl/issues/28602
 * youtube-dl --add-header 'Cookie:' --verbose --dump-json http://music.youtube.com/watch?v=SAsJ_n747T4
 * youtube-dl --cookies $HOME/Projects-adrhc/audio-manager/m3u8-playlists-manager/config/youtube.com_cookies.txt --verbose --dump-json http://music.youtube.com/watch?v=SAsJ_n747T4
 * <p>
 * yt-dlp --dump-json http://music.youtube.com/watch?v=oDLU3e34G1o
 */
@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@Slf4j
class YoutubeDlIT {
	@Autowired
	private YoutubeDl youtubeDl;

	@Test
	void getYouTubeMetadata() {
		Collection<YoutubeDlRecord> metadataList = youtubeDl.getYoutubeDlRecords(
				List.of("http://music.youtube.com/watch?v=D4ZWOyUxS4Y",
						"http://music.youtube.com/watch?v=oDLU3e34G1o"));
		// info is necessary to actually print the log to console
		log.info("\nYoutubeDlRecord(s):\n{}",
				concat(metadataList.stream().map(YoutubeDlRecord::toString)));
		assertThat(metadataList).hasSize(2);
		assertThat(metadataList).extracting(YoutubeDlRecord::getFulltitle)
				.contains("Moonlight Ride", "Touch in the Night");
	}
}