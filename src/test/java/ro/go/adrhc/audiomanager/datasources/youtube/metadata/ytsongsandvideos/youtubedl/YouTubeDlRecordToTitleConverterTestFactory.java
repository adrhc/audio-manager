package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl;

import lombok.experimental.UtilityClass;
import ro.go.adrhc.audiomanager.config.TestTokenizerProperties;
import ro.go.adrhc.persistence.lucene.core.bare.analysis.TokenizerProperties;
import ro.go.adrhc.persistence.lucene.core.bare.token.TokenComparisonUtils;
import ro.go.adrhc.persistence.lucene.core.bare.token.TokenizationUtils;

import static org.apache.lucene.util.automaton.LevenshteinAutomata.MAXIMUM_SUPPORTED_DISTANCE;
import static ro.go.adrhc.persistence.lucene.core.bare.analysis.AnalyzerFactory.defaultAnalyzer;

@UtilityClass
public class YouTubeDlRecordToTitleConverterTestFactory {
	public static YouTubeDlRecordToTitleConverter create() {
		TokenizerProperties tokenizerProperties = TestTokenizerProperties.create();
		TokenizationUtils tokenizationUtils = defaultAnalyzer(tokenizerProperties)
				.map(TokenizationUtils::new).orElseThrow();
		TokenComparisonUtils tokenUtils = new TokenComparisonUtils(tokenizationUtils);
		return new YouTubeDlRecordToTitleConverter(tokenUtils, MAXIMUM_SUPPORTED_DISTANCE);
	}
}
