package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.core.io.Resource;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser.ytliked.YtMusicSearchResultParser;
import ro.go.adrhc.audiomanager.util.ResourceUtils;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtMusicSearchResultParserTest {
	@Value("${:classpath:ytMusicHotStuff.json}")
	private Resource jsonResource;

	@Test
	void extractYouTubeMetadata() throws IOException {
		YtMusicSearchResultParser parser = ytMusicSearchResultParser();
		List<YouTubeMetadata> metadata = parser.extractYouTubeMetadata();
		log.debug("\nfound {} songs", metadata.size());
		assertThat(metadata).isNotEmpty();
		assertThat(metadata).map(YouTubeMetadata::title).contains(
				"Hot Stuff (12 Version) - Donna Summer",
				"Hot Stuff (Single Version) - Donna Summer",
				"Hot Stuff - Donna Summer, Kygo");
	}

	@Test
	void parseYtIds() throws IOException {
		YtMusicSearchResultParser parser = ytMusicSearchResultParser();
		Set<String> ids = parser.parseYtIds();
		log.debug("\nfound {} song ids", ids.size());
		assertThat(ids).isNotEmpty();
		assertThat(ids).contains("P_7N3iUeEUk", "sAu-wx5LIiU", "bHfrdQ8h2Pw");
	}

	private YtMusicSearchResultParser ytMusicSearchResultParser() throws IOException {
		String json = ResourceUtils.getText(jsonResource);
		DocumentContext context = JsonPath.parse(json);
		return new YtMusicSearchResultParser(false, context);
	}
}