package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory.ytMusicPlLocation;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtMusicPlContentMetadataProviderIT {
	@Autowired
	private YtMusicPlContentMetadataProvider provider;

	/**
	 * Clamavi De Profundis = "VL" + "PLJw1EkQJl1zkzbFu6Yotwhy0gSry4Tcf6"
	 * VL is necessary only when using music.youtube.com instead of Google API.
	 */
	@Test
	void getPlContent() {
		Collection<YouTubeMetadata> metadata = provider.getPlContent(
				ytMusicPlLocation("PLJw1EkQJl1zkzbFu6Yotwhy0gSry4Tcf6"));
		log.debug("\nytMusicPl metadata.size = {}", metadata.size());
		assertFalse(metadata.isEmpty(), "Update yt-music-liked-curl.txt!");
	}
}