package ro.go.adrhc.audiomanager.datasources.youtube.domain;

import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class YouTubeLocationPatternsTest {
	@Autowired
	private YouTubeLocationPatterns youTubeLocationPatterns;

	@ParameterizedTest
	@ValueSource(strings = {"youtube:video:123", "yt:video:123"})
	void parseYouTubeVideoUrnLocationId(String urn) {
		Optional<String[]> parts = youTubeLocationPatterns.parseYtLocationCode(urn);
		assertThat(parts).isPresent();
		assertThat(parts.get()).hasSize(3);
		assertThat(parts.get()).containsSequence("123", "video");
		assertThat(parts.get()).containsAnyOf("youtube", "yt");
	}

	@ParameterizedTest
	@ValueSource(strings = {"ytmusic:track:123", "ytmusic\\track\\123", "ytmusic/track/123"})
	void parseYouTubeMusicUrnLocationId(String urn) {
		Optional<String[]> parts = youTubeLocationPatterns.parseYtLocationCode(urn);
		assertThat(parts).isPresent();
		assertThat(parts.get()).hasSize(3);
		assertThat(parts.get()).containsExactly("123", "track", "ytmusic");
	}

	@ParameterizedTest
	@ValueSource(strings = {"yt:http://www.youtube.com/watch?v=123",
			"yt:https://www.youtube.com/watch?v=123",
			"youtube:http://www.youtube.com/watch?v=123",
			"youtube:https://www.youtube.com/watch?v=123"})
	void parseYouTubeLocationId(String youTubeLocationId) {
		Optional<String[]> parts = youTubeLocationPatterns
				.parseYtLocationCode(youTubeLocationId);
		assertThat(parts).isPresent();
		assertThat(parts.get()).hasSize(1);
		assertThat(parts.get()).containsExactly("123");
	}
}