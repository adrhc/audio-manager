package ro.go.adrhc.audiomanager.datasources.audiofiles.index;

import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.queryparser.flexible.core.QueryNodeException;
import org.apache.lucene.search.FieldExistsQuery;
import org.apache.lucene.search.Query;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.stubs.AudioMetadataGenerator;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;
import ro.go.adrhc.persistence.lucene.core.bare.query.BooleanQueryBuilder;
import ro.go.adrhc.persistence.lucene.core.bare.query.DefaultFieldAwareQueryParser;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Strings.concat;
import static ro.go.adrhc.audiomanager.datasources.audiofiles.index.AudioMetadataFields.*;
import static ro.go.adrhc.audiomanager.stubs.AudioMetadataGenerator.ofTag;
import static ro.go.adrhc.audiomanager.stubs.AudioMetadataGenerator.ofTags;
import static ro.go.adrhc.util.fn.FunctionFactory.nullFailResultFn;

@SpringBootTest(properties = "lucene.search.result-includes-missing-files=true",
		classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
				HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@Slf4j
public class AMIndexQueriesIT extends AbstractIndexRepositoryHolderAwareTest {
	@Autowired
	private FileSystemIndex<URI, AudioMetadata> amIndexRepository;

	@Test
	void hasManyTags() throws IOException {
		int initCount = amIndexRepository.count();
		amIndexRepository.addOne(ofTags(Set.of("LIKED", "LOVED")));
		amIndexRepository.addOne(ofTag("LIKED"));
		Query query = AMQueryFactory.hasAllTags(Set.of("LIKED", "LOVED"));
		List<AudioMetadata> metadata = amIndexRepository.findMany(query);
		assertThat(metadata).hasSize(1);
		amIndexRepository.removeByQuery(AMQueryFactory.hasTag("LIKED"));
		log.debug("\nentries:\n{}\ncount before: {}\ncount after: {}",
				concat(metadata), initCount, amIndexRepository.count());
		assertThat(amIndexRepository.count()).isEqualTo(initCount);
	}

	@Test
	void hasOneTag() throws IOException {
		amIndexRepository.addOne(ofTag("LIKED"));
		Query query = AMQueryFactory.hasTag("LIKED");
		List<AudioMetadata> metadata = amIndexRepository.findMany(query);
		log.debug("\nLIKED entries:\n{}", concat(metadata));
		assertThat(metadata).hasSize(1);
		amIndexRepository.removeByQuery(AMQueryFactory.hasTag("LIKED"));
	}

	@Test
	void noLocation() throws IOException {
		AudioMetadata metadata = AudioMetadataGenerator.ofNoLocation();
		amIndexRepository.addOne(metadata);
		List<AudioMetadata> sample = amIndexRepository.findMany(noLocationQuery());
		log.debug("\nno location entries: {}", sample.size());
	}

	@Test
	void noFileNameNoExt() throws IOException {
		List<AudioMetadata> sample = amIndexRepository.findMany(noFileNameNoExtQuery());
		log.debug("\nno fileNameNoExt entries: {}", sample.size());
	}

	@Test
	void fileNameNoExtQueryParser() throws IOException, QueryNodeException {
		FileSystemIndex<URI, AudioMetadata> repo = repositoryHolder.getIndexRepository();
		assertThat(repo)
				.as("IndexRepositoryHolder.getIndexRepository() must not return null!")
				.isNotNull();
		DefaultFieldAwareQueryParser fileNameNoExtQParser = DefaultFieldAwareQueryParser
				.create(repo.getIndexServicesParamsFactory().getAnalyzer(), fileNameNoExt);
		Optional<Query> query = fileNameNoExtQParser.parse("Alabina Charles Burbank");
		List<AudioMetadata> sample = query.map(
				nullFailResultFn(amIndexRepository::findMany)).orElseGet(List::of);
		log.debug("\ncount: {}", sample.size());
	}

	@Test
	void removeNullLocations() throws IOException {
		log.debug("\ncount before not(DISK) removal: {}", amIndexRepository.count());
		amIndexRepository.removeByQuery(noLocationQuery());
		log.debug("\ncount after not(DISK) removal: {}", amIndexRepository.count());
	}

	private static Query noLocationQuery() {
//		Query query = mustNotSatisfy(new FieldExistsQuery(locationType.name()));
//		Query query = new FieldExistsQuery("locationType");
//		Query query = LOCATION_TYPE_QUERY.newExactQuery("");
//		Query query = LOCATION_TYPE_QUERY.newExactQuery((String) null); // NPE
		BooleanQueryBuilder builder = new BooleanQueryBuilder();
		builder.mustSatisfy(new FieldExistsQuery(uri.name()));
		builder.mustNotSatisfy(new FieldExistsQuery(locationType.name()));
//		builder.mustNotSatisfy(new FieldExistsQuery(fileNameNoExt.name()));
//		builder.mustNotSatisfy(LOCATION_TYPE_QUERY.newExactQuery(DISK));
		return builder.build();
	}

	private static Query noFileNameNoExtQuery() {
//		Query query = mustNotSatisfy(new FieldExistsQuery(locationType.name()));
//		Query query = new FieldExistsQuery("locationType");
//		Query query = LOCATION_TYPE_QUERY.newExactQuery("");
//		Query query = LOCATION_TYPE_QUERY.newExactQuery((String) null); // NPE
		BooleanQueryBuilder builder = new BooleanQueryBuilder();
		builder.mustSatisfy(new FieldExistsQuery(uri.name()));
		builder.mustNotSatisfy(new FieldExistsQuery(fileNameNoExt.name()));
//		builder.mustNotSatisfy(new FieldExistsQuery(fileNameNoExt.name()));
//		builder.mustNotSatisfy(LOCATION_TYPE_QUERY.newExactQuery(DISK));
		return builder.build();
	}
}
