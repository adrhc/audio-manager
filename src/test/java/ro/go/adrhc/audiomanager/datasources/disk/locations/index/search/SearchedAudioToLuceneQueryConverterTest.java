package ro.go.adrhc.audiomanager.datasources.disk.locations.index.search;

import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.search.Query;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import ro.go.adrhc.audiomanager.config.lucene.AMIndexConfig;
import ro.go.adrhc.audiomanager.config.lucene.AMIndexProperties;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.SearchedAudio;
import ro.go.adrhc.audiomanager.stubs.SearchedAudioGenerator;
import ro.go.adrhc.util.io.FileSystemUtils;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@EnableConfigurationProperties
@SpringBootTest(properties = "lucene.search.result-includes-missing-files=true",
		classes = {AMIndexProperties.class, FileSystemUtils.class, AMIndexConfig.class,
				SearchedAudioToLuceneQueryConverterFactory.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class SearchedAudioToLuceneQueryConverterTest {
	@Autowired
	private SearchedAudioToLuceneQueryConverterFactory<SearchedAudio> converterFactory;

	@Test
	void convert() {
		SearchedAudio searchedAudio = SearchedAudioGenerator.longWords();
		Optional<Query> queryOptional = converterFactory.create().convert(searchedAudio);
		assertThat(queryOptional).isNotEmpty();
		Query query = queryOptional.get();
		log.debug("\nquery: {}", query);
	}
}