package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams.YouTubeApiParams;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtLikedMusicParamsTest {
	@Autowired
	@Qualifier("ytLikedMusicParams")
	private YouTubeApiParams ytLikedMusicParams;

	@Test
	void extract() {
		log.debug("\n{}", ytLikedMusicParams);
		assertAll(Stream.of("authorization", "cookie", "x-origin")
				.map(h -> () -> {
					assertTrue(ytLikedMusicParams.getFirstHeaderValue(h).isPresent());
					assertThat(ytLikedMusicParams.getFirstHeaderValue(h).get()).isNotBlank();
				}));
		assertThat(ytLikedMusicParams.key()).isNotEmpty().isNotBlank();
		assertThat(ytLikedMusicParams.payload()).isNotEmpty().isNotBlank();
	}
}