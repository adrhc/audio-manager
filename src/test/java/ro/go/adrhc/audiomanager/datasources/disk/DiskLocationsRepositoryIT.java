package ro.go.adrhc.audiomanager.datasources.disk;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.core.io.Resource;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.audiofiles.index.AbstractIndexRepositoryHolderAwareTest;
import ro.go.adrhc.audiomanager.datasources.audiofiles.metadata.AudioMetadataLoader;
import ro.go.adrhc.audiomanager.datasources.disk.locations.DiskLocationsRepository;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.SongQueryLocationPairs;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.datasources.domain.SongQuery.ofTitle;

@EnableConfigurationProperties
@SpringBootTest(properties = "lucene.search.result-includes-missing-files=true",
		classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
				HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class DiskLocationsRepositoryIT extends AbstractIndexRepositoryHolderAwareTest {
	private static final String FILE_NAME =
			"Alabina (Original Version) - Alabina, Charles Burbank, Pedro Maza Luaces, Samy Goz";
	@Autowired
	private AppPaths appPaths;
	@Autowired
	private AudioMetadataLoader audioMetadataLoader;
	@Autowired
	private DiskPlTestRepository diskPlTestRepository;
	@Autowired
	private DiskPlaylistsRepository diskPlaylistsRepository;
	@Autowired
	private DiskLocationsRepository<PlEntrySongQuery> diskLocationsRepository;
	@Value("${:classpath:searchLocalMatchesByPaths.m3u8}")
	private Resource searchLocalMatchesByPathsResource;

	@BeforeAll
	void beforeAll() throws IOException {
		insertAlabina();
	}

	@Test
	@SneakyThrows
	void findBestMatch() {
		Optional<DiskLocation> bestMatch = diskLocationsRepository
				.findBestMatch(ofTitle("Alabina Charles Burbank"));
		assertThat(bestMatch).map(DiskLocation::name).contains(FILE_NAME);
	}

	@Test
	@SneakyThrows
	void findMany() {
		List<DiskLocation> matches = diskLocationsRepository.findMany(
				ofTitle("Alabina Charles Burbank"));
		assertThat(matches).map(DiskLocation::path)
				.map(Path::getFileName).map(Path::toString)
				.contains(STR."\{FILE_NAME}.mp3");
	}

	@Test
	@SneakyThrows
	void findBestOfEach() {
		List<PlEntrySongQuery> queries = diskPlTestRepository
				.loadEntries(searchLocalMatchesByPathsResource)
				.filter(PlaylistEntry::hasTitle)
				.map(PlEntrySongQuery::of)
				.toList();
		SongQueryLocationPairs<PlEntrySongQuery, DiskLocation> result =
				diskLocationsRepository.findBestMatches(queries);
		assertThat(result.size()).isEqualTo(1);
		assertThat(result.pairs().keySet())
				.map(PlEntrySongQuery::plEntryTitle)
				.containsExactly("Alabina)");
	}

	private void insertAlabina() throws IOException {
		Optional<AudioMetadata> metadataOptional = audioMetadataLoader.load(
				appPaths.getSongsPath().resolve("Youtube_mp3")
						.resolve(STR."\{FILE_NAME}.mp3"));
		assertThat(metadataOptional).isNotEmpty();
		AudioMetadata metadata = metadataOptional.get();
		getIndexRepository().upsert(metadata);
	}
}