package ro.go.adrhc.audiomanager.datasources.youtube;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ro.go.adrhc.audiomanager.datasources.AbstractStubsBasedTest;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.audiomanager.util.TestDataUtils;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class YtPlEntriesRepositoryTest extends AbstractStubsBasedTest {
	@Autowired
	private TestDataUtils testDataUtils;
	@Autowired
	private YtPlEntriesRepository entriesRepository;

	@Test
	void findAllYtMatchesByTitle() {
		PlaylistEntry expectedEntry = testDataUtils.ytVideoPlEntry("oDLU3e34G1o");
		PlaylistEntries plEntries = entriesRepository.findAllVideoMatches(
				SongQuery.ofTitle("Touch in the Night"));
		assertThat(plEntries).contains(expectedEntry);
	}
}
