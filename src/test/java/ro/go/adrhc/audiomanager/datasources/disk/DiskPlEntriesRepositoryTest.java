package ro.go.adrhc.audiomanager.datasources.disk;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ro.go.adrhc.audiomanager.datasources.AbstractStubsBasedTest;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.domain.playlist.entries.*;
import ro.go.adrhc.audiomanager.util.TestDataUtils;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;

public class DiskPlEntriesRepositoryTest extends AbstractStubsBasedTest {
	@Autowired
	private TestDataUtils testDataUtils;
	@Autowired
	private DiskPlEntriesRepository entriesRepository;

	@Test
	void findBestDiskMatches() throws IOException {
		PlaylistEntry plEntry = PlEntryFactory.ofNamedLocation(createDiskLocation("Big in Japan"));
		PlaylistEntry expectedEntry = testDataUtils.indexPlEntry("Big in Japan");
		EntriesPairs pairs = entriesRepository.findBestMatches(PlEntriesFactory.of(plEntry));
		assertThat(pairs.getPair(plEntry)).hasValue(expectedEntry);
	}

	@Test
	void findAllDiskMatches() throws IOException {
		PlaylistEntry expectedEntry = testDataUtils.indexPlEntry(
				"Big in Japan", "Alphaville - Big_in_Japan_2019_Remaster");
		PlaylistEntries plEntries = entriesRepository.findMany(SongQuery.ofTitle("Big in Japan"));
		assertThat(plEntries).contains(expectedEntry);
	}
}
