package ro.go.adrhc.audiomanager.datasources.audiofiles.metadata;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.domain.location.UriLocation;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.stubs.AudioMetadataGenerator.ofUriLocation;
import static ro.go.adrhc.audiomanager.stubs.AudioMetadataGenerator.ofYtLocation;

@EnableConfigurationProperties
@SpringBootTest(properties = {"lucene.search.result-includes-missing-files=true"},
		classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
				HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@DirtiesContext
@Slf4j
class AudioMetadataToPlEntryConverterTest {
	private static final String URI = "http://89.238.227.6:8032/;stream/1";

	@Autowired
	private AudioMetadataToPlEntryConverter converter;

	@Test
	void convertToYtEntry() {
		AudioMetadata metadata = ofYtLocation("ytmusic:track:Yp60yUb6nYo");
		Optional<PlaylistEntry> entryOptional = converter.convert(metadata);
		assertThat(entryOptional).isNotEmpty();
		PlaylistEntry entry = entryOptional.get();
		assertThat(entry.location()).isInstanceOf(YouTubeLocation.class);
		YouTubeLocation ytLocation = (YouTubeLocation) entry.location();
		assertThat(ytLocation).hasToString("ytmusic:track:Yp60yUb6nYo");
	}

	@Test
	void convertToUriEntry() {
		AudioMetadata metadata = ofUriLocation(URI, "Bucuresti FM");
		Optional<PlaylistEntry> entryOptional = converter.convert(metadata);
		assertThat(entryOptional).isNotEmpty();
		PlaylistEntry entry = entryOptional.get();
		assertThat(entry.location()).isInstanceOf(UriLocation.class);
		assertThat(entry.uri()).hasToString(URI);
		assertThat(entry.title()).hasToString("Bucuresti FM");
	}
}