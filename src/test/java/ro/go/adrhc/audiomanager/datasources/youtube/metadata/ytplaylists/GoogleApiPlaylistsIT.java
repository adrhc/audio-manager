package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists;

import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static ro.go.adrhc.audiomanager.stubs.TestData.YT_MALUKAH_PL_ID;
import static ro.go.adrhc.audiomanager.stubs.TestData.YT_MALUKAH_PL_TITLE;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class GoogleApiPlaylistsIT {
	@Autowired
	private GoogleApiPlaylists client;

	@Test
	void list() {
		var json = client.list(Set.of(YT_MALUKAH_PL_ID));
		assertFalse(json.isEmpty());
//		log.debug("\n{}", json);
		JSONArray nodes = JsonPath.read(json, "$.items[*].snippet.title");
		assertThat(nodes).containsExactly(YT_MALUKAH_PL_TITLE);
	}
}