package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.disk.DiskPlTestRepository;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;

import java.nio.file.Path;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YtLocationAccessors.ytCode;
import static ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntriesFactory.ofYtMetadata;
import static ro.go.adrhc.audiomanager.stubs.TestData.YT_LIKES_IDS;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtLikedMusicMetadataProviderIT {
	@Autowired
	private YtLikedMusicMetadataProvider ytLikedMusicMetadataProvider;
	@Autowired
	private DiskPlTestRepository diskPlTestRepository;

	@Test
	void isYouTubeMusicAuthValid() {
		assertThat(ytLikedMusicMetadataProvider.isYouTubeMusicAuthValid()).isTrue();
	}

	@Test
	void getLikedYtMetadata() {
		Collection<YouTubeMetadata> likedYtMetadata = ytLikedMusicMetadataProvider.loadMetadata();

		log.debug("\nlikedYtMetadata.size = {}", likedYtMetadata.size());
		assertFalse(likedYtMetadata.isEmpty(),
				"Usually, only updating yt-music-liked-curl.txt fixes it!");

		log.debug("\nYT_LIKES_IDS.size = {}", YT_LIKES_IDS.size());

		assertThat(YT_LIKES_IDS.stream()
				.filter(id -> likedYtMetadata.stream().anyMatch(m -> ytCode(m).equals(id))))
				.hasSizeGreaterThan((int) (YT_LIKES_IDS.size() * 0.8));

		assertThat(likedYtMetadata)
				.filteredOn(it -> !hasText(it.title()))
				.isEmpty();
	}

	@Disabled("Used for debug only!")
	@Test
	void saveYtLikedPlaylist(@TempDir Path tempDir) {
		Collection<YouTubeMetadata> likedYtMetadata = ytLikedMusicMetadataProvider.loadMetadata();
		Path plPath = tempDir.resolve("lm-only.copy.m3u8");
		diskPlTestRepository.storeIfNotEmpty(plPath, ofYtMetadata(likedYtMetadata).sort());
		log.debug("\n{}", plPath);
	}
}