package ro.go.adrhc.audiomanager.datasources.youtube.locations;

import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.config.YouTubeProperties;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ro.go.adrhc.audiomanager.stubs.TestData.*;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class GoogleApiPlItemsIT {
	@Autowired
	private GoogleApiPlItems client;
	@Autowired
	private YouTubeProperties properties;

	@Test
	void list() {
		var json = client.list(properties.getMaxResults(), YT_MALUKAH_PL_ID, null);
		assertFalse(json.isEmpty());
//		log.debug("\n{}", json);
		JSONArray nodes = JsonPath.read(json, "$.items[*].contentDetails.videoId");
		assertThat(nodes).contains(YT_MALUKAH_PL_ITEM1_ID, YT_MALUKAH_PL_ITEM2_ID);
		JSONArray nextPageToken = JsonPath.read(json, "$.[?(@.nextPageToken)]");
		assertTrue(nextPageToken.isEmpty());
		JSONArray kind = JsonPath.read(json, "$.[?(@.kind)]");
		assertFalse(kind.isEmpty());
	}
}