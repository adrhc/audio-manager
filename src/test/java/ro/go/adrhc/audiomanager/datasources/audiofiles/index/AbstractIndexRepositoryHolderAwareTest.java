package ro.go.adrhc.audiomanager.datasources.audiofiles.index;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;

import java.io.IOException;
import java.net.URI;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class AbstractIndexRepositoryHolderAwareTest {
	@Autowired
	protected IndexRepositoryHolder repositoryHolder;

	protected FileSystemIndex<URI, AudioMetadata> getIndexRepository() {
		return repositoryHolder.getIndexRepository();
	}

	@AfterAll
	void afterAll() throws IOException {
		repositoryHolder.close();
	}
}
