package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ro.go.adrhc.audiomanager.stubs.TestData.YTM_LIKES_ID1;

class YouTubeDlRecordToTitleConverterTest {
	private static final YouTubeDlRecordToTitleConverter TO_TITLE_CONVERTER =
			YouTubeDlRecordToTitleConverterTestFactory.create();

	@Test
	void checkSmallLevenshteinDistance() throws IOException {
		YoutubeDlRecord metadata = new YoutubeDlRecord();
		metadata.setFulltitle("Touch in the Night - Silent iCrcle");
		metadata.setTitle("Touch in the Night");
		metadata.setArtist("Silent Circle");
		assertEquals(metadata.getFulltitle(), TO_TITLE_CONVERTER.convert(metadata));
	}

	@Test
	void checkFindLargestAndDeduplicateCSV() throws IOException {
		YoutubeDlRecord metadata = new YoutubeDlRecord();
		metadata.setFulltitle("small title");
		metadata.setTitle("Touch in the Night"); // longest
		metadata.setArtist("Silent,Circle,Silent,Circle,Silent,Circle");
		assertEquals("Touch in the Night - Circle, Silent", TO_TITLE_CONVERTER.convert(metadata));
	}

	@Test
	void checkTitleContainingAlbum() throws IOException {
		YoutubeDlRecord metadata = new YoutubeDlRecord();
		metadata.setFulltitle("Touch in the Night - Silent Circle");
		metadata.setTitle("Touch in the Night");
		metadata.setAlbum("Silent,Circle,Silent,Circle,Silent,Circle");
		assertEquals(metadata.getFulltitle(), TO_TITLE_CONVERTER.convert(metadata));
	}

	@Test
	void checkTitleNotHavingArtistAndNotContainingAlbum() throws IOException {
		YoutubeDlRecord metadata = new YoutubeDlRecord();
		metadata.setTitle("Touch in the Night");
		metadata.setAlbum("WordNotFoundInTitle");
		assertEquals("Touch in the Night - WordNotFoundInTitle",
				TO_TITLE_CONVERTER.convert(metadata));
	}

	@Test
	void checkTitleWithoutArtistAndAlbum() throws IOException {
		YoutubeDlRecord metadata = new YoutubeDlRecord();
		metadata.setFulltitle("small title");
		metadata.setTitle("Touch in the Night"); // longest
		metadata.setAlt_title("small alt title");
		metadata.setTrack("small track title");
		metadata.setCreator("creator");
		metadata.setUploader("uploader");
		metadata.setId(YTM_LIKES_ID1);
		metadata.setTags(Set.of("tag1"));
		assertEquals("Touch in the Night", TO_TITLE_CONVERTER.convert(metadata));
	}
}