package ro.go.adrhc.audiomanager.datasources.audiofiles.index;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.lang.NonNull;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.config.context.ObservableAppPaths;
import ro.go.adrhc.audiomanager.datasources.audiofiles.metadata.AudioMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.FoundAudio;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.SearchedAudio;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.search.DiskLocationsIndexSearchService;
import ro.go.adrhc.audiomanager.stubs.AudioMetadataGenerator;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;
import ro.go.adrhc.persistence.lucene.operations.restore.IndexDataSource;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static ro.go.adrhc.audiomanager.stubs.AppPathsGenerator.createContextPaths;
import static ro.go.adrhc.audiomanager.stubs.FoundAudioGenerator.diskFoundAudio;

@SpringBootTest(properties = "lucene.search.result-includes-missing-files=true",
		classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
				HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@ContextConfiguration(initializers = AMIndexCrudTest.IndexPathInitializer.class)
@DirtiesContext
@Slf4j
class AMIndexCrudTest {
	@TempDir
	static Path tmpDir;
	@Autowired
	private FileSystemIndex<URI, AudioMetadata> amIndexRepository;
	@Autowired
	private DiskLocationsIndexSearchService<SearchedAudio> diskLocationsIndexSearcher;
	@Autowired
	private ObservableAppPaths observableAppPaths;
	@Autowired
	private IndexDataSource<URI, AudioMetadata> indexDataSource;
	@Autowired
	private AudioMetadataProvider audioMetadataProvider;

	@Test
	void createIndexAddDocAndSearch() throws IOException {
		// index creation
		amIndexRepository.reset(audioMetadataProvider.loadAll());

		amIndexRepository.shallowUpdate(indexDataSource);

		// document adding
		AudioMetadata metadata = AudioMetadataGenerator.of();
		amIndexRepository.addOne(metadata);

		// search
		FoundAudio<SearchedAudio, DiskLocation> foundAudio = diskFoundAudio();
		List<FoundAudio<SearchedAudio, DiskLocation>> result = diskLocationsIndexSearcher
				.findMany(foundAudio.searchedAudio());

		assertThat(result).hasSize(1);
		assertEquals(foundAudio, result.getFirst());

		amIndexRepository.shallowUpdate(indexDataSource);
		result = diskLocationsIndexSearcher.findMany(foundAudio.searchedAudio());
		assertThat(result).isEmpty();
	}

	static class IndexPathInitializer
			implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		@SneakyThrows
		@Override
		public void initialize(@NonNull ConfigurableApplicationContext context) {
			createContextPaths(tmpDir);
			TestPropertyValues.of(
					STR."context-paths.index-path=\{tmpDir.resolve("Index")}").applyTo(context);
			TestPropertyValues.of(
					STR."context-paths.songs-path=\{tmpDir.resolve("Songs")}").applyTo(context);
			TestPropertyValues.of(
					STR."context-paths.playlists-path=\{tmpDir.resolve("Playlists")}").applyTo(
					context);
			TestPropertyValues.of(
					STR."context-paths.backups-path=\{tmpDir.resolve("Playlist-backups")}").applyTo(
					context);
			TestPropertyValues.of(
					STR."context-paths.scripts-path=\{tmpDir.resolve("Download-scripts")}").applyTo(
					context);
		}
	}
}