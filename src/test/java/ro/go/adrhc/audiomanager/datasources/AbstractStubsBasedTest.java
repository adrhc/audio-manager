package ro.go.adrhc.audiomanager.datasources;

import org.apache.lucene.search.Query;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.ApplicationContext;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.audiofiles.index.DefaultAMIndexRepository;
import ro.go.adrhc.audiomanager.datasources.audiofiles.index.IndexRepositoryHolder;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.locations.YtVideoPlContentLocationsProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.YtLikedMusicMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.YtVideoPlMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsearch.YtSearchMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl.YtDlSongsAndVideosMetadataProvider;
import ro.go.adrhc.audiomanager.stubs.DiskLocationsRepositoryStub;
import ro.go.adrhc.audiomanager.stubs.FileSystemUtilsStub;
import ro.go.adrhc.audiomanager.stubs.IndexRepositoryProxyStub;
import ro.go.adrhc.audiomanager.stubs.YtSearchMetadataProviderStub;
import ro.go.adrhc.util.io.FileSystemUtils;
import ro.go.adrhc.util.io.SimpleDirectory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;

import static java.lang.Integer.MAX_VALUE;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.reset;
import static ro.go.adrhc.audiomanager.domain.location.LocationsGenerator.*;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class AbstractStubsBasedTest {
	@MockBean
	protected IndexRepositoryHolder indexRepositoryHolder;
	@MockBean
	protected DefaultAMIndexRepository amIndexRepositoryProxy;
	@MockBean
	protected YtLikedMusicMetadataProvider ytLikedMusicMetadataProvider;
	@MockBean
	protected YtVideoPlContentLocationsProvider ytVideoPlContentLocationsProvider;
	@SpyBean
	protected YtDlSongsAndVideosMetadataProvider ytDlSongsAndVideosMetadataProvider;
	@SpyBean
	protected YtVideoPlMetadataProvider ytVideoPlMetadataProvider;
	@SpyBean(name = "audioFilesDirectory")
	protected SimpleDirectory audioFilesDirectory;
	@SpyBean
	protected FileSystemUtils fileSystemUtils;
	@SpyBean
	protected YtSearchMetadataProvider ytSearchMetadataProvider;
	@Autowired
	protected DiskLocationsRepositoryStub diskLocationsRepositoryStub;
	@Autowired
	protected IndexRepositoryProxyStub indexRepositoryProxyStub;
	@Autowired
	protected YtSearchMetadataProviderStub ytSearchMetadataProviderStub;
	@Autowired
	private ApplicationContext ac;

	/**
	 * Doesn't work with @BeforeAll! the when-thenReturn is simply not applied.
	 */
	@BeforeEach
	protected void beforeEach() throws IOException {
		BeforEachConfig beforEachConfig = beforEachConfig();
		reset(ytLikedMusicMetadataProvider);
		reset(ytVideoPlContentLocationsProvider);
		reset(ytDlSongsAndVideosMetadataProvider);
		reset(ytVideoPlMetadataProvider);
		prepareMock(amIndexRepositoryProxy);
		doAnswer(_ -> generateYtMetadata(beforEachConfig.likedMaxSize()))
				.when(ytLikedMusicMetadataProvider).loadMetadata();
		doAnswer(invocation -> createPlContent(beforEachConfig.otherPlMaxSize(),
				invocation.getArgument(0)))
				.when(ytVideoPlContentLocationsProvider).getPlContent(any(YouTubeLocation.class));
		doAnswer(invocation -> ytLocationMetadataPairsOf(invocation.getArgument(0)))
				.when(ytDlSongsAndVideosMetadataProvider).loadMetadata(
						Mockito.<Collection<YouTubeLocation>>any());
		doAnswer(invocation -> ytLocationMetadataPairsOf(invocation.getArgument(0)))
				.when(ytVideoPlMetadataProvider).loadMetadata(
						Mockito.<Collection<YouTubeLocation>>any());
		doAnswer(invocation -> ytSearchMetadataProviderStub
				.findAllWebVideoMatches(invocation.getArgument(0)))
				.when(ytSearchMetadataProvider).findAllWebVideoMatches(any(SongQuery.class));
	}

	protected BeforEachConfig beforEachConfig() {
		return new BeforEachConfig(MAX_VALUE, MAX_VALUE);
	}

	private void prepareMock(DefaultAMIndexRepository amIndexRepositoryProxy) throws IOException {
		reset(amIndexRepositoryProxy);
		doAnswer(_ -> youtubeMp3Paths()).when(audioFilesDirectory)
				.transformPathStream(eq(YOUTUBE_MP3.path()), any());
		doAnswer(invocation -> indexRepositoryProxyStub
				.findBestMatches(invocation.getArgument(1)))
				.when(amIndexRepositoryProxy).findBestMatches(any(), anyCollection());
		doAnswer(invocation -> indexRepositoryProxyStub
				.findMany(invocation.getArgument(0)))
				.when(amIndexRepositoryProxy).findMany(any(Query.class));
		doAnswer(invocation -> FileSystemUtilsStub.isRegularFile(invocation.getArgument(0)))
				.when(fileSystemUtils).isRegularFile(any(Path.class));
	}

	public record BeforEachConfig(int likedMaxSize, int otherPlMaxSize) {
	}
}
