package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsearch;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import ro.go.adrhc.audiomanager.AudioManagerConfig;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.util.text.StringUtils;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static ro.go.adrhc.audiomanager.datasources.domain.SongQuery.ofFreeText;
import static ro.go.adrhc.audiomanager.datasources.domain.SongQuery.ofTitleAndArtist;
import static ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsearch.YtSearchAssertions.ytMetadataAssert;
import static ro.go.adrhc.audiomanager.stubs.TestData.*;

@EnableConfigurationProperties
@SpringBootTest(classes = {FeignAutoConfiguration.class, JacksonAutoConfiguration.class,
		HttpMessageConvertersAutoConfiguration.class, AudioManagerConfig.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class YtSearchMetadataProviderIT {
	@Autowired
	private YtSearchMetadataProvider ytSearch;

	@Disabled("Used for debug only.")
	@Test
	void debugWebVideoSearch() {
		List<YouTubeMetadata> result = ytSearch.findAllWebVideoMatches(
				ofFreeText("Colinde Românești 2025 Colaj cu Cele Mai Iubite"));
//		log.debug("\n{}", StringUtils.concat(result.stream().map(YouTubeMetadata::title)));
		log.debug("\n{}", StringUtils.concat(result.stream().map(it -> it.location().uri())));
	}

	@Test
	void webVideoSearch() {
		List<YouTubeMetadata> result = ytSearch.findAllWebVideoMatches(
				ofTitleAndArtist("Hot Stuff", "Donna Summer"));
		log.debug("\nfound {} videos", result.size());
		assertThat(result).anySatisfy(m -> ytMetadataAssert(YTM_LIKES_ID7, YTM_LIKES_TITLE7, m));
	}

	@Test
	void webMusicSearch() {
		List<YouTubeMetadata> result = ytSearch.findAllWebMusicMatches(
				ofTitleAndArtist("Hot Stuff", "Donna Summer"));
		log.debug("\nfound {} songs", result.size());
		assertThat(result).anySatisfy(m -> ytMetadataAssert(
				"tJxOXzE5A8w", "Hot Stuff (12 Version) - Donna Summer", m));
	}

	@Disabled("Consumes too many (i.e. 100) YouTube API credits!")
	@Test
	void searchVideo() {
		List<YouTubeMetadata> result = ytSearch.findMany(
				ofTitleAndArtist("Hot Stuff", "Donna Summer"));
		assertThat(result).anySatisfy(
				m -> ytMetadataAssert(YTM_LIKES_ID6, YTM_LIKES_TITLE6, m));
	}
}