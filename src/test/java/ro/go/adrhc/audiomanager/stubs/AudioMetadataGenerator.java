package ro.go.adrhc.audiomanager.stubs;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.audiomanager.domain.location.LocationType;
import ro.go.adrhc.lib.audiometadata.domain.AudioDurations;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.lib.audiometadata.domain.AudioTags;

import java.net.URI;
import java.nio.file.Path;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import static ro.go.adrhc.audiomanager.domain.location.LocationType.DISK;
import static ro.go.adrhc.audiomanager.domain.location.LocationType.YOUTUBE;
import static ro.go.adrhc.audiomanager.util.PathTestUtils.resolveToHome;

@UtilityClass
@Slf4j
public class AudioMetadataGenerator {

	public static final String FILENAME_NO_EXT = "Elvis Presley - Are You";

	public static final Path PATH = resolveToHome(FILENAME_NO_EXT + ".wma");

	public static final URI URI = PATH.toUri();

	public static final String DURATIONS = "10 186";

	public static final AudioTags AUDIO_TAGS = AudioTags.of("audio-tag1", "audio-tag1-value");

	public static final HashSet<String> TAGS = new HashSet<>(Set.of("TAG"));

	public static AudioMetadata ofTag(String tag) {
		return ofTags(new HashSet<>(Set.of(tag)));
	}

	public static AudioMetadata ofTags(Set<String> tags) {
		return new AudioMetadata(URI, FILENAME_NO_EXT, AUDIO_TAGS,
				AudioDurations.of(DURATIONS), new HashSet<>(tags),
				Instant.now(), DISK);
	}

	public static AudioMetadata ofNoLocation() {
		return ofLocationType(null);
	}

	@SneakyThrows
	public static AudioMetadata ofYtLocation(String urn) {
		return new AudioMetadata(new URI(urn),
				FILENAME_NO_EXT, AUDIO_TAGS, AudioDurations.of(DURATIONS),
				TAGS, Instant.now(), YOUTUBE);
	}

	@SneakyThrows
	public static AudioMetadata ofUriLocation(String uri, String title) {
		return new AudioMetadata(new URI(uri),
				title, AUDIO_TAGS, AudioDurations.of(DURATIONS),
				TAGS, Instant.now(), LocationType.URI);
	}

	public static AudioMetadata of() {
		return ofLocationType(DISK);
	}

	public static AudioMetadata ofLocationType(LocationType locationType) {
		return new AudioMetadata(URI, FILENAME_NO_EXT, AUDIO_TAGS,
				AudioDurations.of(DURATIONS), TAGS, Instant.now(), locationType);
	}
}
