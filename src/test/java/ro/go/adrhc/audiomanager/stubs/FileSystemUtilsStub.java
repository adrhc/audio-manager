package ro.go.adrhc.audiomanager.stubs;

import lombok.experimental.UtilityClass;

import java.nio.file.Path;

import static ro.go.adrhc.audiomanager.stubs.TestData.YOUTUBE_MP3_PATHS;

@UtilityClass
public class FileSystemUtilsStub {
	public static boolean isRegularFile(Path path) {
		return YOUTUBE_MP3_PATHS.stream().anyMatch(
				p -> path.endsWith(p) || p.endsWith(path));
	}
}
