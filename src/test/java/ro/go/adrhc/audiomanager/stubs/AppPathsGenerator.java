package ro.go.adrhc.audiomanager.stubs;

import lombok.experimental.UtilityClass;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.condition.OS;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.config.context.ObservableAppPaths;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.file.Files.createDirectories;
import static org.junit.jupiter.api.Assertions.assertTrue;

@UtilityClass
public class AppPathsGenerator {
	public static AppPaths create(boolean checkPaths) {
		ObservableAppPaths observableAppPaths = ObservableAppPaths.empty();
		populateProdPaths(checkPaths, observableAppPaths);
		return observableAppPaths.cloneAppPaths();
	}

	public static void populateTestPaths(Path rootPath, ObservableAppPaths appPaths)
			throws IOException {
		appPaths.setIndexPath(createDirectories(rootPath.resolve("Index")));
		appPaths.setSongsPath(createDirectories(rootPath.resolve("Songs")));
		appPaths.setPlaylistsPath(createDirectories(rootPath.resolve("Playlists")));
		appPaths.setBackupsPath(createDirectories(rootPath.resolve("Playlist-backups")));
		appPaths.setScriptsPath(createDirectories(rootPath.resolve("Download-scripts")));
	}

	public static void createContextPaths(Path rootPath) throws IOException {
		createDirectories(rootPath.resolve("Index"));
		createDirectories(rootPath.resolve("Songs"));
		createDirectories(rootPath.resolve("Playlists"));
		createDirectories(rootPath.resolve("Playlist-backups"));
		createDirectories(rootPath.resolve("Download-scripts"));
	}

	public static void populateColindePaths(ObservableAppPaths appPaths) {
		Path userHome = FileUtils.getUserDirectory().toPath();
		if (OS.WINDOWS.isCurrentOs()) {
			appPaths.setIndexPath(userHome.resolve("Music\\Index Colinde"));
			appPaths.setSongsPath(Path.of("M:\\MUZICA\\Colinde"));
			appPaths.setPlaylistsPath(userHome.resolve("Music\\PlaylistsColinde"));
			appPaths.setBackupsPath(userHome.resolve("Music\\PlaylistsColindeBackup"));
			appPaths.setScriptsPath(userHome.resolve("Music\\Download-scripts"));
		} else {
			appPaths.setIndexPath(userHome.resolve(".backup/database/Colinde"));
			appPaths.setSongsPath(userHome.resolve("Music/MUZICA/Colinde"));
			appPaths.setPlaylistsPath(userHome.resolve("Music/Playlists/colinde"));
			appPaths.setBackupsPath(userHome.resolve("temp/Playlists-backup"));
			appPaths.setScriptsPath(userHome.resolve("temp/Download-scripts"));
		}
		checkPaths(appPaths.cloneAppPaths());
	}

	/**
	 * Should be used only for "read" operations!
	 */
	public static void populateProdPaths(boolean checkPaths, ObservableAppPaths appPaths) {
		Path userHome = FileUtils.getUserDirectory().toPath();
		if (OS.WINDOWS.isCurrentOs()) {
			appPaths.setIndexPath(userHome.resolve("Music\\Index MUZICA"));
			appPaths.setSongsPath(Path.of("M:\\"));
			appPaths.setPlaylistsPath(userHome.resolve("Music\\Playlists"));
			appPaths.setBackupsPath(userHome.resolve("Music\\Playlists-backup"));
			appPaths.setScriptsPath(userHome.resolve("Music\\Download-scripts"));
		} else {
			appPaths.setIndexPath(userHome.resolve(".backup/database/audio-db-web"));
			appPaths.setSongsPath(userHome.resolve("Music/MUZICA"));
			appPaths.setPlaylistsPath(Path.of("/var/lib/mopidy/m3u"));
			appPaths.setBackupsPath(userHome.resolve("Backups 6M/Playlists-backup"));
			appPaths.setScriptsPath(userHome);
		}
		if (checkPaths) {
			checkPaths(appPaths.cloneAppPaths());
		}
	}

	private static void checkPaths(AppPaths appPaths) {
		assertTrue(Files.isDirectory(appPaths.getIndexPath()));
		assertTrue(Files.isDirectory(appPaths.getSongsPath()));
		assertTrue(Files.isDirectory(appPaths.getPlaylistsPath()));
		assertTrue(Files.isDirectory(appPaths.getBackupsPath()));
		assertTrue(Files.isDirectory(appPaths.getScriptsPath()));
	}
}
