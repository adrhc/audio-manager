package ro.go.adrhc.audiomanager.stubs;

import lombok.experimental.UtilityClass;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.FoundAudio;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.SearchedAudio;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;
import static ro.go.adrhc.audiomanager.stubs.AudioMetadataGenerator.DURATIONS;
import static ro.go.adrhc.audiomanager.stubs.AudioMetadataGenerator.PATH;

@UtilityClass
public class FoundAudioGenerator {
	public static FoundAudio<SearchedAudio, DiskLocation> diskFoundAudio() {
		FoundAudio<SearchedAudio, DiskLocation> foundAudio = new FoundAudio<>(
				SearchedAudioGenerator.create());
		foundAudio.location(createDiskLocation(PATH));
		foundAudio.durations(DURATIONS);
		return foundAudio;
	}
}
