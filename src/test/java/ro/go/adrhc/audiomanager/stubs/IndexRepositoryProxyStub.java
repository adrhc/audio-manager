package ro.go.adrhc.audiomanager.stubs;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.search.Query;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.operations.search.QueryAndValue;
import ro.go.adrhc.util.fn.BiFunctionUtils;
import ro.go.adrhc.util.pair.Pair;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiomanager.stubs.TestData.INDEX;

@Component
@RequiredArgsConstructor
@Slf4j
public class IndexRepositoryProxyStub {
	private static final Map<String, String> QUERY_TITLE_MAP =
			Map.of("fileNameNoExt:alabina~1 +words:alabina~1", "Alabina-)",
					"fileNameNoExt:big fileNameNoExt:in fileNameNoExt:japan~1 +((words:big words:in words:japan~1)~3)",
					"Big in Japan",
					"fileNameNoExt:the fileNameNoExt:in fileNameNoExt:silent~1 fileNameNoExt:night~1 fileNameNoExt:touch~1 fileNameNoExt:circle~1 +((words:the words:in words:silent~1 words:night~1 words:touch~1 words:circle~1)~5)",
					"Touch in the Night - Silent Circle",
					"fileNameNoExt:moon fileNameNoExt:blue fileNameNoExt:ride fileNameNoExt:ft fileNameNoExt:isaac~1 fileNameNoExt:moonlight~1 fileNameNoExt:chambers~1 +((words:moon words:blue words:ride words:ft words:isaac~1 words:moonlight~1 words:chambers~1)~6)",
					"Isaac Chambers - Moonlight Ride (ft. Blue Moon)");
	protected final AppPaths appPaths;

	public List<QueryAndValue<AudioMetadata>> findBestMatches(
			Collection<? extends Query> queries) {
		return queries.stream()
				.map(q -> findByQuery(q).map(BiFunctionUtils.curry(Pair::new, q)))
				.flatMap(Optional::stream)
				.map(p -> new QueryAndValue<>(p.left(),
						AudioMetadata.of(p.right().path())))
				.toList();
	}

	public List<AudioMetadata> findMany(Query query) {
		return findByQuery(query)
				.map(loc -> AudioMetadata.of(loc.path()))
				.stream()
				.toList();
	}

	private Optional<DiskLocation> findByQuery(Query query) {
		Optional<DiskLocation> location = findByTitle(QUERY_TITLE_MAP.get(query.toString()));
		if (location.isEmpty()) {
			log.debug("\nlocation not found for:\n{}", query);
		}
		return location;
	}

	private Optional<DiskLocation> findByTitle(String title) {
		if (!hasText(title)) {
			return Optional.empty();
		}
		String relativeSongPath = INDEX.get(title);
		if (hasText(relativeSongPath)) {
			DiskLocation songLocation = DiskLocationFactory.createDiskLocation(
					appPaths.getSongsPath().resolve(Path.of(relativeSongPath)));
			return Optional.of(songLocation);
		} else {
			return Optional.empty();
		}
	}
}
