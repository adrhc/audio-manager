package ro.go.adrhc.audiomanager.stubs;

import lombok.experimental.UtilityClass;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.SearchedAudio;

import static ro.go.adrhc.audiomanager.datasources.domain.SongQuery.ofTitle;
import static ro.go.adrhc.audiomanager.stubs.AudioMetadataGenerator.FILENAME_NO_EXT;

@UtilityClass
public class SearchedAudioGenerator {
	public static final String LONG_WORDS_FILENAME_NO_EXT =
			"The Rite of Spring - Royal Philharmonic Orchestra, Yuri Simonov, Yuri Simonov";

	public static SearchedAudio create() {
		return SearchedAudio.of(ofTitle(FILENAME_NO_EXT));
	}

	public static SearchedAudio longWords() {
		return SearchedAudio.of(ofTitle(LONG_WORDS_FILENAME_NO_EXT));
	}
}
