package ro.go.adrhc.audiomanager.managers.diskplupdate;

import java.util.Objects;

public record SongSelection(String uri, String title, boolean selected) {
	public static SongSelection ofIncluded(String uri, String title) {
		return new SongSelection(uri, title, true);
	}

	public static SongSelection ofExcluded(String uri, String title) {
		return new SongSelection(uri, title, false);
	}

	public boolean hasUri(SongSelection selection) {
		return Objects.equals(uri, selection.uri);
	}
}
