package ro.go.adrhc.audiomanager.managers.diskplquery;

import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;

public record LocationSelection(DiskLocation location, boolean selected) {
	public static LocationSelection selected(DiskLocation location) {
		return new LocationSelection(location, true);
	}

	public static LocationSelection deselected(DiskLocation location) {
		return new LocationSelection(location, false);
	}
}
