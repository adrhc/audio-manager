package ro.go.adrhc.audiomanager.managers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.config.context.AppPathType;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.config.context.validation.AppPathsValidator;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;
import ro.go.adrhc.persistence.lucene.operations.restore.IndexDataSource;
import ro.go.adrhc.util.StopWatchUtils;

import java.io.IOException;
import java.net.URI;

import static ro.go.adrhc.audiomanager.config.context.AppPathType.INDEX;
import static ro.go.adrhc.audiomanager.config.context.AppPathType.SONGS;
import static ro.go.adrhc.audiomanager.datasources.audiofiles.index.AMQueryFactory.LOCATION_TYPE_QUERY;
import static ro.go.adrhc.audiomanager.domain.location.LocationType.DISK;

@Service
@RequiredArgsConstructor
@Slf4j
public class AMIndexManager {
	private final AppPaths appPaths;
	private final IndexDataSource<URI, AudioMetadata> indexDataSource;
	private final FileSystemIndex<URI, AudioMetadata> amIndexRepository;

	public int reset() throws IOException {
		throwIfInvalid(SONGS);
		StopWatch watch = StopWatchUtils.start();
		amIndexRepository.reset(indexDataSource.loadAll());
		watch.stop();
		int count = amIndexRepository.count();
		log.debug("\n{} index was reset!\nIndexed {} songs in {}.",
				appPaths.getIndexPath(), amIndexRepository.count(), watch.formatTime());
		return count;
	}

	public int shallowDiskUpdate() throws IOException {
		throwIfInvalid(INDEX, SONGS);
		amIndexRepository.shallowUpdateSubset(indexDataSource,
				LOCATION_TYPE_QUERY.newExactQuery(DISK));
		int count = amIndexRepository.count();
		log.debug("\n{} index updated!\nit contains {} entries",
				appPaths.getIndexPath(), amIndexRepository.count());
		return count;
	}

	protected void throwIfInvalid(AppPathType... appPathTypes) {
		pathTypesValidator(appPathTypes).throwIfInvalid();
	}

	protected AppPathsValidator pathTypesValidator(AppPathType... appPathTypes) {
		return contextPathsValidator().addCtxPathTypes(appPathTypes);
	}

	@Lookup
	protected AppPathsValidator contextPathsValidator() {
		return null;
	}
}
