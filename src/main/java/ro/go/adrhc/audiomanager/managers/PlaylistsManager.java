package ro.go.adrhc.audiomanager.managers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.disk.DiskPlEntriesRepository;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.PlaylistStorageOutcome;
import ro.go.adrhc.audiomanager.domain.NameAware;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.services.PlaylistsService;

import java.io.IOException;
import java.util.function.UnaryOperator;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createPlLocation;

/**
 * Managing playlists files content.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class PlaylistsManager {
	private final DiskPlEntriesRepository diskPlEntriesRepository;
	private final DiskPlaylistsRepository diskPlaylistsRepository;
	private final PlaylistsService playlistsService;

	public PlaylistStorageOutcome createPlFromDirectory(DiskLocation dirLocation)
			throws IOException {
		PlaylistEntries plEntries = diskPlEntriesRepository.loadContent(dirLocation);
		Playlist<DiskLocation> playlist = createDiskPlaylist(dirLocation, plEntries);
		return diskPlaylistsRepository.storeIfNotEmpty(playlist);
	}

	public void fixTitles(boolean missingTitleOnly) throws IOException {
		diskPlaylistsRepository.getPlaylistsLocations()
				.forEach(plLocation -> this.fixTitles(missingTitleOnly, plLocation));
	}

	public void fixTitles(boolean missingTitleOnly, DiskLocation plLocation) {
		Playlist<DiskLocation> playlist = diskPlaylistsRepository.load(plLocation);
		log.debug("\nfixing titles of %s (missingTitleOnly = %b)".formatted(plLocation,
				missingTitleOnly));
		playlist = playlistsService.fixTitles(missingTitleOnly, playlist);
		diskPlaylistsRepository.storeIfNotEmpty(playlist);
	}

	/**
	 * Use case where intoLocation plays two roles: merge "into"-playlist and merge-location
	 */
	public void sumDiffs(DiskLocation intoLocation, DiskLocation fromLocation) {
		sumDiffs(intoLocation, intoLocation, fromLocation);
	}

	public void sumDiffs(DiskLocation mergedPlLocation, DiskLocation intoLocation,
			DiskLocation fromLocation) {
		Playlist<DiskLocation> into = diskPlaylistsRepository.load(intoLocation);
		Playlist<?> from = diskPlaylistsRepository.load(fromLocation);
		Playlist<DiskLocation> mergedPl = playlistsService.sumDiffs(into, from);
		diskPlaylistsRepository.storeIfNotEmpty(mergedPl.location(mergedPlLocation));
	}

	public void deduplicate() throws IOException {
		diskPlaylistsRepository.getPlaylistsLocations().forEach(this::deduplicate);
	}

	public PlaylistStorageOutcome deduplicate(DiskLocation plLocation) {
		log.debug("\ndeduplicating %s".formatted(plLocation));
		return loadTransformStore(Playlist::deduplicate, plLocation);
	}

	public PlaylistStorageOutcome deduplicateByMediaId(DiskLocation plLocation) {
		log.debug("\ndeduplicating %s".formatted(plLocation));
		return loadTransformStore(Playlist::deduplicateByMediaId, plLocation);
	}

	public void sort() throws IOException {
		diskPlaylistsRepository.getPlaylistsLocations().forEach(this::sort);
	}

	public void sort(DiskLocation plLocation) {
		log.debug("\nsorting (by title, location and duration) %s".formatted(plLocation));
		loadTransformStore(Playlist::sort, plLocation);
	}

	protected PlaylistStorageOutcome loadTransformStore(
			UnaryOperator<Playlist<DiskLocation>> transformer, DiskLocation plLocation) {
		Playlist<DiskLocation> playlist = diskPlaylistsRepository.load(plLocation);
		playlist = transformer.apply(playlist);
		return diskPlaylistsRepository.storeIfNotEmpty(playlist);
	}

	protected Playlist<DiskLocation> createDiskPlaylist(
			NameAware plNameSupplier, PlaylistEntries plEntries) {
		DiskLocation plLocation = createPlLocation(plNameSupplier);
		return new Playlist<>(plLocation, plEntries).sort();
	}
}
