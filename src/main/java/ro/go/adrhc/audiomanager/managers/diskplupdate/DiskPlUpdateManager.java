package ro.go.adrhc.audiomanager.managers.diskplupdate;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.disk.DiskPlLocationQueries;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.LocationParsers;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.audiomanager.domain.services.PlaylistsService;
import ro.go.adrhc.audiomanager.managers.diskplquery.LocationSelection;
import ro.go.adrhc.audiomanager.managers.diskplquery.UriSelectedLocations;
import ro.go.adrhc.lib.m3u8.M3u8DiskWriter;
import ro.go.adrhc.lib.m3u8.domain.M3u8Record;
import ro.go.adrhc.util.pair.UnaryPair;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.function.Predicate.not;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;
import static ro.go.adrhc.audiomanager.util.FileUtils.isLastCharNewline;

@Service
@RequiredArgsConstructor
@Slf4j
public class DiskPlUpdateManager {
	private final LocationParsers locationParsers;
	private final DiskPlLocationQueries diskPlLocationQueries;
	private final M3u8DiskWriter m3u8Writer;
	private final DiskPlaylistsRepository diskPlaylistsRepository;
	private final PlaylistsService playlistsService;

	public Optional<DiskLocation> updatePlContent(PlContentUpdateRequest request) {
		DiskLocation plLocation = createDiskLocation(request.playlistUri());
		Playlist<DiskLocation> storedPl = diskPlaylistsRepository.load(plLocation);
		Playlist<DiskLocation> updatedPl = playlistsService.update(storedPl, request);
		return diskPlaylistsRepository.store(updatedPl);
	}

	public UriPlAllocationResult updateUriPlaylists(UriSelectedLocations uriSelectedLocations) {
		Location location = locationParsers.parse(uriSelectedLocations.getUri());
		PlaylistEntry plEntry = PlaylistEntry.of(location, uriSelectedLocations.getTitle());
		List<DiskLocation> currentLocations = diskPlLocationQueries.findByEntry(location);
		UriSelectedLocations requestedChanges = uriSelectedLocations.changes(currentLocations);
		UnaryPair<Stream<DiskLocation>> performedChanges =
				performChanges(requestedChanges, plEntry);
		return UriPlAllocationResult.of(requestedChanges, performedChanges);
	}

	public Optional<DiskLocation> removePlEntry(Location entryLocation, DiskLocation plLocation) {
		Playlist<DiskLocation> playlist = diskPlaylistsRepository.load(plLocation);
		Playlist<DiskLocation> newPlaylist = playlist.removeEntry(entryLocation);
		return diskPlaylistsRepository.store(newPlaylist)
				.map(_ -> playlist.size() != newPlaylist.size())
				.filter(it -> it)
				.map(_ -> plLocation);
	}

	private Optional<DiskLocation> appendM3u8Record(
			M3u8Record m3u8Record, DiskLocation plLocation) {
		return isLastCharNewline(plLocation.path())
				.map(isTrue -> m3u8Writer.append(
						plLocation.path(), isTrue ? null : "\n", m3u8Record))
				.filter(it -> it)
				.map(_ -> plLocation);
	}

	private UnaryPair<Stream<DiskLocation>> performChanges(
			UriSelectedLocations changes, PlaylistEntry plEntry) {
		M3u8Record m3u8Record = M3u8Record.of(plEntry.uri(), plEntry.title());
		Stream<DiskLocation> addedTo = add(changes, m3u8Record);
		Stream<DiskLocation> removedFrom = remove(changes, plEntry.location());
		return new UnaryPair<>(addedTo, removedFrom);
	}

	private Stream<DiskLocation> add(UriSelectedLocations changes, M3u8Record m3u8Record) {
		return changes.rawFilter(LocationSelection::selected)
				.map(sl -> appendM3u8Record(m3u8Record, sl.location()))
				.flatMap(Optional::stream);
	}

	private Stream<DiskLocation> remove(UriSelectedLocations changes, Location entryLocation) {
		return changes.rawFilter(not(LocationSelection::selected))
				.map(sl -> removePlEntry(entryLocation, sl.location()))
				.flatMap(Optional::stream);
	}
}
