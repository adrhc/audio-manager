package ro.go.adrhc.audiomanager.managers.diskplupdate;

import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.managers.diskplquery.UriSelectedLocations;
import ro.go.adrhc.util.pair.UnaryPair;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public record UriPlAllocationResult(Set<DiskLocation> addedTo,
		Set<DiskLocation> removedFrom, Set<DiskLocation> failedToChange) {
	public static UriPlAllocationResult of(UriSelectedLocations requestedChanges,
			UnaryPair<Stream<DiskLocation>> performedChanges) {
		Set<DiskLocation> addedTo = performedChanges.left().collect(Collectors.toSet());
		Set<DiskLocation> removedFrom = performedChanges.right().collect(Collectors.toSet());
		List<DiskLocation> failedToChange = requestedChanges
				.minus(addedTo).minus(removedFrom).getLocations();
		return new UriPlAllocationResult(addedTo, removedFrom, new HashSet<>(failedToChange));
	}
}
