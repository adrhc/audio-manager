package ro.go.adrhc.audiomanager.managers.diskplupdate;

import java.util.ArrayList;
import java.util.List;

import static java.util.function.Predicate.not;
import static ro.go.adrhc.util.fn.PredicateFactory.anyMatch;

public record PlContentUpdateRequest(String playlistUri, List<SongSelection> selections) {
	public static PlContentUpdateRequest of(String playlistUri) {
		return new PlContentUpdateRequest(playlistUri, new ArrayList<>());
	}

	public PlContentUpdateRequest removeExcludedIfIncludedExists() {
		List<SongSelection> filteredSongs = selections.stream().filter(anyMatch(
				SongSelection::selected, not(this::includeRequestExists))).toList();
		return new PlContentUpdateRequest(playlistUri, filteredSongs);
	}

	public void add(SongSelection selection) {
		selections.add(selection);
	}

	private boolean includeRequestExists(SongSelection selection) {
		return selections.stream().anyMatch(ss -> ss.selected() && ss.hasUri(selection));
	}
}
