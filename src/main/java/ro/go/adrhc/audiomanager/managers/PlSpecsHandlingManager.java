package ro.go.adrhc.audiomanager.managers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.config.MainProperties;
import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpec;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.PlaylistStorageOutcome;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.domain.services.PlaylistsService;
import ro.go.adrhc.audiomanager.util.LogUtils;

import java.util.Collection;

@Service
@RequiredArgsConstructor
@Slf4j
public class PlSpecsHandlingManager {
	private final MainProperties appProperties;
	private final DiskPlaylistsRepository diskPlaylistsRepository;
	private final PlaylistsService playlistsService;

	public void createPlFromSpecs() {
		Collection<PlaylistSpec> plSpecs = appProperties.getPlaylistSpecs();
		playlistsService.createFromSpecs(plSpecs)
				.map(diskPlaylistsRepository::storeIfNotEmpty)
				.forEach(LogUtils::log);
	}

	public PlaylistStorageOutcome createPlFromSpec(PlaylistSpec plSpec) {
		Playlist<DiskLocation> playlist = playlistsService.createFromSpec(plSpec);
		return diskPlaylistsRepository.storeIfNotEmpty(playlist);
	}
}
