package ro.go.adrhc.audiomanager.managers.diskplquery;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static java.util.function.Predicate.not;

@Getter
@Setter
public class UriSelectedLocations extends LocationSelections {
	private final String uri;
	private final String title;

	@JsonCreator
	public UriSelectedLocations(
			@JsonProperty("uri") String uri, @JsonProperty("title") String title,
			@JsonProperty("locations") List<LocationSelection> locations) {
		super(locations);
		this.uri = uri;
		this.title = title;
	}

	public static UriSelectedLocations of(String uri, String title) {
		return new UriSelectedLocations(uri, title, new ArrayList<>());
	}

	public void addSelected(DiskLocation location) {
		selections.add(LocationSelection.selected(location));
	}

	public void addDeselected(DiskLocation location) {
		selections.add(LocationSelection.deselected(location));
	}

	public UriSelectedLocations minus(Collection<DiskLocation> locations) {
		Stream<LocationSelection> newLocations =
				rawFilter(it -> !locations.contains(it.location()));
		return new UriSelectedLocations(uri, title, newLocations.toList());
	}

	/**
	 * @return selected(this) - oldSelection + deselect(oldSelection - selected(this))
	 */
	public UriSelectedLocations changes(List<DiskLocation> oldSelection) {
		List<DiskLocation> thisSelected = rawFilter(LocationSelection::selected)
				.map(LocationSelection::location).toList();
		Stream<LocationSelection> toSelect = thisSelected.stream()
				.filter(not(oldSelection::contains))
				.map(LocationSelection::selected);
		Stream<LocationSelection> toDeselect = oldSelection.stream()
				.filter(not(thisSelected::contains))
				.map(LocationSelection::deselected);
		List<LocationSelection> toChange = Stream.concat(toSelect, toDeselect).toList();
		return new UriSelectedLocations(uri, title, toChange);
	}
}
