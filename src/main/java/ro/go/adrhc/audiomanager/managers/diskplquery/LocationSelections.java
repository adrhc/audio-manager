package ro.go.adrhc.audiomanager.managers.diskplquery;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.NonNull;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;

import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Getter
@Setter
@AllArgsConstructor
public class LocationSelections implements Iterable<LocationSelection> {
	protected final List<LocationSelection> selections;

	public static LocationSelections of(
			List<DiskLocation> plLocations,
			List<DiskLocation> selected) {
		List<LocationSelection> locations = plLocations.stream()
				.map(pl -> new LocationSelection(pl, selected.contains(pl)))
				.toList();
		return new LocationSelections(locations);
	}

	public List<DiskLocation> getLocations() {
		return rawMap(LocationSelection::location).toList();
	}

	public <R> Stream<R> rawMap(Function<? super LocationSelection, ? extends R> mapper) {
		return selections.stream().map(mapper);
	}

	public Stream<LocationSelection> rawFilter(Predicate<? super LocationSelection> predicate) {
		return selections.stream().filter(predicate);
	}

	@Override
	@NonNull
	public Iterator<LocationSelection> iterator() {
		return selections.iterator();
	}
}
