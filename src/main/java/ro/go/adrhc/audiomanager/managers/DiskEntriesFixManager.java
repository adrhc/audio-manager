package ro.go.adrhc.audiomanager.managers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.domain.services.PlaylistsService;

import java.io.IOException;

@Service
@RequiredArgsConstructor
@Slf4j
public class DiskEntriesFixManager {
	private final DiskPlaylistsRepository diskPlaylistsRepository;
	private final PlaylistsService playlistsService;

	public void fixDiskLocations() throws IOException {
		for (DiskLocation diskLocation : diskPlaylistsRepository.getPlaylistsLocations()) {
			fixDiskLocations(diskLocation);
		}
	}

	public void fixDiskLocations(DiskLocation plLocation) throws IOException {
		Playlist<DiskLocation> playlist = diskPlaylistsRepository.load(plLocation);
		log.debug("\nfixing disk locations of %s".formatted(plLocation));
		playlist = playlistsService.fixDiskLocations(playlist);
		diskPlaylistsRepository.storeIfNotEmpty(
				playlistsService.invalidDiskEntriesPlaylist(playlist));
		diskPlaylistsRepository.storeIfNotEmpty(
				playlistsService.otherThanInvalidDiskEntriesPlaylist(playlist));
	}
}
