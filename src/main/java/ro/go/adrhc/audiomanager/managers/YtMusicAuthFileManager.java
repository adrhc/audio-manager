package ro.go.adrhc.audiomanager.managers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.ParseException;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams.ReloadableHttpCommand;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.YtMusicAuthFileRepository;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.config.ConfigFileRepository;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.config.FileNameAndContent;

import java.io.IOException;

@Service
@RequiredArgsConstructor
@Slf4j
public class YtMusicAuthFileManager {
	private final ConfigFileRepository configFileRepository;
	private final ReloadableHttpCommand<ParseException> ytLikedMusicHttpCmdLine;
	private final YtMusicAuthFileRepository ytMusicAuthFileRepository;

	/**
	 * Update YouTube Music curl file, reload the corresponding
	 * ReloadableHttpCommand then write the YouTube Music Plugin
	 * authentication file.
	 */
	public void updateYtMusicAuth(FileNameAndContent ytLikedMusicCurl) throws ParseException, IOException {
		configFileRepository.updateContent(ytLikedMusicCurl);
		ytLikedMusicHttpCmdLine.refresh();
		ytMusicAuthFileRepository.writeYouTubeMusicAuth();
	}
}
