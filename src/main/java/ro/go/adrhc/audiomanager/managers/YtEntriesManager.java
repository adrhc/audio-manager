package ro.go.adrhc.audiomanager.managers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.config.YtToDiskSwitchProperties;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.PlaylistStorageOutcome;
import ro.go.adrhc.audiomanager.datasources.youtube.YtPlEntriesRepository;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.domain.playlist.PlaylistFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntriesFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.audiomanager.domain.services.PlaylistsService;

import java.io.IOException;
import java.util.Collection;

import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YtMusicPlLocation.YT_LIKES_MUSIC;

@Service
@RequiredArgsConstructor
@Slf4j
public class YtEntriesManager {
	private final YtToDiskSwitchProperties ytToDiskSwitchProperties;
	private final DiskLocationFactory diskLocationFactory;
	private final DiskPlaylistsRepository diskPlaylistsRepository;
	private final YtPlEntriesRepository ytPlEntriesRepository;
	private final PlaylistsService playlistsService;
	private final YouTubeLocationFactory ytLocationFactory;

	public PlaylistEntries loadFromPlaylist(String uri) {
		return ytLocationFactory.parse(uri)
				.map(ytPlEntriesRepository::loadFromPlaylist)
				.map(PlaylistEntries::sort)
				.map(PlaylistEntries::deduplicateByMediaId)
				.orElseGet(PlEntriesFactory::empty);
	}

	public PlaylistEntries loadMusicPlaylists() {
		return ytPlEntriesRepository.loadMusicPlaylists().sort().deduplicateByMediaId();
	}

	/**
	 * Change YouTube uris to local paths.
	 */
	public PlaylistStorageOutcome switchYtToDiskLocation(DiskLocation plLocation)
			throws IOException {
		Playlist<DiskLocation> playlist = diskPlaylistsRepository.load(plLocation);
		playlist = playlistsService.updateYtEntries(true, playlist);
		playlist = playlistsService.switchYtToDiskLocation(playlist);
		playlist = ytToDiskSwitchProperties.toLocalPlLocation(playlist);
		return diskPlaylistsRepository.storeIfNotEmpty(playlist);
	}

	public PlaylistStorageOutcome appendYtMedia(DiskLocation plLocation,
			Collection<YouTubeLocation> ytLocations) {
		Playlist<DiskLocation> playlist = diskPlaylistsRepository.loadOrCreate(plLocation);
		PlaylistEntries ytEntries = ytPlEntriesRepository.loadFromLocations(ytLocations);
		playlist.entries().addAll(ytEntries);
		return diskPlaylistsRepository.storeIfNotEmpty(playlist);
	}

	/**
	 * Creates a playlist with 1 entry, i.e. youtube:playlist:{0}, YouTubeLocationType.PLAYLIST
	 */
	public PlaylistStorageOutcome createLinkToYtPlaylist(YouTubeLocation ytPlLocation) {
		DiskLocation linkLocation = createPlLinkLocation(ytPlLocation);
		return createLinkToYtPlaylist(ytPlLocation, linkLocation);
	}

	public PlaylistStorageOutcome createLinkToYtPlaylist(
			YouTubeLocation ytPlLocation, DiskLocation linkLocation) {
		PlaylistEntry plEntry = ytPlEntriesRepository.loadPlEntryFromLocation(ytPlLocation);
		Playlist<DiskLocation> linkToYtPl = PlaylistFactory.of(linkLocation, plEntry);
		return diskPlaylistsRepository.storeIfNotEmpty(linkToYtPl);
	}

	public PlaylistStorageOutcome downloadYtLikedMusicPlaylist() {
		DiskLocation likedPlLocation = diskLocationFactory.createLikedMusicPlLocation();
		return downloadYtLikedMusicPlaylist(likedPlLocation);
	}

	public PlaylistStorageOutcome downloadYtLikedMusicPlaylist(DiskLocation likedPlLocation) {
		return downloadYtPlaylist(YT_LIKES_MUSIC, likedPlLocation);
	}

	public PlaylistStorageOutcome downloadYtPlaylist(YouTubeLocation ytPlLocation) {
		DiskLocation plCopyLocation = createPlCopyLocation(ytPlLocation);
		return downloadYtPlaylist(ytPlLocation, plCopyLocation);
	}

	public PlaylistStorageOutcome downloadYtPlaylist(
			YouTubeLocation ytPlLocation, DiskLocation plCopyLocation) {
		log.debug("\ngetting the content of {}", ytPlLocation);
		PlaylistEntries entries = ytPlEntriesRepository.loadFromPlaylist(ytPlLocation);
		Playlist<DiskLocation> plCopy = new Playlist<>(plCopyLocation, entries.sort());
		return diskPlaylistsRepository.storeIfNotEmpty(plCopy);
	}

	private DiskLocation createPlCopyLocation(YouTubeLocation ytPlLocation) {
		PlaylistEntry plEntry = ytPlEntriesRepository.loadPlEntryFromLocation(ytPlLocation);
		return diskLocationFactory.createPlCopyLocation(plEntry);
	}

	private DiskLocation createPlLinkLocation(YouTubeLocation ytPlLocation) {
		PlaylistEntry plEntry = ytPlEntriesRepository.loadPlEntryFromLocation(ytPlLocation);
		return diskLocationFactory.createPlLinkLocation(plEntry);
	}
}
