package ro.go.adrhc.audiomanager.managers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.disk.DiskPlEntriesRepository;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.domain.location.LocationPredicates;
import ro.go.adrhc.audiomanager.domain.location.NamedLocation;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.domain.playlist.entries.EntriesPairs;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.audiomanager.domain.services.PlaylistEntriesService;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.dlscripts.DlBashScriptsDirectory;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.dlscripts.ytdlbashscriptswriter.YtDlBashScriptsWriter;
import ro.go.adrhc.util.pair.Pair;

import java.io.IOException;

import static java.util.function.Predicate.not;

@Service
@RequiredArgsConstructor
@Slf4j
public class YtDlBashScriptsManager {
	private final DiskPlaylistsRepository diskPlaylistsRepository;
	private final YtDlBashScriptsWriter ytDlBashScriptsWriter;
	private final DlBashScriptsDirectory dlBashScriptsDirectory;
	private final PlaylistEntriesService plEntriesService;
	private final DiskPlEntriesRepository diskPlEntriesRepository;

	/**
	 * Create the bash scripts containing youtube-dl commands to download
	 * the not found files corresponding to the YouTube urls in playlistPath.
	 */
	public void create(DiskLocation plLocation, int partitionsSize) throws IOException {
		Playlist<? extends NamedLocation> playlist = diskPlaylistsRepository.load(plLocation);

		PlaylistEntries ytEntries = playlist.entries().filter(LocationPredicates::hasYtLocation);
		// updating the title of YouTube entries
		ytEntries = plEntriesService.updateYtEntries(true, ytEntries);

		// unique (determined by searched entries' uniqueness) [searched, found] pairs
		EntriesPairs ytDiskPairs = diskPlEntriesRepository
				.findBestMatches(ytEntries.filter(PlaylistEntry::hasTitle));

		// YouTube entries missing the related DISK location
		PlaylistEntries noDiskYtEntries = ytEntries.filter(not(ytDiskPairs::contains));

		ytDlBashScriptsWriter.write(partitionsSize,
				playlist.location().name(), new Pair<>(noDiskYtEntries, ytDiskPairs));
	}

	public void remove(DiskLocation plLocation) {
		dlBashScriptsDirectory.removeScripts(plLocation.name());
	}
}
