package ro.go.adrhc.audiomanager.managers.diskplquery;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.disk.DiskPlLocationQueries;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.LocationParsers;

import java.io.IOException;

@Service
@RequiredArgsConstructor
@Slf4j
public class DiskPlQueryManager {
	private final LocationParsers locationParsers;
	private final DiskPlLocationQueries diskPlLocationQueries;
	private final DiskPlaylistsRepository diskPlaylistsRepository;

	public LocationSelections findPlLocationsByEntry(String uri) throws IOException {
		Location entryLocation = locationParsers.parse(uri);
		return LocationSelections.of(
				diskPlaylistsRepository.getPlaylistsLocations(),
				diskPlLocationQueries.findByEntry(entryLocation));
	}
}
