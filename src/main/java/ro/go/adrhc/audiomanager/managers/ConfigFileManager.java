package ro.go.adrhc.audiomanager.managers;

import lombok.RequiredArgsConstructor;
import org.apache.commons.cli.ParseException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import ro.go.adrhc.audiomanager.datasources.youtube.config.YouTubeProperties;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.config.ConfigFileRepository;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.config.FileNameAndContent;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class ConfigFileManager {
	private final YouTubeProperties youTubeProperties;
	private final ConfigFileRepository configFileRepository;
	private final YtMusicAuthFileManager ytMusicAuthFileManager;

	public void updateContent(@RequestBody FileNameAndContent nameAndContent)
			throws IOException, ParseException {
		if (youTubeProperties.hasLikedMusicCurlPath(nameAndContent)) {
			ytMusicAuthFileManager.updateYtMusicAuth(nameAndContent);
		} else {
			configFileRepository.updateContent(nameAndContent);
		}
	}
}
