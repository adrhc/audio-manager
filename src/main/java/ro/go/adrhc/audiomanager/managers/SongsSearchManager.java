package ro.go.adrhc.audiomanager.managers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ro.go.adrhc.audiomanager.config.context.AppPathType;
import ro.go.adrhc.audiomanager.config.context.validation.AppPathsValidator;
import ro.go.adrhc.audiomanager.datasources.disk.DiskPlEntriesRepository;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.datasources.youtube.YtPlEntriesRepository;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;

import java.io.IOException;

import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiomanager.config.context.AppPathType.INDEX;

@Service
@RequiredArgsConstructor
public class SongsSearchManager {
	private final DiskPlEntriesRepository diskPlEntriesRepository;
	private final YtPlEntriesRepository ytPlEntriesRepository;

	public PlaylistEntries search(String text) throws IOException {
		Assert.isTrue(hasText(text), "Searched text must not be empty!");
		return structuredSearch(null, null, text);
	}

	public PlaylistEntries structuredSearch(String title, String artist, String text)
			throws IOException {
		return searchOnDisk(title, artist, text).plus(searchYouTubeVideos(title, artist, text));
	}

	public PlaylistEntries searchOnDisk(String title, String artist, String text)
			throws IOException {
		throwIfInvalid(INDEX);
		SongQuery query = new SongQuery(title, artist, text);
		songQueryAssert(query);
		return diskPlEntriesRepository.findMany(query);
	}

	public PlaylistEntries searchYouTubeVideos(String title, String artist, String text) {
		SongQuery query = new SongQuery(title, artist, text);
		songQueryAssert(query);
		return ytPlEntriesRepository.findAllVideoMatches(query);
	}

	public PlaylistEntries searchYouTubeMusic(String title, String artist, String text) {
		SongQuery query = new SongQuery(title, artist, text);
		songQueryAssert(query);
		return ytPlEntriesRepository.findAllMusicMatches(query);
	}

	protected void throwIfInvalid(AppPathType... appPathTypes) {
		pathTypesValidator(appPathTypes).throwIfInvalid();
	}

	protected AppPathsValidator pathTypesValidator(AppPathType... appPathTypes) {
		return contextPathsValidator().addCtxPathTypes(appPathTypes);
	}

	@Lookup
	protected AppPathsValidator contextPathsValidator() {
		return null;
	}

	private static void songQueryAssert(SongQuery query) {
		Assert.isTrue(query.hasTitle() || query.hasArtist() || query.hasFreeText(),
				"At least one parameter must contain not empty text!");
	}
}
