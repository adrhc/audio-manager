package ro.go.adrhc.audiomanager.lib;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.audiomanager.util.FileUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Supplier;

import static org.apache.commons.collections4.ListUtils.partition;
import static ro.go.adrhc.util.io.FilenameUtils.addSuffix;
import static ro.go.adrhc.util.io.PathUtils.setPosixFilePermissions;

@Slf4j
@RequiredArgsConstructor
public class PartitionedLinesFileWriter {
	protected final Supplier<Path> rootPathSupplier;
	private final EnumSet<PosixFilePermission> permissions;

	/**
	 * Write the toDownloadCommands into partitions numbered playlistPath files.
	 * e.g. for playlistPath = playlist.m3u8 with partitionsSize = 2
	 * than playlist1.m3u8 and playlist2.m3u8 will be generated
	 */
	public void write(String fileName, int partitionsSize, List<String> lines) throws IOException {
		if (lines.isEmpty()) {
			log.debug("\nAll songs are already on disk, no download is required!");
			return;
		}
		if (partitionsSize > 1) {
			List<List<String>> toDownloadPartitions = partition(lines, partitionsSize);
			writePartitions(fileName, toDownloadPartitions);
		} else {
			writeAndSetPermissionsPartition(resolvePath(fileName), lines);
		}
	}

	protected Path resolvePath(String fileName) {
		return rootPathSupplier.get().resolve(Path.of(fileName));
	}

	private void writePartitions(String fileName,
			List<List<String>> toDownloadPartitions) throws IOException {
		for (List<String> partition : toDownloadPartitions) {
			writePartition(fileName, toDownloadPartitions, partition);
		}
	}

	private void writePartition(String fileName, List<?> toDownloadPartitions,
			Collection<String> partitionLines) throws IOException {
		String partitionFileName = partitionFileName(fileName, toDownloadPartitions,
				partitionLines);
		Path partitionPath = resolvePath(partitionFileName);
		writeAndSetPermissionsPartition(partitionPath, partitionLines);
	}

	private void writeAndSetPermissionsPartition(Path path, Collection<String> lines)
			throws IOException {
		FileUtils.writeLines(path, lines);
		setPosixFilePermissions(permissions, path);
	}

	private String partitionFileName(String fileName, List<?> toDownloadPartitions,
			Object partition) {
		return addSuffix(fileName, partitionIndex(toDownloadPartitions, partition));
	}

	private String partitionIndex(List<?> toDownloadPartitions, Object partition) {
		return String.valueOf(toDownloadPartitions.indexOf(partition) + 1);
	}
}
