package ro.go.adrhc.audiomanager.lib.paginateddata;

import java.util.Collection;
import java.util.stream.Stream;

public abstract class AbstractPaginatedDataReader<T, N> {
	public Stream<T> getData() {
		DataPage<T, N> dataPage = getDataPage(null);
		Stream<DataPage<T, N>> chunks = Stream.iterate(
				dataPage, DataPage::hasNext, this::getDataPage);
		return chunks
				.map(DataPage::data)
				.flatMap(Collection::stream);
	}

	protected N nextDataPageToken(DataPage<T, N> dataPage) {
		return dataPage == null ? null : dataPage.nextDataPageToken();
	}

	protected DataPage<T, N> getDataPage(DataPage<T, N> previousDataPage) {
		if (DataPage.isLastDataPage(previousDataPage)) {
			return new DataPage<>();
		}
		return doGetDataPage(previousDataPage);
	}

	abstract protected DataPage<T, N> doGetDataPage(DataPage<T, N> previousChunk);
}
