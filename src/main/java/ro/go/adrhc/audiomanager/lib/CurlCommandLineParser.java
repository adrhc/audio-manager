package ro.go.adrhc.audiomanager.lib;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@RequiredArgsConstructor
@Slf4j
public class CurlCommandLineParser {
	private static final Options CURL_OPTIONS = new Options();
	/**
	 * https://stackoverflow.com/questions/1570896/what-does-mean-in-a-regular-expression
	 * '\d+(?=\.\w+$)'
	 * file4.txt will match 4
	 * file123.txt will match 123
	 * https://stackoverflow.com/questions/2341184/what-does-x-mean-in-regex
	 * (?<=a)b (positive lookbehind) matches the b (and only the b) in cab, but does not match bed or debt.
	 */
	private static final String CURL_SPLIT_REGEX =
			"((?=curl +)|(?<=curl )|" +
					"(?=--data-raw +)|(?<=--data-raw )|" +
					"(?=--compressed)|(?<=--compressed )|" +
					"(?= +-[a-zA-Z] +)|(?<= -[a-zA-Z] ))";

	static {
		CURL_OPTIONS.addOption(null, "compressed", false, "compressed");
		CURL_OPTIONS.addOption("D", "data-raw", true, "data");
		CURL_OPTIONS.addOption("H", true, "header/@file");
		CURL_OPTIONS.addOption("X", true, "method");
	}

	public CommandLine parse(String curlCommand) throws ParseException {
		String[] args = curlCommand.split(CURL_SPLIT_REGEX);
		args = Arrays.stream(args)
				.map(String::trim)
				.map(this::parseBetweenQuotes)
				.map(String::trim)
				.filter(StringUtils::hasText)
				.toArray(String[]::new);
		CommandLineParser parser = new DefaultParser();
		return parser.parse(CURL_OPTIONS, args);
	}

	private String parseBetweenQuotes(String input) {
		Pattern pattern = Pattern.compile("^'(.+)'( +\\\\)?$");
		Matcher matcher = pattern.matcher(input);
		return matcher.find() ? matcher.group(1) : input;
	}
}
