package ro.go.adrhc.audiomanager.lib.paginateddata;

import java.util.Collection;

public record DataPage<T, N>(Collection<T> data, N nextDataPageToken) {
	public DataPage() {
		this(null, null);
	}

	public static boolean isLastDataPage(DataPage<?, ?> dataPage) {
		return dataPage != null && dataPage.nextDataPageToken() == null;
	}

	public boolean hasNext() {
		return data != null && !data.isEmpty();
	}
}
