package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked;

import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams.YouTubeApiParams;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser.ytliked.YtMusicSearchResultParser;
import ro.go.adrhc.audiomanager.lib.paginateddata.AbstractPaginatedDataReader;
import ro.go.adrhc.audiomanager.lib.paginateddata.DataPage;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static ro.go.adrhc.util.text.StringUtils.concat;

@Service
@RequiredArgsConstructor
@Slf4j
public class YtLikedMusicMetadataProvider extends
		AbstractPaginatedDataReader<YouTubeMetadata, Map<String, String>> {
	private final YtLikedMusicRestClient ytLikedMusicRestClient;
	private final YouTubeApiParams ytLikedMusicParams;

	public boolean isYouTubeMusicAuthValid() {
		try {
			loadMetadata();
			return true;
		} catch (FeignException.Forbidden | FeignException.Unauthorized fe) {
			log.error(fe.getMessage(), fe);
		}
		return false;
	}

	/**
	 * Outside (i.e. YouTube) text must be sanitized (i.e. sanitizeSongTitle(...)).
	 * see YtTextDetailsToTitleConverter for sanitization
	 */
	public Set<YouTubeMetadata> loadMetadata() {
		return getData().collect(Collectors.toSet());
	}

	protected DataPage<YouTubeMetadata, Map<String, String>>
	doGetDataPage(DataPage<YouTubeMetadata, Map<String, String>> previousChunk) {
		String json = getLikedJson(nextDataPageToken(previousChunk));
		YtMusicSearchResultParser parser = ytMusicPlSearchResultParser(json);
		Collection<YouTubeMetadata> metadataSet = parser.extractYouTubeMetadata();
		Map<String, String> nextChunkToken = parser.extractNextChunkToken();
		return new DataPage<>(metadataSet, nextChunkToken);
	}

	protected String getLikedJson(Map<String, String> nextChunkToken) {
		log.debug("\nHEADERS:\n{}", ytLikedMusicParams.headers().keySet()
				.stream().sorted().map(k -> STR."""
				\{k}: \{concat(", ", ytLikedMusicParams.getHeader(k))}""")
				.collect(Collectors.joining("\n")));
		log.debug("\nPAYLOAD:\n{}", ytLikedMusicParams.payload());
		log.debug("\nnextChunkToken: {}", nextChunkToken);
		return ytLikedMusicRestClient.getLiked(ytLikedMusicParams.headers(),
				ytLikedMusicParams.payload(), ytLikedMusicParams.queryParams(nextChunkToken));
	}

	@Lookup("ytMusicPlSearchResultParser")
	protected YtMusicSearchResultParser ytMusicPlSearchResultParser(String json) {
		return null;
	}
}
