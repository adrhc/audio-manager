package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Use -v for debugging.
 * youtube-dl --cookies %homedrive%%homepath%\Projects-adrhc\audio-manager\audio-manager\m3u8-playlists-manager\config\youtube.com_cookies.txt --dump-json http://music.youtube.com/watch?v=4zgsPO48wvw http://music.youtube.com/watch?v=eBo9IrJxQzw http://music.youtube.com/watch?v=n5scparvQWE http://music.youtube.com/watch?v=BwU7FR0uaaY http://music.youtube.com/watch?v=sFvkHaB0yfA http://music.youtube.com/watch?v=QzHXt6n1QhM http://music.youtube.com/watch?v=72oJGTPSWIM http://music.youtube.com/watch?v=zwp8UxIY_iM http://music.youtube.com/watch?v=4SENkrissNw http://music.youtube.com/watch?v=0dEZMAeiAiY http://music.youtube.com/watch?v=eOR2QYy670k http://music.youtube.com/watch?v=hbDx2FVd9e0 http://music.youtube.com/watch?v=Zqo3dCxv20U http://music.youtube.com/watch?v=0q5SSoho0cs http://music.youtube.com/watch?v=87obnKnpIds http://music.youtube.com/watch?v=Z3nzbRftpeU http://music.youtube.com/watch?v=KNDT7EInclo http://music.youtube.com/watch?v=uX1HnvlWfc4 http://music.youtube.com/watch?v=lc_3vcv8IDI http://music.youtube.com/watch?v=j91eq3O_R-o http://music.youtube.com/watch?v=8pO2KTnLPaI http://music.youtube.com/watch?v=W_3cjNiFvFE http://music.youtube.com/watch?v=Hem9ZE3U8O8 http://music.youtube.com/watch?v=VIqUuNiCG_s http://music.youtube.com/watch?v=UjEpw4O-WNI http://music.youtube.com/watch?v=re32xnyYP3A http://music.youtube.com/watch?v=LQKspH34BkM http://music.youtube.com/watch?v=MDSaYvs-LJI http://music.youtube.com/watch?v=4z9TdDCWN7g http://music.youtube.com/watch?v=2-lGevvO2vw http://music.youtube.com/watch?v=VOvadjUtE0s > videos.txt
 * yt-dlp --dump-json http://music.youtube.com/watch?v=oDLU3e34G1o
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class YoutubeDl {
	private final JsonToYoutubeDlRecordConverter converter;
	private final YoutubeDlProperties properties;

	public Set<YoutubeDlRecord> getYoutubeDlRecords(Collection<String> videoURLs) {
		return getRawMetadata(videoURLs)
				.map(jsons -> jsons.split("\n"))
				.map(lines -> Arrays.stream(lines)
						.map(converter::convert)
						.filter(Objects::nonNull)
						.collect(Collectors.toSet()))
				.orElseGet(Set::of);
	}

	/**
	 * Use -v for debugging.
	 * youtube-dl --cookies %homedrive%%homepath%\Projects-adrhc\audio-manager\audio-manager\m3u8-playlists-manager\config\youtube.com_cookies.txt --dump-json http://www.youtube.com/watch?v=SAsJ_n747T4
	 * youtube-dl --dump-json https://music.youtube.com/watch?v=SAsJ_n747T4 https://music.youtube.com/watch?v=oDLU3e34G1o
	 */
	private Optional<String> getRawMetadata(Collection<String> videoURLs) {
		ProcessBuilder processBuilder = createProcessBuilder(toParams(videoURLs));
//		log.info("\nyt-dlp command:\n{}", concat(" ", processBuilder.command()));

		try {
			Process process = processBuilder.start();

			String metadata = IOUtils.toString(process.getInputStream(), StandardCharsets.UTF_8);
			// log.info("\nyt-dlp command result:\n{}", metadata);

			// process waiting must happen after reading its output!
			waitFor(videoURLs, process);

			return Optional.of(metadata).filter(StringUtils::hasText);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			log.error("\nFailed to get YouTube metadata!");
		}
		return Optional.empty();
	}

	private void waitFor(Collection<String> videoURLs, Process process)
			throws InterruptedException {
		if (properties.getMetadataExtractPerVideoTimeout().isPositive()) {
			long timeout = properties.getMetadataExtractPerVideoTimeout().getSeconds() * videoURLs.size();
			if (!process.waitFor(timeout, TimeUnit.SECONDS)) {
				log.error("\nyt-dlp is processing longer than acceptable ({}s)! interrupted it.",
						timeout);
			}
		} else {
			process.waitFor();
		}
	}

	private List<String> toParams(Collection<String> videoURLs) {
		List<String> params = new ArrayList<>(videoURLs);
		params.add(0, "--dump-json");
		if (properties.getYoutubeCookiesPath() != null) {
			params.add(0, properties.getYoutubeCookiesPath().toString());
			params.add(0, "--cookies");
		}
		params.add(0, "--ignore-errors");
//        params.add(0, "youtube-dl");
		params.add(0, "yt-dlp");
		return params;
	}

	private ProcessBuilder createProcessBuilder(List<String> parameters) {
		ProcessBuilder processBuilder = new ProcessBuilder(parameters);
		processBuilder.redirectError(ProcessBuilder.Redirect.INHERIT);
		return processBuilder;
	}
}
