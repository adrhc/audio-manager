package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.websearch;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "youtube.YtMusicLibrary",
		url = "${youtube-api.web-music-library}")
public interface YtMusicLibrary {
	@PostMapping
	String getAll(@RequestHeader HttpHeaders headers,
			@RequestBody String context,
			@RequestParam Map<String, Object> queryParams);
}
