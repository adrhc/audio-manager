package ro.go.adrhc.audiomanager.datasources.audiofiles.metadata;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.lib.audiometadata.loaders.AudioSystemLoader;
import ro.go.adrhc.lib.audiometadata.loaders.Jid3LibMp3Loader;
import ro.go.adrhc.lib.audiometadata.loaders.TikaMp3Loader;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import static java.nio.file.StandardOpenOption.READ;
import static ro.go.adrhc.lib.audiometadata.domain.AudioMetadataAccessors.path;

/**
 * This is an infrastructure service because is directly dealing with disk access.
 * This could be seen as a metadata repository, a disk based one.
 */
@Slf4j
@RequiredArgsConstructor
public class AudioMetadataLoader {
	private final Set<Function<InputStream, ro.go.adrhc.lib.audiometadata.AudioMetadataLoader>> streamMetadataParsers;
	private final Set<ro.go.adrhc.lib.audiometadata.AudioMetadataLoader> fileMetadataParsers;

	public static AudioMetadataLoader create() {
		return new AudioMetadataLoader(
				Set.of(TikaMp3Loader::new, AudioSystemLoader::new),
				Set.of(new Jid3LibMp3Loader()));
	}

	public Optional<AudioMetadata> load(Path path) {
		AudioMetadata metadata = AudioMetadata.of(path);
		if (path.getFileName().toString().equals(metadata.fileNameNoExt())) {
			// skipping no-extension files
			return Optional.empty();
		} else {
			doLoadMetadata(metadata);
			return Optional.of(metadata);
		}
	}

	private void doLoadMetadata(AudioMetadata metadata) {
		Optional<Path> pathOptional = path(metadata);
		if (pathOptional.isEmpty()) {
			return;
		}
		try (BufferedInputStream bis = new BufferedInputStream(
				Files.newInputStream(pathOptional.get(), READ))) {
			bis.mark(Integer.MAX_VALUE);
			streamMetadataParsers
					.forEach(loaderFactory -> {
						loaderFactory.apply(bis).populate(metadata);
						try {
							bis.reset();
						} catch (IOException e) {
							log.error(e.getMessage(), e);
						}
						// some AudioSystem.getAudioFileFormat() -> AudioFileReader resets bis too!
						bis.mark(Integer.MAX_VALUE);
					});
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		fileMetadataParsers.forEach(audioMetadataLoader ->
				audioMetadataLoader.populate(metadata));
		metadata.updateDurationFromTags();
	}
}
