package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * The API used by YouTube Music page (i.e. https://music.youtube.com/playlist?list=LM).
 */
@FeignClient(name = "youtube.music.liked", url = "${youtube-api.music-liked-url}")
public interface YtLikedMusicRestClient {
	@PostMapping
	String getLiked(@RequestHeader HttpHeaders headers,
			@RequestBody String context,
			@RequestParam Map<String, Object> queryParams);
}
