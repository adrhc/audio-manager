package ro.go.adrhc.audiomanager.datasources.disk.playlists.converters;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.playlistfiledirectory.PlaylistFilesDirectory;
import ro.go.adrhc.lib.m3u8.domain.M3u8Playlist;
import ro.go.adrhc.lib.m3u8.domain.M3u8Record;

import java.nio.file.Path;
import java.util.List;

@Component
@RequiredArgsConstructor
public class DiskPlaylistToM3u8PlaylistConverter
		implements Converter<Playlist<DiskLocation>, M3u8Playlist> {
	private final PlaylistFilesDirectory playlistFilesDirectory;
	private final PlEntryToM3u8RecordConverter toM3u8RecordConverter;

	@Override
	@NonNull
	public M3u8Playlist convert(@NonNull Playlist<DiskLocation> playlist) {
		return new M3u8Playlist(toPath(playlist), toM3u8Records(playlist));
	}

	private List<M3u8Record> toM3u8Records(Playlist<DiskLocation> playlist) {
		return playlist.entries().map(toM3u8RecordConverter::convert).toList();
	}

	private Path toPath(Playlist<DiskLocation> playlist) {
		return playlistFilesDirectory.resolvePath(playlist.location().path());
	}
}
