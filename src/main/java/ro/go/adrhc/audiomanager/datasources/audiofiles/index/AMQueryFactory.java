package ro.go.adrhc.audiomanager.datasources.audiofiles.index;

import lombok.experimental.UtilityClass;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.SortedNumericSortField;
import ro.go.adrhc.persistence.lucene.core.bare.query.FieldQueries;
import ro.go.adrhc.persistence.lucene.core.typed.ExactQuery;

import java.util.EnumSet;
import java.util.Locale;
import java.util.Set;

import static org.apache.lucene.search.SortField.Type.LONG;
import static ro.go.adrhc.audiomanager.datasources.audiofiles.index.AudioMetadataFields.updateMoment;
import static ro.go.adrhc.persistence.lucene.core.bare.query.BooleanQueryFactory.mustSatisfy;

@UtilityClass
public class AMQueryFactory {
	public static final FieldQueries FILENAME_NO_EXT_QUERY =
			FieldQueries.create(AudioMetadataFields.fileNameNoExt);
	public static final FieldQueries TAGS_QUERY =
			FieldQueries.create(AudioMetadataFields.tags);
	public static final ExactQuery LOCATION_TYPE_QUERY =
			ExactQuery.create(AudioMetadataFields.locationType);

	public static final SortField UPDATE_MOMENT_SORT_FIELD =
			new SortedNumericSortField(updateMoment.name(), LONG, false);
	public static final Sort SORT_BY_UPDATE_MOMENT =
			new Sort(UPDATE_MOMENT_SORT_FIELD);

	public static final SortField UPDATE_MOMENT_SORT_REVERSE_FIELD =
			new SortedNumericSortField(updateMoment.name(), LONG, true);
	public static final Sort SORT_REVERSE_BY_UPDATE_MOMENT =
			new Sort(UPDATE_MOMENT_SORT_REVERSE_FIELD);

	public static <E extends Enum<E>> Query hasTag(E tag) {
		return hasTag(tag.name());
	}

	public static <E extends Enum<E>> Query hasAllTags(EnumSet<E> tags) {
		return mustSatisfy(tags.stream().map(AMQueryFactory::hasTag).toList());
	}

	public static Query hasTag(String tag) {
		return TAGS_QUERY.tokenEquals(tag.toLowerCase(Locale.ROOT));
	}

	public static Query hasAllTags(Set<String> tags) {
		return mustSatisfy(tags.stream().map(AMQueryFactory::hasTag).toList());
	}
}
