package ro.go.adrhc.audiomanager.datasources.disk.locations.domain;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import org.springframework.lang.NonNull;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.SearchedAudio;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.util.Collection;
import java.util.Set;

import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static ro.go.adrhc.audiomanager.domain.location.LocationAccessors.locationName;
import static ro.go.adrhc.util.collection.SetUtils.mapToSet;
import static ro.go.adrhc.util.collection.SetUtils.setOf;

@Getter
public class TraceableSearchedAudio<T> extends SearchedAudio {
	private final T rawSearchData;

	public TraceableSearchedAudio(@NotNull String title, Set<String> words,
			Integer duration, T rawSearchData) {
		super(title, words, duration);
		this.rawSearchData = rawSearchData;
	}

	public static TraceableSearchedAudio<PlaylistEntry> of(@NonNull PlaylistEntry playlistEntry) {
		String title = firstNonNull(playlistEntry.title(), locationName(playlistEntry));
		Set<String> words = setOf(playlistEntry.title(), locationName(playlistEntry));
		return new TraceableSearchedAudio<>(title, words, playlistEntry.duration(), playlistEntry);
	}

	public static <T extends SongQuery>
	Set<TraceableSearchedAudio<T>> ofAnySongQueries(@NonNull Collection<T> queries) {
		return mapToSet(queries, TraceableSearchedAudio::ofAnySongQuery);
	}

	public static <T extends SongQuery>
	TraceableSearchedAudio<T> ofAnySongQuery(@NonNull T query) {
		return new TraceableSearchedAudio<>(query.concatenateParts(), query.toSet(), null, query);
	}
}
