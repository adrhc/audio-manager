package ro.go.adrhc.audiomanager.datasources.disk.locations.domain;

import lombok.experimental.UtilityClass;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.FoundAudio;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.util.pair.Pair;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.FoundAudioAccessors.rawSearchData;

@UtilityClass
public class TraceableSearchedAudioLocationPairFactory {
	/**
	 * key = searched SongQuery, value = found location
	 *
	 * @param <T> is the raw searched data
	 * @param <L> is the found location
	 */
	public static <T, L extends Location> Pair<T, L> ofTraceableFoundAudio(
			FoundAudio<TraceableSearchedAudio<T>, L> foundAudio) {
		return new Pair<>(rawSearchData(foundAudio), foundAudio.location());
	}
}
