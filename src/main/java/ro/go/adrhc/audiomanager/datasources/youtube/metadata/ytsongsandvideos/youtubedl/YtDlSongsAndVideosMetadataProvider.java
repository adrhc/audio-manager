package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationType;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YtLocationAccessors;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.LogUtils;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.YtMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.YtSongsAndVideosMetadataProviderUtils;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.stream.Stream;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneaked;
import static java.util.concurrent.CompletableFuture.supplyAsync;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationPredicates.hasSongOrVideoOnly;
import static ro.go.adrhc.util.concurrency.FutureUtils.safelyGetAll;

@RequiredArgsConstructor
@Slf4j
public class YtDlSongsAndVideosMetadataProvider implements YtMetadataProvider {
	private final YoutubeDl youtubeDl;
	private final YouTubeDlRecordToTitleConverter ytDlRecordToTitleConverter;
	private final ExecutorService cachedThreadPool;
	private final YouTubeLocationsToYtURLSetConverter ytLocationsToYtURLSetConverter;
	private final LogUtils logUtils;

	public YtLocationMetadataPairs loadMetadata(Collection<YouTubeLocation> ytLocations) {
		Map<YouTubeLocationType, Set<YouTubeLocation>> ytIdsGroupedByType =
				YtLocationAccessors.ytLocationsGroupedByType(ytLocations);
		YtSongsAndVideosMetadataProviderUtils.assertHasSongOrVideoOnly(
				hasSongOrVideoOnly(ytIdsGroupedByType.keySet()));
		Stream<CompletableFuture<YtLocationMetadataPairs>> futures = asyncGetMetadata(
				ytIdsGroupedByType);
		return YtLocationMetadataPairs.sum(safelyGetAll(futures));
	}

	private Stream<CompletableFuture<YtLocationMetadataPairs>>
	asyncGetMetadata(Map<YouTubeLocationType, Set<YouTubeLocation>> ytLocationsByType) {
		return ytLocationsByType.keySet().stream().map(
				type -> asyncGetMetadataWithYtLocationType(ytLocationsByType, type));
	}

	private CompletableFuture<YtLocationMetadataPairs> asyncGetMetadataWithYtLocationType(
			Map<YouTubeLocationType, Set<YouTubeLocation>> ytLocationsByType,
			YouTubeLocationType type) {
		return supplyAsync(sneaked(() -> loadYtLocationMetadataPairs(
				type, ytLocationsByType.get(type))), cachedThreadPool);
	}

	/**
	 * Outside (i.e. YouTube) text must be sanitized (i.e. sanitizeSongTitle(...))
	 * see YouTubeDlRecordToTitleConverter for sanitization
	 */
	private YtLocationMetadataPairs loadYtLocationMetadataPairs(
			YouTubeLocationType ytLocationType, Collection<YouTubeLocation> ytLocations)
			throws IOException {
		Set<String> ytURLs = ytLocationsToYtURLSetConverter.convert(ytLocations);
		Set<YouTubeMetadata> youTubeMetadata = loadYouTubeMetadata(ytLocationType, ytURLs);
		YtLocationMetadataPairs metadata = YtLocationMetadataPairs.of(youTubeMetadata);
		logUtils.logNotFound(ytLocations, metadata);
		return metadata;
	}

	private Set<YouTubeMetadata> loadYouTubeMetadata(
			YouTubeLocationType ytLocationType, Set<String> ytURLs) throws IOException {
		Set<YouTubeMetadata> result = new HashSet<>();
		Set<YoutubeDlRecord> ytDlRecords = youtubeDl.getYoutubeDlRecords(ytURLs);
		for (YoutubeDlRecord ytDlRecord : ytDlRecords) {
			result.add(toYouTubeLocation(ytLocationType, ytDlRecord));
		}
		return result;
	}

	private YouTubeMetadata toYouTubeLocation(
			YouTubeLocationType ytLocationType, YoutubeDlRecord ytDlRecord) throws IOException {
		return YouTubeMetadata.of(
				new YouTubeLocation(ytLocationType, ytDlRecord.getId()),
				ytDlRecordToTitleConverter.convert(ytDlRecord));
	}
}
