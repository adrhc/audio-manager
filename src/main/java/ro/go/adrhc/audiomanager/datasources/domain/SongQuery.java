package ro.go.adrhc.audiomanager.datasources.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.util.StringUtils;
import ro.go.adrhc.util.text.StringBuilderEx;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.util.text.StringBuilderEx.spaceSeparator;

@RequiredArgsConstructor
@Getter
@Accessors(fluent = true)
@EqualsAndHashCode
public class SongQuery {
	private final String title;
	private final String artist;
	private final String freeText;

	public static SongQuery ofTitleAndArtist(String title, String artist) {
		return new SongQuery(title, artist, null);
	}

	public static SongQuery ofTitle(String title) {
		return new SongQuery(title, null, null);
	}

	public static SongQuery ofFreeText(String freeText) {
		return new SongQuery(null, null, freeText);
	}

	public String concatenateParts() {
		StringBuilderEx builder = spaceSeparator();
		builder.append(title);
		builder.append(artist);
		builder.append(freeText);
		return builder.toString();
	}

	public Set<String> toSet() {
		return Stream.of(title, artist, freeText)
				.filter(StringUtils::hasText)
				.collect(Collectors.toSet());
	}

	@Override
	public String toString() {
		StringBuilderEx sb = new StringBuilderEx(", ");
		sb.appendKeyValue("title", title);
		sb.appendKeyValue("artist", artist);
		sb.appendKeyValueWord("free text", freeText);
		return sb.toString();
	}

	public boolean hasTitle() {
		return hasText(title);
	}

	public boolean hasArtist() {
		return hasText(artist);
	}

	public boolean hasFreeText() {
		return hasText(freeText);
	}
}
