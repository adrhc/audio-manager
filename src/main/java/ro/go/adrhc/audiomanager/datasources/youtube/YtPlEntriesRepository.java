package ro.go.adrhc.audiomanager.datasources.youtube;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.YtMetadataRepository;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.YtLibraryProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsearch.YtSearchMetadataProvider;
import ro.go.adrhc.audiomanager.domain.playlist.entries.*;

import java.util.Collection;

import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YtLocationAccessors.ytLocations;
import static ro.go.adrhc.audiomanager.domain.playlist.entries.filters.EntryComparators.followLocationsOrder;

/**
 * A repository of YouTube PlaylistEntry(s).
 */
@RequiredArgsConstructor
@Component
@Slf4j
public class YtPlEntriesRepository {
	private final YtMetadataRepository ytMetadataRepository;
	private final YtSearchMetadataProvider ytSearchMetadataProvider;
	private final YtLibraryProvider ytLibraryProvider;

	public PlaylistEntries loadMusicPlaylists() {
		return PlEntriesFactory.of(ytLibraryProvider.loadMusicLibrary());
	}

	public PlaylistEntries findAllVideoMatches(SongQuery query) {
		return PlEntriesFactory
				.ofYtMetadata(ytSearchMetadataProvider.findAllWebVideoMatches(query))
				.deduplicateByMediaId();
	}

	public PlaylistEntries findAllMusicMatches(SongQuery query) {
		return PlEntriesFactory
				.ofYtMetadata(ytSearchMetadataProvider.findAllWebMusicMatches(query))
				.deduplicateByMediaId();
	}

	/**
	 * Each resulting PlaylistEntry will contain the same location but possibly an updated title.
	 */
	public EntriesPairs loadMatches(PlaylistEntries searchedEntries) {
		Collection<YouTubeLocation> ytLocations = ytLocations(searchedEntries);
		PlaylistEntries ytEntries = loadFromLocations(ytLocations);
		return EntriesPairs.of(searchedEntries, ytEntries);
	}

	public PlaylistEntries loadFromPlaylist(YouTubeLocation ytPlLocation) {
		return PlEntriesFactory.of(ytMetadataRepository.loadFromPlaylist(ytPlLocation));
	}

	public PlaylistEntry loadPlEntryFromLocation(YouTubeLocation ytLocation) {
		return PlEntryFactory.of(ytMetadataRepository.loadFromLocation(ytLocation));
	}

	public PlaylistEntries loadFromLocations(Collection<YouTubeLocation> ytLocations) {
		YtLocationMetadataPairs metadataPairs = ytMetadataRepository.loadFromLocations(ytLocations);
		return PlEntriesFactory.of(metadataPairs).sort(followLocationsOrder(ytLocations));
	}
}
