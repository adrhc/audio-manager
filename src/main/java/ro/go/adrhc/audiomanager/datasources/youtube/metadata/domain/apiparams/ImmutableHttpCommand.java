package ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.commons.cli.CommandLine;
import org.springframework.http.HttpHeaders;

import java.util.function.Predicate;

/**
 * Unpacked commandLine (stored too) into uriComponents and httpHeaders.
 */
@RequiredArgsConstructor
@Getter
@Accessors(fluent = true)
public class ImmutableHttpCommand implements HttpCommand {
	private final CommandLine commandLine;
	private final HttpHeaders httpHeaders;

	@Override
	public HttpHeaders filterHeaders(Predicate<String> keyPredicate) {
		return httpHeaders.entrySet().stream()
				.filter(e -> keyPredicate.test(e.getKey()))
				.collect(HttpHeaders::new,
						(h, e) -> h.put(e.getKey(), e.getValue()),
						HttpHeaders::addAll);
	}

	@Override
	public String payload() {
		return commandLine.getOptionValue('D');
	}
}
