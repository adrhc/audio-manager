package ro.go.adrhc.audiomanager.datasources.disk.locations.index.search;

import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.Query;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import ro.go.adrhc.audiomanager.datasources.audiofiles.index.AudioMetadataFields;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.config.QueryProperties;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.SearchedAudio;
import ro.go.adrhc.persistence.lucene.core.bare.query.BooleanQueryBuilder;
import ro.go.adrhc.persistence.lucene.core.bare.token.TokenizationUtils;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import static ro.go.adrhc.lib.audiometadata.domain.DurationValidators.isValid;

@Slf4j
public class SearchedAudioWordsToQueryConverter<SA extends SearchedAudio>
		extends AbstractSearchedAudioToQueryConverter<SA>
		implements Converter<SA, Optional<Query>> {
	public SearchedAudioWordsToQueryConverter(
			TokenizationUtils tokenizationUtils, QueryProperties queryProperties,
			BiFunction<String, Collection<String>, Stream<FuzzyQuery>> fuzzyQueryFactory) {
		super(tokenizationUtils, queryProperties, fuzzyQueryFactory);
	}

	@NonNull
	@Override
	public Optional<Query> convert(@NonNull SA searchedAudio) {
		// words: try to match a specific amount of words (depends on durationIsValid)
		// words smaller than a certain size are matched exactly while the rest are matched fuzzy
		Set<String> tokenizedWords;
		try {
			tokenizedWords = tokenizationUtils.wordsToTokenSet(searchedAudio.getWords());
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			return Optional.empty();
		}

		if (tokenizedWords.isEmpty()) {
			return Optional.empty();
		}

		boolean durationIsValid = isValid(searchedAudio.getDuration());
		return Optional.of(wordsQuery(durationIsValid, tokenizedWords));
	}

	private Query wordsQuery(boolean hasValidDuration, Collection<String> tokenizedWords) {
		List<Query> queries = toQueries(AudioMetadataFields.words, tokenizedWords).toList();
		if (queries.size() == 1) {
			return queries.getFirst();
		}
		return manyWordsQuery(hasValidDuration, queries);
	}

	private Query manyWordsQuery(boolean hasValidDuration, Collection<Query> queries) {
		BooleanQueryBuilder queryBuilder = new BooleanQueryBuilder();
		queryBuilder.setMinimumNumberShouldMatch(
				queryProperties.minWordsToFind(hasValidDuration, queries.size()));
		queries.forEach(queryBuilder::shouldSatisfy);
		return queryBuilder.build();
	}
}
