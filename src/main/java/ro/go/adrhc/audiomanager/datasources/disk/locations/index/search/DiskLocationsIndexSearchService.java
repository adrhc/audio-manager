package ro.go.adrhc.audiomanager.datasources.disk.locations.index.search;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.search.Query;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.FoundAudio;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.FoundAudioFactory;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.SearchedAudio;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;
import ro.go.adrhc.persistence.lucene.operations.search.BestMatchingStrategy;
import ro.go.adrhc.persistence.lucene.operations.search.QueryAndValue;
import ro.go.adrhc.util.Assert;
import ro.go.adrhc.util.pair.Pair;
import ro.go.adrhc.util.pair.PairUtils;
import ro.go.adrhc.util.pair.PairsCollection;

import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * This is part of infrastructure layer because contains methods directly using lucene API.
 * It is a service because one could swap it with other implementation without any other change.
 */
@RequiredArgsConstructor
@Slf4j
public class DiskLocationsIndexSearchService<SA extends SearchedAudio> {
	private final SearchedToQueryConverter<SA> toQueryConverter;
	private final FoundAudioFactory<SA> foundAudioFactory;
	private final Function<Function<Query, SA>, BestAudioFileMatcher<SA>> bestMatcherFactory;
	private final FileSystemIndex<URI, AudioMetadata> amIndexRepository;

	public List<FoundAudio<SA, DiskLocation>> findMany(SA searchedAudio) throws IOException {
		Optional<Query> optionalQuery = toQueryConverter.convert(searchedAudio);
		if (optionalQuery.isEmpty()) {
			return List.of();
		}
		List<AudioMetadata> results = amIndexRepository.findMany(optionalQuery.get());
		return toFoundAudios(searchedAudio, results);
	}

	public Optional<FoundAudio<SA, DiskLocation>> findBestMatch(SA searchedAudio)
			throws IOException {
		Optional<Query> optionalQuery = toQueryConverter.convert(searchedAudio);
		if (optionalQuery.isEmpty()) {
			return Optional.empty();
		}
		return amIndexRepository
				.findBestMatch(bestMatcherFactory.apply(_ -> searchedAudio), optionalQuery.get())
				.map(result -> foundAudioFactory.create(searchedAudio, result));
	}

	public List<FoundAudio<SA, DiskLocation>> findBestMatches(Collection<SA> searchedAudios)
			throws IOException {
		PairsCollection<Query, SA> pairsCollection = toPairsCollection(searchedAudios);
		return amIndexRepository.findBestMatches(
						bestMatcher(pairsCollection), pairsCollection.getLefts())
				.stream()
				.map(qv -> toFoundAudio(pairsCollection, qv))
				.flatMap(Optional::stream)
				.toList();
	}

	private BestMatchingStrategy<AudioMetadata>
	bestMatcher(PairsCollection<Query, SA> pairsCollection) {
		return bestMatcherFactory.apply(q -> toSearchedAudio(pairsCollection, q));
	}

	private SA toSearchedAudio(PairsCollection<Query, SA> pairsCollection, Query q) {
		Optional<SA> optionalSA = pairsCollection.getRightByLeftRef(q);
		Assert.isTrue(optionalSA.isPresent(), "Query should not miss!");
		return optionalSA.get();
	}

	private Optional<FoundAudio<SA, DiskLocation>> toFoundAudio(
			PairsCollection<Query, SA> pairsCollection,
			QueryAndValue<AudioMetadata> result) {
		return pairsCollection.getRightByLeftRef(result.query())
				.map(sa -> foundAudioFactory.create(sa, result.value()));
	}

	private List<FoundAudio<SA, DiskLocation>> toFoundAudios(
			SA searchedAudio, List<AudioMetadata> searchResults) {
		return searchResults.stream()
				.map(am -> foundAudioFactory.create(searchedAudio, am))
				.toList();
	}

	private PairsCollection<Query, SA> toPairsCollection(Collection<SA> searchedAudios) {
		return PairUtils.toPairsCollection(searchedAudios.stream()
				.map(sa -> toQueryConverter.convert(sa).map(q -> new Pair<>(q, sa)))
				.flatMap(Optional::stream));
	}
}
