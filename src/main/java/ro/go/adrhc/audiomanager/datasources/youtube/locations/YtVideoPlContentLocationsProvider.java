package ro.go.adrhc.audiomanager.datasources.youtube.locations;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ro.go.adrhc.audiomanager.datasources.youtube.config.YouTubeProperties;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory;
import ro.go.adrhc.audiomanager.lib.paginateddata.AbstractPaginatedDataReader;
import ro.go.adrhc.audiomanager.lib.paginateddata.DataPage;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationPredicates.isYtVideoPlaylist;
import static ro.go.adrhc.util.collection.CollectionUtils.firstValue;

/**
 * Provider for the locations included/referenced by a YouTube playlist.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class YtVideoPlContentLocationsProvider extends AbstractPaginatedDataReader<String, String> {
	private static final ThreadLocal<YouTubeLocation> params = new ThreadLocal<>();
	private final GoogleApiPlItems googleApiPlItems;
	private final YouTubeProperties ytProperties;

	/**
	 * @return YouTubeId VIDEO in the playlist identified by playlistId
	 */
	public List<YouTubeLocation> getPlContent(YouTubeLocation ytPlLocation) {
		Assert.isTrue(isYtVideoPlaylist(ytPlLocation),
				"Can't process other than VIDEO_PLAYLIST!");
		params.set(ytPlLocation);
		return getData()
				.map(YouTubeLocationFactory::ytVideoLocation)
				.collect(Collectors.toList());
	}

	protected DataPage<String, String> doGetDataPage(DataPage<String, String> previousChunk) {
		String ytPlaylistJson = ytDataApiPlaylistItemsJson(nextDataPageToken(previousChunk));
		DocumentContext context = JsonPath.parse(ytPlaylistJson);
		return new DataPage<>(ytIds(context), nextPageToken(context));
	}

	private String nextPageToken(DocumentContext context) {
		return firstValue(context.read("$.[?(@.nextPageToken)].nextPageToken"));
	}

	private Set<String> ytIds(DocumentContext context) {
		JSONArray nodes = context.read("$.items[*].contentDetails.videoId");
		return nodes.stream().map(Object::toString).collect(Collectors.toSet());
	}

	private String ytDataApiPlaylistItemsJson(String nextChunkToken) {
		return googleApiPlItems.list(ytProperties.getMaxResults(),
				params.get().code(), nextChunkToken);
	}
}
