package ro.go.adrhc.audiomanager.datasources.youtube.metadata;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationType;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YtLocationAccessors;
import ro.go.adrhc.audiomanager.datasources.youtube.locations.YtVideoPlContentLocationsProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.YtLikedMusicMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.YtMusicPlContentMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.YtMusicPlMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.YtVideoPlMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl.YtDlSongsAndVideosMetadataProvider;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import static java.util.concurrent.CompletableFuture.supplyAsync;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationPredicates.isYtMusicPlaylist;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationPredicates.isYtVideoPlaylist;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YtMusicPlLocation.isLikedMusic;
import static ro.go.adrhc.util.concurrency.FutureUtils.safelyGetAll;

/**
 * This is a mere Facade for more specialized (YouTube) metadata providers.
 * <p>
 * The YouTube metadata providers are organized by their technology:
 * - Shazam (video metadata, uses Search Google API)
 * - YouTube-Music liked playlist (music metadata)
 * - youtube-dl (music & video metadata)
 * - Google API:
 * -- Search (video metadata)
 * -- Videos (music & video metadata, it's an alternative to youtube-dl)
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class YtMetadataRepository {
	private final ExecutorService cachedThreadPool;
	private final YtVideoPlContentLocationsProvider ytVideoPlContentLocationsProvider;
	private final YtMusicPlContentMetadataProvider ytMusicPlContentMetadataProvider;
	private final YtDlSongsAndVideosMetadataProvider ytDlSongsAndVideosMetadataProvider;
	private final YtVideoPlMetadataProvider ytVideoPlMetadataProvider;
	private final YtMusicPlMetadataProvider ytMusicPlMetadataProvider;
	private final YtLikedMusicMetadataProvider ytLikedMusicMetadataProvider;

	public YouTubeMetadata loadFromLocation(YouTubeLocation ytLocation) {
		return switch (ytLocation.ytType()) {
			case MUSIC_PLAYLIST -> ytMusicPlMetadataProvider.loadMetadata(ytLocation);
			case VIDEO_PLAYLIST -> ytVideoPlMetadataProvider.loadMetadata(ytLocation);
			case MUSIC, VIDEO -> ytDlSongsAndVideosMetadataProvider.loadMetadata(ytLocation);
//			case MUSIC, VIDEO -> ytGASongsAndVideosMetadataProvider.loadMetadata(ytLocation);
		};
	}

	public YtLocationMetadataPairs loadFromPlaylist(YouTubeLocation ytPlLocation) {
		if (isLikedMusic(ytPlLocation)) {
			return YtLocationMetadataPairs.of(ytLikedMusicMetadataProvider.loadMetadata());
		} else if (isYtVideoPlaylist(ytPlLocation)) {
			return ytDlSongsAndVideosMetadataProvider.loadMetadata(
					ytVideoPlContentLocationsProvider.getPlContent(ytPlLocation)
			);
		} else if (isYtMusicPlaylist(ytPlLocation)) {
			return YtLocationMetadataPairs.of(
					ytMusicPlContentMetadataProvider.getPlContent(ytPlLocation)
			);
		} else {
			throw new UnsupportedOperationException(ytPlLocation + " is not a playlist!");
		}
	}

	public YtLocationMetadataPairs loadFromLocations(Collection<YouTubeLocation> ytLocations) {
		Map<YouTubeLocationType, Set<YouTubeLocation>> ytLocationsByType =
				YtLocationAccessors.ytLocationsGroupedByType(ytLocations);
		return safelyGetAll(ytLocationsByType.keySet().stream()
				.map(type -> asyncLoadFromLocations(ytLocationsByType, type)))
				.collect(YtLocationMetadataPairs::empty,
						YtLocationMetadataPairs::appendDiffs,
						YtLocationMetadataPairs::appendDiffs);
	}

	private CompletableFuture<YtLocationMetadataPairs> asyncLoadFromLocations(
			Map<YouTubeLocationType, Set<YouTubeLocation>> ytLocationsByType,
			YouTubeLocationType ytLocationType) {
		return supplyAsync(() -> loadFromLocations(ytLocationType,
				ytLocationsByType.get(ytLocationType)), cachedThreadPool);
	}

	private YtLocationMetadataPairs loadFromLocations(
			YouTubeLocationType type, Collection<YouTubeLocation> ytLocations) {
		return switch (type) {
			case VIDEO_PLAYLIST, MUSIC_PLAYLIST ->
					ytVideoPlMetadataProvider.loadMetadata(ytLocations);
			case MUSIC, VIDEO -> ytDlSongsAndVideosMetadataProvider.loadMetadata(ytLocations);
//			case MUSIC, VIDEO -> ytGASongsAndVideosMetadataProvider.loadMetadata(ytLocations);
		};
	}
}
