package ro.go.adrhc.audiomanager.datasources.audiofiles.index;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import ro.go.adrhc.audiomanager.domain.location.LocationType;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.core.bare.field.FieldType;
import ro.go.adrhc.persistence.lucene.core.typed.field.LuceneFieldSpec;
import ro.go.adrhc.persistence.lucene.core.typed.field.ObjectLuceneFieldMapper;

import java.util.function.Function;

import static ro.go.adrhc.persistence.lucene.core.bare.field.FieldType.*;
import static ro.go.adrhc.persistence.lucene.core.typed.field.ObjectLuceneFieldMapper.*;

@Getter
@Accessors(fluent = true)
@RequiredArgsConstructor
public enum AudioMetadataFields implements LuceneFieldSpec<AudioMetadata> {
	uri(KEYWORD, uriField(AudioMetadata::getId), true),
	fileNameNoExt(PHRASE, AudioMetadata::fileNameNoExt),
	words(PHRASE, AudioMetadata::getWords),
	duration(PHRASE, AudioMetadata::getDurationsAsString),
	tags(TAGS, tagsField(AudioMetadata::tags)),
	updateMoment(LONG, instantField(AudioMetadata::updateMoment)),
	locationType(KEYWORD, enumField(LocationType.class, AudioMetadata::locationType));

	private final FieldType fieldType;

	private final ObjectLuceneFieldMapper<AudioMetadata, ?> fieldSerde;

	private final boolean isIdField;

	AudioMetadataFields(FieldType fieldType, Function<AudioMetadata, String> typedAccessor) {
		this(fieldType, stringField(typedAccessor));
	}

	AudioMetadataFields(FieldType fieldType, ObjectLuceneFieldMapper<AudioMetadata, ?> fieldSerde) {
		this.fieldType = fieldType;
		this.isIdField = false;
		this.fieldSerde = fieldSerde;
	}
}
