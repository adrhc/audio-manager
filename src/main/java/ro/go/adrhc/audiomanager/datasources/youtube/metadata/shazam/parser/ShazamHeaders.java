package ro.go.adrhc.audiomanager.datasources.youtube.metadata.shazam.parser;

public enum ShazamHeaders {
	Index, TagTime, Title, Artist, URL, TrackKey
}
