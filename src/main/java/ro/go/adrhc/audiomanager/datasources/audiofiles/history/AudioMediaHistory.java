package ro.go.adrhc.audiomanager.datasources.audiofiles.history;

import org.apache.lucene.search.ScoreDoc;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;

import java.io.IOException;

public interface AudioMediaHistory {
	HistoryPage<AudioMetadata> getFirst() throws IOException;

	HistoryPage<AudioMetadata> getPrevious(ScoreDoc before) throws IOException;

	HistoryPage<AudioMetadata> getNext(ScoreDoc after) throws IOException;
}
