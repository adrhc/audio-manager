package ro.go.adrhc.audiomanager.datasources.disk;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.DiskLocationsRepository;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.SongQueryLocationPairs;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.domain.playlist.entries.EntriesPairs;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntriesFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;

import java.io.IOException;
import java.util.List;

/**
 * A repository of DISK PlaylistEntry(s).
 */
@RequiredArgsConstructor
@Component
@Slf4j
public class DiskPlEntriesRepository {
	private final DiskLocationsRepository<PlEntrySongQuery> diskLocationsRepository;

	public PlaylistEntries findMany(SongQuery songQuery) throws IOException {
		return PlEntriesFactory.ofDiskLocations(diskLocationsRepository.findMany(songQuery));
	}

	/**
	 * @return only those having an on-disk present match
	 */
	public EntriesPairs findBestMatches(PlaylistEntries searchedPlEntries) throws IOException {
		SongQueryLocationPairs<PlEntrySongQuery, DiskLocation> songQueryLocationPairs =
				diskLocationsRepository.findBestMatches(entrySongQueries(searchedPlEntries))
						.filter(pair -> diskLocationsRepository.isValid(pair.right()));
		return EntriesPairs.of(songQueryLocationPairs);
	}

	public PlaylistEntries loadContent(DiskLocation dirLocation) throws IOException {
		return PlEntriesFactory.of(diskLocationsRepository.getContent(dirLocation));
	}

	private static List<PlEntrySongQuery> entrySongQueries(PlaylistEntries searchedPlEntries) {
		return searchedPlEntries.map(PlEntrySongQuery::of).toList();
	}
}
