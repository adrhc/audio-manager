package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsearch;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;

import java.util.List;

@Component
public class GoogleApiSearchResultParser {
	public List<YouTubeMetadata> parse(String json) {
		JSONArray nodes = JsonPath.read(json, "$.items");
		if (nodes.isEmpty()) {
			return List.of();
		}
		return nodes.stream().map(GoogleApiSearchResultParser::parseOne).toList();
	}

	private static YouTubeMetadata parseOne(Object item) {
		DocumentContext context = JsonPath.parse(item);
		String videoId = context.read("id.videoId");
		String title = context.read("snippet.title");
		return YouTubeMetadata.of(YouTubeLocationFactory.ytVideoLocation(videoId), title);
	}
}
