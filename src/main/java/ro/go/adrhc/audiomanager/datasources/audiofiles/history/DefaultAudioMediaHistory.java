package ro.go.adrhc.audiomanager.datasources.audiofiles.history;

import lombok.RequiredArgsConstructor;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.ScoreDoc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.audiofiles.index.IndexRepositoryHolder;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;
import ro.go.adrhc.persistence.lucene.operations.search.ScoreDocAndValues;

import java.io.IOException;
import java.net.URI;

import static ro.go.adrhc.audiomanager.datasources.audiofiles.index.AMQueryFactory.SORT_BY_UPDATE_MOMENT;
import static ro.go.adrhc.audiomanager.datasources.audiofiles.index.AMQueryFactory.SORT_REVERSE_BY_UPDATE_MOMENT;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DefaultAudioMediaHistory implements AudioMediaHistory {
	private final IndexRepositoryHolder holder;
	@Value("${history.page-size}")
	private final int pageSize;

	@Override
	public HistoryPage<AudioMetadata> getFirst() throws IOException {
		return getNext(null);
	}

	@Override
	public HistoryPage<AudioMetadata> getPrevious(ScoreDoc before) throws IOException {
		ScoreDocAndValues<AudioMetadata> page = indexRepository().findManyAfter(before,
				new MatchAllDocsQuery(), pageSize + 1, SORT_BY_UPDATE_MOMENT).reverse();
		if (page.size() > pageSize) {
			return new HistoryPage<>(page.removeFirst(), true, true);
		} else {
			return new HistoryPage<>(page, false, true);
		}
	}

	@Override
	public HistoryPage<AudioMetadata> getNext(ScoreDoc after) throws IOException {
		ScoreDocAndValues<AudioMetadata> page = indexRepository().findManyAfter(after,
				new MatchAllDocsQuery(), pageSize + 1, SORT_REVERSE_BY_UPDATE_MOMENT);
		if (page.size() > pageSize) {
			return new HistoryPage<>(page.removeLast(), after != null, true);
		} else {
			return new HistoryPage<>(page, after != null, false);
		}
	}

	private FileSystemIndex<URI, AudioMetadata> indexRepository() {
		return holder.getIndexRepository();
	}
}
