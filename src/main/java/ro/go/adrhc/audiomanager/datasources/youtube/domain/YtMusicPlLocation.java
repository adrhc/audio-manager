package ro.go.adrhc.audiomanager.datasources.youtube.domain;

import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationType.MUSIC_PLAYLIST;

public class YtMusicPlLocation extends YouTubeLocation {
	public static final YtMusicPlLocation YT_LIKES_MUSIC =
			new YtMusicPlLocation(YT_LIKES_MUSIC_CODE);

	public YtMusicPlLocation(String code) {
		super(MUSIC_PLAYLIST, code);
	}

	public static boolean isLikedMusic(YouTubeLocation ytPlLocation) {
		return ytPlLocation.equals(YtMusicPlLocation.YT_LIKES_MUSIC);
	}

	public static YtMusicPlLocation of(String ytCode) {
		return new YtMusicPlLocation(ytCode);
	}
}
