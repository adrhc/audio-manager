package ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;

@RequiredArgsConstructor
@Component
public class AudioMetadataToYtLocationConverter
		implements Converter<AudioMetadata, YouTubeLocation> {
	private final YouTubeLocationFactory youTubeLocationFactory;

	@Override
	@NonNull
	public YouTubeLocation convert(@NonNull AudioMetadata metadata) {
		return youTubeLocationFactory.parse(metadata.uri().toString()).orElseThrow();
	}
}
