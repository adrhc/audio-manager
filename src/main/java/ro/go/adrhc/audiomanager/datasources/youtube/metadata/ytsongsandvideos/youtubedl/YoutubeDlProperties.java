package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.time.Duration;

@Component
@ConfigurationProperties(prefix = "youtube-dl")
@Setter
@Getter
@ToString
public class YoutubeDlProperties {
	private Duration metadataExtractPerVideoTimeout;
	private Path youtubeCookiesPath;
}
