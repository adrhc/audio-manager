package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import org.springframework.lang.NonNull;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiomanager.util.PlaylistUtils.sanitizeSongTitle;
import static ro.go.adrhc.util.text.StringUtils.concat;

@RequiredArgsConstructor
@Slf4j
public class YtVideoSearchResultParser {
	//	private static final String SONGS_PATH1 = "$..runs[*][?(@.text empty false && @.navigationEndpoint.watchEndpoint.videoId empty false)]['text','navigationEndpoint']";
	private static final String SONGS_PATH = "$..videoRenderer[?(@.videoId empty false && @.title.runs empty false)]['videoId','title','thumbnail']";
	private final DocumentContext context;

	@NonNull
	public List<YouTubeMetadata> extractYouTubeMetadata() {
		JSONArray nodes = context.read(SONGS_PATH);
		return nodes.stream().map(this::youTubeMetadata)
				.flatMap(Optional::stream)
				.toList();
	}

	/**
	 * Outside (i.e. YouTube) text must be sanitized (i.e. sanitizeSongTitle(...)).
	 * see YtTextDetailsToTitleConverter for sanitization
	 */
	private Optional<YouTubeMetadata> youTubeMetadata(Object item) {
		DocumentContext itemContext = JsonPath.parse(item);
		String videoId = itemContext.read("videoId");
		String title = parseTitle(videoId, itemContext);
		if (!hasText(title) || !hasText(videoId)) {
			return Optional.empty();
		}
		YouTubeLocation ytVideoLocation = YouTubeLocationFactory.ytVideoLocation(videoId);
		return Optional.of(
				YouTubeMetadata.of(ytVideoLocation, title, parseThumbnails(itemContext)));
	}

	/**
	 * Outside (i.e. YouTube) text must be sanitized (i.e. sanitizeSongTitle(...)).
	 * see YtTextDetailsToTitleConverter for sanitization
	 */
	private Optional<YouTubeMetadata> youTubeMetadata1(Object item) {
		DocumentContext itemContext = JsonPath.parse(item);
		String videoId = itemContext.read("navigationEndpoint.watchEndpoint.videoId");
		String title = parseTitle(videoId, itemContext);
		if (!hasText(title) || !hasText(videoId)) {
			return Optional.empty();
		}
		YouTubeLocation ytVideoLocation = YouTubeLocationFactory.ytVideoLocation(videoId);
		return Optional.of(
				YouTubeMetadata.of(ytVideoLocation, title, parseThumbnails(itemContext)));
	}

	private static String parseTitle(String videoId, DocumentContext itemContext) {
//		String title = sanitizeSongTitle(context.read("text"));
		String title = sanitizeSongTitle(itemContext.read("title.runs[0].text"));
		Integer titlesCount = itemContext.read("title.runs.length()", Integer.class);
		if (titlesCount > 1) {
			log.warn("\nFound {} titles for {}!\n{}", titlesCount, videoId,
					concat(itemContext.read("title.runs[*].text", List.class)));
		}
		return title;
	}

	private static List<Map<String, Object>> parseThumbnails(DocumentContext context) {
		try {
			List<Map<String, Object>> thumbnails =
					context.read("thumbnail.thumbnails", List.class);
			return thumbnails == null ? List.of() : thumbnails;
		} catch (PathNotFoundException e) {
			// do nothing
		}
		return List.of();
	}
}
