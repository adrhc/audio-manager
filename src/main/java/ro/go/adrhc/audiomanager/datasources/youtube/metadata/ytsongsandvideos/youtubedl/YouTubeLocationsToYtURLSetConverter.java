package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;

import java.util.Collection;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class YouTubeLocationsToYtURLSetConverter
		implements Converter<Collection<YouTubeLocation>, Set<String>> {
	private final Function<YouTubeLocation, String> YouTubeLocationToURLConverter;

	@Override
	@NonNull
	public Set<String> convert(@NonNull Collection<YouTubeLocation> ytLocations) {
		return ytLocations.stream().map(YouTubeLocationToURLConverter).collect(Collectors.toSet());
	}
}
