package ro.go.adrhc.audiomanager.datasources.youtube.domain;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import ro.go.adrhc.util.text.MessageFormats;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

@Component
@ConfigurationProperties(prefix = "youtube-uri")
public class YouTubeLocationPatterns {
	private MessageFormat videoPlUrnPattern;
	private MessageFormat musicPlUrnPattern;
	/**
	 * <a href="https://github.com/natumbri/mopidy-youtube">...</a>
	 * <p>
	 * [yt|youtube]:<url to youtube video>
	 * [yt|youtube]:video:<id>
	 */
	private MessageFormat videoUrnPattern;
	private MessageFormat musicUrnPattern;
	private MessageFormats singleMediaUriPatterns;

	public void setSingleMediaUriPatterns(Collection<String> youtubeSingleMediaUriPatterns) {
		this.singleMediaUriPatterns = MessageFormats.of(youtubeSingleMediaUriPatterns);
	}

	public void setVideoUrnPattern(String urnPattern) {
		this.videoUrnPattern = new MessageFormat(urnPattern);
	}

	public void setMusicUrnPattern(String urnPattern) {
		this.musicUrnPattern = new MessageFormat(urnPattern);
	}

	public void setVideoPlUrnPattern(String urnPattern) {
		this.videoPlUrnPattern = new MessageFormat(urnPattern);
	}

	public void setMusicPlUrnPattern(String urnPattern) {
		this.musicPlUrnPattern = new MessageFormat(urnPattern);
	}

	public Optional<String[]> parseYtLocationCode(String text) {
		return singleMediaUriPatterns.parse(text)
				.map(objects -> Arrays.copyOf(objects, objects.length, String[].class));
	}

	public String ytUriOf(YouTubeLocation ytLocation) {
		return switch (ytLocation.ytType()) {
			case VIDEO -> toYtVideoUrn(ytLocation.code());
			case MUSIC -> toYtMusicUrn(ytLocation.code());
			case VIDEO_PLAYLIST -> toYtVideoPlUrn(ytLocation.code());
			case MUSIC_PLAYLIST -> toYtMusicPlUrn(ytLocation.code());
		};
	}

	@Override
	public String toString() {
		return "videoPlUrnPattern: " + videoPlUrnPattern +
				"\nmusicPlUrnPattern: " + musicPlUrnPattern +
				"\nvideoUrnPattern: " + videoUrnPattern +
				"\nmusicUrnPattern: " + musicUrnPattern +
				"\nsingleMediaUriPatterns:\n" + singleMediaUriPatterns;
	}

	private String toYtVideoPlUrn(String ytVideoPlCode) {
		return videoPlUrnPattern.format(new String[]{ytVideoPlCode});
	}

	private String toYtMusicPlUrn(String ytMusicPlCode) {
		return musicPlUrnPattern.format(new String[]{ytMusicPlCode});
	}

	private String toYtVideoUrn(String youTubeCode) {
		return videoUrnPattern.format(new String[]{youTubeCode});
	}

	private String toYtMusicUrn(String youTubeCode) {
		return musicUrnPattern.format(new String[]{youTubeCode});
	}
}
