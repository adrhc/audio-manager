package ro.go.adrhc.audiomanager.datasources.audiofiles.index;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.audiomanager.config.lucene.AMIndexProperties;
import ro.go.adrhc.audiomanager.domain.location.LocationType;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.core.typed.read.ScoreDocAndValue;
import ro.go.adrhc.persistence.lucene.operations.search.SearchResultFilter;
import ro.go.adrhc.util.io.FileSystemUtils;

import static ro.go.adrhc.lib.audiometadata.domain.AudioMetadataAccessors.path;

@RequiredArgsConstructor
@Slf4j
public class AMDiskSearchResultFilter implements SearchResultFilter<AudioMetadata> {
	private final FileSystemUtils fsUtils;
	private final boolean includeFindingsWithMissingFile;

	public static AMDiskSearchResultFilter create(
			FileSystemUtils fsUtils, AMIndexProperties amIndexProperties) {
		return new AMDiskSearchResultFilter(fsUtils,
				amIndexProperties.getSearch().isResultIncludesMissingFiles());
	}

	@Override
	public boolean filter(ScoreDocAndValue<AudioMetadata> foundAudio) {
		if (includeFindingsWithMissingFile) {
			return true;
		}
		AudioMetadata metadata = foundAudio.value();
		if (metadata.locationType() == LocationType.DISK) {
			return path(metadata).map(fsUtils::exists).orElse(false);
		} else {
			log.warn("\nSkipping {} found song file (only accept DISK):\n{}, {}",
					metadata.locationType(), metadata.fileNameNoExt(), metadata.uri());
			return false;
		}
	}
}
