package ro.go.adrhc.audiomanager.datasources.youtube.domain;

import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationType.VIDEO_PLAYLIST;

public class YtVideoPlLocation extends YouTubeLocation {
	public YtVideoPlLocation(String code) {
		super(VIDEO_PLAYLIST, code);
	}

	public static YtVideoPlLocation of(String ytCode) {
		return new YtVideoPlLocation(ytCode);
	}
}
