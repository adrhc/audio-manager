package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

/**
 * https://github.com/ytdl-org/youtube-dl/issues/28602
 * youtube-dl --add-header 'Cookie:' --verbose --dump-json http://music.youtube.com/watch?v=oDLU3e34G1o
 * youtube-dl --cookies $HOME/Projects-adrhc/audio-manager/m3u8-playlists-manager/config/youtube.com_cookies.txt --verbose --dump-json http://music.youtube.com/watch?v=SAsJ_n747T4
 * yt-dlp --dump-json http://music.youtube.com/watch?v=oDLU3e34G1o
 */
@Component
@RequiredArgsConstructor
public class JsonToYoutubeDlRecordConverter implements Converter<String, YoutubeDlRecord> {
	private final ObjectMapper mapper;

	@SneakyThrows
	@Override
	public YoutubeDlRecord convert(@NonNull String json) {
		return mapper.readValue(json, YoutubeDlRecord.class);
	}
}
