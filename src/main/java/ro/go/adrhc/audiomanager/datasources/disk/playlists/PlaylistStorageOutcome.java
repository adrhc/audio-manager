package ro.go.adrhc.audiomanager.datasources.disk.playlists;

import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;

public record PlaylistStorageOutcome(boolean stored, int entryCount, DiskLocation location) {
	@Override
	public String toString() {
		if (stored) {
			return "Stored %d entries to %s!".formatted(entryCount, location);
		} else {
			return "Won't store %s because would be empty!".formatted(location);
		}
	}
}
