package ro.go.adrhc.audiomanager.datasources.audiofiles.metadata;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.operations.restore.IndexDataSource;
import ro.go.adrhc.util.io.FilesMetadataLoader;
import ro.go.adrhc.util.io.SimpleDirectory;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.util.Optional;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
@Slf4j
public class AudioMetadataProvider implements IndexDataSource<URI, AudioMetadata> {
	private final SimpleDirectory audioFilesDirectory;
	private final FilesMetadataLoader<Optional<AudioMetadata>> filesMetadataLoader;

	@Override
	public Stream<URI> loadAllIds() throws IOException {
		return audioFilesDirectory.getAllPaths().stream().map(Path::toUri);
	}

	@Override
	public Stream<AudioMetadata> loadByIds(Stream<URI> uriStream) {
		return filesMetadataLoader.loadByPaths(uriStream.map(Path::of)).flatMap(Optional::stream);
	}

	@Override
	public Stream<AudioMetadata> loadAll() {
		return filesMetadataLoader.loadAll().flatMap(Optional::stream);
	}
}
