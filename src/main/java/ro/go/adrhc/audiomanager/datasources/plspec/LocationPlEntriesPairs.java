package ro.go.adrhc.audiomanager.datasources.plspec;

import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpec;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.util.pair.Pair;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

public record LocationPlEntriesPairs(Map<Location, PlaylistEntries> entriesByLocation) {
	public static LocationPlEntriesPairs of(
			Stream<Pair<Location, PlaylistEntries>> entriesPairStream) {
		return entriesPairStream.collect(LocationPlEntriesPairs::empty,
				LocationPlEntriesPairs::put, LocationPlEntriesPairs::putAll);
	}

	public static LocationPlEntriesPairs empty() {
		return new LocationPlEntriesPairs(new LinkedHashMap<>());
	}

	public void put(Location location, PlaylistEntries entries) {
		entriesByLocation.put(location, entries);
	}

	public void put(Pair<Location, PlaylistEntries> pair) {
		entriesByLocation.put(pair.left(), pair.right());
	}

	public void putAll(LocationPlEntriesPairs locationsEntries) {
		entriesByLocation.putAll(locationsEntries.entriesByLocation);
	}

	public LocationPlEntriesPairs filter(
			Predicate<? super Pair<Location, PlaylistEntries>> predicate) {
		return entriesByLocation.entrySet().stream()
				.map(Pair::ofMapEntry)
				.filter(predicate)
				.collect(LocationPlEntriesPairs::empty,
						LocationPlEntriesPairs::put,
						LocationPlEntriesPairs::putAll);
	}

	public List<PlaylistEntries> plEntries(PlaylistSpec plSpec) {
		return this.filter(pair -> plSpec.contains(pair.left())).plEntries();
	}

	public List<PlaylistEntries> plEntries() {
		return entriesByLocation.values().stream().toList();
	}
}
