package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.ytdataapi;

import com.jayway.jsonpath.JsonPath;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.LogUtils;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.YtMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.YtSongsAndVideosMetadataProviderUtils;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationPredicates.hasOnlySongOrVideoLocationTypes;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YtLocationAccessors.ytCodes;
import static ro.go.adrhc.util.collection.CollectionUtils.partition;
import static ro.go.adrhc.util.concurrency.FutureUtils.safelyGetAll;

@Service
@RequiredArgsConstructor
@Slf4j
public class YtGASongsAndVideosMetadataProvider implements YtMetadataProvider {
	private static final int PARTITION_CAPACITY = 50;
	private final ExecutorService cachedThreadPool;
	private final LogUtils logUtils;
	private final GoogleApiVideos googleApiVideos;

	public YtLocationMetadataPairs loadMetadata(Collection<YouTubeLocation> ytLocations) {
		YtSongsAndVideosMetadataProviderUtils.assertHasSongOrVideoOnly(
				hasOnlySongOrVideoLocationTypes(ytLocations));
		Map<String, String> ytCodeTitlePairs = loadYtCodeTitlePairs(ytCodes(ytLocations));
		YtLocationMetadataPairs pairs = createYtLocationMetadataPairs(ytLocations,
				ytCodeTitlePairs);
		logUtils.logNotFound(ytLocations, pairs);
		return pairs;
	}

	private YtLocationMetadataPairs createYtLocationMetadataPairs(
			Collection<YouTubeLocation> ytLocations, Map<String, String> ytCodeTitlePairs
	) {
		return YtLocationMetadataPairs.of(ytLocations.stream()
				.filter(ytl -> ytCodeTitlePairs.containsKey(ytl.code()))
				.map(ytl -> YouTubeMetadata.of(ytl, ytCodeTitlePairs.get(ytl.code()))));
	}

	private Map<String, String> loadYtCodeTitlePairs(Set<String> ytCodes) {
		return loadRawMetadata(ytCodes)
				.map(json -> JsonPath.<JSONArray>read(json, "$.items"))
				.flatMap(Collection::stream)
				.map(this::parseYtCodeAndTitle)
				.collect(HashMap::new, (acc, yct) -> acc.put(yct.ytCode(), yct.title()),
						Map::putAll);
	}

	private YtCodeAndTitle parseYtCodeAndTitle(Object item) {
		String ytCode = JsonPath.read(item, "$.id");
		String title = JsonPath.read(item, "$.snippet.title");
		return new YtCodeAndTitle(ytCode, title);
	}

	private Stream<String> loadRawMetadata(Set<String> ytCodes) {
		Stream<CompletableFuture<String>> futures =
				partition(HashSet::new, PARTITION_CAPACITY, ytCodes)
						.stream().map(this::asyncLoadRawMetadata);
		return safelyGetAll(futures);
	}

	private CompletableFuture<String> asyncLoadRawMetadata(Collection<String> ytCodes) {
		return supplyAsync(() -> googleApiVideos.list(ytCodes), cachedThreadPool);
	}

	private record YtCodeAndTitle(String ytCode, String title) {
	}
}
