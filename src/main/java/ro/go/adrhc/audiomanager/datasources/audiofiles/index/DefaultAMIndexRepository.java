package ro.go.adrhc.audiomanager.datasources.audiofiles.index;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.springframework.stereotype.Component;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;
import ro.go.adrhc.persistence.lucene.core.typed.field.LuceneFieldSpec;
import ro.go.adrhc.persistence.lucene.operations.params.IndexServicesParamsFactory;
import ro.go.adrhc.persistence.lucene.operations.restore.IndexDataSource;
import ro.go.adrhc.persistence.lucene.operations.search.BestMatchingStrategy;
import ro.go.adrhc.persistence.lucene.operations.search.QueryAndValue;
import ro.go.adrhc.persistence.lucene.operations.search.ScoreDocAndValues;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
@Slf4j
public class DefaultAMIndexRepository implements
		FileSystemIndex<URI, AudioMetadata> {
	private final IndexRepositoryHolder holder;

	@Override
	public Path getIndexPath() {
		return indexRepository().getIndexPath();
	}

	@Override
	public IndexServicesParamsFactory<AudioMetadata> getIndexServicesParamsFactory() {
		return indexRepository().getIndexServicesParamsFactory();
	}

	@Override
	public void addMany(Collection<AudioMetadata> audioFileMetadata) throws IOException {
		indexRepository().addMany(audioFileMetadata);
	}

	@Override
	public void addMany(Stream<AudioMetadata> audioFileMetadataStream) throws IOException {
		indexRepository().addMany(audioFileMetadataStream);
	}

	@Override
	public void addOne(AudioMetadata metadata) throws IOException {
		indexRepository().addOne(metadata);
	}

	@Override
	public void removeById(URI uri) throws IOException {
		indexRepository().removeById(uri);
	}

	@Override
	public void removeByIds(Collection<URI> uris) throws IOException {
		indexRepository().removeByIds(uris);
	}

	@Override
	public void removeByQuery(Query query) throws IOException {
		indexRepository().removeByQuery(query);
	}

	@Override
	public void removeAll() throws IOException {
		indexRepository().removeAll();
	}

	@Override
	public void reset(Iterable<AudioMetadata> audioFileMetadata) throws IOException {
		indexRepository().reset(audioFileMetadata);
	}

	@Override
	public void reset(Stream<AudioMetadata> audioFileMetadataStream) throws IOException {
		indexRepository().reset(audioFileMetadataStream);
	}

	@Override
	public void shallowUpdate(IndexDataSource<URI, AudioMetadata> dataSource)
			throws IOException {
		indexRepository().shallowUpdate(dataSource);
	}

	@Override
	public void shallowUpdateSubset(
			IndexDataSource<URI, AudioMetadata> dataSource, Query query)
			throws IOException {
		indexRepository().shallowUpdateSubset(dataSource, query);
	}

	@Override
	public void upsert(AudioMetadata metadata) throws IOException {
		indexRepository().upsert(metadata);
	}

	@Override
	public void upsertMany(Collection<AudioMetadata> metadata) throws IOException {
		indexRepository().upsertMany(metadata);
	}

	@Override
	public void merge(AudioMetadata metadata) throws IOException {
		indexRepository().merge(metadata);
	}

	@Override
	public void merge(AudioMetadata metadata,
			BinaryOperator<AudioMetadata> mergeStrategy) throws IOException {
		indexRepository().merge(metadata);
	}

	@Override
	public void mergeMany(Collection<AudioMetadata> metadata,
			BinaryOperator<AudioMetadata> mergeStrategy) throws IOException {
		indexRepository().mergeMany(metadata, mergeStrategy);
	}

	@Override
	public boolean isEmpty() throws IOException {
		return indexRepository().isEmpty();
	}

	@Override
	public int count() throws IOException {
		return indexRepository().count();
	}

	@Override
	public int count(Query query) throws IOException {
		return indexRepository().count(query);
	}

	@Override
	public ScoreDocAndValues<AudioMetadata> findMany(
			Query query, int hitsCount, Sort sort) throws IOException {
		return indexRepository().findMany(query, hitsCount, sort);
	}

	@Override
	public ScoreDocAndValues<AudioMetadata> findMany(
			Query query, Sort sort) throws IOException {
		return indexRepository().findMany(query, sort);
	}

	@Override
	public ScoreDocAndValues<AudioMetadata> findMany(
			Query query, int hitsCount) throws IOException {
		return indexRepository().findMany(query, hitsCount);
	}

	@Override
	public List<AudioMetadata> findMany(Query query) throws IOException {
		return indexRepository().findMany(query);
	}

	@Override
	public ScoreDocAndValues<AudioMetadata> findManyAfter(ScoreDoc after,
			Query query, int hitsCount, Sort sort) throws IOException {
		return indexRepository().findManyAfter(after, query, hitsCount, sort);
	}

	@Override
	public ScoreDocAndValues<AudioMetadata> findManyAfter(ScoreDoc after,
			Query query, Sort sort) throws IOException {
		return indexRepository().findManyAfter(after, query, sort);
	}

	@Override
	public Optional<AudioMetadata> findBestMatch(
			BestMatchingStrategy<AudioMetadata> bestMatchingStrategy, Query query)
			throws IOException {
		return indexRepository().findBestMatch(bestMatchingStrategy, query);
	}

	@Override
	public Optional<AudioMetadata> findBestMatch(Query query) throws IOException {
		return indexRepository().findBestMatch(query);
	}

	@Override
	public List<QueryAndValue<AudioMetadata>> findBestMatches(
			BestMatchingStrategy<AudioMetadata> bestMatchingStrategy,
			Collection<? extends Query> queries)
			throws IOException {
		return indexRepository().findBestMatches(bestMatchingStrategy, queries);
	}

	@Override
	public List<QueryAndValue<AudioMetadata>> findBestMatches(
			Collection<? extends Query> queries)
			throws IOException {
		return indexRepository().findBestMatches(queries);
	}

	@Override
	public Optional<AudioMetadata> findById(URI uri) throws IOException {
		return indexRepository().findById(uri);
	}

	@Override
	public Set<AudioMetadata> findByIds(Set<URI> uris) throws IOException {
		return indexRepository().findByIds(uris);
	}

	@Override
	public List<AudioMetadata> getAll() throws IOException {
		return indexRepository().getAll();
	}

	@Override
	public List<URI> getAllIds() throws IOException {
		return indexRepository().getAllIds();
	}

	@Override
	public <F> List<F> getFieldOfAll(LuceneFieldSpec<AudioMetadata> field) throws IOException {
		return indexRepository().getFieldOfAll(field);
	}

	@Override
	public <R> R reduce(Function<Stream<AudioMetadata>, R> reducer) throws IOException {
		return indexRepository().reduce(reducer);
	}

	@Override
	public <R> R reduceIds(Function<Stream<URI>, R> idsReducer) throws IOException {
		return indexRepository().reduceIds(idsReducer);
	}

	@Override
	public void close() throws IOException {
		// Don't use indexRepository().close()!!! see holder.close() implementation.
		holder.close();
	}

	private FileSystemIndex<URI, AudioMetadata> indexRepository() {
		return holder.getIndexRepository();
	}
}
