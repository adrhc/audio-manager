package ro.go.adrhc.audiomanager.datasources.youtube.domain;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Path;
import java.util.Optional;

@Slf4j
@Getter
public enum YouTubeLocationType {
	VIDEO("youtube", "video"),
	MUSIC("ytmusic", "track"),
	VIDEO_PLAYLIST("youtube", "playlist"),
	MUSIC_PLAYLIST("ytmusic", "playlist");
	private final String server;
	private final String resourceType;

	YouTubeLocationType(String server, String resourceType) {
		this.server = server;
		this.resourceType = resourceType;
	}

	public static Optional<YouTubeLocationType> of(YouTubeUrnType ytUrnType, String type) {
		if (type == null) {
			return Optional.of(VIDEO);
		}
		if (type.equalsIgnoreCase("video")) {
			return Optional.of(VIDEO);
		}
		if (type.equalsIgnoreCase("track")) {
			return Optional.of(MUSIC);
		}
		if (type.equalsIgnoreCase("playlist")) {
			return Optional.of(
					ytUrnType.equals(YouTubeUrnType.ytmusic) ? MUSIC_PLAYLIST : VIDEO_PLAYLIST);
		}
		log.warn("\nUnknown YouTubeLocationType: %s".formatted(type));
		return Optional.empty();
	}

	public String toURN(String ytCode) {
		return STR."\{server}:\{resourceType}:\{ytCode}";
	}

	public Path toPath(String ytCode) {
		return Path.of(STR."\{server}/\{resourceType}/\{ytCode}");
	}
}
