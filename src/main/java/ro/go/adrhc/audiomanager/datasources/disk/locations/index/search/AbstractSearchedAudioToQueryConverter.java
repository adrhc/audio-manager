package ro.go.adrhc.audiomanager.datasources.disk.locations.index.search;

import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.Query;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.config.QueryProperties;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.SearchedAudio;
import ro.go.adrhc.persistence.lucene.core.bare.query.TermQueryFactory;
import ro.go.adrhc.persistence.lucene.core.bare.token.TokenizationUtils;

import java.util.Collection;
import java.util.function.BiFunction;
import java.util.stream.Stream;

@RequiredArgsConstructor
public abstract class AbstractSearchedAudioToQueryConverter<SA extends SearchedAudio> {
	protected final TokenizationUtils tokenizationUtils;
	protected final QueryProperties queryProperties;
	protected final BiFunction<String, Collection<String>, Stream<FuzzyQuery>> fuzzyQueryFactory;

	protected Stream<Query> toQueries(Enum<?> field, Collection<String> tokens) {
		String fieldName = field.name();
		Collection<String> toMatchExactlyTokens = queryProperties.exactMatchTokens(tokens);
		Collection<String> toFuzzyMatchTokens = CollectionUtils.subtract(tokens,
				toMatchExactlyTokens);
		return Stream.concat(
				TermQueryFactory.create(fieldName, toMatchExactlyTokens),
				fuzzyQueryFactory.apply(fieldName, toFuzzyMatchTokens));
	}
}
