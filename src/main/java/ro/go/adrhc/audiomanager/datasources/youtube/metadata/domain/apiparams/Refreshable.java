package ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams;

public interface Refreshable<E extends Exception> {
	void refresh() throws E;
}
