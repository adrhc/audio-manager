package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.YtMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;

import java.util.Collection;

@Service
@RequiredArgsConstructor
@Slf4j
public class YtMusicPlMetadataProvider implements YtMetadataProvider {
	@Override
	public YtLocationMetadataPairs loadMetadata(Collection<YouTubeLocation> ytLocations) {
		throw new UnsupportedOperationException();
	}
}
