package ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain;

import lombok.experimental.UtilityClass;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.TraceableSearchedAudio;

@UtilityClass
public class FoundAudioPredicates {
	public static <T> boolean match(FoundAudio<TraceableSearchedAudio<T>, ?> foundAudio,
			T rawSearchedData) {
		return FoundAudioAccessors.rawSearchData(foundAudio) == rawSearchedData;
	}
}
