package ro.go.adrhc.audiomanager.datasources.disk.playlists;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.converters.DiskPlaylistToM3u8PlaylistConverter;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.converters.M3u8PlaylistToDiskPlaylistConverter;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.domain.playlist.PlaylistFactory;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.BackupFilesDirectory;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.playlistfiledirectory.PlaylistFilesDirectory;
import ro.go.adrhc.lib.m3u8.DiskM3u8Repository;
import ro.go.adrhc.lib.m3u8.domain.M3u8Playlist;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocations;

@Component
@RequiredArgsConstructor
@Slf4j
public class DiskPlaylistsRepository {
	private final PlaylistFilesDirectory playlistFilesDirectory;
	private final BackupFilesDirectory backupFilesDirectory;
	private final DiskM3u8Repository diskM3u8Repository;
	private final M3u8PlaylistToDiskPlaylistConverter toDiskPlaylistConverter;
	private final DiskPlaylistToM3u8PlaylistConverter toM3u8PlaylistConverter;

	public List<DiskLocation> getPlaylistsLocations() throws IOException {
		return createDiskLocations(playlistFilesDirectory.getAllExceptFixes());
	}

	public boolean remove(String fileName) throws IOException {
		Path plPath = playlistFilesDirectory.resolvePath(fileName);
		return Files.deleteIfExists(plPath);
	}

	public Playlist<DiskLocation> load(DiskLocation plLocation) {
		Path plPath = playlistFilesDirectory.resolvePath(plLocation.path());
		M3u8Playlist m3u8Playlist = diskM3u8Repository.load(plPath);
		return toDiskPlaylistConverter.convert(m3u8Playlist);
	}

	public Playlist<DiskLocation> loadOrCreate(DiskLocation plLocation) {
		Path plPath = playlistFilesDirectory.resolvePath(plLocation.path());
		return diskM3u8Repository.get(plPath)
				.map(toDiskPlaylistConverter::convert)
				.orElseGet(() -> PlaylistFactory.of(createDiskLocation(plPath)));
	}

	public Optional<DiskLocation> store(Playlist<DiskLocation> playlist) {
		M3u8Playlist m3u8Playlist = toM3u8PlaylistConverter.convert(playlist);
		if (diskM3u8Repository.store(m3u8Playlist)) {
			return Optional.of(createDiskLocation(m3u8Playlist.path()));
		} else {
			return Optional.empty();
		}
	}

	public PlaylistStorageOutcome storeIfNotEmpty(Playlist<DiskLocation> playlist) {
		if (playlist.isEmpty()) {
			return new PlaylistStorageOutcome(
					false, playlist.size(), playlist.location());
		} else {
			return new PlaylistStorageOutcome(
					true, playlist.size(), store(playlist).orElse(null));
		}
	}

	public List<Playlist<DiskLocation>> loadPlaylists() throws IOException {
		return loadMany(playlistFilesDirectory.getAllExceptFixes());
	}

	public List<Playlist<DiskLocation>> getFoundAndNotFound() throws IOException {
		return loadMany(playlistFilesDirectory.getFoundAndNotFound());
	}

	public List<Playlist<DiskLocation>> getFoundOnly() throws IOException {
		return loadMany(playlistFilesDirectory.getFoundOnly());
	}

	public List<Playlist<DiskLocation>> getNotFoundOnly() throws IOException {
		return loadMany(playlistFilesDirectory.getNotFoundOnly());
	}

	public List<Playlist<DiskLocation>> loadBackups() throws IOException {
		return loadMany(backupFilesDirectory.getBackups());
	}

	/**
	 * @param playlists are absolute paths
	 * @return the Playlist(s) corresponding to provided paths
	 */
	private List<Playlist<DiskLocation>> loadMany(List<Path> playlists) {
		return playlists.stream().map(diskM3u8Repository::load)
				.map(toDiskPlaylistConverter::convert).toList();
	}
}
