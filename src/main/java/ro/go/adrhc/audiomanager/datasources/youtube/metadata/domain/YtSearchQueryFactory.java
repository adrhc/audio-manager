package ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain;

import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.util.text.StringBuilderEx;

import static ro.go.adrhc.util.text.StringBuilderEx.spaceSeparator;

public class YtSearchQueryFactory {
	public static String textQuery(SongQuery songQuery) {
		StringBuilderEx builder = spaceSeparator();
		builder.appendWord(songQuery.title());
		builder.appendWord(songQuery.artist());
		builder.append(songQuery.freeText());
		return builder.toString();
	}
}
