package ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;

import java.nio.file.Path;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;

@Component
public class AudioMetadataToDiskLocationConverter
		implements Converter<AudioMetadata, DiskLocation> {
	@Override
	@NonNull
	public DiskLocation convert(@NonNull AudioMetadata metadata) {
		return createDiskLocation(Path.of(metadata.uri()));
	}
}
