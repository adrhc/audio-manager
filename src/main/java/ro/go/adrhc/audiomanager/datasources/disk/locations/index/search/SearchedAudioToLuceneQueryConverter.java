package ro.go.adrhc.audiomanager.datasources.disk.locations.index.search;

import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.search.*;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;
import ro.go.adrhc.audiomanager.datasources.audiofiles.index.AudioMetadataFields;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.config.QueryProperties;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.SearchedAudio;
import ro.go.adrhc.persistence.lucene.core.bare.query.BooleanQueryBuilder;
import ro.go.adrhc.persistence.lucene.core.bare.query.TermQueryFactory;
import ro.go.adrhc.persistence.lucene.core.bare.token.TokenizationUtils;

import java.io.IOException;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static ro.go.adrhc.lib.audiometadata.domain.DurationValidators.isValid;
import static ro.go.adrhc.persistence.lucene.core.bare.query.BooleanQueryFactory.shouldSatisfy;

@Slf4j
public class SearchedAudioToLuceneQueryConverter<SA extends SearchedAudio>
		extends AbstractSearchedAudioToQueryConverter<SA> {
	private final SearchedAudioWordsToQueryConverter<SA> wordsToQueryConverter;

	public SearchedAudioToLuceneQueryConverter(TokenizationUtils tokenizationUtils,
			QueryProperties queryProperties,
			BiFunction<String, Collection<String>, Stream<FuzzyQuery>> fuzzyQueryFactory,
			SearchedAudioWordsToQueryConverter<SA> wordsToQueryConverter) {
		super(tokenizationUtils, queryProperties, fuzzyQueryFactory);
		this.wordsToQueryConverter = wordsToQueryConverter;
	}

	@NonNull
	public Optional<Query> convert(SA searchedAudio) {
		BooleanQueryBuilder queryBuilder = new BooleanQueryBuilder();

		boolean durationIsValid = isValid(searchedAudio.getDuration());

		// duration
		if (durationIsValid) {
			queryBuilder.shouldSatisfy(durationQuery(searchedAudio));
		}

		// fileNameNoExt: try to match any word
		// only used to make additional difference between results
		Set<String> tokenizedTitle;
		try {
			tokenizedTitle = tokenizationUtils.textToTokenSet(searchedAudio.getTitle());
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			return Optional.empty();
		}

		Assert.isTrue(!tokenizedTitle.isEmpty(), "Tokenized title can't be empty!");
		toQueries(AudioMetadataFields.fileNameNoExt, tokenizedTitle)
				.forEach(queryBuilder::shouldSatisfy);

		Optional<Query> wordsQuery = wordsToQueryConverter.convert(searchedAudio);
		if (wordsQuery.isEmpty()) {
			return Optional.empty();
		} else {
			queryBuilder.mustSatisfy(wordsQuery.get());
		}

//		log.debug("\n{}", queryBuilder.build());

		// fileNameNoExt:are fileNameNoExt:you fileNameNoExt:presley~1
		// fileNameNoExt:elvis~1 +((words:are words:you words:presley~1 words:elvis~1)~3)
		return Optional.of(queryBuilder.build());
	}

	/**
	 * duration search range: (seconds - CONFIG.durationSearchRange) ... (seconds + CONFIG.durationSearchRange)
	 */
	private BoostQuery durationQuery(SA searchedAudio) {
		IntStream secondsStream = queryProperties.durationSearchRange(searchedAudio.getDuration());
		Stream<TermQuery> durationQueries = TermQueryFactory.create(
				AudioMetadataFields.duration, secondsStream);
		BooleanQuery durationQuery = shouldSatisfy(durationQueries);
		return new BoostQuery(durationQuery, queryProperties.getDurationQueryBoost());
	}
}
