package ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams;

import lombok.Builder;
import org.springframework.http.HttpHeaders;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Builder
public record YouTubeApiParams(String key, String payload,
		HttpHeaders headers, boolean prettyPrint) {
	public Map<String, Object> queryParams(Map<String, String> nextChunkToken) {
		return nextChunkToken != null ? chunkParams(nextChunkToken) : basicParams();
	}

	public List<String> getHeader(String name) {
		return headers.get(name);
	}

	public Optional<String> getFirstHeaderValue(String name) {
		List<String> values = headers.get(name);
		return values != null && !values.isEmpty() ? Optional.of(
				values.getFirst()) : Optional.empty();
	}

	private Map<String, Object> chunkParams(Map<String, String> nextChunkToken) {
		HashMap<String, Object> params = new HashMap<>(basicParams());
		params.put("type", "next");
		params.put("ctoken", nextChunkToken.get("continuation"));
		params.put("continuation", nextChunkToken.get("continuation"));
		params.put("itct", nextChunkToken.get("clickTrackingParams"));
		return params;
	}

	private Map<String, Object> basicParams() {
		return Map.of("key", key, "prettyPrint", true);
	}
}
