package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsearch.websearch;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "youtube.WebMusicSearch", url = "${youtube-api.web-music-search}")
public interface YtMusicWebSearch {
	@PostMapping
	String search(@RequestHeader HttpHeaders headers, @RequestBody String payload);
}
