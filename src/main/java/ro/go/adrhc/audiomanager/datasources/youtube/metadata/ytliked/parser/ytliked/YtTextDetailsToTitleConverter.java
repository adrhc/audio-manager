package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser.ytliked;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.SongTitleFactory;
import ro.go.adrhc.audiomanager.util.PlaylistUtils;

import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.function.Predicate.not;
import static ro.go.adrhc.util.text.StringUtils.concat;

@Slf4j
public class YtTextDetailsToTitleConverter implements Converter<Collection<YtTextDetail>, String> {
	public static final YtTextDetailsToTitleConverter
			INSTANCE = new YtTextDetailsToTitleConverter();
	/**
	 * excluding blank or 1 character/digit texts, e.g. " & "
	 */
	private static final Predicate<String> isIgnoredText = Pattern
			.compile("\\s*[\\d\\w]?\\s*").asMatchPredicate();

	/**
	 * Some types are ignored, e.g. MUSIC_PAGE_TYPE_USER_CHANNEL.
	 */
	public static Set<String> TYPES = Set.of("MUSIC_VIDEO_TYPE_ATV",
			"MUSIC_VIDEO_TYPE_OMV", "MUSIC_VIDEO_TYPE_OFFICIAL_SOURCE_MUSIC",
			"MUSIC_VIDEO_TYPE_UGC", "MUSIC_PAGE_TYPE_ARTIST",
			"MUSIC_PAGE_TYPE_ALBUM", "MUSIC_PAGE_TYPE_USER_CHANNEL",
			"MUSIC_PAGE_TYPE_PLAYLIST");

	@Override
	public String convert(@NonNull Collection<YtTextDetail> textDetails) {
		return new YtTitleBuilder(textDetails).build();
	}

	private record YtTitleBuilder(Collection<YtTextDetail> textDetails) {
		/**
		 * MUSIC_VIDEO_TYPE_ATV = Associated Television
		 * MUSIC_VIDEO_TYPE_OMV = Official Music Video
		 * MUSIC_VIDEO_TYPE_UGC = User-Generated Content
		 * title: MUSIC_VIDEO_TYPE_ATV - [MUSIC_PAGE_TYPE_ARTIST | text for "null" type | MUSIC_PAGE_TYPE_ALBUM]
		 * title: MUSIC_VIDEO_TYPE_OMV - [MUSIC_PAGE_TYPE_ARTIST | text for "null" type | MUSIC_PAGE_TYPE_ALBUM]
		 * title: MUSIC_VIDEO_TYPE_OFFICIAL_SOURCE_MUSIC - [MUSIC_PAGE_TYPE_ARTIST | text for "null" type | MUSIC_PAGE_TYPE_ALBUM]
		 * title: MUSIC_VIDEO_TYPE_UGC
		 */
		public String build() {
			return Stream.of("MUSIC_VIDEO_TYPE_ATV",
							"MUSIC_VIDEO_TYPE_OMV",
							"MUSIC_VIDEO_TYPE_OFFICIAL_SOURCE_MUSIC",
							"MUSIC_PAGE_TYPE_PLAYLIST")
					.flatMap(this::getTextsByType)
					.findFirst()
					.map(this::createTitle)
					.orElseGet(
							() -> SongTitleFactory.create(
									getTextsByType("MUSIC_VIDEO_TYPE_UGC").sorted()));
		}

		private String createTitle(String songName) {
			// null refers to a YtTextDetail with type null!
			return Stream.of("MUSIC_PAGE_TYPE_ARTIST", null, "MUSIC_PAGE_TYPE_ALBUM")
					.map(type -> createTitle(songName, type))
					.flatMap(Optional::stream)
					.findFirst()
					.orElse(songName);
		}

		private Optional<String> createTitle(String songName, String type) {
			List<String> typeValues = getTextsByType(type).toList();
			if (typeValues.isEmpty()) {
				return Optional.empty();
			}
			return Optional.of(SongTitleFactory.create(songName, typeValues));
		}

		/**
		 * Outside (i.e. YouTube) text must be sanitized (i.e. sanitizeSongTitle(...)).
		 */
		private Stream<String> getTextsByType(String type) {
			Set<String> ignoredText = ignoredText();
			if (!ignoredText.isEmpty()) {
				log.warn("\ntype = {}, ignoring: {}", type,
						concat(", ", ignoredText.stream().sorted()));
			}
			return textDetails.stream()
					.filter(Objects::nonNull)
					.filter(td -> td.hasType(type))
					.map(YtTextDetail::text)
					.map(PlaylistUtils::sanitizeSongTitle)
					.filter(not(ignoredText::contains));
		}

		private Set<String> ignoredText() {
			return textDetails.stream().map(YtTextDetail::text)
					.filter(isIgnoredText).collect(Collectors.toSet());
		}
	}
}
