package ro.go.adrhc.audiomanager.datasources.youtube.locations;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <a href="https://developers.google.com/youtube/v3/docs/playlistItems">PlaylistItems</a>
 * list Quota impact: A call to this method has a quota cost of 1 unit.
 */
@FeignClient(name = "youtube-api.PlaylistItems", url = "${youtube-api.playlist-items}")
public interface GoogleApiPlItems {
	@GetMapping
	String list(@RequestParam int maxResults, @RequestParam String playlistId,
			@RequestParam String pageToken);
}
