package ro.go.adrhc.audiomanager.datasources.disk;

import lombok.Getter;
import lombok.experimental.Accessors;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import static ro.go.adrhc.audiomanager.domain.location.LocationAccessors.locationName;

@Getter
@Accessors(fluent = true)
public class PlEntrySongQuery extends SongQuery {
	private final PlaylistEntry plEntry;

	public PlEntrySongQuery(String title, String artist, String freeText, PlaylistEntry plEntry) {
		super(title, artist, freeText);
		this.plEntry = plEntry;
	}

	public static PlEntrySongQuery of(PlaylistEntry entry) {
		return new PlEntrySongQuery(entry.title(), null, locationName(entry), entry);
	}

	public String plEntryTitle() {
		return plEntry().title();
	}
}
