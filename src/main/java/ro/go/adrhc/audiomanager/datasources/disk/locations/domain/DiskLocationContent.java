package ro.go.adrhc.audiomanager.datasources.disk.locations.domain;

import java.util.List;

public record DiskLocationContent(DiskLocation container, List<DiskLocation> locations) {
}
