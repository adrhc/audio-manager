package ro.go.adrhc.audiomanager.datasources.disk.locations.index.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ro.go.adrhc.lib.audiometadata.domain.DurationValidators;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Setter
@Getter
@ToString
public class QueryProperties {
	private int durationSearchRange;
	private float durationQueryBoost;
	private int toMatchExactlyMaxTokenLength;
	private int longTokenMinLength;
	private int minimumWordToMatch;
	private float minimumWordToMatchPart1;
	private float minimumWordToMatchPart2;
	private float minimumWordToMatchWhenMissingDurationPart1;
	private float minimumWordToMatchWhenMissingDurationPart2;
	private int longTokenLevenshteinDistance;

	public int minWordsToFind(boolean hasValidDurations, int tokensAmount) {
		float configuredProportion = hasValidDurations ?
				getWordsToMatchRatio() :
				getWordsToMatchRatioWhenMissingDuration();
		int ratioBasedMinimum = Math.round(tokensAmount * configuredProportion);
		if (ratioBasedMinimum > minimumWordToMatch) {
			return ratioBasedMinimum;
		} else if (tokensAmount > minimumWordToMatch) {
			return tokensAmount - 1;
		} else {
			return tokensAmount;
		}
	}

	/**
	 * toMatchExactlyMaxTokenLength refers to tokens to match exactly
	 * while this method returns exactly that, i.e. tokens to match
	 * exactly but nothing else, hence it makes sense to stay here.
	 */
	public Set<String> exactMatchTokens(Collection<String> tokens) {
		return tokens.stream()
				.filter(it -> it.length() <= toMatchExactlyMaxTokenLength)
				.collect(Collectors.toSet());
	}

	public Set<String> longTokens(Collection<String> tokenizedWords) {
		return tokenizedWords.stream()
				.filter(it -> it.length() >= longTokenMinLength)
				.collect(Collectors.toSet());
	}

	/**
	 * durationSearchRange refers to the duration range to search for
	 * while this method returns exactly that, i.e. the duration range
	 * but nothing more, hence it makes sens to stay here.
	 */
	public IntStream durationSearchRange(Integer duration) {
		return IntStream.range(duration - durationSearchRange, duration + durationSearchRange)
				.filter(DurationValidators::isValid);
	}

	private float getWordsToMatchRatio() {
		return minimumWordToMatchPart1 / minimumWordToMatchPart2;
	}

	private float getWordsToMatchRatioWhenMissingDuration() {
		return minimumWordToMatchWhenMissingDurationPart1 / minimumWordToMatchWhenMissingDurationPart2;
	}
}
