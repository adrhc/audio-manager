package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser.ytliked;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import org.springframework.lang.NonNull;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser.AbstractYtMusicJsonParser;

import java.util.*;

import static ro.go.adrhc.util.collection.CollectionUtils.firstValue;

@RequiredArgsConstructor
@Slf4j
public class YtMusicSearchResultParser extends AbstractYtMusicJsonParser {
	private static final String MAIN = "$..contents[*].musicResponsiveListItemRenderer[?(@.flexColumns empty false)]['thumbnail', 'flexColumns']";
	private static final String IDS = "$..contents[*].musicResponsiveListItemRenderer[*]..musicResponsiveListItemFlexColumnRenderer.text.runs[?(@.text empty false || @.navigationEndpoint.watchEndpoint.videoId empty false)].navigationEndpoint.watchEndpoint.videoId";
	private final boolean excludeOutsidePlaylist;
	private final DocumentContext context;

	@NonNull
	public Set<String> parseYtIds() {
		// {thumbnail, flexColumns}[]
		List<String> ids = context.read(IDS, List.class);
		return new HashSet<>(ids);
	}

	@NonNull
	public List<YouTubeMetadata> extractYouTubeMetadata() {
		// {thumbnail, flexColumns}[]
		List<?> nodes = context.read(MAIN, List.class);
		return nodes.stream()
				.map(this::extractYouTubeMetadata)
				.flatMap(Optional::stream)
				.toList();
	}

	public Optional<YouTubeMetadata> extractYouTubeMetadata(Object node) {
		// {thumbnail, flexColumns}
		DocumentContext dc = JsonPath.parse(node);
		List<Map<String, Object>> thumbnails = parseThumbnails(dc,
				"thumbnail.musicThumbnailRenderer.thumbnail.thumbnails");
		// flexColumns[*]..text.runs -> {text, navigationEndpoint}[]
		JSONArray runs = dc.read(
				"flexColumns[*].musicResponsiveListItemFlexColumnRenderer.text.runs[?(@.text empty false || @.navigationEndpoint.watchEndpoint.videoId empty false)]['text','navigationEndpoint']");
		dc = JsonPath.parse(runs);
		String musicId = excludeOutsidePlaylist
				? parseId(dc,
				"$[*].navigationEndpoint.watchEndpoint[?(@.playlistId empty false)].videoId")
				: parseId(dc, "$[*].navigationEndpoint.watchEndpoint.videoId");
		if (musicId == null || musicId.isBlank()) {
			return Optional.empty();
		}
		String title = parseTitle(musicId, runs);
		if (title == null || title.isBlank()) {
			log.warn("\nNo title found for YouTube music id {}!", musicId);
			return Optional.empty();
		}
		// Mopidy doesn't play ytMusicLocation(musicId)!
		YouTubeLocation ytLocation = YouTubeLocationFactory.ytVideoLocation(musicId);
		return Optional.of(YouTubeMetadata.of(ytLocation, title, thumbnails));
	}

	public Map<String, String> extractNextChunkToken() {
		return firstValue(context.read("$..continuations[*].nextContinuationData"));
	}
}
