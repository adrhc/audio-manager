package ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.datasources.youtube.config.YouTubeProperties;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtSearchQueryFactory;
import ro.go.adrhc.audiomanager.util.JsonPathUtils;

import java.util.Set;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

@Configuration
@RequiredArgsConstructor
public class YtApiParamsFactory {
	private static final Set<String> PAYLOAD_ROOT = Set.of("context");
	private static final Set<String> PAYLOAD_CONTEXT = Set.of("client");
	private static final Set<String> PAYLOAD_CLIENT = Set.of("clientName", "clientVersion");

	private static final Set<String> YT_MUSIC_QUERY_PAYLOAD_ROOT = Set.of("context", "params");
	private static final Set<String> YT_MUSIC_LIB_PAYLOAD_ROOT = Set.of("browseId", "context");

	private static final Set<String> YT_QUERY_HEADERS =
			Set.of("x-youtube-client-name", "x-youtube-client-version");

	private static final Set<String> YT_MUSIC_BROWSER_HEADERS =
			Set.of("authorization", "cookie", "x-origin");

	private final YouTubeProperties youTubeProperties;
	private final HttpCommand ytMusicSearchHttpCmdLine;
	private final HttpCommand ytVideoSearchHttpCmdLine;
	private final HttpCommand ytLikedMusicHttpCmdLine;
	private final HttpCommand ytMusicLibraryHttpCmdLine;

	@Bean
	@Scope(SCOPE_PROTOTYPE)
	public YouTubeApiParams ytMusicSearchParams(SongQuery songQuery) {
		return createYtMusicQueryApiParams(ytMusicSearchHttpCmdLine, songQuery);
	}

	@Bean
	@Scope(SCOPE_PROTOTYPE)
	public YouTubeApiParams ytVideoSearchParams(SongQuery songQuery) {
		return createYtQueryApiParams(ytVideoSearchHttpCmdLine, songQuery);
	}

	@Bean
	@Scope(SCOPE_PROTOTYPE)
	public YouTubeApiParams ytMusicPlContentParams(String ytMusicPlCode) {
		String browseId = youTubeProperties.withYtMusicPrefix(ytMusicPlCode);
		return createYtMusicBrowserApiParams(ytLikedMusicHttpCmdLine, browseId);
	}

	@Bean
	public YouTubeApiParams ytMusicLibraryParams() {
		// stick with the browseId from the command line
		return createYtMusicLibApiParams(ytMusicLibraryHttpCmdLine, null);
	}

	@Bean
	public YouTubeApiParams ytLikedMusicParams() {
		// stick with the browseId from the command line
		return createYtMusicLibApiParams(ytLikedMusicHttpCmdLine, null);
	}

	@Bean
	public YouTubeApiParams mopidyYouTubeAuth() {
		return YouTubeApiParams.builder()
				.prettyPrint(true)
				.key(youTubeProperties.getKey())
				.headers(ytLikedMusicHttpCmdLine.httpHeaders())
				.payload(ytLikedMusicHttpCmdLine.payload())
				.build();
	}

	public YouTubeApiParams createYtQueryApiParams(
			HttpCommand httpCommand, SongQuery songQuery) {
		return doCreateYtQueryApiParams(httpCommand, PAYLOAD_ROOT, songQuery);
	}

	public YouTubeApiParams createYtMusicQueryApiParams(
			HttpCommand httpCommand, SongQuery songQuery) {
		return doCreateYtQueryApiParams(httpCommand, YT_MUSIC_QUERY_PAYLOAD_ROOT, songQuery);
	}

	public YouTubeApiParams createYtMusicBrowserApiParams(
			HttpCommand httpCommand, String browseId) {
		return doCreateYtMusicBrowserApiParams(httpCommand, PAYLOAD_ROOT, browseId);
	}

	public YouTubeApiParams createYtMusicLibApiParams(
			HttpCommand httpCommand, String browseId) {
		return doCreateYtMusicBrowserApiParams(httpCommand, YT_MUSIC_LIB_PAYLOAD_ROOT, browseId);
	}

	public YouTubeApiParams doCreateYtMusicBrowserApiParams(HttpCommand httpCommand,
			Set<String> payloadRootProperties, String browseId) {
		return create(createHeaders(httpCommand, YT_MUSIC_BROWSER_HEADERS),
				browseId == null ? createPayload(httpCommand, payloadRootProperties) :
						createPayload(httpCommand, payloadRootProperties, browseId));
	}

	private YouTubeApiParams doCreateYtQueryApiParams(HttpCommand httpCommand,
			Set<String> payloadRootProperties, SongQuery songQuery) {
		return create(createHeaders(httpCommand, YT_QUERY_HEADERS),
				createPayload(httpCommand, payloadRootProperties, songQuery));
	}

	private YouTubeApiParams create(
			HttpHeaders headers, DocumentContext payload) {
		return YouTubeApiParams.builder()
				.prettyPrint(true)
				.key(youTubeProperties.getKey())
				.headers(headers)
				.payload(payload.jsonString())
				.build();
	}

	private DocumentContext createPayload(HttpCommand httpCommand,
			Set<String> payloadRootProperties, String browseId) {
		DocumentContext payload = createPayload(httpCommand, payloadRootProperties);
		return payload.put("$", "browseId", browseId);
	}

	private DocumentContext createPayload(HttpCommand httpCommand,
			Set<String> payloadRootProperties, SongQuery songQuery) {
		DocumentContext defaultPayload = createPayload(httpCommand, payloadRootProperties);
		String query = YtSearchQueryFactory.textQuery(songQuery);
		return defaultPayload.put("$", "query", query);
	}

	private DocumentContext createPayload(HttpCommand httpCommand,
			Set<String> payloadRootProperties) {
		String cmdLinePayload = httpCommand.payload();
		DocumentContext dc = JsonPath.parse(cmdLinePayload);
		JsonPathUtils.keep("$", payloadRootProperties, dc);
		JsonPathUtils.keep("$.context", PAYLOAD_CONTEXT, dc);
		JsonPathUtils.keep("$.context.client", PAYLOAD_CLIENT, dc);
		return dc;
	}

	private HttpHeaders createHeaders(
			HttpCommand httpCommand, Set<String> mandatoryHeaders) {
		return httpCommand.filterHeaders(mandatoryHeaders::contains);
	}
}
