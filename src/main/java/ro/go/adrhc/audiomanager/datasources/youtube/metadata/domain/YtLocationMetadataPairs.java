package ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain;

import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * YouTubeLocation is a record, i.e. immutable, hence it's safe to have it as the Map's key.
 */
public record YtLocationMetadataPairs(Map<YouTubeLocation, YouTubeMetadata> metadataMap) {
	public static YtLocationMetadataPairs empty() {
		return new YtLocationMetadataPairs(new HashMap<>());
	}

	public static YtLocationMetadataPairs sum(
			Stream<YtLocationMetadataPairs> ytLocationMetadataPairs) {
		return ytLocationMetadataPairs
				.collect(YtLocationMetadataPairs::empty,
						YtLocationMetadataPairs::appendDiffs, YtLocationMetadataPairs::appendDiffs);
	}

	public static YtLocationMetadataPairs of(Collection<YouTubeMetadata> youTubeMetadata) {
		return of(youTubeMetadata.stream());
	}

	public static YtLocationMetadataPairs of(Stream<YouTubeMetadata> youTubeMetadata) {
		return youTubeMetadata
				.collect(YtLocationMetadataPairs::empty,
						YtLocationMetadataPairs::put,
						YtLocationMetadataPairs::appendDiffs);
	}

	public Set<YouTubeLocation> ytLocations() {
		return metadataMap.keySet();
	}

	public void put(YouTubeMetadata ytMetadata) {
		this.metadataMap.put(ytMetadata.location(), ytMetadata);
	}

	public void appendDiffs(YtLocationMetadataPairs ytMetadataBundle) {
		metadataMap.putAll(ytMetadataBundle.metadataMap);
	}

	public boolean contains(YouTubeLocation ytLocation) {
		return metadataMap.containsKey(ytLocation);
	}

	public YouTubeMetadata ytMetadata(YouTubeLocation youTubeId) {
		return metadataMap.get(youTubeId);
	}

	public int size() {
		return metadataMap.size();
	}

	public boolean isEmpty() {
		return metadataMap.isEmpty();
	}

	public <R> Stream<R> valuesMap(Function<? super YouTubeMetadata, ? extends R> mapper) {
		return metadataMap.values().stream().map(mapper);
	}
}
