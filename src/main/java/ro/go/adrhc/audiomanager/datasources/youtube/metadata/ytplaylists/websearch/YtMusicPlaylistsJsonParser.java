package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.websearch;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser.AbstractYtMusicJsonParser;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.UnaryOperator;

@RequiredArgsConstructor
@Slf4j
public class YtMusicPlaylistsJsonParser extends AbstractYtMusicJsonParser {
	private static final String MAIN = "$..musicTwoRowItemRenderer[?(@.title.runs empty false)]['title', 'thumbnailRenderer']";
	private final UnaryOperator<String> ytCodeParser;
	private final DocumentContext context;

	public Collection<YouTubeMetadata> parse() {
		List<?> nodes = context.read(MAIN, List.class);
		return nodes.stream()
				.map(this::extractYouTubeMetadata)
				.flatMap(Optional::stream)
				.toList();
	}

	protected String parseId(DocumentContext context, String idJsonPath) {
		String id = super.parseId(context, idJsonPath);
		return ytCodeParser.apply(id);
	}

	private Optional<YouTubeMetadata> extractYouTubeMetadata(Object node) {
		DocumentContext dc = JsonPath.parse(node);
		JSONArray runs = dc.read(
				"title.runs[?(@.text empty false && @.navigationEndpoint.browseEndpoint.browseId empty false)]['text', 'navigationEndpoint']");
		if (runs.isEmpty()) {
			return Optional.empty();
		}
		List<Map<String, Object>> thumbnails = parseThumbnails(dc,
				"thumbnailRenderer.musicThumbnailRenderer.thumbnail.thumbnails");
		dc = JsonPath.parse(runs);
		String id = parseId(dc, "$[*].navigationEndpoint.browseEndpoint.browseId");
		if (id == null || id.isBlank()) {
			log.warn("\nNo id found for:\n{}", node.toString());
			return Optional.empty();
		}
		String title = parseTitle(id, runs);
		if (title == null || title.isBlank()) {
			log.warn("\nNo title found for YouTube Music playlist id {}!", id);
			return Optional.empty();
		}
		YouTubeLocation ytLocation = YouTubeLocationFactory.ytMusicPlLocation(id);
		return Optional.of(YouTubeMetadata.of(ytLocation, title, thumbnails));
	}
}
