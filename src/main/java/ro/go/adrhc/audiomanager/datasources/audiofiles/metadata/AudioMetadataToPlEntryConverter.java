package ro.go.adrhc.audiomanager.datasources.audiofiles.metadata;

import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.domain.location.LocationParsers;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class AudioMetadataToPlEntryConverter
		implements Converter<AudioMetadata, Optional<PlaylistEntry>> {
	private final LocationParsers locationParsers;

	@NonNull
	@Override
	public Optional<PlaylistEntry> convert(@NotNull AudioMetadata metadata) {
		return locationParsers
				.getForLocationType(metadata.locationType())
				.flatMap(p -> p.parse(metadata.uri().toString()))
				.map(loc -> new PlaylistEntry(loc, metadata.fileNameNoExt(),
						metadata.audioDurations().getIfOne(), List.of()));
	}
}
