package ro.go.adrhc.audiomanager.datasources.youtube.metadata;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.SetUtils;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import static ro.go.adrhc.util.text.StringUtils.concat;

@RequiredArgsConstructor
@Slf4j
public class LogUtils {
	private final Function<YouTubeLocation, String> ytIdToURLConverter;

	public void logNotFound(Collection<YouTubeLocation> searchedYtLocations,
			YtLocationMetadataPairs foundEntries) {
		Set<YouTubeLocation> searchedYtLocationsSet = new HashSet<>(searchedYtLocations);
		Collection<YouTubeLocation> diff = SetUtils.difference(
				searchedYtLocationsSet, foundEntries.ytLocations());
		if (!diff.isEmpty()) {
			log.warn("\n{}\nCouldn't find {} of {} YouTube (unique) ids!",
					concat(diff.stream().map(ytIdToURLConverter).sorted()),
					diff.size(), searchedYtLocationsSet.size());
		}
	}
}
