package ro.go.adrhc.audiomanager.datasources.youtube.domain;

import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.domain.location.LocationParser;
import ro.go.adrhc.audiomanager.domain.location.LocationType;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Component
@Slf4j
@Order(1)
public class YouTubeLocationFactory implements LocationParser<YouTubeLocation> {
	private final YouTubeLocationPatterns youTubeLocationPatterns;

	public static YouTubeLocation ytVideoPlLocation(@NotNull String ytCode) {
		return new YouTubeLocation(YouTubeLocationType.VIDEO_PLAYLIST, ytCode);
	}

	public static YouTubeLocation ytMusicPlLocation(@NotNull String ytCode) {
		return new YouTubeLocation(YouTubeLocationType.MUSIC_PLAYLIST, ytCode);
	}

	public static YouTubeLocation ytMusicLocation(@NotNull String ytCode) {
		return new YouTubeLocation(YouTubeLocationType.MUSIC, ytCode);
	}

	public static YouTubeLocation ytVideoLocation(@NotNull String ytCode) {
		return new YouTubeLocation(YouTubeLocationType.VIDEO, ytCode);
	}

	public static List<YouTubeLocation> ytMusicLocations(@NotNull Collection<String> ytCodes) {
		return ytCodes.stream().map(YouTubeLocationFactory::ytMusicLocation).toList();
	}

	public static List<YouTubeLocation> ytVideoLocations(@NotNull Collection<String> ytCodes) {
		return ytCodes.stream().map(YouTubeLocationFactory::ytVideoLocation).toList();
	}

	public static List<YouTubeLocation> ytVideoLocations(String... ytCodes) {
		return Stream.of(ytCodes).map(YouTubeLocationFactory::ytVideoLocation).toList();
	}

	public static List<YouTubeLocation> ytMusicLocations(String... ytCodes) {
		return Stream.of(ytCodes).map(YouTubeLocationFactory::ytMusicLocation).toList();
	}

	public static List<YouTubeLocation> ytMusicPlLocations(String... ytCodes) {
		return ytMusicPlLocations(Arrays.asList(ytCodes));
	}

	public static List<YouTubeLocation> ytVideoPlLocations(String... ytCodes) {
		return ytVideoPlLocations(Arrays.asList(ytCodes));
	}

	public static List<YouTubeLocation> ytMusicPlLocations(Collection<String> ytCodes) {
		return ytCodes.stream().map(YouTubeLocationFactory::ytMusicPlLocation).toList();
	}

	public static List<YouTubeLocation> ytVideoPlLocations(Collection<String> ytCodes) {
		return ytCodes.stream().map(YouTubeLocationFactory::ytVideoPlLocation).toList();
	}

	public Optional<YouTubeLocation> parse(String text) {
		return youTubeLocationPatterns.parseYtLocationCode(text).flatMap(this::ofParsedUrn);
	}

	public boolean supports(LocationType locationType) {
		return locationType == LocationType.YOUTUBE;
	}

	private Optional<YouTubeLocation> ofParsedUrn(String[] urnParts) {
		if (urnParts.length == 1) {
			return Optional.of(new YouTubeLocation(YouTubeLocationType.VIDEO, urnParts[0]));
		} else {
			try {
				YouTubeUrnType urnType = YouTubeUrnType.valueOf(urnParts[2]);
				return YouTubeLocationType.of(urnType, urnParts[1])
						.map(type -> new YouTubeLocation(type, urnParts[0]));
			} catch (IllegalArgumentException e) {
				log.trace("\nUnknown YouTubeUrnType: %s\nurnParts: %s"
						.formatted(urnParts[2],
								Arrays.stream(urnParts).sorted(Comparator.reverseOrder())
										.collect(Collectors.joining(" "))));
			}
			return Optional.empty();
		}
	}
}
