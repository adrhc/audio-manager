package ro.go.adrhc.audiomanager.datasources.youtube.metadata.shazam;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.shazam.parser.ShazamCSVFileParser;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsearch.YtSearchMetadataProvider;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;
import static ro.go.adrhc.util.concurrency.FutureUtils.safelyGetAll;

@Service
@RequiredArgsConstructor
public class ShazamYtMetadataProvider {
	private final ExecutorService cachedThreadPool;
	private final ShazamCSVFileParser shazamCSVFileParser;
	private final YtSearchMetadataProvider ytSearchMetadataProvider;

	public List<YouTubeMetadata> loadMetadata(File shazamCSVFile) throws IOException {
		List<SongQuery> songQueries = shazamCSVFileParser.parse(shazamCSVFile);
		Stream<YouTubeMetadata> metadata = safelyGetAll(asyncFindFirstMatches(songQueries));
		return metadata.filter(Objects::nonNull).toList();
	}

	private Stream<CompletableFuture<YouTubeMetadata>> asyncFindFirstMatches(
			Collection<SongQuery> songQueries) {
		return songQueries.stream().map(this::asyncFindFirstMatch);
	}

	private CompletableFuture<YouTubeMetadata> asyncFindFirstMatch(SongQuery songQuery) {
		return asyncFindAllMatches(songQuery).thenApply(
				list -> list.size() < 2 ? null : list.getFirst());
	}

	private CompletableFuture<List<YouTubeMetadata>> asyncFindAllMatches(SongQuery songQuery) {
//		return ytSearchMetadataProvider.searchVideo(songQuery);
//		return ytSearchMetadataProvider.webMusicSearch(songQuery);
		return supplyAsync(() -> ytSearchMetadataProvider.findAllWebVideoMatches(songQuery),
				cachedThreadPool);
	}
}
