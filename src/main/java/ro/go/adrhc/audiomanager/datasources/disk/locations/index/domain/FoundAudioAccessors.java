package ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain;

import lombok.experimental.UtilityClass;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.TraceableSearchedAudio;

@UtilityClass
public class FoundAudioAccessors {
	public static <T> T rawSearchData(FoundAudio<TraceableSearchedAudio<T>, ?> foundAudio) {
		return foundAudio.searchedAudio().getRawSearchData();
	}
}
