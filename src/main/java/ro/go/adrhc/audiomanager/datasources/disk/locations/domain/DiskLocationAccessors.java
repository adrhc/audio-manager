package ro.go.adrhc.audiomanager.datasources.disk.locations.domain;

import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.LocationAware;

import java.nio.file.Path;

public class DiskLocationAccessors {
	public static String rawPath(Location location) {
		return ((DiskLocation) location).rawPath();
	}

	public static Path path(Location location) {
		return ((DiskLocation) location).path();
	}

	public static Path path(LocationAware<?> locationAware) {
		return diskLocation(locationAware).path();
	}

	public static String rawPath(LocationAware<?> locationAware) {
		return diskLocation(locationAware).rawPath();
	}

	public static DiskLocation diskLocation(LocationAware<?> entry) {
		return (DiskLocation) entry.location();
	}
}
