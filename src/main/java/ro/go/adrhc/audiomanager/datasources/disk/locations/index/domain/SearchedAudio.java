package ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain;

import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.springframework.lang.NonNull;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;

import java.util.Set;

@RequiredArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class SearchedAudio {
	@NotNull
	private final String title;
	private final Set<String> words;
	private final Integer duration;

	/**
	 * title = query.title + query.artist when query.title has text
	 * title = query.title + query.artist + query.freeText when query.title doesn't have text
	 */
	public static SearchedAudio of(@NonNull SongQuery query) {
		return of(query.concatenateParts(), query.toSet());
	}

	public static SearchedAudio of(@NonNull String title, Set<String> words) {
		return new SearchedAudio(title, words, null);
	}
}
