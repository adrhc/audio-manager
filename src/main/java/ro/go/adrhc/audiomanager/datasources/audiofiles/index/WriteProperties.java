package ro.go.adrhc.audiomanager.datasources.audiofiles.index;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Setter
@Getter
@ToString
public class WriteProperties {
	private Set<String> audioTagsToIndex;
}
