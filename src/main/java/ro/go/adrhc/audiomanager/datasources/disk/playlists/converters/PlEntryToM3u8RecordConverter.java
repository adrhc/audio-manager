package ro.go.adrhc.audiomanager.datasources.disk.playlists.converters;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.UnknownLocation;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.lib.m3u8.domain.Extinf;
import ro.go.adrhc.lib.m3u8.domain.M3u8Record;
import ro.go.adrhc.util.Assert;

import static ro.go.adrhc.lib.m3u8.domain.Extinf.UNKNOWN_SECONDS;

@Component
@RequiredArgsConstructor
public class PlEntryToM3u8RecordConverter implements Converter<PlaylistEntry, M3u8Record> {
	@Override
	public M3u8Record convert(@NonNull PlaylistEntry plEntry) {
		Extinf extinf = toExtinf(plEntry);
		String location = toString(plEntry.location());
		Assert.isTrue(extinf != null || location != null,
				"Extinf or location should not be empty!");
		return new M3u8Record(location, extinf);
	}

	private String toString(Location location) {
		if (location instanceof UnknownLocation unk) {
			return unk.raw();
		} else if (location instanceof DiskLocation disk) {
			return disk.path().toString();
		} else {
			return location.uri().toString();
		}
	}

	private Extinf toExtinf(@NonNull PlaylistEntry plEntry) {
		boolean hasTitle = plEntry.hasTitle();
		boolean hasDuration = plEntry.hasDuration();
		if (hasDuration && hasTitle) {
			return new Extinf(plEntry.duration(), plEntry.title());
		} else if (hasTitle) {
			return new Extinf(UNKNOWN_SECONDS, plEntry.title());
		} else if (hasDuration) {
			return new Extinf(plEntry.duration(), null);
		}
		return null;
	}
}
