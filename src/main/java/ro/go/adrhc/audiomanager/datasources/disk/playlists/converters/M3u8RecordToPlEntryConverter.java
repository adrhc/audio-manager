package ro.go.adrhc.audiomanager.datasources.disk.playlists.converters;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.LocationParsers;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.lib.m3u8.domain.M3u8Record;

@Component
@RequiredArgsConstructor
public class M3u8RecordToPlEntryConverter implements Converter<M3u8Record, PlaylistEntry> {
	private final LocationParsers locationParsers;

	@Override
	public PlaylistEntry convert(@NonNull M3u8Record m3u8Record) {
		Location location = locationParsers.parse(m3u8Record.location());
		return PlaylistEntry.of(location, m3u8Record.title(), m3u8Record.seconds());
	}
}
