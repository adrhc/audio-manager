package ro.go.adrhc.audiomanager.datasources.disk.locations.index.search;

import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.lucene.search.FuzzyQuery;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.lucene.AMIndexProperties;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.SearchedAudio;
import ro.go.adrhc.persistence.lucene.core.bare.query.FuzzyQueryFactory;
import ro.go.adrhc.persistence.lucene.core.bare.token.TokenizationUtils;

import java.util.Collection;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class SearchedAudioToLuceneQueryConverterFactory<SA extends SearchedAudio> {
	private final AMIndexProperties amIndexProperties;
	private final TokenizationUtils tokenizationUtils;

	public SearchedAudioToLuceneQueryConverter<SA> create() {
		return new SearchedAudioToLuceneQueryConverter<>(
				tokenizationUtils, amIndexProperties.getQuery(), this::createFuzzyQueryStream,
				createSearchedAudioWordsToQueryConverter(this::createFuzzyQueryStream));
	}

	private SearchedAudioWordsToQueryConverter<SA> createSearchedAudioWordsToQueryConverter(
			BiFunction<String, Collection<String>, Stream<FuzzyQuery>> fuzzyQueryFactory) {
		return new SearchedAudioWordsToQueryConverter<>(tokenizationUtils,
				amIndexProperties.getQuery(), fuzzyQueryFactory);
	}

	private Stream<FuzzyQuery> createFuzzyQueryStream(String fieldName, Collection<String> tokens) {
		int longTokenLevenshteinDistance = amIndexProperties
				.getQuery().getLongTokenLevenshteinDistance();
		Set<String> longTokens = amIndexProperties.getQuery().longTokens(tokens);
		Collection<String> smallTokens = CollectionUtils.subtract(tokens, longTokens);
		return Stream.concat(
				FuzzyQueryFactory.create(1, fieldName, smallTokens),
				FuzzyQueryFactory.create(longTokenLevenshteinDistance, fieldName, longTokens));
	}
}
