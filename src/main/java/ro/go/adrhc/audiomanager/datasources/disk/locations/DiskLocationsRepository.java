package ro.go.adrhc.audiomanager.datasources.disk.locations;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocationContent;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.SongQueryLocationPairs;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.TraceableSearchedAudio;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.FoundAudio;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.SearchedAudio;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.search.DiskLocationsIndexSearchService;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.util.io.FileSystemUtils;
import ro.go.adrhc.util.io.SimpleDirectory;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * A repository of DiskLocation(s).
 */
@RequiredArgsConstructor
@Component
@Slf4j
public class DiskLocationsRepository<T extends SongQuery> {
	private final SimpleDirectory audioFilesDirectory;
	private final FileSystemUtils fileSystemUtils;
	private final DiskLocationsIndexSearchService<SearchedAudio> searchService;
	private final DiskLocationsIndexSearchService<TraceableSearchedAudio<T>> traceSearchService;

	public boolean isValid(DiskLocation diskLocation) {
		return fileSystemUtils.isRegularFile(diskLocation.path());
	}

	public DiskLocationContent getContent(DiskLocation diskLocation) throws IOException {
		return new DiskLocationContent(diskLocation, audioFilesDirectory
				.transformPathStream(diskLocation.path(),
						paths -> paths
								.map(DiskLocationFactory::createDiskLocation)
								.toList()));
	}

	public List<DiskLocation> findMany(SongQuery query) throws IOException {
		List<FoundAudio<SearchedAudio, DiskLocation>> foundAudios =
				searchService.findMany(SearchedAudio.of(query));
		return foundAudios.stream().map(FoundAudio::location).toList();
	}

	public Optional<DiskLocation> findBestMatch(SongQuery query) throws IOException {
		return searchService.findBestMatch(SearchedAudio.of(query)).map(FoundAudio::location);
	}

	public SongQueryLocationPairs<T, DiskLocation> findBestMatches(
			Collection<T> queries) throws IOException {
		List<FoundAudio<TraceableSearchedAudio<T>, DiskLocation>> bestDiskMatches =
				traceSearchService.findBestMatches(
						TraceableSearchedAudio.ofAnySongQueries(queries));
//		log.debug("\nFound {} matches for {} queries.", bestDiskMatches.size(), queries.size());
		return SongQueryLocationPairs.of(queries, bestDiskMatches);
	}
}
