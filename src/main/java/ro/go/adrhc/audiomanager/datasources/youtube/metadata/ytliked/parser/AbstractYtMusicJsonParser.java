package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import org.springframework.util.Assert;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser.ytliked.YtTextDetail;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser.ytliked.YtTextDetailsToTitleConverter;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;
import static ro.go.adrhc.audiomanager.util.ObjectUtils.firstNotNull;
import static ro.go.adrhc.util.collection.CollectionUtils.firstValue;
import static ro.go.adrhc.util.text.StringUtils.concat;

@Slf4j
public abstract class AbstractYtMusicJsonParser {
	protected String parseId(DocumentContext context, String idJsonPath) {
		Collection<String> ids = context.read(idJsonPath, Collection.class);
		String musicId = firstValue(ids);
		ids = ids == null ? Set.of() : ids;
		if (ids.size() != 1) {
			log.warn("\nFound {} YouTube id(s)!\n{}", ids.size(), concat(ids));
		}
		return musicId;
	}

	protected List<Map<String, Object>> parseThumbnails(
			DocumentContext context, String thumbnailsJsonPath) {
		try {
			List<Map<String, Object>> thumbnails =
					context.read(thumbnailsJsonPath,
							List.class);
			return thumbnails == null ? List.of() : thumbnails;
		} catch (PathNotFoundException e) {
			// do nothing
		}
		return List.of();
	}

	protected String parseTitle(String ytId, JSONArray runs) {
		Set<YtTextDetail> ytTextDetails = ytTextDetails(runs);
		assertHasOnlyKnownTypes(ytId, ytTextDetails);
		return YtTextDetailsToTitleConverter.INSTANCE.convert(ytTextDetails);
	}

	private Set<YtTextDetail> ytTextDetails(JSONArray nodes) {
		return nodes.stream().map(elem -> {
					// e.g. Touch in the Night
					String text = JsonPath.read(elem, "$.text");
					// e.g. MUSIC_VIDEO_TYPE_ATV, MUSIC_VIDEO_TYPE_OMV, MUSIC_VIDEO_TYPE_UGC
					String musicVideoType = parseMusicVideoType(elem);
					// e.g. MUSIC_PAGE_TYPE_ARTIST, MUSIC_PAGE_TYPE_ALBUM, MUSIC_PAGE_TYPE_USER_CHANNEL
					String pageType = parsePageType(elem);
					// breakpoint:
					// musicVideoType == null && pageType == null && !text.matches("\\s(•|&)\\s|(\\d+(:\\d+|(\\.\\d+)?\\w\\splays))")
					return new YtTextDetail(firstNotNull(pageType, musicVideoType), text);
				})
				.filter(YtTextDetail::hasType)
				.collect(Collectors.toSet());
	}

	private String parseMusicVideoType(Object elem) {
		return firstValue(JsonPath.read(elem,
				"$[?(@.navigationEndpoint)].navigationEndpoint..musicVideoType"));
	}

	private String parsePageType(Object elem) {
		return firstValue(
				JsonPath.read(elem, "$[?(@.navigationEndpoint)].navigationEndpoint..pageType"));
	}

	private void assertHasOnlyKnownTypes(
			String ytId, Collection<YtTextDetail> textDetails) {
		Collection<String> unknownTypes = textDetails.stream()
				.map(YtTextDetail::type)
				.filter(not(YtTextDetailsToTitleConverter.TYPES::contains))
				.collect(Collectors.toSet());
		Assert.isTrue(unknownTypes.isEmpty(), "%s has unknown types: %s"
				.formatted(ytId, concat(", ", unknownTypes.stream().sorted())));
	}
}
