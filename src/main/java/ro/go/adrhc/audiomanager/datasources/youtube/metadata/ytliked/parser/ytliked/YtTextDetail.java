package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser.ytliked;

import java.util.Objects;

import static ro.go.adrhc.util.ComparisonUtils.compareComparable;

public record YtTextDetail(String type, String text) implements Comparable<YtTextDetail> {
	@Override
	public int compareTo(YtTextDetail other) {
		int typeComparison = compareComparable(type, other.type);
		if (typeComparison != 0) {
			return typeComparison;
		} else {
			return compareComparable(text, other.text);
		}
	}

	public boolean hasType() {
		return type != null && !type.isBlank();
	}

	public boolean hasType(String type) {
		return Objects.equals(this.type, type);
	}
}
