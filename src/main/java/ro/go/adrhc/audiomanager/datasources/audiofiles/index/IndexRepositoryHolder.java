package ro.go.adrhc.audiomanager.datasources.audiofiles.index;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.store.LockObtainFailedException;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.context.IndexPathObserver;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;
import ro.go.adrhc.persistence.lucene.FileSystemIndexImpl;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;

@Component
@RequiredArgsConstructor
@Slf4j
public class IndexRepositoryHolder implements IndexPathObserver, Closeable {
	private final AMIndexParamsFactory paramsFactory;
	private FileSystemIndex<URI, AudioMetadata> indexRepository;

	@Nullable
	public FileSystemIndex<URI, AudioMetadata> getIndexRepository() {
		initialize();
		return indexRepository;
	}

	@SneakyThrows
	@Override
	public void indexPathChanged() {
		close();
		initialize();
	}

	protected void initialize() {
		if (indexRepository != null) {
			return;
		}
		try {
			indexRepository = paramsFactory.create()
					.map(FileSystemIndexImpl::of).orElse(null);
		} catch (LockObtainFailedException e) {
			// is about the Lucene lock
			log.error("\n{}", e.getMessage());
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		if (indexRepository == null) {
			log.error("\nFailed to create FileSystemIndex<URI, AudioMetadata>!");
		} else {
			log.info("\nopened {} ({})", indexRepository.getIndexPath(),
					indexRepository.isReadOnly() ? "read only" : "writable");
		}
	}

	@Override
	public void close() throws IOException {
		if (indexRepository != null) {
			log.info("\n[close] {}", indexRepository.getIndexPath());
			indexRepository.close();
			indexRepository = null;
		} else {
			log.info(
					"\n[close] indexRepository is null (aka, was never opened or it couldn't be opened)");
		}
	}
}
