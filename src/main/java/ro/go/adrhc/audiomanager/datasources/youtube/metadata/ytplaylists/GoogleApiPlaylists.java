package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;

/**
 * <a href="https://developers.google.com/youtube/v3/docs/playlists">Playlists</a>
 * Quota impact: A call to this method has a quota cost of 1 unit.
 */
@FeignClient(name = "youtube-api.Playlists", url = "${youtube-api.playlists}")
public interface GoogleApiPlaylists {
	/**
	 * @param id The id parameter specifies a comma-separated list of one or more unique playlist item IDs.
	 */
	@GetMapping
	String list(@RequestParam Collection<String> id);
}
