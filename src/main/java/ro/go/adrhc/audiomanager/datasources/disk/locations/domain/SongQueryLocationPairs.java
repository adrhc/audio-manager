package ro.go.adrhc.audiomanager.datasources.disk.locations.domain;

import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.FoundAudio;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.util.pair.Pair;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.FoundAudioPredicates.match;

public record SongQueryLocationPairs<T extends SongQuery, L extends Location>(Map<T, L> pairs) {
	public static <T extends SongQuery, L extends Location> SongQueryLocationPairs<T, L> empty() {
		return new SongQueryLocationPairs<>(new HashMap<>());
	}

	public static <T extends SongQuery, L extends Location> SongQueryLocationPairs<T, L> of(
			Collection<T> queries, List<FoundAudio<TraceableSearchedAudio<T>, L>> bestDiskMatches) {
		return bestDiskMatches.stream()
				.filter(bdm -> queries.stream().anyMatch(query -> match(bdm, query)))
				.map(TraceableSearchedAudioLocationPairFactory::ofTraceableFoundAudio)
				.collect(SongQueryLocationPairs::empty, SongQueryLocationPairs::put,
						SongQueryLocationPairs::putAll);
	}

	public void put(Pair<T, L> pair) {
		pairs.put(pair.left(), pair.right());
	}

	public void putAll(SongQueryLocationPairs<T, L> other) {
		pairs.putAll(other.pairs);
	}

	public SongQueryLocationPairs<T, L> filter(Predicate<? super Pair<T, L>> filter) {
		return stream().filter(filter)
				.collect(SongQueryLocationPairs::empty, SongQueryLocationPairs::put,
						SongQueryLocationPairs::putAll);
	}

	public Stream<Pair<T, L>> stream() {
		return pairs.entrySet().stream()
				.map(e -> new Pair<>(e.getKey(), e.getValue()));
	}

	public <R> Stream<R> map(Function<? super Pair<T, L>, ? extends R> mapper) {
		return stream().map(mapper);
	}

	public int size() {
		return pairs.size();
	}
}
