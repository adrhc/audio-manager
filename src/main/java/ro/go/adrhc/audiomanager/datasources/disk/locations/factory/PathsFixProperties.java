package ro.go.adrhc.audiomanager.datasources.disk.locations.factory;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

import static ro.go.adrhc.util.io.FilenameUtils.removeSuffix;
import static ro.go.adrhc.util.io.PathUtils.isPathEndingWith;

@ConfigurationProperties(prefix = "paths-fix")
@Component
@Getter
@Setter
@ToString
public class PathsFixProperties {
	private String foundPlaylistSuffix;
	private String notFoundPlaylistSuffix;

	public boolean isFoundPlaylist(Path path) {
		return isPathEndingWith(foundPlaylistSuffix, path);
	}

	public boolean isNotFoundPlaylist(Path path) {
		return isPathEndingWith(notFoundPlaylistSuffix, path);
	}

	public Path getFixedPlaylistPath(Path path) {
		return Path.of(removeSuffix(path.toString(), foundPlaylistSuffix));
	}
}
