package ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain;

import ro.go.adrhc.util.ComparisonUtils;

import java.util.Map;

public record Thumbnail(String uri, Integer width, Integer height)
		implements Comparable<Thumbnail> {
	public static Thumbnail of(Map<String, Object> map) {
		return new Thumbnail((String) map.get("url"),
				(Integer) map.get("width"), (Integer) map.get("height"));
	}

	@Override
	public int compareTo(Thumbnail o) {
		int urlComparison = ComparisonUtils.compareComparable(uri, o.uri);
		if (urlComparison != 0) {
			return urlComparison;
		}
		int widthComparison = ComparisonUtils.compareComparable(width, o.width);
		if (widthComparison != 0) {
			return widthComparison;
		}
		return ComparisonUtils.compareComparable(height, o.height);
	}
}
