package ro.go.adrhc.audiomanager.datasources.youtube.metadata;

import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;

import java.util.Collection;
import java.util.List;

public interface YtMetadataProvider {
	YtLocationMetadataPairs loadMetadata(Collection<YouTubeLocation> ytLocations);

	default YouTubeMetadata loadMetadata(YouTubeLocation ytLocation) {
		return loadMetadata(List.of(ytLocation)).ytMetadata(ytLocation);
	}
}
