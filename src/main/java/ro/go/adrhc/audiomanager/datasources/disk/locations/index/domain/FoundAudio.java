package ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.LocationAware;

@AllArgsConstructor
@RequiredArgsConstructor
@Accessors(fluent = true)
@Data
@Slf4j
public class FoundAudio<SA extends SearchedAudio, L extends Location> implements LocationAware<L> {
	private final SA searchedAudio;
	private L location;
	private String durations;

	public String toString() {
		return """
				%s
				durations: %s
				Best match (by score) found at: %s"""
				.formatted(searchedAudio, durations, location);
	}
}
