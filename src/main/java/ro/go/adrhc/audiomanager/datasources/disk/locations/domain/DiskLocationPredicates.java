package ro.go.adrhc.audiomanager.datasources.disk.locations.domain;

import lombok.experimental.UtilityClass;
import ro.go.adrhc.audiomanager.domain.location.LocationAware;

import static ro.go.adrhc.audiomanager.util.PathPredicates.isFileUri;

@UtilityClass
public class DiskLocationPredicates {
	public static boolean hasFileUriRawPath(LocationAware<?> locationAware) {
		return hasFileUriRawPath(DiskLocationAccessors.diskLocation(locationAware));
	}

	public static boolean hasFileUriRawPath(DiskLocation diskLocation) {
		return isFileUri(diskLocation.rawPath());
	}
}
