package ro.go.adrhc.audiomanager.datasources.youtube.domain;

import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.LocationType;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

/**
 * It is used as a Map key hence better be immutable!
 */
@Getter
@Accessors(fluent = true)
@EqualsAndHashCode(callSuper = true)
@Slf4j
public class YouTubeLocation extends Location {
	protected static final String YT_LIKES_MUSIC_CODE = "LM";
	@NotNull
	private final YouTubeLocationType ytType;
	@NotNull
	private final String code;

	public YouTubeLocation(@NotNull YouTubeLocationType ytType, @NotNull String code) {
		super(LocationType.YOUTUBE);
		this.ytType = ytType;
		this.code = code.equalsIgnoreCase(YT_LIKES_MUSIC_CODE) ? YT_LIKES_MUSIC_CODE : code;
	}

	@Override
	public int compareTo(Location other) {
		int locationsComparison = super.compareTo(other);
		if (locationsComparison != 0) {
			return locationsComparison;
		}
		YouTubeLocation otherYtLocation = (YouTubeLocation) other;
		if (!Objects.equals(this.ytType, otherYtLocation.ytType)) {
			return this.ytType.compareTo(otherYtLocation.ytType);
		} else if (!Objects.equals(this.code, otherYtLocation.code)) {
			return this.code.compareTo(otherYtLocation.code);
		}
		return 0;
	}

	@Override
	public String mediaId() {
		return ytType.toURN(code);
	}

	@Override
	public URI uri() {
		try {
			return new URI(mediaId());
		} catch (URISyntaxException e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}

	@Override
	public String toString() {
		return ytType.toURN(code);
	}
}
