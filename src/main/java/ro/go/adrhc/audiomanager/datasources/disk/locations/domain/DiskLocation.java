package ro.go.adrhc.audiomanager.datasources.disk.locations.domain;

import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.lang.Nullable;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.NamedLocation;
import ro.go.adrhc.util.io.FilenameUtils;

import java.io.File;
import java.net.URI;
import java.nio.file.Path;
import java.util.Objects;

import static ro.go.adrhc.audiomanager.domain.location.LocationType.DISK;
import static ro.go.adrhc.util.io.FilenameUtils.filenameNoExt;

/**
 * the path can be relative to "playlists" path
 */
@Getter
@Accessors(fluent = true)
@EqualsAndHashCode(callSuper = true)
public class DiskLocation extends NamedLocation {
	@NotNull
	private final Path path;
	@NotNull
	private final String name;
	@NotNull
	private final String rawPath;

	public DiskLocation(@NotNull Path path, @NotNull String rawPath) {
		super(DISK);
		this.path = path;
		this.rawPath = rawPath;
		this.name = locationName(path);
	}

	public static String locationName(@Nullable Path path) {
		return filenameNoExt(path).map(FilenameUtils::sanitize).orElse(null);
	}

	public File toFile() {
		return path.toFile();
	}

	@Override
	public int compareTo(Location other) {
		int locationsComparison = super.compareTo(other);
		if (locationsComparison != 0) {
			return locationsComparison;
		}
		DiskLocation otherDiskLocation = (DiskLocation) other;
		if (!Objects.equals(this.name, otherDiskLocation.name)) {
			return this.name.compareTo(otherDiskLocation.name);
		} else if (!Objects.equals(this.path, otherDiskLocation.path)) {
			return this.path.compareTo(otherDiskLocation.path);
		} else if (!Objects.equals(this.rawPath, otherDiskLocation.rawPath)) {
			return this.rawPath.compareTo(otherDiskLocation.rawPath);
		}
		return 0;
	}

	@Override
	public Path mediaId() {
		return path;
	}

	@Override
	public URI uri() {
		return path.toUri();
	}

	@Override
	public String toString() {
		return "%s: %s".formatted(type(), rawPath);
	}
}
