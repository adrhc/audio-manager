package ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams;

import org.springframework.http.HttpHeaders;

import java.util.function.Predicate;

public interface HttpCommand {
	HttpHeaders filterHeaders(Predicate<String> keyPredicate);

	String payload();

	HttpHeaders httpHeaders();
}
