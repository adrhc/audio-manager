package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsearch;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams.YouTubeApiParams;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser.YtVideoSearchResultParser;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser.ytliked.YtMusicSearchResultParser;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsearch.websearch.YtMusicWebSearch;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsearch.websearch.YtVideosWebSearch;

import java.util.List;
import java.util.stream.Collectors;

import static ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtSearchQueryFactory.textQuery;
import static ro.go.adrhc.util.text.StringUtils.concat;

/**
 * Searches for YouTube videos only (i.e. videoId in JSON).
 * Web based search could gather playlistId too.
 * <p>
 * Web based search is using the json based endpoint a
 * web page is using, i.e. YouTube or YouTube-Music pages.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class YtSearchMetadataProvider {
	private final GoogleApiSearchResultParser googleApiSearchResultParser;
	private final GoogleApiSearch googleApiSearch;
	private final YtVideosWebSearch ytVideosWebSearch;
	private final YtMusicWebSearch ytMusicWebSearch;

	public List<YouTubeMetadata> findAllWebVideoMatches(SongQuery songQuery) {
		YouTubeApiParams params = ytVideoSearchParams(songQuery);
		log(params);
		String json = ytVideosWebSearch.search(params.headers(), params.payload());
		return parseWebVideos(json);
	}

	public List<YouTubeMetadata> findAllWebMusicMatches(SongQuery songQuery) {
		YouTubeApiParams params = ytMusicSearchParams(songQuery);
		log(params);
		String json = ytMusicWebSearch.search(params.headers(), params.payload());
		return parseWebMusic(json);
	}

	/**
	 * Consumes too many (i.e. 100) YouTube API credits!
	 */
	public List<YouTubeMetadata> findMany(SongQuery songQuery) {
		String json = googleApiSearch.list(10, textQuery(songQuery));
		return googleApiSearchResultParser.parse(json);
	}

	@Lookup("ytMusicSearchParams")
	protected YouTubeApiParams ytMusicSearchParams(SongQuery songQuery) {
		return null;
	}

	@Lookup("ytVideoSearchParams")
	protected YouTubeApiParams ytVideoSearchParams(SongQuery songQuery) {
		return null;
	}

	@Lookup("ytMusicSearchResultParser")
	protected YtMusicSearchResultParser ytMusicSearchResultParser(String json) {
		return null;
	}

	@Lookup
	protected YtVideoSearchResultParser ytVideoSearchResultParser(String json) {
		return null;
	}

	/**
	 * Web based search result (i.e. JSON) parser.
	 */
	private List<YouTubeMetadata> parseWebVideos(String json) {
		return ytVideoSearchResultParser(json).extractYouTubeMetadata();
	}

	private List<YouTubeMetadata> parseWebMusic(String json) {
		return ytMusicSearchResultParser(json).extractYouTubeMetadata();
	}

	private void log(YouTubeApiParams params) {
		log.debug("\nHEADERS:\n{}", params.headers().keySet().stream()
				.sorted().map(k -> STR."""
				\{k}: \{concat(", ", params.getHeader(k))}""")
				.collect(Collectors.joining("\n")));
		log.debug("\nPAYLOAD:\n{}", params.payload());
	}
}
