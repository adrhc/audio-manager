package ro.go.adrhc.audiomanager.datasources.youtube.config;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.config.FileNameAndContent;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

@Component
@ConfigurationProperties(prefix = "youtube-api-props")
@Setter
@Getter
@ToString
@Slf4j
public class YouTubeProperties {
	private String musicPlaylistCodePrefix; // VL
	private String key;
	private int maxResults;
	private int artistOrAlbumLevenshteinDistanceToTitle;
	private String musicUrl;
	private String videoUrl;
	private String playlistUrl;
	private String likedMusicCurlCommand;
	private String musicSearchCurlCommand;
	private String musicLibraryCurlCommand;
	private String videoSearchCurlCommand;
	private Path likedMusicCurlPath;
	private Path musicSearchCurlPath;
	private Path musicLibraryCurlPath;
	//	private Path musicPlContentCurlPath;
	private Path videoSearchCurlPath;
	private boolean prettyPrint;

	public String ytUrl(YouTubeLocation ytLocation) {
		return switch (ytLocation.ytType()) {
			case MUSIC -> musicUrl.concat(ytLocation.code());
			case VIDEO -> videoUrl.concat(ytLocation.code());
			case VIDEO_PLAYLIST, MUSIC_PLAYLIST -> playlistUrl.concat(ytLocation.code());
		};
	}

	@SneakyThrows
	public void setLikedMusicCurlPath(Path likedMusicCurlPath) {
		log.debug("\nCreating likedMusicCurlPath from:\n{}", likedMusicCurlPath);
		this.likedMusicCurlPath = likedMusicCurlPath;
		this.likedMusicCurlCommand = Files.readString(likedMusicCurlPath).trim();
	}

	@SneakyThrows
	public void setMusicSearchCurlPath(Path musicSearchCurlPath) {
		log.debug("\nCreating musicSearchCurlPath from:\n{}", musicSearchCurlPath);
		this.musicSearchCurlPath = musicSearchCurlPath;
		this.musicSearchCurlCommand = Files.readString(musicSearchCurlPath).trim();
	}

	@SneakyThrows
	public void setMusicLibraryCurlPath(Path musicLibraryCurlPath) {
		log.debug("\nCreating musicLibraryCurlPath from:\n{}", musicLibraryCurlPath);
		this.musicLibraryCurlPath = musicLibraryCurlPath;
		this.musicLibraryCurlCommand = Files.readString(musicLibraryCurlPath).trim();
	}

	@SneakyThrows
	public void setVideoSearchCurlPath(Path videoSearchCurlPath) {
		log.debug("\nCreating videoSearchCurlCommand from:\n{}", videoSearchCurlPath);
		this.videoSearchCurlPath = videoSearchCurlPath;
		this.videoSearchCurlCommand = Files.readString(videoSearchCurlPath).trim();
	}

	/**
	 * musicPlaylistCodePrefix is necessary only when
	 * using music.youtube.com instead of Google API.
	 */
	public String withYtMusicPrefix(String ytCode) {
		return musicPlaylistCodePrefix + ytCode;
	}

	public String withoutYtMusicPrefix(String ytMusicCode) {
		return ytMusicCode.substring(musicPlaylistCodePrefix.length());
	}

	public Map<String, Path> getFilesMap() {
		return Map.of(
				likedMusicCurlPath.getFileName().toString(), likedMusicCurlPath,
				musicSearchCurlPath.getFileName().toString(), musicSearchCurlPath,
				musicLibraryCurlPath.getFileName().toString(), musicLibraryCurlPath,
//				musicPlContentCurlPath.getFileName().toString(), musicPlContentCurlPath,
				videoSearchCurlPath.getFileName().toString(), videoSearchCurlPath
		);
	}

	public boolean hasLikedMusicCurlPath(FileNameAndContent nameAndContent) {
		return likedMusicCurlPath.getFileName().toString().equals(nameAndContent.filename());
	}
}
