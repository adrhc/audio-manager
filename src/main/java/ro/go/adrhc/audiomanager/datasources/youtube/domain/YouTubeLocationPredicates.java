package ro.go.adrhc.audiomanager.datasources.youtube.domain;

import lombok.experimental.UtilityClass;

import java.util.Collection;
import java.util.EnumSet;

import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationType.MUSIC_PLAYLIST;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationType.VIDEO_PLAYLIST;

@UtilityClass
public class YouTubeLocationPredicates {
	private static final EnumSet<YouTubeLocationType> YT_MEDIA_TYPES =
			EnumSet.of(YouTubeLocationType.VIDEO, YouTubeLocationType.MUSIC);
	private static final EnumSet<YouTubeLocationType> YT_PLAYLIST_TYPES =
			EnumSet.of(VIDEO_PLAYLIST, MUSIC_PLAYLIST);

	public static boolean isYtVideoPlaylist(YouTubeLocation ytLocation) {
		return ytLocation.ytType() == VIDEO_PLAYLIST;
	}

	public static boolean isYtMusicPlaylist(YouTubeLocation ytLocation) {
		return ytLocation.ytType() == MUSIC_PLAYLIST;
	}

	public static boolean isYtPlaylist(YouTubeLocation ytLocation) {
		return YT_PLAYLIST_TYPES.contains(ytLocation.ytType());
	}

	public static boolean isYtSongOrVideo(YouTubeLocation ytLocation) {
		return isYtSongOrVideo(ytLocation.ytType());
	}

	public static boolean isYtSongOrVideo(YouTubeLocationType ytLocationType) {
		return YT_MEDIA_TYPES.contains(ytLocationType);
	}

	public static boolean hasSongOrVideoOnly(Collection<YouTubeLocationType> ytLocationTypes) {
		return ytLocationTypes.stream().allMatch(YouTubeLocationPredicates::isYtSongOrVideo);
	}

	public static boolean hasOnlySongOrVideoLocationTypes(Collection<YouTubeLocation> ytLocations) {
		return ytLocations.stream().allMatch(YouTubeLocationPredicates::isYtSongOrVideo);
	}

	public static boolean hasOnlySongOrVideoPlLocationTypes(
			Collection<YouTubeLocation> ytLocations) {
		return ytLocations.stream().allMatch(YouTubeLocationPredicates::isYtPlaylist);
	}
}
