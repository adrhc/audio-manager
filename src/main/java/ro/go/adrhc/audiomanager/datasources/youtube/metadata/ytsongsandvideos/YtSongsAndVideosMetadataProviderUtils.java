package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos;

import org.springframework.util.Assert;

public class YtSongsAndVideosMetadataProviderUtils {
	public static void assertHasSongOrVideoOnly(boolean containsSongOrVideoOnly) {
		Assert.isTrue(containsSongOrVideoOnly, "Can't process other than MUSIC & VIDEO!");
	}
}
