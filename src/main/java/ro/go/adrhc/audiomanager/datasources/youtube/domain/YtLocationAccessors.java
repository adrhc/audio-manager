package ro.go.adrhc.audiomanager.datasources.youtube.domain;

import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.LocationAware;
import ro.go.adrhc.audiomanager.domain.playlist.entries.EntriesPairs;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toSet;

public class YtLocationAccessors {
	public static YouTubeLocation ytLocation(LocationAware<?> locationAware) {
		return (YouTubeLocation) locationAware.location();
	}

	public static YouTubeLocation ytLocation(Location location) {
		return (YouTubeLocation) location;
	}

	public static Set<YouTubeLocation> ytLocations(EntriesPairs foundEntries) {
		return ytLocations(foundEntries.entryPairs().values());
	}

	public static Set<YouTubeLocation> ytLocations(PlaylistEntries plEntries) {
		return plEntries.map(YtLocationAccessors::ytLocation).collect(toSet());
	}

	public static Set<YouTubeLocation> ytLocations(
			Collection<? extends LocationAware<?>> locationAwares) {
		return locationAwares.stream().map(YtLocationAccessors::ytLocation).collect(toSet());
	}

	public static String ytCode(LocationAware<?> locationAware) {
		return ytLocation(locationAware).code();
	}

	public static String ytCode(Location location) {
		return ytLocation(location).code();
	}

	public static Set<String> ytCodes(Collection<? extends YouTubeLocation> ytLocations) {
		return ytLocations.stream().map(YouTubeLocation::code).collect(toSet());
	}

	public static Map<YouTubeLocationType, Set<YouTubeLocation>>
	ytLocationsGroupedByType(Collection<YouTubeLocation> ytLocations) {
		return ytLocations.stream().collect(
				Collectors.groupingBy(YouTubeLocation::ytType, mapping(it -> it, toSet())));
	}

	public static Map<String, Set<YouTubeLocation>>
	ytLocationsGroupedByYtCodes(Collection<YouTubeLocation> ytLocations) {
		return ytLocations.stream().collect(
				Collectors.groupingBy(YouTubeLocation::code, mapping(it -> it, toSet())));
	}
}
