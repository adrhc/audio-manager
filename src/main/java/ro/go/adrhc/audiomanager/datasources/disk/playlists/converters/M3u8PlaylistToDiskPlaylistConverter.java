package ro.go.adrhc.audiomanager.datasources.disk.playlists.converters;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.lib.m3u8.domain.M3u8Playlist;

import java.util.List;

import static ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntriesFactory.ofEntries;

@Component
@RequiredArgsConstructor
public class M3u8PlaylistToDiskPlaylistConverter
		implements Converter<M3u8Playlist, Playlist<DiskLocation>> {
	private final M3u8RecordToPlEntryConverter toPlEntryConverter;

	@Override
	@NonNull
	public Playlist<DiskLocation> convert(@NonNull M3u8Playlist m3u8Playlist) {
		return new Playlist<>(toDiskLocation(m3u8Playlist), ofEntries(toEntries(m3u8Playlist)));
	}

	private List<PlaylistEntry> toEntries(M3u8Playlist m3u8Playlist) {
		return m3u8Playlist.map(toPlEntryConverter::convert).toList();
	}

	private DiskLocation toDiskLocation(M3u8Playlist m3u8Playlist) {
		return DiskLocationFactory.createDiskLocation(m3u8Playlist.path());
	}
}
