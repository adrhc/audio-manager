package ro.go.adrhc.audiomanager.datasources.disk.locations.index.search;

import lombok.NonNull;
import org.apache.lucene.search.Query;

import java.util.Optional;

public interface SearchedToQueryConverter<S> {
	@NonNull
	Optional<Query> convert(S searched);
}
