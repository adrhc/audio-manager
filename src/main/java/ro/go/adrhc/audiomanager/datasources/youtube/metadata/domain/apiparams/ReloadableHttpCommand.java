package ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams;

import com.rainerhahnekamp.sneakythrow.functional.SneakySupplier;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;

import java.util.function.Predicate;

@AllArgsConstructor
public class ReloadableHttpCommand<E extends Exception> implements HttpCommand, Refreshable<E> {
	private final SneakySupplier<HttpCommand, E> httpCommandSupplier;
	private HttpCommand httpCommand;

	public static <E extends Exception> ReloadableHttpCommand<E>
	of(SneakySupplier<HttpCommand, E> httpCommandSupplier) throws E {
		return new ReloadableHttpCommand<>(httpCommandSupplier, httpCommandSupplier.get());
	}

	public void refresh() throws E {
		httpCommand = httpCommandSupplier.get();
	}

	@Override
	public HttpHeaders filterHeaders(Predicate<String> keyPredicate) {
		return httpCommand.filterHeaders(keyPredicate);
	}

	@Override
	public HttpHeaders httpHeaders() {
		return httpCommand.httpHeaders();
	}

	@Override
	public String payload() {
		return httpCommand.payload();
	}
}
