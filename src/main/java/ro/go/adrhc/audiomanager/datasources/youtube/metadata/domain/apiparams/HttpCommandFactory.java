package ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams;

import lombok.RequiredArgsConstructor;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import ro.go.adrhc.audiomanager.datasources.youtube.config.YouTubeProperties;
import ro.go.adrhc.audiomanager.lib.CurlCommandLineParser;
import ro.go.adrhc.util.pair.Pair;

import java.util.Arrays;

import static java.util.function.Predicate.not;
import static org.springframework.http.HttpHeaders.ACCEPT_ENCODING;

@Configuration
@RequiredArgsConstructor
public class HttpCommandFactory {
	private final YouTubeProperties youTubeProperties;
	private final CurlCommandLineParser curlCommandLineParser;

	@Bean
	public ReloadableHttpCommand<ParseException> ytMusicSearchHttpCmdLine() throws ParseException {
		return ReloadableHttpCommand.of(() -> create(ytMusicSearchCommandLine()));
	}

	@Bean
	public ReloadableHttpCommand<ParseException> ytVideoSearchHttpCmdLine() throws ParseException {
		return ReloadableHttpCommand.of(() -> create(ytVideoSearchCommandLine()));
	}

	@Bean
	public ReloadableHttpCommand<ParseException> ytLikedMusicHttpCmdLine() throws ParseException {
		return ReloadableHttpCommand.of(() -> create(ytLikedMusicCommandLine()));
	}

	@Bean
	public ReloadableHttpCommand<ParseException> ytMusicLibraryHttpCmdLine() throws ParseException {
		return ReloadableHttpCommand.of(() -> create(ytMusicLibraryCommandLine()));
	}

	private CommandLine ytMusicSearchCommandLine() throws ParseException {
		return curlCommandLineParser.parse(youTubeProperties.getMusicSearchCurlCommand());
	}

	/**
	 * required to correctly set "browseId" and maybe the headers
	 * e.g. browseId = FEmusic_liked_playlists
	 */
	private CommandLine ytMusicLibraryCommandLine() throws ParseException {
		return curlCommandLineParser.parse(youTubeProperties.getMusicLibraryCurlCommand());
	}

	/**
	 * - required to correctly set "params" and maybe the headers
	 * - could be replaced with ytLikedMusicCommandLine if params are setting in another way
	 */
	private CommandLine ytVideoSearchCommandLine() throws ParseException {
		return curlCommandLineParser.parse(youTubeProperties.getVideoSearchCurlCommand());
	}

	/**
	 * required to correctly set "clientName", "clientVersion" and maybe the headers
	 */
	private CommandLine ytLikedMusicCommandLine() throws ParseException {
		return curlCommandLineParser.parse(youTubeProperties.getLikedMusicCurlCommand());
	}

	private static HttpCommand create(CommandLine commandLine) {
//		UriComponents uriComponents =
//				UriComponentsBuilder.fromHttpUrl(commandLine.getArgs()[1]).build();
		String[] rawHeaders = commandLine.getOptionValues('H');
		return new ImmutableHttpCommand(commandLine, toHttpHeaders(rawHeaders));
	}

	private static HttpHeaders toHttpHeaders(String[] headers) {
		HttpHeaders httpHeaders = new HttpHeaders();
		Arrays.stream(headers)
				.map(HttpCommandFactory::toPair)
				.filter(not(e -> e.left().equalsIgnoreCase(ACCEPT_ENCODING)))
				.forEach(e -> httpHeaders.add(e.left(), e.right()));
		return httpHeaders;
	}

	private static Pair<String, String> toPair(String headerAndValue) {
		String key = headerAndValue.substring(0, headerAndValue.indexOf(":"));
		String value = headerAndValue.substring(headerAndValue.indexOf(":") + 1);
		return new Pair<>(key, value.trim());
	}
}
