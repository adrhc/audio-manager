package ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain;

import lombok.experimental.UtilityClass;

import java.util.Collection;
import java.util.stream.Stream;

import static ro.go.adrhc.util.text.StringUtils.concat;

@UtilityClass
public class SongTitleFactory {
	private static final String TITLE_ARTIST = "%s - %s";

	public static String create(String songTitle, String artist) {
		return TITLE_ARTIST.formatted(songTitle, artist);
	}

	public static String create(String songTitle, Collection<String> artists) {
		return TITLE_ARTIST.formatted(songTitle, create(artists.stream().sorted()));
	}

	public static String create(String songTitle, Stream<String> artists) {
		return TITLE_ARTIST.formatted(songTitle, create(artists.sorted()));
	}

	public static String create(Stream<String> texts) {
		return concat(", ", texts);
	}
}
