package ro.go.adrhc.audiomanager.datasources.plspec;

import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpec;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.util.pair.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public record PlSpecPlEntriesPairs(Map<PlaylistSpec, PlaylistEntries> pairs) {
	public static PlSpecPlEntriesPairs empty() {
		return new PlSpecPlEntriesPairs(new HashMap<>());
	}

	public void add(PlSpecPlEntriesPairs another) {
		pairs.putAll(another.pairs);
	}

	public void put(Pair<PlaylistSpec, PlaylistEntries> pair) {
		pairs.put(pair.left(), pair.right());
	}

	public Stream<PlaylistSpec> plSpecStream() {
		return pairs.keySet().stream();
	}

	public PlaylistEntries plEntries(PlaylistSpec plSpec) {
		return pairs.get(plSpec);
	}
}
