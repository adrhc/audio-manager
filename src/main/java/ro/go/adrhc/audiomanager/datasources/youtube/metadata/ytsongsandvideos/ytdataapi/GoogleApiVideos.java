package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.ytdataapi;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collection;

/**
 * <a href="https://developers.google.com/youtube/v3/docs/videos">Videos</a>
 * list Quota impact: A call to this method has a quota cost of 1 unit.
 */
@FeignClient(name = "youtube-api.Videos", url = "${youtube-api.videos}")
public interface GoogleApiVideos {
	/**
	 * @param id The id parameter specifies a comma-separated list of the YouTube
	 *           video ID(s) for the resource(s) that are being retrieved.
	 *           In a video resource, the id property specifies the video's ID.
	 */
	@GetMapping
	String list(@RequestParam Collection<String> id);
}
