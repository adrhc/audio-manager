package ro.go.adrhc.audiomanager.datasources.disk.locations.factory;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpec;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.domain.NameAware;
import ro.go.adrhc.audiomanager.domain.location.LocationParser;
import ro.go.adrhc.audiomanager.domain.location.LocationType;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static ro.go.adrhc.audiomanager.domain.location.LocationAccessors.locationName;
import static ro.go.adrhc.audiomanager.util.PathUtils.rawToDiskPath;
import static ro.go.adrhc.util.io.FilenameUtils.addSuffix;

@RequiredArgsConstructor
@Component
@Order(2)
public class DiskLocationFactory implements LocationParser<DiskLocation> {
	private final PathsFixProperties pathsFixProperties;
	private final PlFileNamePatterns plFileNamePatterns;
	@Value("${app.liked-music-pl-name}")
	private Path likedMusicPlName;

	public static DiskLocation createPlLocation(NameAware nameAware) {
		return createPlLocation(nameAware.name());
	}

	public static DiskLocation createPlLocation(String title) {
		return new DiskLocation(Path.of(title + ".m3u8"), title);
	}

	public static DiskLocation createDiskLocation(PlaylistSpec plSpec) {
		return createDiskLocation(plSpec.filename());
	}

	public static DiskLocation createDiskLocation(String path) {
		return new DiskLocation(Path.of(path), path);
	}

	public static DiskLocation createDiskLocation(Path path) {
		if (path == null) {
			return null;
		}
		return new DiskLocation(path, path.toString());
	}

	public static List<DiskLocation> createDiskLocations(String... path) {
		return createDiskLocations(Arrays.stream(path).map(Path::of).toList());
	}

	public static List<DiskLocation> createDiskLocations(Collection<Path> paths) {
		return paths.stream().map(DiskLocationFactory::createDiskLocation).toList();
	}

	public static DiskLocation createDiskLocation(Resource resource) throws IOException {
		File file = resource.getFile();
		return new DiskLocation(file.toPath(), file.toString());
	}

	public Optional<DiskLocation> parse(String rawPath) {
		return rawToDiskPath(rawPath).map(path -> new DiskLocation(path, rawPath));
	}

	public DiskLocation createNotFoundPlLocation(Playlist<?> playlist) {
		return createDiskLocation(addSuffix(locationName(playlist) + ".m3u8",
				pathsFixProperties.getNotFoundPlaylistSuffix()));
	}

	public DiskLocation createOtherThanNotFoundPlLocation(Playlist<?> playlist) {
		return createDiskLocation(addSuffix(locationName(playlist) + ".m3u8",
				pathsFixProperties.getFoundPlaylistSuffix()));
	}

	public DiskLocation createPlCopyLocation(PlaylistEntry ytPlEntry) {
		Path playlistName = plFileNamePatterns.playlistCopyPathOf(ytPlEntry);
		return createDiskLocation(playlistName);
	}

	public DiskLocation createPlLinkLocation(PlaylistEntry ytPlEntry) {
		Path playlistName = plFileNamePatterns.playlistLinkPathOf(ytPlEntry);
		return createDiskLocation(playlistName);
	}

	public DiskLocation createLikedMusicPlLocation() {
		return createDiskLocation(likedMusicPlName);
	}

	public boolean supports(LocationType locationType) {
		return locationType == LocationType.DISK;
	}
}
