package ro.go.adrhc.audiomanager.datasources.audiofiles.history;

import org.apache.lucene.search.ScoreDoc;
import ro.go.adrhc.persistence.lucene.operations.search.ScoreDocAndValues;
import ro.go.adrhc.util.stream.StreamAware;

import java.util.stream.Stream;

public record HistoryPage<T>(ScoreDocAndValues<T> scoreDocAndValues,
		boolean pageBeforeExists, boolean pageAfterExists) implements StreamAware<T> {
	public ScoreDoc firstPosition() {
		return scoreDocAndValues.firstPosition();
	}

	public ScoreDoc lastPosition() {
		return scoreDocAndValues.lastPosition();
	}

	@Override
	public Stream<T> rawStream() {
		return scoreDocAndValues.rawStream();
	}
}
