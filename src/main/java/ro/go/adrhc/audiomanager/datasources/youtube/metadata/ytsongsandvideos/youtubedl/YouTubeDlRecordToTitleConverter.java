package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.SongTitleFactory;
import ro.go.adrhc.persistence.lucene.core.bare.token.TokenComparisonUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;

import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiomanager.util.PlaylistUtils.sanitizeSongTitle;

@RequiredArgsConstructor
public class YouTubeDlRecordToTitleConverter {
	private static final String NA = "NA";
	private final TokenComparisonUtils tokenUtils;
	private final int titlePartsLevenshteinDistance;

	@NonNull
	public String convert(@NonNull YoutubeDlRecord metadata) throws IOException {
		String rawYtTitle = findLargest(
				nullIfNA(metadata.getFulltitle()),
				nullIfNA(metadata.getTitle()),
				nullIfNA(metadata.getTrack()),
				nullIfNA(metadata.getAlt_title()));
		Assert.isTrue(hasText(nullIfNA(rawYtTitle)),
				"Song's titles shouldn't be empty or \"%s\"!\n%s".formatted(NA, this));
		// cleaning YouTube metadata; intention: don't stumble on bad YouTube metadata
		String ytTitle = sanitizeSongTitle(rawYtTitle);
		Assert.isTrue(hasText(ytTitle),
				"Sanitized title shouldn't be empty!\n%s".formatted(this));
		if (hasText(metadata.getArtist()) && differsSlightly(ytTitle, metadata.getArtist())) {
			return ytTitle;
		}
		if (hasText(nullIfNA(metadata.getArtist()))) {
			return SongTitleFactory.create(ytTitle, deduplicateCSV(metadata.getArtist()));
		}
		if (hasText(metadata.getAlbum()) && differsSlightly(ytTitle, metadata.getAlbum())) {
			return ytTitle;
		}
		if (hasText(nullIfNA(metadata.getAlbum()))) {
			return SongTitleFactory.create(ytTitle, metadata.getAlbum());
		}
		return ytTitle;
	}

	private String nullIfNA(String text) {
		return NA.equals(text) ? null : text;
	}

	private Stream<String> deduplicateCSV(String csv) {
		return Arrays.stream(csv.split("\\s*,\\s*"))
				.sorted(String::compareToIgnoreCase)
				.distinct();
	}

	private String findLargest(String... text) {
		return Arrays.stream(text).filter(Objects::nonNull)
				.reduce((largest, string) ->
						string.length() < largest.length() ? largest : string)
				.orElse(null);
	}

	private boolean differsSlightly(String containing, String text) throws IOException {
		return tokenUtils.containedDiffersSlightly(titlePartsLevenshteinDistance, containing, text);
	}
}
