package ro.go.adrhc.audiomanager.datasources.disk.locations.factory;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.nio.file.Path;
import java.text.MessageFormat;

import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YtLocationAccessors.ytCode;

@Component
@ConfigurationProperties(prefix = "playlist-file-name-patterns")
public class PlFileNamePatterns {
	private MessageFormat playlistCopy;
	private MessageFormat playlistLink;

	public void setPlaylistLink(String playlistLink) {
		this.playlistLink = new MessageFormat(playlistLink);
	}

	public void setPlaylistCopy(String playlistCopy) {
		this.playlistCopy = new MessageFormat(playlistCopy);
	}

	public Path playlistLinkPathOf(PlaylistEntry ytPlEntry) {
		return Path.of(playlistLink.format(new String[]{ytPlEntry.title(), ytCode(ytPlEntry)}));
	}

	public Path playlistCopyPathOf(PlaylistEntry ytPlEntry) {
		return Path.of(playlistCopy.format(new String[]{ytPlEntry.title(), ytCode(ytPlEntry)}));
	}

	@Override
	public String toString() {
		return "playlistCopy: " + playlistCopy +
				"\nplaylistLink: " + playlistLink;
	}
}
