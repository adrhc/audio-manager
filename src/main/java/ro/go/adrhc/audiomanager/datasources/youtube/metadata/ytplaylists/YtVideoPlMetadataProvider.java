package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists;

import com.jayway.jsonpath.JsonPath;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import org.springframework.util.Assert;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationPredicates;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YtLocationAccessors;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.LogUtils;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.YtMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationPredicates.hasOnlySongOrVideoPlLocationTypes;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YtLocationAccessors.ytLocationsGroupedByYtCodes;
import static ro.go.adrhc.audiomanager.util.PlaylistUtils.sanitizeSongTitle;

/**
 * YouTube playlist(s) metadata provider.
 */
@RequiredArgsConstructor
@Slf4j
public class YtVideoPlMetadataProvider implements YtMetadataProvider {
	private final GoogleApiPlaylists googleApiPlaylists;
	private final LogUtils logUtils;

	/**
	 * External text (i.e. YouTube) must be sanitized (i.e. sanitizeSongTitle(...)).
	 */
	@Override
	public YtLocationMetadataPairs loadMetadata(Collection<YouTubeLocation> ytPlLocations) {
		Assert.isTrue(ytPlLocations.stream().allMatch(
						YouTubeLocationPredicates::isYtVideoPlaylist),
				"Can't process other than VIDEO_PLAYLIST!");
		Map<String, Set<YouTubeLocation>> ytLocationsGroupedByYtId =
				ytLocationsGroupedByYtCodes(ytPlLocations);
		Assert.isTrue(hasOnlySongOrVideoPlLocationTypes(ytPlLocations),
				"Can't process other than VIDEO_PLAYLIST and MUSIC_PLAYLIST!");
		String json = googleApiPlaylists.list(YtLocationAccessors.ytCodes(ytPlLocations));
		JSONArray nodes = JsonPath.read(json, "$.items[*]");
		Set<YouTubeMetadata> youTubeMetadata = nodes.stream()
				.map(it -> (Map<String, ?>) it)
				// https://developers.google.com/youtube/v3/docs/playlists#resource
				.flatMap(item -> ytLocationsGroupedByYtId.get(id(item)).stream()
						.map(ytId -> YouTubeMetadata.of(ytId, sanitizeSongTitle(title(item)))))
				.collect(Collectors.toSet());
		YtLocationMetadataPairs metadata = YtLocationMetadataPairs.of(youTubeMetadata);
		logUtils.logNotFound(ytPlLocations, metadata);
		return metadata;
	}

	private String title(Map<String, ?> item) {
		return ((Map<String, ?>) item.get("snippet")).get("title").toString();
	}

	private String id(Map<String, ?> item) {
		return item.get("id").toString();
	}
}
