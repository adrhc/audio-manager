package ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain;

import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.domain.location.LocationAware;
import ro.go.adrhc.util.ComparisonUtils;

import java.util.List;
import java.util.Map;

/**
 * rawPath examples:
 * yt:http://www.youtube.com/watch?v=SAsJ_n747T4
 * youtube:http://www.youtube.com/watch?v=SAsJ_n747T4
 * ytmusic:track:SAsJ_n747T4
 * yt:video:SAsJ_n747T4
 * yt:playlist:SAsJ_n747T4
 * youtube:video:SAsJ_n747T4
 * youtube:playlist:SAsJ_n747T4
 */
public record YouTubeMetadata(YouTubeLocation location, String title, List<Thumbnail> thumbnails)
		implements LocationAware<YouTubeLocation>, Comparable<YouTubeMetadata> {
	public static YouTubeMetadata of(YouTubeLocation location, String title) {
		return new YouTubeMetadata(location, title, List.of());
	}

	public static YouTubeMetadata of(YouTubeLocation location,
			String title, List<Map<String, Object>> thumbnails) {
		return new YouTubeMetadata(location, title,
				thumbnails.stream().map(Thumbnail::of).toList());
	}

	@Override
	public int compareTo(YouTubeMetadata other) {
		int titleComparison = this.title.compareTo(other.title);
		if (titleComparison != 0) {
			return titleComparison;
		}
		int locationComparison = this.location.compareTo(other.location);
		if (locationComparison != 0) {
			return locationComparison;
		}
		return ComparisonUtils.compareLists(thumbnails, other.thumbnails);
	}
}
