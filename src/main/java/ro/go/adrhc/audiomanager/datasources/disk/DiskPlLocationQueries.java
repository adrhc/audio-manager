package ro.go.adrhc.audiomanager.datasources.disk;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.DiskPlaylistsRepository;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.PlFileContentQueries;

import java.util.List;

import static ro.go.adrhc.util.fn.SneakySupplierUtils.failToEmptyList;

@Service
@RequiredArgsConstructor
@Slf4j
public class DiskPlLocationQueries {
	private final DiskPlaylistsRepository diskPlaylistsRepository;
	private final PlFileContentQueries plFileContentQueries;

	public List<DiskLocation> findByEntry(Location entryLocation) {
		return failToEmptyList(diskPlaylistsRepository::getPlaylistsLocations)
				.stream()
				.filter(plLocation -> plFileContentQueries
						.doesPlContainLocation(entryLocation, plLocation))
				.toList();
	}
}
