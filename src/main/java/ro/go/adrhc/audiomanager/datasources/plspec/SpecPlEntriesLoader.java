package ro.go.adrhc.audiomanager.datasources.plspec;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpec;
import ro.go.adrhc.audiomanager.datasources.disk.DiskPlEntriesRepository;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.YtPlEntriesRepository;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntriesFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.util.pair.Pair;

import java.io.IOException;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;
import static ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntriesFactory.sumDiffs;
import static ro.go.adrhc.util.concurrency.FutureUtils.safelyGetAll;

@RequiredArgsConstructor
@Service
@Slf4j
public class SpecPlEntriesLoader {
	private final ExecutorService cachedThreadPool;
	private final DiskPlEntriesRepository diskPlEntriesRepository;
	private final YtPlEntriesRepository ytPlEntriesRepository;

	public PlaylistEntries loadByPlSpec(PlaylistSpec plSpec) {
		LocationPlEntriesPairs locationPlEntriesPairs = locationPlEntriesPairs(plSpec);
		return sumDiffs(locationPlEntriesPairs.plEntries());
	}

	public PlSpecPlEntriesPairs loadByPlSpecs(Collection<PlaylistSpec> plSpecs) {
		Optional<PlaylistSpec> mergedPlSpecs = PlaylistSpec.merge(plSpecs);
		return mergedPlSpecs
				.map(this::locationPlEntriesPairs)
				.map(mergePairs -> loadByPlSpecs(plSpecs, mergePairs))
				.orElseGet(PlSpecPlEntriesPairs::empty);
	}

	private PlSpecPlEntriesPairs loadByPlSpecs(Collection<PlaylistSpec> plSpecs,
			LocationPlEntriesPairs locationPlEntriesPairs) {
		return plSpecs.stream()
				.map(Pair.ofRightFactory(locationPlEntriesPairs::plEntries))
				.map(pair -> pair.transformRight(PlEntriesFactory::sumDiffs))
				.collect(PlSpecPlEntriesPairs::empty, PlSpecPlEntriesPairs::put,
						PlSpecPlEntriesPairs::add);
	}

	private LocationPlEntriesPairs locationPlEntriesPairs(PlaylistSpec plSpec) {
		return LocationPlEntriesPairs.of(locationLocationPlEntriesPairs(plSpec));
	}

	private Stream<Pair<Location, PlaylistEntries>> locationLocationPlEntriesPairs(
			PlaylistSpec plSpec) {
		return safelyGetAll(plSpec.map(this::loadContentAsync)).flatMap(Optional::stream);
	}

	private CompletableFuture<Optional<Pair<Location, PlaylistEntries>>> loadContentAsync(
			Location location) {
		return supplyAsync(() -> toLocationPlaylistEntriesPair(location), cachedThreadPool);
	}

	private Optional<Pair<Location, PlaylistEntries>> toLocationPlaylistEntriesPair(
			Location location) {
		try {
			return Optional.of(new Pair<>(location, loadContent(location)));
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			return Optional.empty();
		}
	}

	private PlaylistEntries loadContent(Location location) throws IOException {
		return switch (location.type()) {
			case DISK -> diskPlEntriesRepository.loadContent((DiskLocation) location);
			case YOUTUBE -> ytPlEntriesRepository.loadFromPlaylist((YouTubeLocation) location);
			case URI, RADIO, UNKNOWN -> PlEntriesFactory.empty();
		};
	}
}
