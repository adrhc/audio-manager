package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams.YouTubeApiParams;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser.ytliked.YtMusicSearchResultParser;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.websearch.YtMusicPlaylistContent;
import ro.go.adrhc.audiomanager.lib.paginateddata.AbstractPaginatedDataReader;
import ro.go.adrhc.audiomanager.lib.paginateddata.DataPage;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationPredicates.isYtMusicPlaylist;
import static ro.go.adrhc.util.text.StringUtils.concat;

@Service
@RequiredArgsConstructor
@Slf4j
public class YtMusicPlContentMetadataProvider extends
		AbstractPaginatedDataReader<YouTubeMetadata, Map<String, String>> {
	private static final ThreadLocal<YouTubeLocation> params = new ThreadLocal<>();
	private final YtMusicPlaylistContent ytMusicPlaylistContent;

	public Set<YouTubeMetadata> getPlContent(YouTubeLocation ytMusicPl) {
		Assert.isTrue(isYtMusicPlaylist(ytMusicPl),
				"Can't process other than MUSIC_PLAYLIST!");
		params.set(ytMusicPl);
		return getData().collect(Collectors.toSet());
	}

	protected DataPage<YouTubeMetadata, Map<String, String>>
	doGetDataPage(DataPage<YouTubeMetadata, Map<String, String>> previousChunk) {
		String json = getChunkJson(nextDataPageToken(previousChunk));
		YtMusicSearchResultParser parser = ytMusicPlSearchResultParser(json);
		Collection<YouTubeMetadata> metadataSet = parser.extractYouTubeMetadata();
		Map<String, String> nextChunkToken = parser.extractNextChunkToken();
		return new DataPage<>(metadataSet, nextChunkToken);
	}

	protected String getChunkJson(Map<String, String> nextChunkToken) {
		var ytMusicPlContentParams = ytMusicPlContentParams(params.get().code());
		log.debug("\nHEADERS:\n{}", ytMusicPlContentParams.headers().keySet()
				.stream().sorted().map(k -> STR."""
				\{k}: \{concat(", ", ytMusicPlContentParams.getHeader(k))}""")
				.collect(Collectors.joining("\n")));
		log.debug("\nPAYLOAD:\n{}", ytMusicPlContentParams.payload());
		log.debug("\nnextChunkToken: {}", nextChunkToken);
		return ytMusicPlaylistContent.getChunk(
				ytMusicPlContentParams.headers(),
				ytMusicPlContentParams.payload(),
				ytMusicPlContentParams.queryParams(nextChunkToken));
	}

	@Lookup("ytMusicPlContentParams")
	protected YouTubeApiParams ytMusicPlContentParams(String ytMusicPlCode) {
		return null;
	}

	@Lookup("ytMusicPlSearchResultParser")
	protected YtMusicSearchResultParser ytMusicPlSearchResultParser(String json) {
		return null;
	}
}
