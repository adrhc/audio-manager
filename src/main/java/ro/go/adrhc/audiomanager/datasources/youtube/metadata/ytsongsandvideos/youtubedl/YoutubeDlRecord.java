package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class YoutubeDlRecord {
	private String id;
	private String title;
	private String track;
	private String artist;
	private String album;
	private String creator;
	private String alt_title;
	private String fulltitle;
	private String uploader;
	private Set<String> tags;
}
