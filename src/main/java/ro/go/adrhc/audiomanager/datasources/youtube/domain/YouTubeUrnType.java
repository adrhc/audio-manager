package ro.go.adrhc.audiomanager.datasources.youtube.domain;

public enum YouTubeUrnType {
	yt, youtube, ytmusic
}
