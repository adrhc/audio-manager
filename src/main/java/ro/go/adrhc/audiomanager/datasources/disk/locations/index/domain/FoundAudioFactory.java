package ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain;

import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;
import static ro.go.adrhc.lib.audiometadata.domain.AudioMetadataAccessors.path;

@Component
public class FoundAudioFactory<SA extends SearchedAudio> {
	public FoundAudio<SA, DiskLocation> create(
			SA searchedAudio, AudioMetadata metadata) {
		return path(metadata)
				.map(p -> new FoundAudio<>(searchedAudio,
						createDiskLocation(p), metadata.getDurationsAsString()))
				.orElse(null);
	}
}
