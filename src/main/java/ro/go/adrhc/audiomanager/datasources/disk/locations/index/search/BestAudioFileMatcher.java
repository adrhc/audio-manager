package ro.go.adrhc.audiomanager.datasources.disk.locations.index.search;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.search.Query;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.SearchedAudio;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.core.bare.token.TokenComparisonUtils;
import ro.go.adrhc.persistence.lucene.operations.search.BestMatchingStrategy;
import ro.go.adrhc.persistence.lucene.operations.search.QueryAndScoreAndValue;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Slf4j
public class BestAudioFileMatcher<SA extends SearchedAudio>
		implements BestMatchingStrategy<AudioMetadata> {
	private final TokenComparisonUtils tokenUtils;
	private final Function<Query, SA> toSearchedAudio;

	public Optional<QueryAndScoreAndValue<AudioMetadata>>
	bestMatch(Stream<QueryAndScoreAndValue<AudioMetadata>> findings) {
		List<QueryAndScoreAndValue<AudioMetadata>> foundSongsList = findings.toList();

		// findings having the same file name as the searched one
		List<QueryAndScoreAndValue<AudioMetadata>> findingsHavingSearchedFilename
				= findingsHavingSearchedFilename(foundSongsList);

		if (findingsHavingSearchedFilename.isEmpty()) {
			// all results have different file name: simply return the 1st found
			return findingsHavingSimilarFilename(foundSongsList).findFirst();
		} else {
			// In most cases it doesn't make sense to check for the directory being the one searched
			// because the searching is supposed to happen only when its location no longer exist; at
			// this point we already identified a finding having the file name equal to the searched
			// one while having also the same path would mean that they are the same which can't be
			// possible because the finding location is supposed to exist while the searched one's not.
			return Optional.of(findingsHavingSearchedFilename.getFirst());
		}
	}

	private Stream<QueryAndScoreAndValue<AudioMetadata>>
	findingsHavingSimilarFilename(List<QueryAndScoreAndValue<AudioMetadata>> foundSongsList) {
		return foundSongsList.stream().filter(this::findingsHavingSimilarFilename);
	}

	private List<QueryAndScoreAndValue<AudioMetadata>>
	findingsHavingSearchedFilename(List<QueryAndScoreAndValue<AudioMetadata>> foundSongsList) {
		return foundSongsList.stream().filter(this::findingsHavingSearchedFilename).toList();
	}

	private boolean findingsHavingSimilarFilename(
			QueryAndScoreAndValue<AudioMetadata> foundAudio) {
		AudioMetadata metadata = foundAudio.value();
		String foundLocationName = metadata.fileNameNoExt();
		String searchedLocationName = searchedTitle(foundAudio);
		try {
			return tokenUtils.containedDiffersSlightly(
					1, foundLocationName, searchedLocationName);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			return false;
		}
	}

	private boolean findingsHavingSearchedFilename(
			QueryAndScoreAndValue<AudioMetadata> foundAudio) {
		AudioMetadata metadata = foundAudio.value();
		String foundLocationName = metadata.fileNameNoExt();
		String searchedLocationName = searchedTitle(foundAudio);
		return StringUtils.equalsIgnoreCase(foundLocationName, searchedLocationName);
	}

	private String searchedTitle(QueryAndScoreAndValue<AudioMetadata> foundAudio) {
		return toSearchedAudio.apply(foundAudio.query()).getTitle();
	}
}
