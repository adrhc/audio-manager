package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsearch;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <a href="https://developers.google.com/youtube/v3/docs/search">Search</a>
 * list Quota impact: A call to this method has a quota cost of 100 units.
 * <p>
 * Is able to return videoId only but not playlistId.
 */
@FeignClient(name = "youtube-api.Search", url = "${youtube-api.search}")
public interface GoogleApiSearch {
	@GetMapping
	String list(@RequestParam int maxResults, @RequestParam String q);
}
