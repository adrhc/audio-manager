package ro.go.adrhc.audiomanager.datasources.youtube.config;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.LogUtils;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser.YtVideoSearchResultParser;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytliked.parser.ytliked.YtMusicSearchResultParser;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.GoogleApiPlaylists;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.YtVideoPlMetadataProvider;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.websearch.YtMusicPlaylistsJsonParser;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl.YouTubeDlRecordToTitleConverter;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl.YouTubeLocationsToYtURLSetConverter;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl.YoutubeDl;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytsongsandvideos.youtubedl.YtDlSongsAndVideosMetadataProvider;
import ro.go.adrhc.audiomanager.lib.CurlCommandLineParser;
import ro.go.adrhc.persistence.lucene.core.bare.token.TokenComparisonUtils;

import java.util.concurrent.ExecutorService;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

/**
 * Role: "application setup" helper
 */
@Configuration
@RequiredArgsConstructor
public class YouTubeConfiguration {
	private final YouTubeProperties youTubeProperties;
	private final CurlCommandLineParser curlCommandLineParser;

	@Bean
	public YouTubeDlRecordToTitleConverter youTubeDlRecordToTitleConverter(
			TokenComparisonUtils tokenUtils) {
		return new YouTubeDlRecordToTitleConverter(tokenUtils,
				youTubeProperties.getArtistOrAlbumLevenshteinDistanceToTitle());
	}

	@Bean
	@Scope(SCOPE_PROTOTYPE)
	public YtMusicSearchResultParser ytMusicSearchResultParser(String json) {
		DocumentContext context = JsonPath.parse(json);
		return new YtMusicSearchResultParser(false, context);
	}

	@Bean
	@Scope(SCOPE_PROTOTYPE)
	public YtMusicSearchResultParser ytMusicPlSearchResultParser(String json) {
		DocumentContext context = JsonPath.parse(json);
		return new YtMusicSearchResultParser(true, context);
	}

	@Bean
	@Scope(SCOPE_PROTOTYPE)
	public YtMusicPlaylistsJsonParser ytMusicPlaylistsJsonParser(String json) {
		DocumentContext context = JsonPath.parse(json);
		return new YtMusicPlaylistsJsonParser(youTubeProperties::withoutYtMusicPrefix, context);
	}

	@Bean
	@Scope(SCOPE_PROTOTYPE)
	public YtVideoSearchResultParser ytVideoSearchResultParser(String json) {
		DocumentContext context = JsonPath.parse(json);
		return new YtVideoSearchResultParser(context);
	}

	@Bean
	public LogUtils ytMetadataHelper() {
		return new LogUtils(youTubeProperties::ytUrl);
	}

	@Bean
	public YouTubeLocationsToYtURLSetConverter youTubeIdsToYtURLSetConverter() {
		return new YouTubeLocationsToYtURLSetConverter(youTubeProperties::ytUrl);
	}

	@Bean
	public YtVideoPlMetadataProvider ytVideoPlMetadataProvider(
			GoogleApiPlaylists googleApiPlaylists) {
		return new YtVideoPlMetadataProvider(googleApiPlaylists, ytMetadataHelper());
	}

	@Bean
	public YtDlSongsAndVideosMetadataProvider ytAVMetadataProvider(
			ExecutorService cachedThreadPool, YoutubeDl youtubeDl,
			TokenComparisonUtils tokenUtils) {
		return new YtDlSongsAndVideosMetadataProvider(youtubeDl,
				youTubeDlRecordToTitleConverter(tokenUtils), cachedThreadPool,
				youTubeIdsToYtURLSetConverter(), ytMetadataHelper());
	}
}
