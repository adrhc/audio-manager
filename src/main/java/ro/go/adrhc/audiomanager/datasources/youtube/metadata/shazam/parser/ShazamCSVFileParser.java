package ro.go.adrhc.audiomanager.datasources.youtube.metadata.shazam.parser;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

import static ro.go.adrhc.util.stream.StreamUtils.stream;

@Component
public class ShazamCSVFileParser {
	public List<SongQuery> parse(File file) throws IOException {
		Reader in = new FileReader(file);
		Iterable<CSVRecord> records = csvFormat().parse(in);
		return stream(records).map(this::parse).toList();
	}

	private CSVFormat csvFormat() {
		return CSVFormat
				.newFormat(',').builder()
				.setIgnoreEmptyLines(true)
				.setSkipHeaderRecord(true)
				.setHeader(ShazamHeaders.class)
				.build();
	}

	private SongQuery parse(CSVRecord record) {
		String title = csvValue(record, ShazamHeaders.Title);
		String artist = csvValue(record, ShazamHeaders.Artist);
		return SongQuery.ofTitleAndArtist(title, artist);
	}

	private String csvValue(CSVRecord record, ShazamHeaders header) {
		return StringUtils.unwrap(record.get(header), "\"");
	}
}
