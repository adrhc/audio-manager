package ro.go.adrhc.audiomanager.datasources.audiofiles.index;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.config.lucene.AMIndexProperties;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.operations.params.IndexServicesParamsFactory;
import ro.go.adrhc.persistence.lucene.operations.params.IndexServicesParamsFactoryBuilder;

import java.io.IOException;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Slf4j
public class AMIndexParamsFactory {
	private final AMIndexProperties indexProperties;
	private final AppPaths appPaths;
	private final AMDiskSearchResultFilter searchResultFilter;

	public Optional<IndexServicesParamsFactory<AudioMetadata>> create() throws IOException {
		log.info("\nopening the index: {}", appPaths.getIndexPath());
		return IndexServicesParamsFactoryBuilder.of(
						AudioMetadata.class, AudioMetadataFields.class,
						appPaths.getIndexPath())
				.tokenizerProperties(indexProperties.getTokenizer())
				.searchResultFilter(searchResultFilter)
				.searchHits(indexProperties.getSearch().getMaxResultsPerSearch())
				.build(indexProperties.isReadOnly());
	}
}
