package ro.go.adrhc.audiomanager.datasources.disk.locations.domain;

import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.FoundAudio;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.util.pair.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.FoundAudioPredicates.match;

public record PlEntryLocationPairs(Map<PlaylistEntry, Location> pairs) {
	public static PlEntryLocationPairs empty() {
		return new PlEntryLocationPairs(new HashMap<>());
	}

	public static PlEntryLocationPairs of(PlaylistEntries entries,
			List<FoundAudio<TraceableSearchedAudio<PlaylistEntry>, ?>> bestMatches) {
		return bestMatches.stream()
				.filter(bm -> entries.anyMatch(e -> match(bm, e)))
				.map(TraceableSearchedAudioLocationPairFactory::ofTraceableFoundAudio)
				.collect(PlEntryLocationPairs::empty, PlEntryLocationPairs::put,
						PlEntryLocationPairs::putAll);
	}

	public PlEntryLocationPairs filter(Predicate<? super Pair<PlaylistEntry, Location>> filter) {
		return stream().filter(filter)
				.collect(PlEntryLocationPairs::empty, PlEntryLocationPairs::put,
						PlEntryLocationPairs::putAll);
	}

	public Stream<Pair<PlaylistEntry, Location>> stream() {
		return pairs.entrySet().stream()
				.map(e -> new Pair<>(e.getKey(), e.getValue()));
	}

	public <R> Stream<R> map(Function<? super Pair<PlaylistEntry, Location>, ? extends R> mapper) {
		return stream().map(mapper);
	}

	public void put(Pair<PlaylistEntry, ? extends Location> pair) {
		pairs.put(pair.left(), pair.right());
	}

	public void putAll(PlEntryLocationPairs other) {
		pairs.putAll(other.pairs);
	}

	public boolean contains(PlaylistEntry plEntry) {
		return pairs.containsKey(plEntry);
	}

	public int size() {
		return pairs.size();
	}
}
