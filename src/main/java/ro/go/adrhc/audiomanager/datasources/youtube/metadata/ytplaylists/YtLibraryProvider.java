package ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams.YouTubeApiParams;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.websearch.YtMusicLibrary;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.ytplaylists.websearch.YtMusicPlaylistsJsonParser;

import java.util.Collection;
import java.util.stream.Collectors;

import static ro.go.adrhc.util.text.StringUtils.concat;

@Service
@RequiredArgsConstructor
@Slf4j
public class YtLibraryProvider {
	private final YouTubeApiParams ytMusicLibraryParams;
	private final YtMusicLibrary ytMusicLibrary;

	public YtLocationMetadataPairs loadMusicLibrary() {
		String json = loadRawMusicLibrary();
		YtMusicPlaylistsJsonParser parser = ytMusicPlaylistsJsonParser(json);
		Collection<YouTubeMetadata> metadata = parser.parse();
		return YtLocationMetadataPairs.of(metadata);
	}

	protected String loadRawMusicLibrary() {
		log.debug("\nHEADERS:\n{}", ytMusicLibraryParams.headers().keySet()
				.stream().sorted().map(k -> STR."""
				\{k}: \{concat(", ", ytMusicLibraryParams.getHeader(k))}""")
				.collect(Collectors.joining("\n")));
		log.debug("\nPAYLOAD:\n{}", ytMusicLibraryParams.payload());
		return ytMusicLibrary.getAll(
				ytMusicLibraryParams.headers(),
				ytMusicLibraryParams.payload(),
				ytMusicLibraryParams.queryParams(null));
	}

	@Lookup
	protected YtMusicPlaylistsJsonParser ytMusicPlaylistsJsonParser(String json) {
		return null;
	}
}
