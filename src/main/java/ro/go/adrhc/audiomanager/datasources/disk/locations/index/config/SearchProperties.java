package ro.go.adrhc.audiomanager.datasources.disk.locations.index.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class SearchProperties {
	private int maxResultsPerSearch;
	private boolean resultIncludesMissingFiles;
}
