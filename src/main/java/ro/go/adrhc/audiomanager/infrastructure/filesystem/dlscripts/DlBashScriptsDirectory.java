package ro.go.adrhc.audiomanager.infrastructure.filesystem.dlscripts;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Supplier;

import static ro.go.adrhc.util.io.FilenameUtils.addSuffix;

@RequiredArgsConstructor
@Slf4j
public class DlBashScriptsDirectory {
	// configured to generate BASH script file names
	private final DlScriptsProperties dlScriptsProperties;
	private final Supplier<Path> rootPathSupplier;

	public void removeScripts(String plLocationName) {
		// removing the commented downloads scripts
		// (for songs found on disk but not with a perfect match)
		removePartitions(dlScriptsProperties.onDiskSongsScriptName(plLocationName));
		// removing the downloads scripts
		// (could be many scripting files to parallelize the downloads)
		removePartitions(dlScriptsProperties.missingSongsScriptName(plLocationName));
	}

	protected Path resolvePath(Path path) {
		return rootPathSupplier.get().resolve(path);
	}

	/**
	 * Removes the files named like playlistPath but additionally ending with a number.
	 * The numbering starts with 1 till the file to remove is missing.
	 * e.g. for favourites.m3u8 the files favourites1.m3u8, favourites2.m3u8, etc. shall be removed
	 */
	@SneakyThrows
	private void removePartitions(String fileName) {
		Path rootPath = resolvePath(Path.of(fileName));
		if (Files.deleteIfExists(rootPath)) {
			log.debug("\nremoved {}", rootPath);
		}
		int i = 1;
		boolean removed;
		do {
			Path partitionPath = resolvePath(Path.of(addSuffix(fileName, String.valueOf(i++))));
			removed = Files.deleteIfExists(partitionPath);
			if (removed) {
				log.debug("\nremoved {}", rootPath);
			}
		} while (removed);
	}
}
