package ro.go.adrhc.audiomanager.infrastructure.filesystem.dlscripts.ytdlbashscriptswriter;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.util.pair.Pair;

import java.text.MessageFormat;

import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocationAccessors.rawPath;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YtLocationAccessors.ytCode;

/**
 * Works with YouTube locations and is configured to generate BASH script snippets.
 */
@Component
@ConfigurationProperties(prefix = "youtube-download-commands")
public class YtDlCommandsProperties {
	private MessageFormat commentedScriptLine;
	private MessageFormat uncommentedScriptLine;

	/**
	 * @param pair is a [YouTube, DISK] pair
	 */
	public String commentedDownloadCommand(Pair<PlaylistEntry, PlaylistEntry> pair) {
		return commentedScriptLine.format(new Object[]{
				ytCode(pair.left()), pair.left().title(), rawPath(pair.right())});
	}

	public String downloadCommand(PlaylistEntry plEntry) {
		return uncommentedScriptLine.format(new Object[]{ytCode(plEntry),
				hasText(plEntry.title()) ? plEntry.title() : ytCode(plEntry)});
	}

	public void setCommentedScriptLinePattern(String commentedScriptLinePattern) {
		this.commentedScriptLine = new MessageFormat(commentedScriptLinePattern);
	}

	public void setUncommentedScriptLinePattern(String uncommentedScriptLinePattern) {
		this.uncommentedScriptLine = new MessageFormat(uncommentedScriptLinePattern);
	}

	@Override
	public String toString() {
		return "commentedScriptLine: " + commentedScriptLine +
				"\nuncommentedScriptLine: " + uncommentedScriptLine;
	}
}
