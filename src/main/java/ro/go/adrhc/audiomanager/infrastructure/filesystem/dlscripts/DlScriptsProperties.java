package ro.go.adrhc.audiomanager.infrastructure.filesystem.dlscripts;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

/**
 * Configured to generate BASH script file names.
 */
@Component
@ConfigurationProperties(prefix = "youtube-download-scripts")
@Setter
@Getter
public class DlScriptsProperties {
	private MessageFormat onDiskSongsScript;
	private MessageFormat missingSongsScript;

	public void setOnDiskSongsScriptPattern(String onDiskSongsScriptPattern) {
		this.onDiskSongsScript = new MessageFormat(onDiskSongsScriptPattern);
	}

	public void setMissingSongsScriptPattern(String missingSongsScriptPattern) {
		this.missingSongsScript = new MessageFormat(missingSongsScriptPattern);
	}

	/**
	 * @return e.g. {0}-youtube-dl-on-disk.sh where {0} is the file name without extension
	 */
	public String onDiskSongsScriptName(String plLocationName) {
		return onDiskSongsScript.format(new String[]{plLocationName});
	}

	/**
	 * @return e.g. {0}-youtube-dl.sh where {0} is the file name without extension
	 */
	public String missingSongsScriptName(String plLocationName) {
		return missingSongsScript.format(new String[]{plLocationName});
	}

	@Override
	public String toString() {
		return "onDiskSongsScript: " + onDiskSongsScript +
				"\nmissingSongsScript: " + missingSongsScript;
	}
}
