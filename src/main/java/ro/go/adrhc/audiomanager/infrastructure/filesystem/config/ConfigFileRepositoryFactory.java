package ro.go.adrhc.audiomanager.infrastructure.filesystem.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ro.go.adrhc.audiomanager.datasources.youtube.config.YouTubeProperties;

@Configuration
@RequiredArgsConstructor
public class ConfigFileRepositoryFactory {
	private final YouTubeProperties youTubeProperties;

	@Bean
	public ConfigFileRepository fileManager() {
		return new ConfigFileRepository(youTubeProperties.getFilesMap());
	}
}
