package ro.go.adrhc.audiomanager.infrastructure.backup;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.context.AppPaths;

import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.springframework.util.StringUtils.hasText;

/**
 * Role:
 * - "infrastructure" configuration-related helper
 * - BackupService (aka backup activity) helper
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class BackupUtils {
	private static final DateTimeFormatter DATE_PATTERN =
			DateTimeFormatter.ofPattern("yyyy.MM.dd");
	private static final Pattern PLAYLIST_NAME_PATTERN =
			Pattern.compile("(.+?)(\\.\\d{4}\\.\\d{2}\\.\\d{2})?$");
	private final AppPaths appPaths;

	public Optional<Path> relativePlaylistPathOf(Path backupPath) {
		return parsePlaylistFileNameParts(backupPath)
				.map(parts -> toRelativePlaylistPath(backupPath, parts));
	}

	public Optional<Path> relativeBackupPathOf(Path playlistPath) {
		return parsePlaylistFileNameParts(playlistPath)
				.map(parts -> toRelativeBackupPath(playlistPath, parts));
	}

	private Optional<PlaylistFileNameParts> parsePlaylistFileNameParts(Path playlistPath) {
		String fileName = playlistPath.getFileName().toString();
		String extension = FilenameUtils.getExtension(fileName);
		if (!hasText(extension)) {
			log.error(
					"\nThe playlist's name is not valid (is missing the extension)!\nplaylistPath: {}",
					playlistPath);
			return Optional.empty();
		}
		String fileNameNoExt = FilenameUtils.removeExtension(fileName);
		// check whether playlistPath is already a backup
		Matcher backupMatcher = PLAYLIST_NAME_PATTERN.matcher(fileNameNoExt);
		if (!backupMatcher.matches()) {
			log.error("\nThe playlist's name is not valid!\nplaylistPath: {}", playlistPath);
			return Optional.empty();
		}
		String playlistFileNameNoExtNoDate = backupMatcher.group(1);
		String backupDate = backupMatcher.group(2);
		if (hasText(backupDate)) {
			return Optional.of(new PlaylistFileNameParts(playlistFileNameNoExtNoDate,
					LocalDate.parse(backupDate.substring(1), DATE_PATTERN), extension));
		} else {
			return Optional.of(
					new PlaylistFileNameParts(playlistFileNameNoExtNoDate, null, extension));
		}
	}

	private Path toRelativeBackupPath(Path playlistPath, PlaylistFileNameParts parts) {
		// identify the playlist's relative path
		Path relativePlaylistPath = appPaths.relativizeToPlaylistsRoot(playlistPath);
		// identify the backup's file name
		String backupFileName = toBackupFileName(parts);
		// identify the backup's relative path, which must be similar to relativePlaylistPath
		return relativePlaylistPath.resolveSibling(backupFileName);
	}

	private Path toRelativePlaylistPath(Path backupPath, PlaylistFileNameParts parts) {
		// identify the backup's relative path
		Path relativeBackupPath = appPaths.relativizeToBackupsRoot(backupPath);
		// identify the playlist's file name
		String playlistFileName = toPlaylistFileName(parts);
		// identify the playlist's relative path, which must be similar to relativeBackupPath
		return relativeBackupPath.resolveSibling(playlistFileName);
	}

	private String toBackupFileName(PlaylistFileNameParts parts) {
		return "%s.%s.%s".formatted(parts.playlistFileNameNoExtNoDate,
				LocalDate.now().format(DATE_PATTERN), parts.extension);
	}

	private String toPlaylistFileName(PlaylistFileNameParts parts) {
		return "%s.%s".formatted(parts.playlistFileNameNoExtNoDate, parts.extension);
	}

	private record PlaylistFileNameParts(String playlistFileNameNoExtNoDate,
			LocalDate backupDate, String extension) {
	}
}
