package ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.playlistfiledirectory;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.PathsFixProperties;
import ro.go.adrhc.util.io.FileSystemUtils;
import ro.go.adrhc.util.io.SimpleDirectory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.nio.file.FileVisitOption.FOLLOW_LINKS;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static ro.go.adrhc.util.io.FilenameUtils.CASE_IGNORE_FILENAME_COMPARATOR;

@Slf4j
public class PlaylistFilesDirectory extends SimpleDirectory {
	private final PathsFixProperties pathsFixProperties;
	private final PlaylistsFilterFactory playlistsFilterFactory;

	public PlaylistFilesDirectory(FileSystemUtils fsUtils,
			Supplier<Path> rootPathSupplier, Predicate<Path> pathsFilter,
			PathsFixProperties pathsFixProperties, PlaylistsFilterFactory playlistsFilterFactory) {
		super(FOLLOW_LINKS, fsUtils, rootPathSupplier, pathsFilter);
		this.pathsFixProperties = pathsFixProperties;
		this.playlistsFilterFactory = playlistsFilterFactory;
	}

	public Path cp(Path source, Path destination) throws IOException {
		return super.cp(source, destination, REPLACE_EXISTING);
	}

	public Path mv(Path source, Path destination) throws IOException {
		return super.mv(source, destination, REPLACE_EXISTING);
	}

	/**
	 * includeFoundOnly = true: remove "found" fixes only
	 * includeFoundOnly = false: remove "not-found" fixes only
	 */
	public PathsRemovalReport rmPathFixes(boolean includeFoundOnly) throws IOException {
		List<Path> fixes = includeFoundOnly ? getFoundOnly() : getNotFoundOnly();
		return fixes.stream().reduce(PathsRemovalReport.of(includeFoundOnly),
				(report, path) -> report.append(path, deleteIfExists(path)),
				PathsRemovalReport::append);
	}

	public void applyFound(Path playlistPath) throws IOException {
		Assert.isTrue(pathsFixProperties.isFoundPlaylist(playlistPath),
				"Won't apply the fix because %s is not a \"found\" playlist!".formatted(
						playlistPath));
		Path fixedPlaylistPath = pathsFixProperties.getFixedPlaylistPath(playlistPath);
		log.debug("\nApplying {} fix to {}.", playlistPath.getFileName(),
				fixedPlaylistPath.getFileName());
		mv(playlistPath, fixedPlaylistPath);
	}

	public void applyFound() throws IOException {
		for (Path path : getFoundOnly()) {
			applyFound(path);
		}
	}

	public List<Path> getAllExceptFixes() throws IOException {
		return findByPlaylistsFilter(playlistsFilterFactory.playlistsOnly());
	}

	public List<Path> getFoundAndNotFound() throws IOException {
		return findByPlaylistsFilter(playlistsFilterFactory.foundAndNotFound());
	}

	public List<Path> getFoundOnly() throws IOException {
		return findByPlaylistsFilter(playlistsFilterFactory.foundOnly());
	}

	public List<Path> getNotFoundOnly() throws IOException {
		return findByPlaylistsFilter(playlistsFilterFactory.notFoundOnly());
	}

	private List<Path> findByPlaylistsFilter(PlaylistsFilter playlistsFilter) throws IOException {
		return transformPathStream(paths -> paths
				.filter(playlistsFilter)
				.sorted(CASE_IGNORE_FILENAME_COMPARATOR)
				.toList());
	}

	private boolean deleteIfExists(Path path) {
		try {
			return fsUtils.deleteIfExists(path);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return false;
	}
}
