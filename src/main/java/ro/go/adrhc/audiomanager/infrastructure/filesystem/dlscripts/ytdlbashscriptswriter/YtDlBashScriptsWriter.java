package ro.go.adrhc.audiomanager.infrastructure.filesystem.dlscripts.ytdlbashscriptswriter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.domain.playlist.entries.EntriesPairs;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.dlscripts.DlScriptsProperties;
import ro.go.adrhc.audiomanager.lib.PartitionedLinesFileWriter;
import ro.go.adrhc.util.pair.Pair;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class YtDlBashScriptsWriter {
	// configured to generate BASH script file names
	private final DlScriptsProperties dlScriptsProperties;
	// works with YouTube locations and is configured to generate BASH script snippets
	private final YtDlCommandsProperties ytDlCmdsProperties;
	private final PartitionedLinesFileWriter partitionedLinesFileWriter;

	/**
	 * pair.key = not on disk yt-entries
	 * pair.value = pairs of yt entry + the corresponding disk one
	 */
	public void write(int partitionsSize, String locationName,
			Pair<PlaylistEntries, EntriesPairs> pair) throws IOException {
		partitionedLinesFileWriter.write(
				dlScriptsProperties.missingSongsScriptName(locationName),
				partitionsSize, downloadCommands(pair.left()));
		partitionedLinesFileWriter.write(
				dlScriptsProperties.onDiskSongsScriptName(locationName),
				0, commentedDownloadCommands(pair.right()));
	}

	/**
	 * @return the commented youtube-dl bash commands for already on-disk YouTube items
	 */
	private List<String>
	commentedDownloadCommands(EntriesPairs ytDiskPairs) {
		return ytDiskPairs
				.map(ytDlCmdsProperties::commentedDownloadCommand)
				.sorted().toList();
	}

	/**
	 * @return the youtube-dl bash commands to get the songs not present on disk
	 */
	private List<String>
	downloadCommands(PlaylistEntries noDiskYtEntries) {
		return noDiskYtEntries
				.map(ytDlCmdsProperties::downloadCommand)
				.sorted().toList();
	}
}
