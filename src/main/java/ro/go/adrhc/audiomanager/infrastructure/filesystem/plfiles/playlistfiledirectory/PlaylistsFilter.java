package ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.playlistfiledirectory;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.PathsFixProperties;

import java.nio.file.Path;
import java.util.function.Predicate;

@RequiredArgsConstructor
@Accessors(fluent = true)
@Setter
public class PlaylistsFilter implements Predicate<Path> {
	private final PathsFixProperties pathsFixProperties;
	private boolean includeNotFound;
	private boolean includeFound;
	private boolean includePlaylists;

	@Override
	public boolean test(Path path) {
		boolean isF = pathsFixProperties.isFoundPlaylist(path);
		boolean isNF = pathsFixProperties.isNotFoundPlaylist(path);
		return (includeNotFound && isNF) ||
				(includeFound && isF) ||
				(includePlaylists && !isF && !isNF);
	}
}
