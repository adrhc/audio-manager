package ro.go.adrhc.audiomanager.infrastructure.filesystem.config;

import lombok.RequiredArgsConstructor;
import ro.go.adrhc.audiomanager.datasources.youtube.config.YouTubeProperties;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@RequiredArgsConstructor
public class ConfigFileRepository {
	private final Map<String, Path> configFiles;

	public static ConfigFileRepository of(YouTubeProperties ytProperties) {
		return new ConfigFileRepository(ytProperties.getFilesMap());
	}

	public void updateContent(FileNameAndContent nameAndContent) throws IOException {
		Path path = configFiles.get(nameAndContent.filename());
		Files.writeString(path, nameAndContent.content(), StandardCharsets.UTF_8,
				StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
	}

	public Optional<FileNameAndContent> getByFileName(String filename) throws IOException {
		Path path = configFiles.get(filename);
		if (path == null) {
			return Optional.empty();
		}
		String content = Files.readString(path);
		return Optional.of(new FileNameAndContent(filename, content));
	}

	public Set<String> getFileNames() {
		return configFiles.keySet();
	}
}
