package ro.go.adrhc.audiomanager.infrastructure.backup;

import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;

import java.nio.file.Path;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;

public record RestorationResult(DiskLocation backup, DiskLocation restored) {
	public static RestorationResult of(Path backup, Path restored) {
		return new RestorationResult(createDiskLocation(backup), createDiskLocation(restored));
	}

	public static RestorationResult ofKey(Path backup) {
		return of(backup, null);
	}

	public boolean isRestoredMissing() {
		return restored == null;
	}
}
