package ro.go.adrhc.audiomanager.infrastructure.backup;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.BackupFilesDirectory;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.playlistfiledirectory.PlaylistFilesDirectory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static ro.go.adrhc.util.fn.SneakyBiFunctionUtils.toOptional;

@Service
@RequiredArgsConstructor
@Slf4j
public class BackupService {
	private final PlaylistFilesDirectory playlistsFilesRepository;
	private final BackupFilesDirectory backupFilesDirectory;
	private final AppPaths appPaths;
	private final BackupUtils backupUtils;

	public List<BackupResult> backup() throws IOException {
		return backupFilesDirectory.transformPathStream(this::doBackup);
	}

	/**
	 * Computing the backup's relative path starting from the playlistPath then perform the actual backup.
	 *
	 * @return the created backup location
	 */
	public Optional<BackupResult> backup(DiskLocation plLocation) {
		return backup(plLocation.path());
	}

	public List<RestorationResult> restore() throws IOException {
		return backupFilesDirectory.transformPathStream(this::doRestore);
	}

	/**
	 * Computing the playlist's relative path starting from the backupPath then perform the actual restoration.
	 *
	 * @return the created restored location
	 */
	public Optional<RestorationResult> restore(DiskLocation backupLocation) {
		return restore(backupLocation.path());
	}

	private List<BackupResult> doBackup(Stream<Path> playlistPaths) {
		return playlistPaths.sorted()
				.map(this::backup)
				.flatMap(Optional::stream)
				.toList();
	}

	private List<RestorationResult> doRestore(Stream<Path> backupPaths) {
		return backupPaths.sorted()
				.map(this::restore)
				.flatMap(Optional::stream)
				.toList();
	}

	/**
	 * @return the absolute backup path
	 */
	private Optional<BackupResult> backup(Path plPath) {
		return backupUtils.relativeBackupPathOf(plPath)
				.map(relativeBackupPath -> {
					// identify the playlist's full path; received playlistPath could be relative or absolute
					Path fullPlaylistPath = appPaths.resolvePlaylistPath(plPath);
					// performing the actual backup
					return toOptional(backupFilesDirectory::cp, fullPlaylistPath,
							relativeBackupPath)
							.map(b -> BackupResult.of(plPath, b)).orElse(
									BackupResult.ofKey(plPath));
				});
	}

	/**
	 * @return the absolute restored path
	 */
	private Optional<RestorationResult> restore(Path backupPath) {
		return backupUtils.relativePlaylistPathOf(backupPath)
				.map(relativePlaylistPath -> {
					// identify the backup's full path; received backupPath could be relative or absolute
					Path fullBackupPath = appPaths.resolveBackupPath(backupPath);
					// performing the actual restoration
					return toOptional(playlistsFilesRepository::cp, fullBackupPath,
							relativePlaylistPath)
							.map(r -> RestorationResult.of(backupPath, r)).orElse(
									RestorationResult.ofKey(backupPath));
				});
	}
}