package ro.go.adrhc.audiomanager.infrastructure.filesystem.config;

public record FileNameAndContent(String filename, String content) {
}
