package ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.playlistfiledirectory;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.PathsFixProperties;

@Component
@RequiredArgsConstructor
public class PlaylistsFilterFactory {
	private final PathsFixProperties pathsFixProperties;

	public PlaylistsFilter foundOnly() {
		return newPlaylistsFilter().includeFound(true);
	}

	public PlaylistsFilter notFoundOnly() {
		return newPlaylistsFilter().includeNotFound(true);
	}

	public PlaylistsFilter foundAndNotFound() {
		return newPlaylistsFilter().includeFound(true).includeNotFound(true);
	}

	public PlaylistsFilter playlistsOnly() {
		return newPlaylistsFilter().includePlaylists(true);
	}

	private PlaylistsFilter newPlaylistsFilter() {
		return new PlaylistsFilter(pathsFixProperties);
	}
}
