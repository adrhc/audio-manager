package ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.playlistfiledirectory;

import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static ro.go.adrhc.util.ComparisonUtils.compareComparable;

public record PathsRemovalReport(boolean includeFoundOnly,
		List<PathRemovalStatus> pathRemovalStatuses) {
	private static final MessageFormat NO_PATH_FIXES_MESSAGE =
			new MessageFormat("No \"{0}\" paths-fix was found! (hence none was removed)");

	public static PathsRemovalReport of(boolean includeFoundOnly) {
		return new PathsRemovalReport(includeFoundOnly, new ArrayList<>());
	}

	public PathsRemovalReport append(Path playlist, boolean removed) {
		pathRemovalStatuses.add(new PathRemovalStatus(playlist, removed));
		return this;
	}

	public PathsRemovalReport append(PathsRemovalReport report) {
		pathRemovalStatuses.addAll(report.pathRemovalStatuses);
		return this;
	}

	@Override
	public String toString() {
		if (pathRemovalStatuses.isEmpty()) {
			return NO_PATH_FIXES_MESSAGE.format(
					new String[]{includeFoundOnly ? "found" : "not found"});
		}
		return pathRemovalStatuses.stream().sorted()
				.map(PathRemovalStatus::toString)
				.collect(Collectors.joining("\n"));
	}

	public record PathRemovalStatus(Path playlist,
			boolean removed) implements Comparable<PathRemovalStatus> {
		@Override
		public int compareTo(final PathRemovalStatus another) {
			int playlistComparison = compareComparable(playlist, another.playlist);
			if (playlistComparison != 0) {
				return playlistComparison;
			} else {
				return removed ? 1 : -1;
			}
		}

		@Override
		public String toString() {
			return removed ? "removed " + playlist : "couldn't remove " + playlist;
		}
	}
}
