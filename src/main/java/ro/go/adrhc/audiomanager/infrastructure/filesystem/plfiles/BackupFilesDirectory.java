package ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles;

import ro.go.adrhc.util.io.FileSystemUtils;
import ro.go.adrhc.util.io.SimpleDirectory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.nio.file.FileVisitOption.FOLLOW_LINKS;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static ro.go.adrhc.util.io.FilenameUtils.CASE_IGNORE_FILENAME_COMPARATOR;

public class BackupFilesDirectory extends SimpleDirectory {
	public BackupFilesDirectory(FileSystemUtils fsUtils,
			Supplier<Path> rootPathSupplier, Predicate<Path> pathsFilter) {
		super(FOLLOW_LINKS, fsUtils, rootPathSupplier, pathsFilter);
	}

	public List<Path> getBackups() throws IOException {
		return transformPathStream(paths -> paths
				.sorted(CASE_IGNORE_FILENAME_COMPARATOR)
				.toList());
	}

	public Path cp(Path source, Path destination) throws IOException {
		return super.cp(source, destination, REPLACE_EXISTING);
	}
}
