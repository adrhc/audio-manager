package ro.go.adrhc.audiomanager.infrastructure.filesystem;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.apiparams.YouTubeApiParams;

import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.file.StandardOpenOption.*;

@Component
@RequiredArgsConstructor
@Slf4j
public class YtMusicAuthFileRepository {
	private final Path YT_MUSIC_AUTH = Path.of("/var/lib/mopidy/.config/auth.json");
	private final ObjectMapper mapper;
	private final YouTubeApiParams mopidyYouTubeAuth;

	/**
	 * Write YouTube Music Plugin authentication file.
	 */
	public Path writeYouTubeMusicAuth() {
		return writeYouTubeMusicAuth(YT_MUSIC_AUTH);
	}

	@SneakyThrows
	public Path writeYouTubeMusicAuth(Path ytMusicAuthPath) {
		ytMusicAuthPath = ytMusicAuthPath == null ||
				ytMusicAuthPath.toString().isBlank() ? YT_MUSIC_AUTH : ytMusicAuthPath;
		log.info("\nwriting YouTube Music authorization to {}", ytMusicAuthPath);
		return Files.writeString(ytMusicAuthPath,
				getYouTubeMusicAuth(), CREATE, WRITE, TRUNCATE_EXISTING);
	}

	@SneakyThrows
	public String getYouTubeMusicAuth() {
		return mapper.writerWithDefaultPrettyPrinter()
				.writeValueAsString(mopidyYouTubeAuth.headers().toSingleValueMap());
	}
}
