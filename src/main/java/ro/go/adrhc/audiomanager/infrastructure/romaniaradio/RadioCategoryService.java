package ro.go.adrhc.audiomanager.infrastructure.romaniaradio;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@ConditionalOnProperty("romaniaradio.enabled")
@Service
@Slf4j
public class RadioCategoryService {
	public Map<String, List<String>> extractCategoriesAndSubcategories() {
		Map<String, List<String>> categories = new HashMap<>();

		try {
			String url = "http://www.romaniaradio.ro/";
			Document doc = Jsoup.connect(url).get();

			// Main categories identified by CSS selector 'font[color="#aa0000"] i'
			Elements mainCategoryElements = doc.select("font[color=\"#aa0000\"] i");

			for (Element mainCategoryElement : mainCategoryElements) {
				String category = mainCategoryElement.text().trim();
				List<String> subcategories = new ArrayList<>();

				// Traverse upwards to find the parent div containing both main category and subcategories
				Element parentDiv = mainCategoryElement.parent();
				while (parentDiv != null && !parentDiv.tagName().equalsIgnoreCase("div")) {
					parentDiv = parentDiv.parent();
				}

				if (parentDiv != null) {
					// Subcategories are typically found in the next sibling div
					Element subcategoriesDiv = parentDiv.nextElementSibling();
					if (subcategoriesDiv != null) {
						Elements subcategoryElements = subcategoriesDiv.select("li");
						for (Element subcategoryElement : subcategoryElements) {
							String subcategory = subcategoryElement.text().trim();
							subcategories.add(subcategory);
						}
					}
				}

				categories.put(category, subcategories);
			}

		} catch (IOException e) {
			log.error("Error while fetching categories and subcategories from '{}': {}",
					"http://www.romaniaradio.ro/", e.getMessage());
			// Return empty map if error occurs during initial fetching
			return Collections.emptyMap();
		}

		return categories;
	}
}
