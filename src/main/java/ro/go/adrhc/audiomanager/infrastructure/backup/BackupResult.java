package ro.go.adrhc.audiomanager.infrastructure.backup;

import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;

import java.nio.file.Path;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;

public record BackupResult(DiskLocation path, DiskLocation backup) {
	public static BackupResult of(Path backup, Path restored) {
		return new BackupResult(createDiskLocation(backup), createDiskLocation(restored));
	}

	public static BackupResult ofKey(Path path) {
		return of(path, null);
	}

	public boolean isBackupMissing() {
		return backup == null;
	}
}
