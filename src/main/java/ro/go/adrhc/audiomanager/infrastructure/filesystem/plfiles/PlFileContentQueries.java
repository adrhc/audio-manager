package ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.UnknownLocation;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static ro.go.adrhc.audiomanager.util.FileUtils.doesFileContainAny;

@Service
@RequiredArgsConstructor
@Slf4j
public class PlFileContentQueries {
	public Boolean doesPlContainLocation(Location entryLocation, DiskLocation plLocation) {
		Set<String> text;
		if (entryLocation instanceof DiskLocation disk) {
			text = new HashSet<>(List.of(entryLocation.uri().toString(),
					disk.path().toString(), disk.rawPath()));
		} else if (entryLocation instanceof UnknownLocation unkw) {
			text = Set.of(unkw.raw());
		} else {
			text = Set.of(entryLocation.uri().toString());
		}
		return doesFileContainAny(plLocation.toFile(), text)
				.orElseGet(() -> {
					log.warn("Failed to search for:\n{}\nin {}!",
							entryLocation.uri(), plLocation.path());
					return false;
				});
	}
}
