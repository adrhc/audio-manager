package ro.go.adrhc.audiomanager.domain.playlist.entries;

import ro.go.adrhc.audiomanager.datasources.disk.PlEntrySongQuery;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.PlEntryLocationPairs;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.SongQueryLocationPairs;
import ro.go.adrhc.audiomanager.datasources.domain.SongQuery;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.LocationPredicates;
import ro.go.adrhc.util.pair.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * @param entryPairs is a collection of PlaylistEntry pairs,
 *                   e.g. [YouTube, Disk] or [searchedEntry, foundEntry], etc
 */
public record EntriesPairs(Map<PlaylistEntry, PlaylistEntry> entryPairs) {
	public static EntriesPairs empty() {
		return new EntriesPairs(new HashMap<>());
	}

	public static EntriesPairs of(PlaylistEntries searched, PlaylistEntries found) {
		return searched.stream()
				.flatMap(s -> found
						.filter(f -> LocationPredicates.areEquivalent(s, f))
						.map(f -> new Pair<>(s, f)))
				.collect(EntriesPairs::empty, EntriesPairs::put, EntriesPairs::putAll);
	}

	public static EntriesPairs of(PlEntryLocationPairs pairs) {
		return pairs
				.map(pair -> new Pair<>(pair.left(), foundEntryByPlEntry(pair)))
				.collect(EntriesPairs::empty, EntriesPairs::put, EntriesPairs::putAll);
	}

	public static EntriesPairs of(SongQueryLocationPairs<PlEntrySongQuery, ?> pairs) {
		return pairs
				.map(pair -> new Pair<>(pair.left().plEntry(), foundEntryBySongQuery(pair)))
				.collect(EntriesPairs::empty, EntriesPairs::put, EntriesPairs::putAll);
	}

	public void put(Pair<PlaylistEntry, PlaylistEntry> pair) {
		entryPairs.put(pair.left(), pair.right());
	}

	public void putAll(EntriesPairs pairs) {
		entryPairs.putAll(pairs.entryPairs);
	}

	public boolean isEmpty() {
		return entryPairs.isEmpty();
	}

	public int size() {
		return entryPairs.size();
	}

	public boolean contains(PlaylistEntry left) {
		return entryPairs.containsKey(left);
	}

	public <R> Stream<R> map(Function<? super Pair<PlaylistEntry, PlaylistEntry>, R> mapper) {
		return entryPairs.entrySet().stream().map(Pair::ofMapEntry).map(mapper);
	}

	public Optional<PlaylistEntry> getPair(PlaylistEntry left) {
		return Optional.ofNullable(entryPairs.get(left));
	}

	/**
	 * @param leftEntry is the entryPairs Map's key
	 * @return the title of rightEntry (i.e. entryPairs Map's value)
	 */
	public Optional<String> title(PlaylistEntry leftEntry) {
		return Optional.ofNullable(entryPairs.get(leftEntry)).map(PlaylistEntry::title);
	}

	private static PlaylistEntry foundEntryByPlEntry(Pair<PlaylistEntry, ? extends Location> pair) {
		return PlaylistEntry.of(pair.right(), pair.left().title());
	}

	private static PlaylistEntry foundEntryBySongQuery(
			Pair<? extends SongQuery, ? extends Location> pair) {
		return PlaylistEntry.of(pair.right(), pair.left().title());
	}
}
