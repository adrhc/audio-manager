package ro.go.adrhc.audiomanager.domain.playlist.entries;

import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocationContent;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YtLocationMetadataPairs;
import ro.go.adrhc.audiomanager.domain.playlist.entries.filters.LocationTypeEntriesMap;

import java.util.*;
import java.util.stream.Stream;

import static ro.go.adrhc.util.fn.BiConsumerUtils.toBiOper;

public class PlEntriesFactory {
	public static PlaylistEntries of(LocationTypeEntriesMap locationTypeEntriesMap) {
		return locationTypeEntriesMap.plEntriesStream()
				.collect(PlEntriesFactory::empty, PlaylistEntries::addAll, PlaylistEntries::addAll);
	}

	public static PlaylistEntries of(YtLocationMetadataPairs ytLocationMetadataPairs) {
		return ofYtMetadata(ytLocationMetadataPairs.metadataMap().values());
	}

	public static PlaylistEntries sumDiffs(Collection<PlaylistEntries> playlistEntries) {
		return playlistEntries.stream().reduce(PlEntriesFactory.empty(),
				PlaylistEntries::plusDiffs, toBiOper(PlaylistEntries::addAll));
	}

	public static PlaylistEntries ofYtMetadata(Collection<YouTubeMetadata> youTubeMetadata) {
		return youTubeMetadata.stream()
				.map(PlEntryFactory::of)
				.collect(PlEntriesFactory::empty,
						PlaylistEntries::add,
						PlaylistEntries::addAll);
	}

	public static PlaylistEntries ofEntries(Collection<PlaylistEntry> entries) {
		return new PlaylistEntries(new ArrayList<>(entries));
	}

	public static PlaylistEntries ofEntries(Stream<PlaylistEntry> entries) {
		return entries.collect(PlEntriesFactory::empty,
				PlaylistEntries::add, PlaylistEntries::addAll);
	}

	public static PlaylistEntries of(Map<?, Optional<PlaylistEntry>> entries) {
		return ofEntries(entries.values().stream().flatMap(Optional::stream).toList());
	}

	public static PlaylistEntries ofEntries(PlaylistEntry... entries) {
		return ofEntries(Arrays.asList(entries));
	}

	public static PlaylistEntries of(DiskLocationContent diskLocationsContent) {
		return ofDiskLocations(diskLocationsContent.locations());
	}

	public static PlaylistEntries ofDiskLocations(DiskLocation... diskLocations) {
		return ofDiskLocations(Arrays.stream(diskLocations).toList());
	}

	public static PlaylistEntries ofDiskLocations(
			Collection<? extends DiskLocation> diskLocations) {
		return diskLocations.stream()
				.map(PlEntryFactory::ofNamedLocation)
				.collect(PlEntriesFactory::empty, PlaylistEntries::add, PlaylistEntries::addAll);
	}

	public static PlaylistEntries of(PlaylistEntry entry) {
		PlaylistEntries playlistEntries = empty();
		playlistEntries.add(entry);
		return playlistEntries;
	}

	public static PlaylistEntries empty() {
		return new PlaylistEntries(new ArrayList<>());
	}
}
