package ro.go.adrhc.audiomanager.domain.location;

import java.util.Optional;

public interface LocationParser<L extends Location> {
	Optional<L> parse(String text);

	boolean supports(LocationType locationType);
}
