package ro.go.adrhc.audiomanager.domain.location;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.Optional;

@Component
@Slf4j
@Order(3)
public class UriLocationParser implements LocationParser<UriLocation> {
	public static Optional<URI> parseURI(String text) {
		try {
			URI uri = new URI(text);
			return Optional.of(uri);
		} catch (Exception e) {
			log.debug("\n{} is not an URI!", text);
		}
		return Optional.empty();
	}

	@Override
	public Optional<UriLocation> parse(String text) {
		return parseURI(text).map(UriLocation::new);
	}

	@Override
	public boolean supports(LocationType locationType) {
		return locationType == LocationType.URI;
	}
}
