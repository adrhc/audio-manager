package ro.go.adrhc.audiomanager.domain.playlist.entries.filters;

import ro.go.adrhc.audiomanager.domain.location.LocationAccessors;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntriesFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;

public class UniqueEntriesPerMediaIdFilter {
	public static final UniqueEntriesPerMediaIdFilter
			INSTANCE = new UniqueEntriesPerMediaIdFilter();

	/**
	 * @return uniques per mediaId but favours those having a title (1st found is picked)
	 */
	public PlaylistEntries filter(PlaylistEntries entries) {
		return uniquesByProperty(LocationAccessors::mediaId, entries);
	}

	private <P> PlaylistEntries uniquesByProperty(
			Function<PlaylistEntry, P> accessor, PlaylistEntries entries) {
		return entries.stream().collect(Collectors.collectingAndThen(
				groupingBy(accessor, reducing(this::firstNotMissingTitleOtherwiseFirstEvaluated)),
				PlEntriesFactory::of));
	}

	private PlaylistEntry firstNotMissingTitleOtherwiseFirstEvaluated(PlaylistEntry e1,
			PlaylistEntry e2) {
		return e1.hasTitle() ? e1 : e2.hasTitle() ? e2 : e1;
	}
}
