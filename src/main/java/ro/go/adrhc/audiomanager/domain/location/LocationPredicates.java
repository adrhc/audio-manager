package ro.go.adrhc.audiomanager.domain.location;

import lombok.experimental.UtilityClass;

import static ro.go.adrhc.audiomanager.domain.location.LocationAccessors.locationType;

@UtilityClass
public class LocationPredicates {
	public static boolean areEquivalent(Location location, LocationAware<?> locationAware) {
		return location.isEquivalent(locationAware.location());
	}

	public static boolean areEquivalent(
			LocationAware<?> locationAware1, LocationAware<?> locationAware2) {
		return areEquivalent(locationAware1.location(), locationAware2);
	}

	public static <L extends Location> boolean hasDiskLocation(LocationAware<L> locationAware) {
		return locationType(locationAware).equals(LocationType.DISK);
	}

	public static <L extends Location> boolean hasRadioLocation(LocationAware<L> locationAware) {
		return locationType(locationAware).equals(LocationType.RADIO);
	}

	public static <L extends Location> boolean hasYtLocation(LocationAware<L> locationAware) {
		return locationType(locationAware).equals(LocationType.YOUTUBE);
	}

	public static <L extends Location> boolean hasUnknownLocation(LocationAware<L> locationAware) {
		return locationType(locationAware) == LocationType.UNKNOWN;
	}

	public static boolean isUnknownLocation(Location location) {
		return location.type() == LocationType.UNKNOWN;
	}

	public static <L extends Location> boolean hasLocation(LocationAware<L> locationAware) {
		return locationAware.location() != null;
	}
}
