package ro.go.adrhc.audiomanager.domain.location;

import ro.go.adrhc.audiomanager.domain.NameAware;

import java.net.URI;

public class LocationAccessors {
	public static Object mediaId(LocationAware<?> locationAware) {
		return locationAware.location().mediaId();
	}

	public static URI locationURI(LocationAware<?> locationAware) {
		return locationAware.location().uri();
	}

	public static String locationName(LocationAware<?> locationAware) {
		return locationName(locationAware.location());
	}

	public static String locationName(Location location) {
		if (location instanceof NameAware nameAware) {
			return nameAware.name();
		}
		return null;
	}

	public static String unknownRaw(Location location) {
		if (location instanceof UnknownLocation unk) {
			return unk.raw();
		}
		return null;
	}

	public static LocationType locationType(LocationAware<?> locationAware) {
		return locationAware.location().type();
	}
}
