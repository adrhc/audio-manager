package ro.go.adrhc.audiomanager.domain.playlist;

public enum PlaylistType {
	PLAYLIST, BACKUP, FOUND, NOT_FOUND;

	@Override
	public String toString() {
		return switch (this) {
			case PLAYLIST -> "playlist";
			case BACKUP -> "backup";
			case FOUND -> "\"found\"";
			case NOT_FOUND -> "\"not found\"";
		};
	}
}
