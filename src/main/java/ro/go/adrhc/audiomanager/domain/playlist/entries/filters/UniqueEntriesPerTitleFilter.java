package ro.go.adrhc.audiomanager.domain.playlist.entries.filters;

import ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntriesFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.reducing;
import static ro.go.adrhc.audiomanager.domain.location.LocationPredicates.hasDiskLocation;
import static ro.go.adrhc.util.fn.BiConsumerUtils.toBiOper;

public class UniqueEntriesPerTitleFilter {
	/**
	 * @return uniques per title plus all not having the title set; it favours
	 * the disk located ones when picking between more having same title
	 */
	public PlaylistEntries filter(PlaylistEntries plEntries) {
		Map<Boolean, PlaylistEntries> groupsByTitlePresence =
				groupByTitlePresence(plEntries);
		PlaylistEntries notEmptyTitleUniques =
				notEmptyTitleUniques(groupsByTitlePresence.get(Boolean.TRUE));
		PlaylistEntries uniques = groupsByTitlePresence.get(Boolean.FALSE);
		uniques.addAll(notEmptyTitleUniques);
		return uniques;
	}

	private Map<Boolean, PlaylistEntries> groupByTitlePresence(PlaylistEntries playlistEntries) {
		return playlistEntries.stream().collect(
				partitioningBy(PlaylistEntry::hasTitle,
						Collector.of(PlEntriesFactory::empty,
								PlaylistEntries::add,
								toBiOper(PlaylistEntries::addAll))));
	}

	/**
	 * It favours the disk located ones when picking between more having same title.
	 */
	private PlaylistEntries notEmptyTitleUniques(PlaylistEntries notEmptyTitleEntries) {
		return notEmptyTitleEntries.stream().collect(
				Collectors.collectingAndThen(
						Collectors.groupingBy(PlaylistEntry::title,
								reducing(this::firstDiskLocatedOtherwiseFirstEvaluated)),
						PlEntriesFactory::of));
	}

	private PlaylistEntry firstDiskLocatedOtherwiseFirstEvaluated(PlaylistEntry e1,
			PlaylistEntry e2) {
		return hasDiskLocation(e1) ? e1 : hasDiskLocation(e2) ? e2 : e1;
	}
}
