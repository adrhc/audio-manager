package ro.go.adrhc.audiomanager.domain.location;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.net.URI;

@AllArgsConstructor
@Accessors(fluent = true)
@Getter
@EqualsAndHashCode
public abstract class Location implements Comparable<Location> {
	@NotNull
	private final LocationType type;

	@Override
	public String toString() {
		return type.name();
	}

	@Override
	public int compareTo(Location another) {
		return this.type.compareTo(another.type);
	}

	public boolean isUnknown() {
		return type == LocationType.UNKNOWN;
	}

	/**
	 * The location they refer to is the same even when the Location objects are not equal, e.g.
	 * DiskLocation with rawPath file:////song1i.mp3
	 * differs from
	 * DiskLocation with rawPath /song1i.mp3
	 * but they point to the same location.
	 */
	public boolean isEquivalent(Location location) {
		return hasLocationType(location.type) && hasMediaId(location.mediaId());
	}

	public boolean hasLocationType(LocationType locationType) {
		return type == locationType;
	}

	public boolean hasMediaId(Object mediaId) {
		return mediaId().equals(mediaId);
	}

	/**
	 * Unique media identifier pertaining to the media stored at current location.
	 * Can be used to filter unique-media locations.
	 */
	public abstract Comparable<?> mediaId();

	public abstract URI uri();
}
