package ro.go.adrhc.audiomanager.domain.location;

import com.fasterxml.jackson.annotation.JsonCreator;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.net.URI;

@Getter
@Accessors(fluent = true)
@EqualsAndHashCode(callSuper = true)
public class UriLocation extends Location {
	@NotNull
	private final URI uri;

	@JsonCreator
	public UriLocation(@NotNull URI uri) {
		super(LocationType.URI);
		this.uri = uri;
	}

	@Override
	public URI mediaId() {
		return uri;
	}
}
