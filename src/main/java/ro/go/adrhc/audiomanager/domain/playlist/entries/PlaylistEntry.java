package ro.go.adrhc.audiomanager.domain.playlist.entries;

import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.Thumbnail;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.LocationAware;

import java.net.URI;
import java.util.List;

import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiomanager.domain.location.LocationAccessors.locationName;
import static ro.go.adrhc.util.ComparisonUtils.compareComparable;
import static ro.go.adrhc.util.ComparisonUtils.compareLists;

public record PlaylistEntry(Location location, String title, Integer duration,
		List<Thumbnail> thumbnails)
		implements Comparable<PlaylistEntry>, LocationAware<Location> {
	public static PlaylistEntry of(Location location, String title, Integer duration) {
		return new PlaylistEntry(location, title, duration, List.of());
	}

	public static PlaylistEntry of(Location location, String title) {
		return new PlaylistEntry(location, title, null, List.of());
	}

	public PlaylistEntry location(Location location) {
		return new PlaylistEntry(location, title, duration, List.of());
	}

	public PlaylistEntry title(String title) {
		return new PlaylistEntry(location, title, duration, List.of());
	}

	public PlaylistEntry duration(Integer duration) {
		return new PlaylistEntry(location, title, duration, List.of());
	}

	public PlaylistEntry replaceTitleWithLocationName(boolean missingTitleOnly) {
		if (!missingTitleOnly || !hasTitle()) {
			String locationName = locationName(location);
			return hasText(locationName) ? title(locationName) : this;
		} else {
			return this;
		}
	}

	/*
	Misleading, see LocationPredicates.haveSameLocation!!!
	public boolean hasLocation(Location location) {
		return this.location.equals(location);
	}*/

	public boolean hasEquivalentLocation(Location location) {
		return this.location.isEquivalent(location);
	}

	public boolean hasTitle() {
		return hasText(title);
	}

	public boolean hasDuration() {
		return duration != null;
	}

	@Override
	public int compareTo(PlaylistEntry another) {
		int titleComparison = compareComparable(title, another.title);
		if (titleComparison != 0) {
			return titleComparison;
		}
		int locationComparison = compareComparable(location, another.location);
		if (locationComparison != 0) {
			return locationComparison;
		}
		int durationComparison = compareComparable(duration, another.duration);
		if (durationComparison != 0) {
			return durationComparison;
		}
		return compareLists(thumbnails, another.thumbnails);
	}

	public URI uri() {
		return location.uri();
	}

	@Override
	public String toString() {
		return "%3s seconds, %-40s, %s".formatted(
				hasDuration() ? duration.toString() : "no",
				hasTitle() ? title : "no title",
				location);
	}
}
