package ro.go.adrhc.audiomanager.domain.playlist.entries.filters;

import lombok.experimental.UtilityClass;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.LocationPredicates;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.util.Collection;
import java.util.Comparator;

@UtilityClass
public class EntryComparators {
	public static Comparator<? super PlaylistEntry> followLocationsOrder(
			Collection<? extends Location> locations) {
		return (e1, e2) ->
				locations.stream()
						.filter(l -> LocationPredicates.areEquivalent(l, e1)
								|| LocationPredicates.areEquivalent(l, e2))
						.findFirst()
						.map(first -> LocationPredicates.areEquivalent(first, e1) ? -1 : 1)
						.orElse(0);
	}
}
