package ro.go.adrhc.audiomanager.domain.playlist;

import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.LocationAware;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.util.function.Predicate;

@Slf4j
public record Playlist<L extends Location>(
		L location, PlaylistEntries entries) implements LocationAware<L> {
	public Playlist<L> location(L location) {
		return new Playlist<>(location, entries);
	}

	public Playlist<L> entries(PlaylistEntries entries) {
		return new Playlist<>(location, entries);
	}

	public Playlist<L> sort() {
		return entries(entries.sort());
	}

	public Playlist<L> replaceTitleWithLocationName(boolean missingTitleOnly) {
		return entries(entries.replaceTitleWithLocationName(missingTitleOnly));
	}

	public Playlist<L> deduplicate() {
		return entries(entries.deduplicate());
	}

	public Playlist<L> deduplicateByMediaId() {
		return entries(entries.deduplicateByMediaId());
	}

	public Playlist<L> filter(Predicate<PlaylistEntry> filter) {
		return new Playlist<>(location, entries.filter(filter));
	}

	public int size() {
		return entries.size();
	}

	public boolean isEmpty() {
		return entries.isEmpty();
	}

	public Playlist<L> removeEntry(Location entryLocation) {
		return new Playlist<>(location, entries.removeEntry(entryLocation));
	}

	public Playlist<L> copy() {
		return entries(entries.copy());
	}

	@Override
	public String toString() {
		return "%-4s entries, %s".formatted(entries.size(), location);
	}
}
