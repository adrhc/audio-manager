package ro.go.adrhc.audiomanager.domain.location;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ro.go.adrhc.util.stream.StreamAware;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.function.Predicate.not;

@RequiredArgsConstructor
@Getter
public class LocationCollection implements StreamAware<Location> {
	private final List<Location> locations;

	public static LocationCollection of(Stream<? extends Location> stream) {
		return new LocationCollection(new ArrayList<>(stream.toList()));
	}

	public LocationCollection known() {
		return of(rawFilter(not(LocationPredicates::isUnknownLocation)));
	}

	public boolean isEmpty() {
		return locations.isEmpty();
	}

	public int size() {
		return locations.size();
	}

	@Override
	public Stream<Location> rawStream() {
		return locations.stream();
	}
}
