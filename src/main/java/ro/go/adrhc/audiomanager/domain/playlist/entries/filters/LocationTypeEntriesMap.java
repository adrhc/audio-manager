package ro.go.adrhc.audiomanager.domain.playlist.entries.filters;

import ro.go.adrhc.audiomanager.domain.location.LocationType;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;

import java.util.Map;
import java.util.stream.Stream;

public record LocationTypeEntriesMap(Map<LocationType, PlaylistEntries> entriesByLocationType) {
	public Stream<PlaylistEntries> plEntriesStream() {
		return entriesByLocationType.values().stream();
	}
}
