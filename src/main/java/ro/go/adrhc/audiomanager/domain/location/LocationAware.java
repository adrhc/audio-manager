package ro.go.adrhc.audiomanager.domain.location;

import java.net.URI;

@FunctionalInterface
public interface LocationAware<L extends Location> {
	L location();

	default boolean hasUri(URI uri) {
		return location().uri().equals(uri);
	}
}
