package ro.go.adrhc.audiomanager.domain.services;

import com.rainerhahnekamp.sneakythrow.functional.SneakyFunction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.datasources.disk.DiskPlEntriesRepository;
import ro.go.adrhc.audiomanager.datasources.youtube.YtPlEntriesRepository;
import ro.go.adrhc.audiomanager.domain.location.LocationPredicates;
import ro.go.adrhc.audiomanager.domain.playlist.entries.EntriesPairs;
import ro.go.adrhc.audiomanager.domain.playlist.entries.EntryPredicates;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.audiomanager.managers.diskplupdate.PlContentUpdateRequest;
import ro.go.adrhc.util.fn.PredicateUtils;

import java.io.IOException;
import java.util.Collection;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.function.Predicate.not;
import static ro.go.adrhc.util.fn.BiConsumerUtils.toBiOper;
import static ro.go.adrhc.util.fn.PredicateFactory.allMatch;

@RequiredArgsConstructor
@Service
@Slf4j
public class PlaylistEntriesService {
	private final YtPlEntriesRepository ytPlEntriesRepository;
	private final DiskPlEntriesRepository diskPlEntriesRepository;
	private final EntryPredicates entryPredicates;
	private final PlEntrySelectionsFactory plEntrySelectionsFactory;

	public PlaylistEntries update(PlContentUpdateRequest request, PlaylistEntries plEntries) {
		PlContentUpdateRequest filteredReq = request.removeExcludedIfIncludedExists();
		PlEntrySelections selections = plEntrySelectionsFactory.create(filteredReq);
		PlaylistEntries newEntries = plEntries.filter(selections::isNotExcluded);
		selections.addIncludedIfMissing(newEntries);
		return newEntries;
	}

	/**
	 * sumDiffs = intoPlEntries, sumDiffs += fromPlEntries.minus(sumDiffs)
	 */
	public PlaylistEntries sumDiffs(PlaylistEntries intoPlEntries,
			Collection<? extends PlaylistEntries> fromPlEntries) {
		return fromPlEntries.stream().reduce(
				intoPlEntries, PlaylistEntries::plusDiffs,
				toBiOper(PlaylistEntries::addAll));
	}

	/**
	 * @return a playlist copy where the title is replaced by
	 * the location name then the YouTube entries are updated
	 */
	public PlaylistEntries fixTitles(boolean missingTitleOnly, PlaylistEntries plEntries) {
		plEntries = plEntries.replaceTitleWithLocationName(missingTitleOnly);
		return updateYtEntries(missingTitleOnly, plEntries);
	}

	/**
	 * @return a plEntries copy where the invalid DISK entries are replaced by their valid DISK counterpart
	 */
	public PlaylistEntries fixDiskLocations(PlaylistEntries plEntries) throws IOException {
		return swapWithBestDiskMatches(plEntries, entryPredicates::isInvalidDiskEntry);
	}

	/**
	 * @param missingTitleOnly true/false
	 * @param plEntries        could also contain other than YouTube entries
	 * @return a plEntries copy where the YouTube entries have their title updated
	 */
	public PlaylistEntries updateYtEntries(boolean missingTitleOnly, PlaylistEntries plEntries) {
		Predicate<PlaylistEntry> titlePredicate = missingTitleOnly
				? not(PlaylistEntry::hasTitle) : PredicateUtils::toTrue;
		return swapWithMatching(plEntries, ytPlEntriesRepository::loadMatches,
				allMatch(LocationPredicates::hasYtLocation, titlePredicate));
	}

	/**
	 * @return a playlist copy having the YouTube entries replaced by their valid DISK counterpart
	 */
	public PlaylistEntries switchYtToDiskLocation(PlaylistEntries plEntries) throws IOException {
		return swapWithBestDiskMatches(plEntries, EntryPredicates::hasYtLocationAndTitle);
	}

	private PlaylistEntries swapWithBestDiskMatches(
			PlaylistEntries plEntries, Predicate<PlaylistEntry> filter) throws IOException {
		return safelySwapWithMatching(plEntries, diskPlEntriesRepository::findBestMatches, filter);
	}

	private <E extends Exception> PlaylistEntries safelySwapWithMatching(
			PlaylistEntries plEntries,
			SneakyFunction<PlaylistEntries, EntriesPairs, E> matcher,
			Predicate<PlaylistEntry> filter) throws E {
		PlaylistEntries changeCandidates = plEntries.filter(filter);
		if (changeCandidates.isEmpty()) {
			log.debug("\nChange candidates not found!");
			return plEntries.copy();
		}

		EntriesPairs searchedAndFoundPairs = matcher.apply(changeCandidates);
		if (searchedAndFoundPairs.isEmpty()) {
			log.debug("\nFound no matches for {} change candidates!", changeCandidates.size());
			return plEntries.copy();
		}

		log(changeCandidates, searchedAndFoundPairs);

		return plEntries.swapEntries(e -> searchedAndFoundPairs.getPair(e).orElse(e));
	}

	private PlaylistEntries swapWithMatching(
			PlaylistEntries plEntries,
			Function<PlaylistEntries, EntriesPairs> matcher,
			Predicate<? super PlaylistEntry> filter) {
		PlaylistEntries changeCandidates = plEntries.filter(filter);
		if (changeCandidates.isEmpty()) {
			log.debug("\nChange candidates not found!");
			return plEntries.copy();
		}

		EntriesPairs searchedAndFoundPairs = matcher.apply(changeCandidates);
		if (searchedAndFoundPairs.isEmpty()) {
			log.debug("\nFound no matches for {} change candidates!", changeCandidates.size());
			return plEntries.copy();
		}

		log(changeCandidates, searchedAndFoundPairs);

		return plEntries.swapEntries(e -> searchedAndFoundPairs.getPair(e).orElse(e));
	}

	private void log(PlaylistEntries changeCandidates, EntriesPairs foundEntries) {
		log.debug("\nFound {} of {} searched entries!",
				foundEntries.size(), changeCandidates.size());
	}
}
