package ro.go.adrhc.audiomanager.domain.services;

import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.audiomanager.managers.diskplupdate.SongSelection;
import ro.go.adrhc.util.pair.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static ro.go.adrhc.audiomanager.domain.playlist.entries.EntryPredicates.hasEquivalentLocation;

public record PlEntrySelections(List<Pair<SongSelection, PlaylistEntry>> pairs) {
	public static PlEntrySelections of(Stream<Pair<SongSelection, PlaylistEntry>> pairs) {
		return new PlEntrySelections(new ArrayList<>(pairs.toList()));
	}

	public static PlEntrySelections of() {
		return new PlEntrySelections(new ArrayList<>());
	}

	public boolean isNotExcluded(PlaylistEntry entry) {
		return pairs.stream()
				.filter(p -> !p.left().selected())
				.map(Pair::right)
				.noneMatch(hasEquivalentLocation(entry));
	}

	public void addIncludedIfMissing(PlaylistEntries newEntries) {
		pairs.stream().filter(p -> p.left().selected())
				.map(Pair::right)
				.filter(included -> newEntries.count(hasEquivalentLocation(included)) < count(included))
				.forEach(newEntries::add);
	}

	public void add(Pair<SongSelection, PlaylistEntry> plEntrySelection) {
		pairs.add(plEntrySelection);
	}

	private long count(PlaylistEntry selection) {
		return pairs.stream().map(Pair::right)
				.map(PlaylistEntry::location)
				.filter(selection::hasEquivalentLocation)
				.count();
	}
}
