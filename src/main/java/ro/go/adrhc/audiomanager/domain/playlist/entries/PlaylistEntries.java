package ro.go.adrhc.audiomanager.domain.playlist.entries;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.LocationPredicates;
import ro.go.adrhc.audiomanager.domain.playlist.entries.filters.UniqueEntriesPerMediaIdFilter;
import ro.go.adrhc.audiomanager.domain.playlist.entries.filters.UniquesPerTitleAndLocationTypeFilter;
import ro.go.adrhc.util.fn.PredicateUtils;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntriesFactory.ofEntries;
import static ro.go.adrhc.util.collection.JoinDsl.join;

@Slf4j
public record PlaylistEntries(List<PlaylistEntry> entries) implements Iterable<PlaylistEntry> {
	public PlaylistEntries copy() {
		return ofEntries(entries);
	}

	public PlaylistEntries removeEntry(Location entryLocation) {
		return filter(e -> !e.hasUri(entryLocation.uri()));
	}

	public PlaylistEntries swapEntries(UnaryOperator<PlaylistEntry> entryToEntryMapper) {
		return map(entryToEntryMapper).collect(PlEntriesFactory::empty,
				PlaylistEntries::add, PlaylistEntries::addAll);
	}

	/**
	 * @param missingTitleOnly specify whether to overwrite or not the existing titles
	 * @return a new PlaylistEntries, copy of "this", where each of its entries
	 * containing an empty title is replaced by its copy with the location name,
	 * if any, set as title, otherwise left unchanged
	 */
	public PlaylistEntries replaceTitleWithLocationName(boolean missingTitleOnly) {
		return swapEntries(e -> e.replaceTitleWithLocationName(missingTitleOnly));
	}

	public PlaylistEntries deduplicateByMediaId() {
		PlaylistEntries uniques = UniqueEntriesPerMediaIdFilter.INSTANCE.filter(this);
		return this.intersect(PredicateUtils::eqInstance, uniques);
	}

	public PlaylistEntries deduplicate() {
		PlaylistEntries renamed = replaceTitleWithLocationName(true);
		return UniquesPerTitleAndLocationTypeFilter.INSTANCE.filter(renamed);
	}

	public PlaylistEntries intersect(BiPredicate<PlaylistEntry, PlaylistEntry> comparator,
			PlaylistEntries another) {
		return join(entries, another.entries)
				.combinedBy((e, _) -> e)
				.on(comparator)
				.collect(PlEntriesFactory::empty, PlaylistEntries::add, PlaylistEntries::addAll);
	}

	public PlaylistEntries minus(
			BiPredicate<PlaylistEntry, PlaylistEntry> comparator,
			PlaylistEntries plEntries) {
		return this.filter(e1 -> plEntries.noneMatch(e2 -> comparator.test(e1, e2)));
	}

	/**
	 * Compares using LocationPredicates.referSameLocation(plEntry1, plEntry2).
	 */
	public PlaylistEntries minus(PlaylistEntries plEntries) {
		return minus(LocationPredicates::areEquivalent, plEntries);
	}

	/**
	 * Uses PlaylistEntries.minus(this) to detect the difference.
	 */
	public PlaylistEntries plusDiffs(PlaylistEntries plEntries) {
		PlaylistEntries diff = plEntries.minus(this);
		PlaylistEntries sum = copy();
		sum.addAll(diff);
		return sum;
	}

	public void add(PlaylistEntry entry) {
		this.entries.add(entry);
	}

	public PlaylistEntries plus(Iterable<PlaylistEntry> plEntries) {
		PlaylistEntries sum = copy();
		sum.addAll(plEntries);
		return sum;
	}

	public void addAll(Iterable<PlaylistEntry> plEntries) {
		plEntries.forEach(entries::add);
	}

	public PlaylistEntries sort() {
		return sort(null);
	}

	public PlaylistEntries sort(@Nullable Comparator<? super PlaylistEntry> comparator) {
		PlaylistEntries sorted = copy();
		sorted.entries.sort(comparator);
		return sorted;
	}

	public Stream<PlaylistEntry> stream() {
		return entries.stream();
	}

	public PlaylistEntries filter(Predicate<? super PlaylistEntry> filter) {
		return ofEntries(rawFilter(filter));
	}

	public long count(Predicate<? super PlaylistEntry> filter) {
		return rawFilter(filter).count();
	}

	public boolean noneMatch(Predicate<PlaylistEntry> predicate) {
		return entries.stream().noneMatch(predicate);
	}

	public boolean anyMatch(Predicate<PlaylistEntry> predicate) {
		return entries.stream().anyMatch(predicate);
	}

	public <R> Stream<R> map(Function<? super PlaylistEntry, ? extends R> mapper) {
		return entries.stream().map(mapper);
	}

	public Stream<PlaylistEntry> rawFilter(Predicate<? super PlaylistEntry> predicate) {
		return entries.stream().filter(predicate);
	}

	@Override
	@NonNull
	public Iterator<PlaylistEntry> iterator() {
		return entries.iterator();
	}

	@JsonIgnore
	public boolean isEmpty() {
		return entries.isEmpty();
	}

	public int size() {
		return entries.size();
	}

	@Override
	public String toString() {
		return map(PlaylistEntry::toString).collect(Collectors.joining("\n"));
	}
}
