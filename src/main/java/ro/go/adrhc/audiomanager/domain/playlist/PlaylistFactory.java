package ro.go.adrhc.audiomanager.domain.playlist;

import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpec;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntriesFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import java.util.function.Function;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocation;

public class PlaylistFactory {
	public static Playlist<DiskLocation> of(
			PlaylistSpec plSpec, Function<PlaylistSpec, PlaylistEntries> plEntriesProvider) {
		DiskLocation plLocation = createDiskLocation(plSpec);
		PlaylistEntries plEntries = plEntriesProvider.apply(plSpec);
		return new Playlist<>(plLocation, plEntries).sort();
	}

	public static <L extends Location> Playlist<L> of(L location) {
		return new Playlist<>(location, PlEntriesFactory.empty());
	}

	public static <L extends Location> Playlist<L> of(L location, PlaylistEntry plEntry) {
		return new Playlist<>(location, PlEntriesFactory.of(plEntry));
	}
}
