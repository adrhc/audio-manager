package ro.go.adrhc.audiomanager.domain.location;

public enum LocationType {
	DISK, YOUTUBE, RADIO, UNKNOWN, URI
}
