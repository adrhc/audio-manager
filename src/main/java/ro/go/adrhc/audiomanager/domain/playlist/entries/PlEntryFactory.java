package ro.go.adrhc.audiomanager.domain.playlist.entries;

import jakarta.validation.constraints.NotNull;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.location.NamedLocation;
import ro.go.adrhc.lib.m3u8.domain.Extinf;

import java.util.List;

public class PlEntryFactory {
	public static PlaylistEntry of(@NotNull Location location) {
		return new PlaylistEntry(location, null, null, List.of());
	}

	public static PlaylistEntry of(@NotNull Location location, @NotNull String title) {
		return new PlaylistEntry(location, title, null, List.of());
	}

	public static PlaylistEntry of(@NotNull Location location, @NotNull String title,
			@NotNull Integer duration) {
		return new PlaylistEntry(location, title, duration, List.of());
	}

	public static PlaylistEntry of(@NotNull Extinf extinf) {
		return new PlaylistEntry(null, extinf.title(), extinf.seconds(), List.of());
	}

	public static PlaylistEntry of(@NotNull YouTubeMetadata ytMetadata) {
		return new PlaylistEntry(ytMetadata.location(), ytMetadata.title(), null,
				ytMetadata.thumbnails());
	}

	/**
	 * If was named "of" instead of "ofDiskLocation" it would clash
	 * with of(@NotNull Location location) which doesn't set the title!
	 */
	public static PlaylistEntry ofNamedLocation(@NotNull NamedLocation namedLocation) {
		return new PlaylistEntry(namedLocation, namedLocation.name(), null, List.of());
	}
}
