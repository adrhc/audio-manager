package ro.go.adrhc.audiomanager.domain.playlist;

import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;

import java.util.Arrays;
import java.util.List;

public class PlaylistAccessors {
	public static List<PlaylistEntries> playlistEntries(Playlist<?>... playlists) {
		return Arrays.stream(playlists).map(Playlist::entries).toList();
	}
}
