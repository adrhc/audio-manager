package ro.go.adrhc.audiomanager.domain.location;

import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.net.URI;

import static ro.go.adrhc.audiomanager.domain.location.LocationType.UNKNOWN;

@Getter
@Accessors(fluent = true)
@EqualsAndHashCode(callSuper = true)
public class UnknownLocation extends Location {
	@NotNull
	private final String raw;

	public UnknownLocation(@NotNull String rawLocation) {
		super(UNKNOWN);
		this.raw = rawLocation;
	}

	@Override
	public int compareTo(Location another) {
		int locationsComparison = super.compareTo(another);
		if (locationsComparison != 0) {
			return locationsComparison;
		}
		return this.raw.compareTo(((UnknownLocation) another).raw);
	}

	@Override
	public String mediaId() {
		return this.raw;
	}

	@Override
	public URI uri() {
		throw new UnsupportedOperationException(
				"UnknownLocation (i.e. %s) doesn't have uri!".formatted(raw));
	}

	@Override
	public String toString() {
		return "%s: %s".formatted(type(), raw);
	}
}
