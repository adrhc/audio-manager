package ro.go.adrhc.audiomanager.domain.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpec;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory;
import ro.go.adrhc.audiomanager.datasources.plspec.PlSpecPlEntriesPairs;
import ro.go.adrhc.audiomanager.datasources.plspec.SpecPlEntriesLoader;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.audiomanager.domain.playlist.PlaylistFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.EntryPredicates;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.audiomanager.managers.diskplupdate.PlContentUpdateRequest;

import java.io.IOException;
import java.util.Collection;
import java.util.stream.Stream;

import static java.util.function.Predicate.not;
import static ro.go.adrhc.audiomanager.domain.playlist.PlaylistAccessors.playlistEntries;
import static ro.go.adrhc.util.text.StringUtils.concat;

@RequiredArgsConstructor
@Service
@Slf4j
public class PlaylistsService {
	private final DiskLocationFactory diskLocationFactory;
	private final SpecPlEntriesLoader specPlEntriesLoader;
	private final PlaylistEntriesService plEntriesService;
	private final EntryPredicates entryPredicates;

	public Playlist<DiskLocation> update(Playlist<DiskLocation> storedPl, PlContentUpdateRequest request) {
		return storedPl.entries(plEntriesService.update(request, storedPl.entries()));
	}

	/**
	 * sumDiffs = intoPlEntries, sumDiffs += fromPlEntries.minus(sumDiffs)
	 */
	public <L extends Location> Playlist<L> sumDiffs(Playlist<L> into, Playlist<?>... from) {
		return into.entries(plEntriesService.sumDiffs(into.entries(), playlistEntries(from)));
	}

	/**
	 * @param missingTitleOnly true/false
	 * @param playlist         could also contain other than YouTube entries
	 * @return a plEntries copy where the YouTube entries are updated
	 */
	public <L extends Location> Playlist<L> updateYtEntries(boolean missingTitleOnly,
			Playlist<L> playlist) {
		PlaylistEntries newPlEntries = plEntriesService
				.updateYtEntries(missingTitleOnly, playlist.entries());
		return playlist.entries(newPlEntries);
	}

	/**
	 * @return a playlist copy where the title is replaced by
	 * the location name then the YouTube entries are updated
	 */
	public <L extends Location> Playlist<L> fixTitles(boolean missingTitleOnly,
			Playlist<L> playlist) {
		PlaylistEntries fixedEntries = plEntriesService
				.fixTitles(missingTitleOnly, playlist.entries());
		return playlist.entries(fixedEntries);
	}

	/**
	 * @return a playlist copy having the YouTube entries replaced by their valid DISK counterpart
	 */
	public <L extends Location> Playlist<L> switchYtToDiskLocation(Playlist<L> playlist)
			throws IOException {
		return playlist.entries(plEntriesService.switchYtToDiskLocation(playlist.entries()));
	}

	/**
	 * @return a playlist copy where the invalid DISK entries are replaced by their valid DISK counterpart
	 */
	public <L extends Location> Playlist<L> fixDiskLocations(Playlist<L> playlist)
			throws IOException {
		return playlist.entries(plEntriesService.fixDiskLocations(playlist.entries()));
	}

	public Playlist<DiskLocation> createFromSpec(PlaylistSpec plSpec) {
		log.debug("\ngetting the content of {}", plSpec.filename());
		return PlaylistFactory.of(plSpec, specPlEntriesLoader::loadByPlSpec);
	}

	public Stream<Playlist<DiskLocation>> createFromSpecs(Collection<PlaylistSpec> plSpecs) {
		log.debug("\ngetting the content of:\n{}",
				concat(plSpecs.stream().map(PlaylistSpec::filename)));
		PlSpecPlEntriesPairs plSpecPlEntriesPairs = specPlEntriesLoader.loadByPlSpecs(plSpecs);
		return plSpecPlEntriesPairs.plSpecStream()
				.peek(plSpec -> log.debug("\ncreating %s ...".formatted(plSpec.filename())))
				.map(plSpec -> PlaylistFactory.of(plSpec, plSpecPlEntriesPairs::plEntries));
	}

	/**
	 * @return a playlist copy containing only the invalid DISK entries
	 */
	public Playlist<DiskLocation> invalidDiskEntriesPlaylist(Playlist<?> playlist) {
		PlaylistEntries playlistEntries = playlist.entries().filter(
				entryPredicates::isInvalidDiskEntry);
		DiskLocation plLocation = diskLocationFactory.createNotFoundPlLocation(playlist);
		return new Playlist<>(plLocation, playlistEntries);
	}

	/**
	 * @return a playlist copy containing all but the invalid DISK entries
	 */
	public Playlist<DiskLocation> otherThanInvalidDiskEntriesPlaylist(Playlist<?> playlist) {
		PlaylistEntries playlistEntries = playlist.entries().filter(
				not(entryPredicates::isInvalidDiskEntry));
		DiskLocation plLocation = diskLocationFactory.createOtherThanNotFoundPlLocation(playlist);
		return new Playlist<>(plLocation, playlistEntries);
	}
}
