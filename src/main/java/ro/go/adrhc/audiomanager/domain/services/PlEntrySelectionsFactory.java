package ro.go.adrhc.audiomanager.domain.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.domain.location.LocationParsers;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntryFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;
import ro.go.adrhc.audiomanager.managers.diskplupdate.PlContentUpdateRequest;
import ro.go.adrhc.audiomanager.managers.diskplupdate.SongSelection;
import ro.go.adrhc.util.pair.Pair;

import java.util.List;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class PlEntrySelectionsFactory {
	private final LocationParsers locationParsers;

	public PlEntrySelections create(PlContentUpdateRequest request) {
		return PlEntrySelections.of(toSelectionPlEntryPairs(request.selections()));
	}

	private Stream<Pair<SongSelection, PlaylistEntry>>
	toSelectionPlEntryPairs(List<SongSelection> selections) {
		return selections.stream().map(ss -> new Pair<>(ss, toPlEntry(ss)));
	}

	private PlaylistEntry toPlEntry(SongSelection selection) {
		return PlEntryFactory.of(locationParsers.parse(selection.uri()), selection.title());
	}
}
