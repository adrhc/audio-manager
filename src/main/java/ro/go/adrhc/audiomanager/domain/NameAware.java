package ro.go.adrhc.audiomanager.domain;

public interface NameAware {
	String name();
}
