package ro.go.adrhc.audiomanager.domain.location;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.springframework.util.StringUtils.hasText;

@RequiredArgsConstructor
@Component
public class LocationParsers {
	private final List<LocationParser<? extends Location>> locationParsers;

	@NonNull
	public LocationCollection parseMany(@NonNull Collection<String> texts) {
		return LocationCollection.of(texts.stream().map(this::parse));
	}

	@NonNull
	public Location parse(@NonNull String text) {
		Assert.isTrue(hasText(text), "\"m3u8Line\" can't be empty!");
		return locationParsers.stream()
				.flatMap(locationParser ->
						locationParser.parse(text).stream())
				.findAny()
				.map(Location.class::cast)
				.orElseGet(() -> new UnknownLocation(text));
	}

	public Optional<LocationParser<? extends Location>>
	getForLocationType(LocationType locationType) {
		return locationParsers.stream()
				.filter(p -> p.supports(locationType))
				.findFirst();
	}
}
