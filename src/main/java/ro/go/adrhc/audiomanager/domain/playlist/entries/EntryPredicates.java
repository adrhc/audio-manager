package ro.go.adrhc.audiomanager.domain.playlist.entries;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.DiskLocationsRepository;
import ro.go.adrhc.audiomanager.domain.location.LocationAware;

import java.util.Objects;
import java.util.function.Predicate;

import static ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocationAccessors.diskLocation;
import static ro.go.adrhc.audiomanager.domain.location.LocationAccessors.locationName;
import static ro.go.adrhc.audiomanager.domain.location.LocationPredicates.hasDiskLocation;
import static ro.go.adrhc.audiomanager.domain.location.LocationPredicates.hasYtLocation;

@Component
@RequiredArgsConstructor
public class EntryPredicates {
	private final DiskLocationsRepository<?> diskLocationsRepository;

	public static Predicate<PlaylistEntry> hasEquivalentLocation(PlaylistEntry entry) {
		return other -> other.hasEquivalentLocation(entry.location());
	}

	public static boolean hasYtLocationAndTitle(PlaylistEntry entry) {
		return hasYtLocation(entry) && entry.hasTitle();
	}

	public static Predicate<PlaylistEntry> hasLocationName(String locationName) {
		return entry -> Objects.equals(locationName, locationName(entry));
	}

	public boolean isInvalidDiskEntry(LocationAware<?> entry) {
		return hasDiskLocation(entry) && !hasValidDiskLocation(entry);
	}

	private boolean hasValidDiskLocation(LocationAware<?> diskLocationAware) {
		return diskLocationsRepository.isValid(diskLocation(diskLocationAware));
	}
}
