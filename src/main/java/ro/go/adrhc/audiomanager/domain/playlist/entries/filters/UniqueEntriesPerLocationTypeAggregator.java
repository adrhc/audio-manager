package ro.go.adrhc.audiomanager.domain.playlist.entries.filters;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.domain.location.LocationAccessors;
import ro.go.adrhc.audiomanager.domain.location.LocationType;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntriesFactory;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static ro.go.adrhc.util.fn.BiConsumerUtils.toBiOper;

@RequiredArgsConstructor
@Component
public class UniqueEntriesPerLocationTypeAggregator {
	private final UniqueEntriesPerMediaIdFilter
			uniqueEntriesPerMediaIdFilter = new UniqueEntriesPerMediaIdFilter();

	/**
	 * @return uniques (by mediaId) per location type but favours those having a title (1st found is picked)
	 */
	public LocationTypeEntriesMap groupUniquesByLocationType(PlaylistEntries plEntries) {
		Map<LocationType, PlaylistEntries> entriesPerLocationType = groupByLocationType(plEntries);
		Map<LocationType, PlaylistEntries> uniqueEntriesPerLocationType = entriesPerLocationType
				.keySet().stream()
				.map(locationType -> uniqueEntriesPerLocationType(locationType,
						entriesPerLocationType))
				.collect(HashMap::new, (acc, e) -> acc.put(e.getKey(), e.getValue()), Map::putAll);
		return new LocationTypeEntriesMap(uniqueEntriesPerLocationType);
	}

	private Map<LocationType, PlaylistEntries> groupByLocationType(PlaylistEntries entries) {
		return entries.stream().collect(
				Collectors.groupingBy(LocationAccessors::locationType,
						Collector.of(PlEntriesFactory::empty,
								PlaylistEntries::add,
								toBiOper(PlaylistEntries::addAll))));
	}

	private AbstractMap.SimpleEntry<LocationType, PlaylistEntries> uniqueEntriesPerLocationType(
			LocationType locationType, Map<LocationType, PlaylistEntries> entriesPerLocationType) {
		return new AbstractMap.SimpleEntry<>(locationType,
				uniqueEntriesPerMediaIdFilter.filter(entriesPerLocationType.get(locationType)));
	}
}
