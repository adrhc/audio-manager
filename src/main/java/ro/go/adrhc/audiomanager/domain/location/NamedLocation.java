package ro.go.adrhc.audiomanager.domain.location;

import jakarta.validation.constraints.NotNull;
import ro.go.adrhc.audiomanager.domain.NameAware;

public abstract class NamedLocation extends Location implements NameAware {
	public NamedLocation(@NotNull LocationType type) {
		super(type);
	}
}
