package ro.go.adrhc.audiomanager.domain.playlist.entries.filters;

import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntries;
import ro.go.adrhc.util.fn.PredicateUtils;

import static ro.go.adrhc.audiomanager.domain.playlist.entries.PlEntriesFactory.of;

public class UniquesPerTitleAndLocationTypeFilter {
	public static final UniquesPerTitleAndLocationTypeFilter
			INSTANCE = new UniquesPerTitleAndLocationTypeFilter();

	private final UniqueEntriesPerTitleFilter
			uniqueEntriesPerTitleFilter = new UniqueEntriesPerTitleFilter();
	private final UniqueEntriesPerLocationTypeAggregator
			uniqueEntriesPerLocationTypeAggregator
			= new UniqueEntriesPerLocationTypeAggregator();

	/**
	 * Algorithm:
	 * 1. uniques per title: it favours the disk located ones among those with same title
	 * 2. uniques per mediaId: it favours those having a title
	 * <p>
	 * Note: same song on disk and e.g. on YouTube won't have the same mediaId
	 * (i.e. Path vs YouTube id) hence both will be included in the result!
	 *
	 * @return uniques per title then per mediaId; it preserves the order in plEntries
	 */
	public PlaylistEntries filter(PlaylistEntries plEntries) {
		PlaylistEntries uniquesPerTitle = uniqueEntriesPerTitleFilter.filter(plEntries);
		LocationTypeEntriesMap locationTypeEntriesMap = uniqueEntriesPerLocationTypeAggregator
				.groupUniquesByLocationType(uniquesPerTitle);
		return plEntries.intersect(PredicateUtils::eqInstance, of(locationTypeEntriesMap));
	}
}
