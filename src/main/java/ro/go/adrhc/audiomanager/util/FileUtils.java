package ro.go.adrhc.audiomanager.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

@UtilityClass
@Slf4j
public class FileUtils {
	public static Optional<Boolean> isLastCharNewline(Path path) {
		try (RandomAccessFile randomAccessFile = new RandomAccessFile(path.toFile(), "r")) {
			long length = randomAccessFile.length();

			if (length == 0) {
				// Empty file
				return Optional.of(Boolean.FALSE);
			}

			// Seek to the last character
			randomAccessFile.seek(length - 1);
			int lastChar = randomAccessFile.read();

			// Check if the last character is '\n'
			return Optional.of(lastChar == '\n');
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			return Optional.empty();
		}
	}

	public static Optional<Boolean> doesFileContainAny(File file, Set<String> text) {
		LineIterator it = null;
		try {
			// Iterate through each line of the file
			it = org.apache.commons.io.FileUtils.lineIterator(file, "UTF-8");
			while (it.hasNext()) {
				String line = it.nextLine();
				// Check if the current line contains the search string
				if (containsAny(text, line)) {
					return Optional.of(Boolean.TRUE);
				}
			}
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} finally {
			IOUtils.closeQuietly(it);
		}
		return Optional.of(Boolean.FALSE);
	}

	public static void writeLines(Path filePath, Collection<String> lines) throws IOException {
		if (lines.isEmpty()) {
			log.debug("\nNo lines to write to {}; skipping writing.", filePath);
			return;
		}
		org.apache.commons.io.FileUtils.writeLines(filePath.toFile(), lines);
		log.debug("\ncreated {}", filePath);
	}

	private static boolean containsAny(Set<String> text, String line) {
		for (String t : text) {
			if (line.contains(t)) {
				return true;
			}
		}
		return false;
	}
}
