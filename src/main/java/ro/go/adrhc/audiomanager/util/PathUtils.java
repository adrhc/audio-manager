package ro.go.adrhc.audiomanager.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;
import ro.go.adrhc.audiomanager.domain.location.UriLocationParser;

import java.nio.file.Path;
import java.util.Optional;

import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiomanager.domain.location.UriLocationParser.parseURI;
import static ro.go.adrhc.audiomanager.util.PathPredicates.*;

/**
 * Created by IntelliJ IDEA.
 * User: adrhc
 * Date: Mar 4, 2011
 * Time: 10:39:03 AM
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
public class PathUtils {
	private static final UriLocationParser URI_LOCATION_PARSER = new UriLocationParser();

	public static Optional<Path> rawToDiskPath(@Nullable String rawPath) {
		if (!hasText(rawPath)) {
			return Optional.empty();
		}
		try {
			if (FILE_URI_MATCHER.test(rawPath)) {
				Optional<Path> uriPath = parseURI(rawPath).map(Path::of);
				return uriPath.isPresent() ? uriPath :
						rawToDiskPath(rawPath.substring("file:///".length()));
			} else if (SHORT_FILE_URI_MATCHER.test(rawPath)) {
				// SHORT_FILE_URI_MATCHER must be after FILE_URI_MATCHER
				return OSUtils.isLinux() ?
						rawToDiskPath(rawPath.substring("file:".length())) :
						rawToDiskPath(rawPath.substring("file:/".length()));
			} else if (URN_LIKE_MATCHER.test(rawPath) || URL_MATCHER.test(rawPath)
					|| YOUTUBE_PREFIXED_URL_MATCHER.test(rawPath)) {
				return Optional.empty();
			} else if (ABSOLUTE_LINUX_PATH_MATCHER.test(rawPath)) {
				return OSUtils.isLinux() ? optionalPath(rawPath) : Optional.empty();
			} else if (ABSOLUTE_WIN_PATH_MATCHER.test(rawPath)) {
				// ABSOLUTE_WIN_PATH_MATCHER must be after URL_MATCHER
				return OSUtils.isLinux() ? Optional.empty() : optionalPath(rawPath);
			} else {
				// could be a relative path
				return optionalPath(rawPath);
			}
		} catch (Exception e) {
//			log.error(e.getMessage(), e);
			log.error("\nFailed to parse {}!", rawPath);
		}
		return Optional.empty();
	}

	private static Optional<Path> optionalPath(String rawPath) {
		return Optional.of(Path.of(rawPath));
	}
}
