package ro.go.adrhc.audiomanager.util.stringifier;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.youtube.config.YouTubeProperties;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.domain.playlist.entries.PlaylistEntry;

import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YtLocationAccessors.ytLocation;

@Component
@RequiredArgsConstructor
public class PlEntryStringifier {
	private final YouTubeProperties ytProperties;

	public String ytPlEntryString(PlaylistEntry entry) {
		YouTubeLocation location = ytLocation(entry);
		return "%s, %s, %s".formatted(
				ytProperties.ytUrl(location),
				location.code(), entry.title());
	}
}
