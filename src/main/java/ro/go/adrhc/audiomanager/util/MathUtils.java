package ro.go.adrhc.audiomanager.util;

public class MathUtils {
	public static int divideRoundUp(int denominator, int divisor) {
		return (denominator + divisor - 1) / divisor;
	}
}
