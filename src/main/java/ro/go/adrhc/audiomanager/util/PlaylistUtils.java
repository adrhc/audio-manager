package ro.go.adrhc.audiomanager.util;

import static ro.go.adrhc.util.io.FilenameUtils.sanitize;

public class PlaylistUtils {
	public static String sanitizeSongTitle(String title) {
		if (title == null) {
			return title;
		} else {
			return sanitize(title);
		}
	}
}
