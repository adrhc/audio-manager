package ro.go.adrhc.audiomanager.util.stringifier;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.config.context.ObservableAppPaths;
import ro.go.adrhc.util.io.FileSystemUtils;
import ro.go.adrhc.util.text.StringUtils;

import java.nio.file.Path;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class AppPathsStringifier {
	private final FileSystemUtils fsUtils;

	public String toString(ObservableAppPaths observableAppPaths) {
		return toString(observableAppPaths.cloneAppPaths());
	}

	public String toString(AppPaths appPaths) {
		return StringUtils.concat(toContextPathInfos(appPaths));
	}

	private Stream<AppPathDetails> toContextPathInfos(AppPaths appPaths) {
		return Stream.of(
				new AppPathDetails(appPaths.getIndexPathParent(), "index parent-path"),
				new AppPathDetails(appPaths.getIndexPath(), "index path"),
				new AppPathDetails(appPaths.getSongsPath(), "songs path"),
				new AppPathDetails(appPaths.getPlaylistsPath(), "playlists path"),
				new AppPathDetails(appPaths.getBackupsPath(), "playlist backups path"),
				new AppPathDetails(appPaths.getScriptsPath(), "download scripts path"));
	}

	@RequiredArgsConstructor
	private class AppPathDetails {
		private final Path path;
		private final String description;

		@Override
		public String toString() {
			if (path == null) {
				return "%s is not set because is not provided!".formatted(description);
			} else {
				String pathAndDescription = "%s (%s) exists: ".formatted(path, description);
				return "%-72s%b".formatted(pathAndDescription,
						AppPathsStringifier.this.fsUtils.exists(path));
			}
		}
	}
}
