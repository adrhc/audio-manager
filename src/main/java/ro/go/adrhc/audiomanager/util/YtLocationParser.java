package ro.go.adrhc.audiomanager.util;

import lombok.experimental.UtilityClass;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.util.UriComponentsBuilder;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationType;
import ro.go.adrhc.util.fn.BiFunctionUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import static org.springframework.util.StringUtils.hasText;
import static ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationType.*;

/**
 * Role: parse shell inputs.
 */
@UtilityClass
public class YtLocationParser {
	@NonNull
	public static List<YouTubeLocation> parseVideoPls(@Nullable String ytUrlOrUriOrIds) {
		return hasText(ytUrlOrUriOrIds) ? parse(VIDEO_PLAYLIST,
				ytUrlOrUriOrIds) : Collections.emptyList();
	}

	@NonNull
	public static List<YouTubeLocation> parseVideos(@Nullable String ytUrlOrUriOrIds) {
		return hasText(ytUrlOrUriOrIds) ? parse(VIDEO, ytUrlOrUriOrIds) : Collections.emptyList();
	}

	@NonNull
	public static List<YouTubeLocation> parseMusic(@Nullable String ytUrlOrUriOrIds) {
		return hasText(ytUrlOrUriOrIds) ? parse(MUSIC, ytUrlOrUriOrIds) : Collections.emptyList();
	}

	@NonNull
	public static YouTubeLocation parseVideoPl(@NonNull String ytUrlOrUriOrId) {
		return parseOne(VIDEO_PLAYLIST, ytUrlOrUriOrId);
	}

	@NonNull
	public static List<YouTubeLocation> parse(
			@NonNull YouTubeLocationType youTubeLocationType,
			@Nullable String ytUrlOrUriOrIds) {
		return hasText(ytUrlOrUriOrIds) ? Arrays.stream(ytUrlOrUriOrIds.split(","))
				.map(BiFunctionUtils.curry(YtLocationParser::parseOne, youTubeLocationType))
				.toList() : Collections.emptyList();
	}

	@NonNull
	public static YouTubeLocation parseOne(
			@NonNull YouTubeLocationType ytLocationType,
			@NonNull String ytUrlOrUriOrId) {
		String ytLocationId = switch (ytLocationType) {
			case VIDEO_PLAYLIST, MUSIC_PLAYLIST -> parseYtPlCode(ytUrlOrUriOrId);
			case VIDEO, MUSIC -> parseYtCode(ytUrlOrUriOrId);
		};
		return new YouTubeLocation(ytLocationType, ytLocationId);
	}

	@NonNull
	private static String parseYtPlCode(@NonNull String ytUrlOrUriOrId) {
		return parseYtId("list", ytUrlOrUriOrId);
	}

	@NonNull
	private static String parseYtCode(@NonNull String ytUrlOrUriOrId) {
		return parseYtId("v", ytUrlOrUriOrId);
	}

	@NonNull
	private static String parseYtId(@NonNull String queryParam, @NonNull String ytUrlOrUriOrId) {
		Supplier<UriComponentsBuilder> fromHttpUrlSupplier =
				() -> UriComponentsBuilder.fromHttpUrl(ytUrlOrUriOrId);
		Supplier<UriComponentsBuilder> fromUriStringSupplier =
				() -> UriComponentsBuilder.fromUriString(ytUrlOrUriOrId);
		Optional<String> ytId = parseYtIdQueryParameter(queryParam, fromHttpUrlSupplier);
		if (ytId.isPresent()) {
			return ytId.get();
		}
		ytId = parseYtIdQueryParameter(queryParam, fromUriStringSupplier);
		if (ytId.isPresent()) {
			return ytId.get();
		}
		ytId = parseYtIdPathParameter(fromHttpUrlSupplier);
		if (ytId.isPresent()) {
			return ytId.get();
		}
		ytId = parseYtIdPathParameter(fromUriStringSupplier);
		return ytId.orElseGet(ytUrlOrUriOrId::trim);
	}

	private static Optional<String> parseYtIdQueryParameter(
			String queryParam, Supplier<UriComponentsBuilder> uriComponentsBuilderSupplier) {
		try {
			String ytId = uriComponentsBuilderSupplier.get()
					.build().getQueryParams().getFirst(queryParam);
			return Optional.ofNullable(ytId).filter(org.springframework.util.StringUtils::hasText);
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	private Optional<String> parseYtIdPathParameter(
			Supplier<UriComponentsBuilder> uriComponentsBuilderSupplier) {
		try {
			UriComponentsBuilder uriComponentsBuilder = uriComponentsBuilderSupplier.get();
			return getPathWithoutSeparator(uriComponentsBuilder);
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	private static Optional<String> getPathWithoutSeparator(
			UriComponentsBuilder uriComponentsBuilder) {
		String path = uriComponentsBuilder.build().getPath();
		return Optional.ofNullable(path).map(p -> p.substring(1));
	}
}
