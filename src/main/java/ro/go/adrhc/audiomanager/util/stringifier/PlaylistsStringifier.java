package ro.go.adrhc.audiomanager.util.stringifier;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.context.AppPathType;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.domain.location.LocationPredicates;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;

import java.nio.file.Path;
import java.util.Collection;

import static ro.go.adrhc.util.text.StringUtils.concat;

@Component
@RequiredArgsConstructor
public class PlaylistsStringifier {
	private final AppPaths appPaths;
	private final LocationStringifier locationStringifier;

	public String toString(AppPathType rootPathType, Collection<? extends Playlist<?>> playlists) {
		return new RelativePathPlaylistsStringifier(rootPathType).toString(playlists);
	}

	@RequiredArgsConstructor
	private class RelativePathPlaylistsStringifier {
		private static final String PLAYLISTS_SUMMARY = "%s%n%d playlists in %s";
		private static final String PLAYLIST_DETAILS = "%-4s local, %-3s streams, %s";
		private final AppPathType rootPathType;

		public String toString(Collection<? extends Playlist<?>> playlists) {
			Path rootPath = appPaths.getByType(rootPathType);
			String playlistsStrings = concat(this::toString, playlists);
			return playlistsSummary(playlistsStrings, playlists.size(), rootPath);
		}

		private String toString(Playlist<?> playlist) {
			String plLocation = locationStringifier.toString(rootPathType, playlist);
			int diskLocationsSize = playlist.entries().filter(
					LocationPredicates::hasDiskLocation).size();
			int streamsCount = playlist.size() - diskLocationsSize;
			return playlistDetails(diskLocationsSize, streamsCount, plLocation);
		}

		private static String playlistsSummary(String playlistsStrings, int count, Path rootPath) {
			return PLAYLISTS_SUMMARY.formatted(playlistsStrings, count, rootPath);
		}

		private static String playlistDetails(int diskLocationsSize, int streamsCount,
				String plLocation) {
			return PLAYLIST_DETAILS.formatted(
					diskLocationsSize > 0 ? String.valueOf(diskLocationsSize) : "no",
					streamsCount > 0 ? String.valueOf(streamsCount) : "no", plLocation);
		}
	}
}
