package ro.go.adrhc.audiomanager.util;

import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.audiomanager.datasources.disk.playlists.PlaylistStorageOutcome;

@Slf4j
public class LogUtils {
	public static void log(PlaylistStorageOutcome storageOutcome) {
		if (storageOutcome.stored()) {
			log.debug("\n{}", storageOutcome);
		} else {
			log.warn("\n{}", storageOutcome);
		}
	}
}
