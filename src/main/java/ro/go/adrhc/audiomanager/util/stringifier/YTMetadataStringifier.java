package ro.go.adrhc.audiomanager.util.stringifier;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.youtube.config.YouTubeProperties;
import ro.go.adrhc.audiomanager.datasources.youtube.metadata.domain.YouTubeMetadata;
import ro.go.adrhc.util.text.StringUtils;

import java.util.Collection;

@Component
@RequiredArgsConstructor
public class YTMetadataStringifier {
	private final YouTubeProperties ytProperties;

	public String toString(YouTubeMetadata metadata) {
		return "%s, %s, %s".formatted(ytProperties.ytUrl(metadata.location()),
				metadata.location().code(), metadata.title());
	}

	public String concat(Collection<YouTubeMetadata> metadata) {
		return StringUtils.concat(metadata.stream().map(this::toString));
	}
}
