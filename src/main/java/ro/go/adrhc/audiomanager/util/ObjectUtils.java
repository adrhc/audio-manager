package ro.go.adrhc.audiomanager.util;

import java.util.Arrays;
import java.util.Objects;

public class ObjectUtils {
	/**
	 * see also java.util.Objects.requireNonNullElse
	 */
	@SafeVarargs
	public static <T> T firstNotNull(T... t) {
		return Arrays.stream(t).filter(Objects::nonNull).findFirst().orElse(null);
	}
}
