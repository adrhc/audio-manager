package ro.go.adrhc.audiomanager.util;

import lombok.experimental.UtilityClass;

import java.io.File;

@UtilityClass
public class OSUtils {
	public static boolean isLinux() {
		return File.separatorChar == '/';
	}
}
