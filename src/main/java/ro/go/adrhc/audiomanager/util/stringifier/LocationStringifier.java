package ro.go.adrhc.audiomanager.util.stringifier;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.context.AppPathType;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;

import java.nio.file.Path;

@Component
@RequiredArgsConstructor
public class LocationStringifier {
	private final AppPaths appPaths;

	public String toString(AppPathType rootPathType, Playlist<?> playlist) {
		Path rootPath = appPaths.getByType(rootPathType);
		Location location = playlist.location();
		if (location instanceof DiskLocation dl) {
			return rootPath.relativize(dl.path()).toString();
		} else {
			return location.toString();
		}
	}
}
