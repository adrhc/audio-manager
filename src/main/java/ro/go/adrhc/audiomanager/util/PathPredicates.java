package ro.go.adrhc.audiomanager.util;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import static ro.go.adrhc.util.text.StringUtils.hasText;

public class PathPredicates {
	public static final Predicate<String> FILE_URI_MATCHER =
			Pattern.compile("^file:///").asPredicate();
	public static final Predicate<String> SHORT_FILE_URI_MATCHER =
			Pattern.compile("^file:/").asPredicate();
	public static final Predicate<String> ABSOLUTE_LINUX_PATH_MATCHER =
			Pattern.compile("^/").asPredicate();
	public static final Predicate<String> ABSOLUTE_WIN_PATH_MATCHER =
			Pattern.compile("^[a-zA-Z]:").asPredicate();
	public static final Predicate<String> URL_MATCHER =
			Pattern.compile("^.*://").asPredicate();
	public static final Predicate<String> YOUTUBE_PREFIXED_URL_MATCHER =
			Pattern.compile("^[a-zA-Z]+:https?://").asPredicate();
	private static final String NID = "([a-zA-Z0-9][-a-zA-Z0-9]{1,31})";
	/**
	 * https://www.ietf.org/rfc/rfc2141.txt
	 * <URN> ::= "urn:" <NID> ":" <NSS>
	 * <NID>         ::= <let-num> [ 1,31<let-num-hyp> ]
	 * <NSS>         ::= 1*<URN chars>
	 */
	public static final Predicate<String> YOUTUBE_URN =
			Pattern.compile("^(yt|youtube|ytmusic):" + NID + ":([\\w()+,-.=@;$_!*']+)$")
					.asMatchPredicate();
	/**
	 * ":" is not included in NSS
	 * \\w includes "_"
	 */
	public static final Predicate<String> URN_LIKE_MATCHER =
			Pattern.compile("^(\\w+):" + NID + ":([\\w()+,-.=@;$_!*']+)$")
					.asMatchPredicate();

	public static boolean isFileUri(String path) {
		return FILE_URI_MATCHER.test(path);
	}

	public static boolean isYouTubeURN(String urn) {
		return hasText(urn) && YOUTUBE_URN.test(urn);
	}
}
