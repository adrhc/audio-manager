package ro.go.adrhc.audiomanager.util;

import com.jayway.jsonpath.DocumentContext;
import lombok.experimental.UtilityClass;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

@UtilityClass
public class JsonPathUtils {
	public static void keep(String path, String include, DocumentContext dc) {
		Set<String> properties = dc.read(STR."\{path}.keys()", Set.class);
		(new HashSet<>(properties)).stream()
				.filter(Predicate.not(include::equals))
				.forEach(p -> dc.delete(STR."\{path}.\{p}"));
	}

	public static void keep(String path, Collection<String> include, DocumentContext dc) {
		Set<String> properties = dc.read(STR."\{path}.keys()", Set.class);
		(new HashSet<>(properties)).stream()
				.filter(Predicate.not(include::contains))
				.forEach(p -> dc.delete(STR."\{path}.\{p}"));
	}
}
