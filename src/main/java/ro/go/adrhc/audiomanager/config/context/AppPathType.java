package ro.go.adrhc.audiomanager.config.context;

public enum AppPathType {
	INDEX, PLAYLISTS, SONGS, BACKUPS, SCRIPTS
}
