package ro.go.adrhc.audiomanager.config.context.validation;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.context.AppPathType;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static ro.go.adrhc.util.text.StringUtils.concat;

/**
 * Path validation requirements are converted to validators which
 * upon validation return error messages that are joined and then
 * are thrown as RuntimeException (see throwIfInvalid()).
 * <p>
 * AppPaths values can be changed from the command line, that's why SCOPE_PROTOTYPE is used.
 */
@Component
@Scope(SCOPE_PROTOTYPE)
@RequiredArgsConstructor
public class AppPathsValidator {
	private final List<AppPathValidator> validators = new ArrayList<>();

	public AppPathsValidator addCtxPathTypes(AppPathType... appPathTypes) {
		Arrays.stream(appPathTypes)
				.map(this::appDirectoryValidator)
				.forEach(validators::add);
		return this;
	}

	public AppPathsValidator addPlaylist(DiskLocation plLocation) {
		validators.add(appRelativePathValidator(AppPathType.PLAYLISTS, plLocation.path()));
		return this;
	}

	public AppPathsValidator addPlaylist(Path relativeOrAbsolutePlaylistPath) {
		validators.add(
				appRelativePathValidator(AppPathType.PLAYLISTS, relativeOrAbsolutePlaylistPath));
		return this;
	}

	public AppPathsValidator addBackup(Path relativeOrAbsoluteBackupPath) {
		validators.add(appRelativePathValidator(AppPathType.BACKUPS, relativeOrAbsoluteBackupPath));
		return this;
	}

	/**
	 * throws a RuntimeException containing the failed validation messages
	 */
	public void throwIfInvalid() {
		List<String> errorMessages = validate();
		if (!errorMessages.isEmpty()) {
			throw new RuntimeException(concat(errorMessages));
		}
	}

	/**
	 * @return the failed validation messages separated by new line
	 */
	protected List<String> validate() {
		return validators.stream().flatMap(v -> v.validate().stream()).toList();
	}

	@Lookup
	protected AppRelativePathValidator appRelativePathValidator(
			AppPathType pathType, Path relativePath) {
		return null;
	}

	@Lookup
	protected AppDirectoryValidator appDirectoryValidator(AppPathType ctxPathType) {
		return null;
	}
}
