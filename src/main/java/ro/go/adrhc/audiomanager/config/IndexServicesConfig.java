package ro.go.adrhc.audiomanager.config;

import lombok.RequiredArgsConstructor;
import org.apache.lucene.search.Query;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.FoundAudioFactory;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.domain.SearchedAudio;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.search.BestAudioFileMatcher;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.search.DiskLocationsIndexSearchService;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.search.SearchedAudioToLuceneQueryConverterFactory;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.persistence.lucene.FileSystemIndex;
import ro.go.adrhc.persistence.lucene.core.bare.token.TokenComparisonUtils;

import java.net.URI;
import java.util.function.Function;

@Configuration
@RequiredArgsConstructor
public class IndexServicesConfig<SA extends SearchedAudio> {
	private final FoundAudioFactory<SA> foundAudioFactory;
	private final TokenComparisonUtils tokenUtils;
	private final FileSystemIndex<URI, AudioMetadata> amIndexRepository;
	private final SearchedAudioToLuceneQueryConverterFactory<SA>
			searchedAudioToLuceneQueryConverterFactory;

	@Bean
	public DiskLocationsIndexSearchService<SA> diskLocationsIndexSearchService() {
		return new DiskLocationsIndexSearchService<>(
				searchedAudioToLuceneQueryConverterFactory.create()::convert,
				foundAudioFactory, this::createBestAudioFileMatcher, amIndexRepository);
	}

	private BestAudioFileMatcher<SA> createBestAudioFileMatcher(
			Function<Query, SA> toSearchedAudio) {
		return new BestAudioFileMatcher<>(tokenUtils, toSearchedAudio);
	}
}
