package ro.go.adrhc.audiomanager.config.plspec;

import ro.go.adrhc.audiomanager.domain.location.Location;
import ro.go.adrhc.util.collection.SetUtils;
import ro.go.adrhc.util.text.StringUtils;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

public record PlaylistSpec(String filename, Set<Location> locations) {
	public static Optional<PlaylistSpec> merge(Collection<PlaylistSpec> playlistSpecs) {
		return playlistSpecs.stream().reduce(PlaylistSpec::merge);
	}

	/**
	 * The filename will be this.filename! i.e. the one of the PlaylistSpec into which the merge occurred
	 */
	public PlaylistSpec merge(PlaylistSpec other) {
		return new PlaylistSpec(this.filename, SetUtils.union(locations, other.locations));
	}

	public boolean contains(Location location) {
		return locations.contains(location);
	}

	public boolean isEmpty() {
		return locations == null || locations.isEmpty();
	}

	public int size() {
		return locations.size();
	}

	public <R> Stream<R> map(Function<? super Location, ? extends R> mapper) {
		return locations.stream().map(mapper);
	}

	@Override
	public String toString() {
		String locationsText = StringUtils.concat("\n\t- ", locations);
		return """
				filename:  %s
				locations: %s"""
				.formatted(filename, locationsText.isBlank() ? "" : "\n\t- " + locationsText);
	}
}
