package ro.go.adrhc.audiomanager.config.context.validation;

import java.util.Optional;

public interface AppPathValidator {
	Optional<String> validate();
}
