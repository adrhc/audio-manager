package ro.go.adrhc.audiomanager.config.context;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.List;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class ObservableAppPaths {
	private final AppPaths appPaths;
	private final List<IndexPathObserver> pathsObservers;

	public static ObservableAppPaths empty() {
		return new ObservableAppPaths(new AppPaths(), List.of());
	}

	public void copy(AppPaths newAppPaths) {
		Path oldIndexPath = newAppPaths.getIndexPath();
		appPaths.copy(newAppPaths);
		notifyObservers(oldIndexPath, appPaths.getIndexPath());
	}

	public void update(Path indexPath, Path songsPath,
			Path playlistsPath, Path backupsPath, Path scriptsPath) {
		Path oldIndexPath = appPaths.getIndexPath();
		appPaths.update(indexPath, songsPath, playlistsPath, backupsPath, scriptsPath);
		notifyObservers(oldIndexPath, indexPath);
	}

	protected void notifyObservers(Path oldIndexPath, Path newIndexPath) {
		if (!Objects.equals(oldIndexPath, newIndexPath)) {
			pathsObservers.forEach(IndexPathObserver::indexPathChanged);
		}
	}

	public AppPaths cloneAppPaths() {
		return appPaths.clone();
	}

	public Path resolvePlaylistPath(String playlistFilename) {
		return appPaths.resolvePlaylistPath(playlistFilename);
	}

	public Path resolveSongsPath(Path path) {
		return appPaths.resolveSongsPath(path);
	}

	public Path getPlaylistsPath() {
		return appPaths.getPlaylistsPath();
	}

	public void setPlaylistsPath(Path playlistsPath) {
		appPaths.setPlaylistsPath(playlistsPath);
	}

	public Path getBackupsPath() {
		return appPaths.getBackupsPath();
	}

	public void setBackupsPath(Path backupsPath) {
		appPaths.setBackupsPath(backupsPath);
	}

	public Path getIndexPath() {
		return appPaths.getIndexPath();
	}

	public void setIndexPath(Path newIndexPath) {
		Path oldIndexPath = appPaths.getIndexPath();
		appPaths.setIndexPath(newIndexPath);
		notifyObservers(oldIndexPath, newIndexPath);
	}

	public Path getScriptsPath() {
		return appPaths.getScriptsPath();
	}

	public void setScriptsPath(Path scriptsPath) {
		appPaths.setScriptsPath(scriptsPath);
	}

	public Path getSongsPath() {
		return appPaths.getSongsPath();
	}

	public void setSongsPath(Path songsPath) {
		appPaths.setSongsPath(songsPath);
	}

	public Path getIndexPathParent() {
		return appPaths.getIndexPathParent();
	}
}
