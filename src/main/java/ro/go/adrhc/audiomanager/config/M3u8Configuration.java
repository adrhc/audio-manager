package ro.go.adrhc.audiomanager.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ro.go.adrhc.lib.m3u8.DiskM3u8Repository;
import ro.go.adrhc.lib.m3u8.M3u8DiskLoader;
import ro.go.adrhc.lib.m3u8.M3u8DiskWriter;
import ro.go.adrhc.lib.m3u8.domain.ExtinfParser;
import ro.go.adrhc.util.io.FileSystemUtils;

@Configuration
@RequiredArgsConstructor
public class M3u8Configuration {
	private final FileSystemUtils fsUtils;

	@Bean
	public ExtinfParser extinfParser() {
		return new ExtinfParser();
	}

	@Bean
	public M3u8DiskLoader m3u8Loader() {
		return new M3u8DiskLoader(extinfParser());
	}

	@Bean
	public M3u8DiskWriter m3u8Writer() {
		return new M3u8DiskWriter();
	}

	@Bean
	public DiskM3u8Repository diskM3u8Repository() {
		return new DiskM3u8Repository(fsUtils, m3u8Loader(), m3u8Writer());
	}
}
