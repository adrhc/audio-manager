package ro.go.adrhc.audiomanager.config.context;

public interface IndexPathObserver {
	void indexPathChanged();
}
