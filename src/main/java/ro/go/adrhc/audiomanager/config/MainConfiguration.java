package ro.go.adrhc.audiomanager.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ro.go.adrhc.audiomanager.config.context.AppPathType;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.config.context.validation.AppDirectoryValidator;
import ro.go.adrhc.audiomanager.config.context.validation.AppRelativePathValidator;
import ro.go.adrhc.util.io.FileSystemUtils;

import java.nio.file.Path;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

@Configuration
@RequiredArgsConstructor
public class MainConfiguration {
	private final AppPaths appPaths;
	private final FileSystemUtils fileSystemUtils;

	@Bean
	public ExecutorService cachedThreadPool() {
		return Executors.newCachedThreadPool();
	}

	@Bean
	public ExecutorService fixedThreadPool(MainProperties appProperties) {
		return Executors.newFixedThreadPool(appProperties.getMetadataLoadingThreads());
	}

	@Bean
	@Scope(SCOPE_PROTOTYPE)
	public AppDirectoryValidator appDirectoryValidator(AppPathType ctxPathType) {
		return new AppDirectoryValidator(appPaths, ctxPathType);
	}

	@Bean
	@Scope(SCOPE_PROTOTYPE)
	public AppRelativePathValidator appRelativePathValidator(
			AppPathType ctxPathType, Path relativePath) {
		return new AppRelativePathValidator(fileSystemUtils, appPaths, ctxPathType, relativePath);
	}
}
