package ro.go.adrhc.audiomanager.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.PathsFixProperties;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.dlscripts.DlBashScriptsDirectory;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.dlscripts.DlScriptsProperties;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.BackupFilesDirectory;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.playlistfiledirectory.PlaylistFilesDirectory;
import ro.go.adrhc.audiomanager.infrastructure.filesystem.plfiles.playlistfiledirectory.PlaylistsFilterFactory;
import ro.go.adrhc.audiomanager.lib.PartitionedLinesFileWriter;
import ro.go.adrhc.util.io.FileSystemUtils;
import ro.go.adrhc.util.io.SimpleDirectory;
import ro.go.adrhc.util.io.SupportedExtensions;

import static ro.go.adrhc.util.io.PathUtils.RWX;

@Configuration
@RequiredArgsConstructor
public class FSConfiguration {
	private static final SupportedExtensions SUPPORTED_PLAYLIST_EXTENSIONS =
			SupportedExtensions.of("m3u8");
	private final AppPaths appPaths;
	private final MainProperties appProperties;
	private final PathsFixProperties pathsFixProperties;
	private final PlaylistsFilterFactory playlistsFilterFactory;

	@Bean
	public PlaylistFilesDirectory playlistFilesDirectory() {
		return new PlaylistFilesDirectory(fileSystemUtils(),
				appPaths::getPlaylistsPath,
				SUPPORTED_PLAYLIST_EXTENSIONS::supports,
				pathsFixProperties, playlistsFilterFactory);
	}

	@Bean
	public BackupFilesDirectory backupFilesDirectory() {
		return new BackupFilesDirectory(fileSystemUtils(),
				appPaths::getBackupsPath, SUPPORTED_PLAYLIST_EXTENSIONS::supports);
	}

	@Bean
	public SimpleDirectory audioFilesDirectory() {
		SupportedExtensions supportedExtensions = appProperties.getSupportedExtensions();
		return SimpleDirectory.of(fileSystemUtils(),
				appPaths::getSongsPath, supportedExtensions::supports);
	}

	@Bean
	public DlBashScriptsDirectory dlBashScriptsDirectory(DlScriptsProperties dlScriptsProperties) {
		return new DlBashScriptsDirectory(dlScriptsProperties, appPaths::getScriptsPath);
	}

	@Bean
	public PartitionedLinesFileWriter partitionedLinesFileWriter() {
		return new PartitionedLinesFileWriter(appPaths::getScriptsPath, RWX);
	}

	@Bean
	public FileSystemUtils fileSystemUtils() {
		return new FileSystemUtils();
	}
}
