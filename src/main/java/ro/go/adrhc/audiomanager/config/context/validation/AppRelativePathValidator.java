package ro.go.adrhc.audiomanager.config.context.validation;

import lombok.RequiredArgsConstructor;
import ro.go.adrhc.audiomanager.config.context.AppPathType;
import ro.go.adrhc.audiomanager.config.context.AppPaths;
import ro.go.adrhc.util.io.FileSystemUtils;

import java.nio.file.Path;
import java.util.Optional;

import static ro.go.adrhc.util.io.PathUtils.isNullOrRelative;
import static ro.go.adrhc.util.io.PathUtils.resolve;

@RequiredArgsConstructor
public class AppRelativePathValidator implements AppPathValidator {
	private final FileSystemUtils fileSystemUtils;
	private final AppPaths appPaths;
	private final AppPathType rootPathType;
	private final Path path;

	public Optional<String> validate() {
		if (isNullOrRelative(getRootPath()) && isNullOrRelative(path)) {
			return Optional.of(
					"Both the %s file name (%s) and the root path (%s) are null or relative!"
							.formatted(rootPathType, path, getRootPath()));
		}
		Path resolvedPath = resolvedPath();
		if (fileSystemUtils.isRegularFile(resolvedPath)) {
			return Optional.empty();
		} else {
			return Optional.of("%s path \"%s\" is missing or is not a file!"
					.formatted(rootPathType, resolvedPath));
		}
	}

	private Path resolvedPath() {
		return path == null ? getRootPath() : resolve(getRootPath(), path);
	}

	private Path getRootPath() {
		return appPaths.getByType(rootPathType);
	}
}
