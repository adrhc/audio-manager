package ro.go.adrhc.audiomanager.config.lucene;

import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.audiofiles.index.WriteProperties;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.config.QueryProperties;
import ro.go.adrhc.audiomanager.datasources.disk.locations.index.config.SearchProperties;
import ro.go.adrhc.persistence.lucene.core.bare.analysis.TokenizerProperties;

import java.util.Set;

@ConfigurationProperties(prefix = "lucene")
@Component
@Setter
@Getter
@ToString
public class AMIndexProperties {
	public static Set<String> audioTagsToIndex;

	private boolean readOnly;
	private TokenizerProperties tokenizer;
	private QueryProperties query;
	private SearchProperties search;
	private WriteProperties write;

	@PostConstruct
	public void postConstruct() {
		audioTagsToIndex = write.getAudioTagsToIndex();
	}
}
