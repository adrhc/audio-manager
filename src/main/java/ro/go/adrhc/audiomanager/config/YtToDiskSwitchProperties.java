package ro.go.adrhc.audiomanager.config;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory;
import ro.go.adrhc.audiomanager.domain.playlist.Playlist;
import ro.go.adrhc.util.text.MessageFormats;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;

import static ro.go.adrhc.util.io.FilenameUtils.filenameNoExt;

@ConfigurationProperties(prefix = "yt-to-disk-switch")
@Component
@Getter
public class YtToDiskSwitchProperties {
	// a copied YouTube playlist (i.e. contains yt ids) filename pattern
	private MessageFormats plCopyPatterns;
	// a localized playlist (i.e. yt ids replaced with local DISK files) filename pattern
	private MessageFormat localPlPattern;

	public Playlist<DiskLocation> toLocalPlLocation(Playlist<DiskLocation> playlist) {
		DiskLocation switchedPlLocation = toLocalPlLocation(playlist.location());
		return playlist.location(switchedPlLocation);
	}

	public void setPlCopyPatterns(List<String> patterns) {
		this.plCopyPatterns = MessageFormats.of(patterns);
	}

	public void setLocalPlPattern(String pattern) {
		this.localPlPattern = new MessageFormat(pattern);
	}

	@Override
	public String toString() {
		return "inputPatterns: " + plCopyPatterns.toString() +
				"\nswitchedPattern: " + localPlPattern.toPattern();
	}

	private DiskLocation toLocalPlLocation(DiskLocation plLocation) {
		String fileNameToSwitch = plLocation.path().getFileName().toString();
		Optional<String> fileName = plCopyPatterns.parse(fileNameToSwitch).map(
				it -> (String) it[0]);
		fileName = fileName.isPresent() ? fileName : filenameNoExt(plLocation.path());
		return fileName.map(it -> new Object[]{it}).map(localPlPattern::format)
				.map(DiskLocationFactory::createDiskLocation)
				.orElseThrow();
	}
}
