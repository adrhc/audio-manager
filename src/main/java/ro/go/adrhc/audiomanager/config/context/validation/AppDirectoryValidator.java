package ro.go.adrhc.audiomanager.config.context.validation;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.audiomanager.config.context.AppPathType;
import ro.go.adrhc.audiomanager.config.context.AppPaths;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import static ro.go.adrhc.util.io.PathUtils.isNullOrRelative;

@RequiredArgsConstructor
@Slf4j
public class AppDirectoryValidator implements AppPathValidator {
	private final AppPaths appPaths;
	private final AppPathType pathType;

	public Optional<String> validate() {
		if (isNullOrRelative(getContextPath())) {
			return Optional.of("%s path \"%s\" is null or relative!"
					.formatted(pathType, getContextPath()));
		}
		if (Files.isDirectory(getContextPath())) {
			return Optional.empty();
		}
		return Optional.of("%s path \"%s\" is missing or is not a directory!"
				.formatted(pathType, getContextPath()));
	}

	private Path getContextPath() {
		return appPaths.getByType(pathType);
	}
}
