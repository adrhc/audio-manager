package ro.go.adrhc.audiomanager.config.plspec;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.apache.commons.collections4.ListUtils;
import ro.go.adrhc.audiomanager.datasources.disk.locations.domain.DiskLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocation;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory;
import ro.go.adrhc.audiomanager.domain.location.Location;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static ro.go.adrhc.audiomanager.config.plspec.RawPlaylistSpec.merge;
import static ro.go.adrhc.audiomanager.datasources.disk.locations.factory.DiskLocationFactory.createDiskLocations;

@RequiredArgsConstructor
@ToString
public class PlaylistSpecFactory {
	private final YouTubeLocationFactory ytLocationFactory;
	private final Map<String, RawPlaylistSpec> playlistSpecs;

	public Set<String> getSpecNames() {
		return playlistSpecs.keySet();
	}

	public PlaylistSpec getPlSpec(String specName) {
		RawPlaylistSpec rawSpec = getRawPlSpec(specName);
		return new PlaylistSpec(rawSpec.getTitle(), Set.copyOf(toLocations(rawSpec)));
	}

	public RawPlaylistSpec getRawPlSpec(String specName) {
		RawPlaylistSpec rawSpec = playlistSpecs.get(specName);
		Set<RawPlaylistSpec> referencedRawSpecs = toRawPlSpecs(rawSpec.getSpecs());
		return merge(referencedRawSpecs).map(rawSpec::merge).orElse(rawSpec);
	}

	private List<Location> toLocations(RawPlaylistSpec rawSpec) {
		List<DiskLocation> diskLocations = rawSpec.getDirectories() == null ?
				List.of() : createDiskLocations(rawSpec.getDirectories());
		List<YouTubeLocation> ytPlLocations = rawSpec.getYtUris() == null ?
				List.of() : rawSpec.getYtUris().stream()
				.map(ytLocationFactory::parse)
				.flatMap(Optional::stream)
				.toList();
		List<Location> specsLocations = rawSpec.getSpecs() == null ?
				List.of() : rawSpec.getSpecs().stream()
				.map(this::getPlSpec)
				.map(PlaylistSpec::locations)
				.flatMap(Set::stream)
				.toList();
		return ListUtils.union(ListUtils.union(diskLocations, ytPlLocations), specsLocations);
	}

	private Set<RawPlaylistSpec> toRawPlSpecs(Set<String> specRefs) {
		if (specRefs == null) {
			return Set.of();
		}
		return specRefs.stream().map(this::toRawPlSpecs)
				.flatMap(Set::stream)
				.collect(Collectors.toSet());
	}

	private Set<RawPlaylistSpec> toRawPlSpecs(String specName) {
		RawPlaylistSpec rawSpec = getRawPlSpec(specName);
		return rawSpec.getSpecs() == null ? Set.of(rawSpec) : toRawPlSpecs(rawSpec.getSpecs());
	}
}
