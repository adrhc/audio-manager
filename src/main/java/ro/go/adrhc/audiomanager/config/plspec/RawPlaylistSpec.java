package ro.go.adrhc.audiomanager.config.plspec;

import lombok.*;
import ro.go.adrhc.util.collection.SetUtils;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class RawPlaylistSpec {
	private String title;
	private Set<String> ytUris;
	private Set<Path> directories;
	private Set<String> specs;

	public static Optional<RawPlaylistSpec> merge(Collection<RawPlaylistSpec> specs) {
		return specs.stream().reduce(RawPlaylistSpec::merge);
	}

	public RawPlaylistSpec merge(RawPlaylistSpec other) {
		return new RawPlaylistSpec(title == null ? other.title : title,
				SetUtils.union(ytUris, other.ytUris),
				SetUtils.union(directories, other.directories),
				SetUtils.union(specs, other.specs));
	}
}
