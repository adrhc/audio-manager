package ro.go.adrhc.audiomanager.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpec;
import ro.go.adrhc.audiomanager.config.plspec.PlaylistSpecFactory;
import ro.go.adrhc.audiomanager.config.plspec.RawPlaylistSpec;
import ro.go.adrhc.audiomanager.datasources.youtube.domain.YouTubeLocationFactory;
import ro.go.adrhc.util.io.SupportedExtensions;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static ro.go.adrhc.util.CpuUtils.cpuCoresMultipliedBy;

@Component
@ConfigurationProperties(prefix = "app")
@RequiredArgsConstructor
public class MainProperties {
	private final YouTubeLocationFactory ytLocationFactory;
	@Getter
	private int metadataLoadingThreads = Runtime.getRuntime().availableProcessors();
	@Setter
	private Set<String> supportedExtensions;
	private PlaylistSpecFactory playlistSpecFactory;

	public SupportedExtensions getSupportedExtensions() {
		return SupportedExtensions.of(supportedExtensions);
	}

	public List<String> getSpecNames() {
		return playlistSpecFactory.getSpecNames().stream().sorted().toList();
	}

	public PlaylistSpec getPlaylistSpec(String specName) {
		return playlistSpecFactory.getPlSpec(specName);
	}

	public List<PlaylistSpec> getPlaylistSpecs() {
		return getSpecNames().stream().map(this::getPlaylistSpec).toList();
	}

	public void setPlaylistSpecs(Map<String, RawPlaylistSpec> playlistSpecs) {
		// todo: fix PlaylistSpecFactory concept
		this.playlistSpecFactory = new PlaylistSpecFactory(ytLocationFactory, playlistSpecs);
	}

	public void setMetadataLoadingThreads(String metadataLoadingThreads) {
		int parsedMetadataLoadingThreads = metadataLoadingThreads.endsWith("c") ?
				cpuCoresMultipliedBy(metadataLoadingThreads) : Integer.parseInt(
				metadataLoadingThreads);
		this.metadataLoadingThreads = parsedMetadataLoadingThreads > 0 ?
				parsedMetadataLoadingThreads : Runtime.getRuntime().availableProcessors();
	}

	@Override
	public String toString() {
		return "AppProperties {" +
				",\n\tmetadataLoadingThreads: " + metadataLoadingThreads +
				",\n\tsupportedExtensions: " + supportedExtensions +
				",\n\tplaylistSpecFactory: " + playlistSpecFactory +
				"\n}";
	}
}
