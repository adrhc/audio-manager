package ro.go.adrhc.audiomanager.config.lucene;

import lombok.RequiredArgsConstructor;
import org.apache.lucene.analysis.Analyzer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ro.go.adrhc.audiomanager.datasources.audiofiles.index.AMDiskSearchResultFilter;
import ro.go.adrhc.persistence.lucene.core.bare.token.TokenComparisonUtils;
import ro.go.adrhc.persistence.lucene.core.bare.token.TokenizationUtils;
import ro.go.adrhc.util.io.FileSystemUtils;

import static ro.go.adrhc.persistence.lucene.core.bare.analysis.AnalyzerFactory.defaultAnalyzer;

@Configuration
@RequiredArgsConstructor
public class AMIndexConfig {
	@Bean
	public AMDiskSearchResultFilter searchResultFilter(
			FileSystemUtils fsUtils, AMIndexProperties amIndexProperties) {
		return AMDiskSearchResultFilter.create(fsUtils, amIndexProperties);
	}

	@Bean
	public TokenComparisonUtils tokenUtils(AMIndexProperties indexProperties) {
		return new TokenComparisonUtils(tokenizationUtils(indexProperties));
	}

	@Bean
	public TokenizationUtils tokenizationUtils(AMIndexProperties indexProperties) {
		Analyzer analyzer = defaultAnalyzer(indexProperties.getTokenizer()).orElseThrow();
		return new TokenizationUtils(analyzer);
	}
}
