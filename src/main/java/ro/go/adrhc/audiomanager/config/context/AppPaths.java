package ro.go.adrhc.audiomanager.config.context;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

import static ro.go.adrhc.util.io.PathUtils.parentOf;
import static ro.go.adrhc.util.io.PathUtils.relativize;

/**
 * Role: "infrastructure" configuration-related helper
 */
@ConfigurationProperties(prefix = "context-paths")
@Component
@Getter
@Setter(AccessLevel.PACKAGE)
@Slf4j
public class AppPaths implements Cloneable {
	private Path indexPath;
	private Path songsPath;
	private Path playlistsPath;
	private Path backupsPath;
	private Path scriptsPath;

	public Path getIndexPathParent() {
		return parentOf(indexPath).orElse(null);
	}

	public Path resolveBackupPath(Path backupPath) {
		return backupsPath.resolve(backupPath);
	}

	public Path resolveSongsPath(Path path) {
		return songsPath.resolve(path);
	}

	public Path resolvePlaylistPath(String playlistPath) {
		return resolvePlaylistPath(Path.of(playlistPath));
	}

	public Path resolvePlaylistPath(Path playlistPath) {
		return playlistsPath.resolve(playlistPath);
	}

	public Path relativizeToPlaylistsRoot(Path path) {
		return relativize(playlistsPath, path);
	}

	public Path relativizeToBackupsRoot(Path path) {
		return relativize(backupsPath, path);
	}

	void copy(AppPaths appPaths) {
		update(appPaths.indexPath, appPaths.songsPath,
				appPaths.playlistsPath, appPaths.backupsPath,
				appPaths.scriptsPath);
	}

	void update(Path indexPath, Path songsPath,
			Path playlistsPath, Path backupsPath, Path scriptsPath) {
		this.indexPath = ObjectUtils.defaultIfNull(indexPath, this.indexPath);
		this.songsPath = ObjectUtils.defaultIfNull(songsPath, this.songsPath);
		this.playlistsPath = ObjectUtils.defaultIfNull(playlistsPath, this.playlistsPath);
		this.backupsPath = ObjectUtils.defaultIfNull(backupsPath, this.backupsPath);
		this.scriptsPath = ObjectUtils.defaultIfNull(scriptsPath, this.scriptsPath);
	}

	public Path getByType(AppPathType appPathType) {
		return switch (appPathType) {
			case AppPathType.INDEX -> indexPath;
			case AppPathType.SONGS -> songsPath;
			case AppPathType.PLAYLISTS -> playlistsPath;
			case AppPathType.BACKUPS -> backupsPath;
			case AppPathType.SCRIPTS -> scriptsPath;
		};
	}

	@Override
	public AppPaths clone() {
		try {
			return (AppPaths) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new AssertionError();
		}
	}
}
