package ro.go.adrhc.audiomanager.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ro.go.adrhc.audiomanager.datasources.audiofiles.metadata.AudioMetadataLoader;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;
import ro.go.adrhc.util.io.FilesMetadataLoader;
import ro.go.adrhc.util.io.SimpleDirectory;

import java.util.Optional;
import java.util.concurrent.ExecutorService;

@Configuration
@RequiredArgsConstructor
public class AMLoaderConfig {
	@Bean
	public AudioMetadataLoader audioMetadataLoader() {
		return AudioMetadataLoader.create();
	}

	@Bean
	public FilesMetadataLoader<Optional<AudioMetadata>> filesMetadataLoader(
			ExecutorService cachedThreadPool, ExecutorService fixedThreadPool,
			SimpleDirectory audioFilesDirectory) {
		return FilesMetadataLoader.create(cachedThreadPool, fixedThreadPool,
				audioFilesDirectory, audioMetadataLoader()::load);
	}
}
