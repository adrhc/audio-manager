package ro.go.adrhc.lib.m3u8.domain;

import lombok.NonNull;

import java.nio.file.Path;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public record M3u8Playlist(Path path, List<M3u8Record> records) implements Iterable<M3u8Record> {
	@NonNull
	public Iterator<M3u8Record> iterator() {
		return records.iterator();
	}

	public <R> Stream<R> map(Function<M3u8Record, R> mapper) {
		return records.stream().map(mapper);
	}
}
