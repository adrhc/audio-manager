package ro.go.adrhc.lib.m3u8.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ro.go.adrhc.util.Assert;
import ro.go.adrhc.util.text.StringBuilderEx;

import java.net.URI;

import static ro.go.adrhc.util.text.StringUtils.hasText;

@Accessors(fluent = true)
@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
public class M3u8Record {
	private String location;
	private Extinf extinf;

	public static M3u8Record ofLocation(String location) {
		return new M3u8Record(location, null);
	}

	public static M3u8Record of(String location, Integer seconds) {
		return new M3u8Record(location, Extinf.of(seconds));
	}

	public static M3u8Record of(URI uri, String title) {
		return new M3u8Record(uri.toString(), title == null ? null : Extinf.of(title));
	}

	public static M3u8Record of(String location, Integer seconds, String title) {
		return new M3u8Record(location, new Extinf(seconds, title));
	}

	public static M3u8Record of(Extinf extinf) {
		return new M3u8Record(null, extinf);
	}

	public boolean hasLocation() {
		return hasText(location);
	}

	public Integer seconds() {
		return extinf == null ? null : extinf.seconds();
	}

	public String title() {
		return extinf == null ? null : extinf.title();
	}

	public String toString() {
		StringBuilderEx sb = new StringBuilderEx("\n");
		if (extinf != null) {
			sb.append(extinf.toString());
		}
		if (location != null) {
			sb.append(location);
		}
		String text = sb.toString();
		Assert.isTrue(hasText(text), "m3u8 record should not be empty!");
		return text;
	}
}
