package ro.go.adrhc.lib.m3u8.domain;

import static ro.go.adrhc.util.text.StringUtils.hasText;

public record Extinf(Integer seconds, String title) {
	public static final Integer UNKNOWN_SECONDS = -1;
	private static final String EXTINF_PATTERN = "#EXTINF:%s,%s";
	private static final String EXTINF_SHORT_PATTERN = "#EXTINF:%s";

	public static Extinf of(Integer seconds) {
		return new Extinf(seconds, null);
	}

	public static Extinf of(String title) {
		return new Extinf(null, title);
	}

	@Override
	public String toString() {
		Integer seconds = this.seconds == null ? UNKNOWN_SECONDS : this.seconds;
		if (hasTitle()) {
			return EXTINF_PATTERN.formatted(seconds, title);
		} else if (hasSeconds()) {
			return EXTINF_SHORT_PATTERN.formatted(seconds);
		} else {
			throw new IllegalArgumentException("Extinf should not be empty!");
		}
	}

	public boolean hasTitle() {
		return hasText(title);
	}

	public boolean hasSeconds() {
		return seconds != null;
	}
}
