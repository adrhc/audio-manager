package ro.go.adrhc.lib.m3u8;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import ro.go.adrhc.lib.m3u8.domain.ExtinfParser;
import ro.go.adrhc.lib.m3u8.domain.M3u8Playlist;
import ro.go.adrhc.lib.m3u8.domain.M3u8Record;
import ro.go.adrhc.util.text.StringUtils;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.function.Predicate.not;

@RequiredArgsConstructor
public class M3u8DiskLoader {
	public static final String EXTM3U = "#EXTM3U";
	private final ExtinfParser extinfParser;

	@SneakyThrows
	public M3u8Playlist load(Path plPath) {
		try (BufferedReader reader = Files.newBufferedReader(plPath, UTF_8)) {
			List<M3u8Record> records = reader.lines()
					.filter(StringUtils::hasText)
					.filter(not(M3u8DiskLoader::isM3u8Header))
					.collect(ArrayList::new, (list, line) -> {
						Optional<M3u8Record> optionalExtinfBasedM3u8Record = extinfParser.parse(
								line).map(
								M3u8Record::of);
						optionalExtinfBasedM3u8Record.ifPresentOrElse(list::add,
								() -> addLocation(list, line));
					}, List::addAll);
			return new M3u8Playlist(plPath, records);
		}
	}

	private static void addLocation(List<M3u8Record> list, String location) {
		if (list.isEmpty()) {
			// this is 1st line
			list.add(M3u8Record.ofLocation(location));
		} else {
			// this is not 1st line
			M3u8Record m3u8Record = list.get(list.size() - 1);
			if (m3u8Record.hasLocation()) {
				// m3u8Record is the previously completely created M3u8Record
				list.add(M3u8Record.ofLocation(location));
			} else {
				// m3u8Record is the previously EXTINF based M3u8Record
				m3u8Record.location(location);
			}
		}
	}

	private static boolean isM3u8Header(String line) {
		return line.startsWith(EXTM3U);
	}
}
