package ro.go.adrhc.lib.m3u8.domain;

import ro.go.adrhc.util.Assert;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ro.go.adrhc.util.text.StringUtils.hasText;

public class ExtinfParser {
	private static final Pattern EXTINF_PATTERN = Pattern.compile("#EXTINF:(-?\\d+)(,(.+)?)?");

	public Optional<Extinf> parse(String extinf) {
		Matcher matcher = EXTINF_PATTERN.matcher(extinf);
		if (matcher.matches()) {
			String duration = matcher.group(1);
			String title = matcher.group(3);
			Assert.isTrue(hasText(duration) || hasText(title),
					"The title or duration must be not empty!");
			return Optional.of(new Extinf(duration == null
					? null : Integer.valueOf(duration), title));
		}
		return Optional.empty();
	}
}
