package ro.go.adrhc.lib.m3u8;

import lombok.RequiredArgsConstructor;
import ro.go.adrhc.lib.m3u8.domain.M3u8Playlist;
import ro.go.adrhc.util.io.FileSystemUtils;

import java.nio.file.Path;
import java.util.Optional;

@RequiredArgsConstructor
public class DiskM3u8Repository {
	private final FileSystemUtils fsUtils;
	private final M3u8DiskLoader m3U8DiskLoader;
	private final M3u8DiskWriter m3U8DiskWriter;

	public Optional<M3u8Playlist> get(Path plPath) {
		if (fsUtils.isRegularFile(plPath)) {
			return Optional.of(load(plPath));
		} else {
			return Optional.empty();
		}
	}

	public M3u8Playlist load(Path plPath) {
		return m3U8DiskLoader.load(plPath);
	}

	public boolean store(M3u8Playlist m3u8Playlist) {
		return m3U8DiskWriter.write(m3u8Playlist);
	}
}
