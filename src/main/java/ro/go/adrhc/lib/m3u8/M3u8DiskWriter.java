package ro.go.adrhc.lib.m3u8;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.lib.m3u8.domain.M3u8Playlist;
import ro.go.adrhc.lib.m3u8.domain.M3u8Record;

import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.*;
import static ro.go.adrhc.lib.m3u8.M3u8DiskLoader.EXTM3U;

@RequiredArgsConstructor
@Slf4j
public class M3u8DiskWriter {
	public boolean write(M3u8Playlist playlist) {
		try (PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(
				Files.newOutputStream(playlist.path(), CREATE, WRITE, TRUNCATE_EXISTING), UTF_8))) {
			printWriter.println(EXTM3U);
			playlist.records().forEach(printWriter::println);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			log.error("\nFailed to write {}!", playlist.path());
		}
		return false;
	}

	public boolean append(Path plPath, String prefix, M3u8Record record) {
		try (PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(
				Files.newOutputStream(plPath, CREATE, WRITE, APPEND), UTF_8))) {
			if (prefix != null) {
				printWriter.write(prefix);
			}
			printWriter.println(record);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			log.error("\nFailed to append {} to {}!", record, plPath);
		}
		return false;
	}
}
