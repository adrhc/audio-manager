package ro.go.adrhc.lib.audiometadata.domain;

import com.rainerhahnekamp.sneakythrow.functional.SneakyFunction;
import lombok.experimental.UtilityClass;
import ro.go.adrhc.audiomanager.domain.location.LocationType;

import java.io.File;
import java.nio.file.Path;
import java.util.Optional;

@UtilityClass
public class AudioMetadataAccessors {
	public static <T, E extends Exception> Optional<T> fileMap(AudioMetadata metadata,
			SneakyFunction<? super File, T, E> mapper)
			throws E {
		Optional<File> fileOptional = file(metadata);
		if (fileOptional.isEmpty()) {
			return Optional.empty();
		} else {
			return Optional.ofNullable(mapper.apply(fileOptional.get()));
		}
	}

	public static File file(AudioMetadata metadata, File defaultFile) {
		return file(metadata).orElse(defaultFile);
	}

	public static Optional<File> file(AudioMetadata metadata) {
		if (metadata.locationType() == LocationType.DISK) {
			return Optional.of(new File(metadata.uri()));
		} else {
			return Optional.empty();
		}
	}

	public static Path path(AudioMetadata metadata, Path defaultPath) {
		return path(metadata).orElse(defaultPath);
	}

	public static Optional<Path> path(AudioMetadata metadata) {
		if (metadata.locationType() == LocationType.DISK) {
			return Optional.of(Path.of(metadata.uri()));
		} else {
			return Optional.empty();
		}
	}
}
