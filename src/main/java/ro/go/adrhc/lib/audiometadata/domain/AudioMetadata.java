package ro.go.adrhc.lib.audiometadata.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.audiomanager.domain.location.LocationType;
import ro.go.adrhc.persistence.lucene.core.typed.Indexable;

import java.net.URI;
import java.nio.file.Path;
import java.time.Instant;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;
import static org.apache.commons.io.FilenameUtils.removeExtension;
import static ro.go.adrhc.audiomanager.config.lucene.AMIndexProperties.audioTagsToIndex;
import static ro.go.adrhc.audiomanager.domain.location.LocationType.DISK;
import static ro.go.adrhc.util.collection.SetUtils.sortThenJoin;

@Slf4j
public record AudioMetadata(URI uri, String fileNameNoExt,
		AudioTags audioTags, AudioDurations audioDurations,
		HashSet<String> tags, Instant updateMoment, LocationType locationType)
		implements Indexable<URI, AudioMetadata>, Comparable<AudioMetadata> {

	public static AudioMetadata of(Path path) {
		return of(path, new AudioTags(), new AudioDurations());
	}

	public static AudioMetadata of(URI uri, LocationType locationType,
			String fileNameNoExt, AudioTags audioTags) {
		return new AudioMetadata(uri, fileNameNoExt, audioTags,
				new AudioDurations(), new HashSet<>(), Instant.now(), locationType);
	}

	public static AudioMetadata of(Path path, LocationType locationType, AudioTags audioTags) {
		String fileNameNoExt = removeExtension(path.getFileName().toString());
		return new AudioMetadata(path.toUri(), fileNameNoExt, audioTags,
				new AudioDurations(), new HashSet<>(), Instant.now(), locationType);
	}

	public static AudioMetadata of(Path path,
			AudioTags audioTags, AudioDurations audioDurations) {
		String fileNameNoExt = removeExtension(path.getFileName().toString());
		return new AudioMetadata(path.toUri(), fileNameNoExt, audioTags,
				audioDurations, new HashSet<>(), Instant.now(), DISK);
	}

	public Set<String> songTags(Set<String> audioTagsToIndex) {
		return audioTagsToIndex.stream()
				.map(this::getSimilarTagNames)
				.flatMap(Set::stream)
				.map(this::getTag)
				.filter(not(String::isBlank))
				.collect(Collectors.toSet());
	}

	public void updateDurationFromTags() {
		audioTags.getSimilarTagNames("duration")
				.forEach(key -> audioDurations.addDurations(audioTags.getTag(key)));
	}

	@Override
	@JsonIgnore
	public URI id() {
		return uri;
	}

	@JsonIgnore
	public String getDurationsAsString() {
		if (!audioDurations.hasDuration()) {
			return null;
		}
		return sortThenJoin(audioDurations.getDurationsSet());
	}

	@JsonIgnore
	public String getWords() {
		Set<String> wordsTokens = new HashSet<>(songTags(audioTagsToIndex));
		wordsTokens.add(fileNameNoExt);
		return sortThenJoin(wordsTokens);
	}

	public boolean hasDuration() {
		return audioDurations.hasDuration();
	}

	@JsonIgnore
	public Set<String> getSimilarTagNames(String key) {
		return audioTags.getSimilarTagNames(key);
	}

	@JsonIgnore
	public String getTag(String key) {
		return audioTags.getTag(key);
	}

	public void putAudioTag(String key, String value) {
		audioTags.putTag(key, value);
	}

	public void putAudioTag(String key, String value, boolean overwrite) {
		audioTags.putTag(key, value, overwrite);
	}

	public void putAudioTags(Map<String, ?> tags) {
		audioTags.putTags(tags);
	}

	public boolean addTag(String tag) {
		return tags.add(tag);
	}

	public void addDuration(int frameLength, float frameRate, int channels, int frameSize) {
		audioDurations.addDuration(frameLength, frameRate, channels, frameSize);
	}

	public void addDurations(String spaceSeparatedDurations) {
		audioDurations.addDurations(spaceSeparatedDurations);
	}

	@Override
	public int compareTo(AudioMetadata audioFileMetadata) {
		return uri.compareTo(audioFileMetadata.uri);
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof AudioMetadata afm)) {
			return false;
		}
		return Objects.equals(uri, afm.uri);
	}

	@Override
	public AudioMetadata merge(AudioMetadata another) {
		AudioTags anotherAudioTags = another.audioTags();
		AudioDurations anotherAudioDurations = another.audioDurations();
		HashSet<String> anotherTags = another.tags();
		return new AudioMetadata(uri, another.fileNameNoExt,
				anotherAudioTags.isEmpty() ? this.audioTags : anotherAudioTags,
				anotherAudioDurations.isEmpty() ? this.audioDurations : anotherAudioDurations,
				anotherTags.isEmpty() ? this.tags : anotherTags,
				another.updateMoment, another.locationType);
	}
}
