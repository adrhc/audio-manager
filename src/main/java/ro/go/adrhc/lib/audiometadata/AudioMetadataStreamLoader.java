package ro.go.adrhc.lib.audiometadata;

import lombok.RequiredArgsConstructor;

import java.io.InputStream;

@RequiredArgsConstructor
public abstract class AudioMetadataStreamLoader implements AudioMetadataLoader {
	protected final InputStream inputStream;
}
