package ro.go.adrhc.lib.audiometadata.loaders;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.farng.mp3.AbstractMP3FragmentBody;
import org.farng.mp3.MP3File;
import org.farng.mp3.TagConstant;
import org.farng.mp3.id3.*;
import org.farng.mp3.object.ObjectNumberHashMap;
import org.farng.mp3.object.ObjectStringSizeTerminated;
import ro.go.adrhc.lib.audiometadata.AudioMetadataLoader;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;

import java.util.Iterator;
import java.util.Optional;

import static ro.go.adrhc.lib.audiometadata.domain.AudioMetadataAccessors.fileMap;

@RequiredArgsConstructor
@Slf4j
public class Jid3LibMp3Loader implements AudioMetadataLoader {
	public void populate(AudioMetadata songMetadata) {
		toMp3File(songMetadata).ifPresent(mp3File -> copyTags(mp3File, songMetadata));
	}

	private void copyTags(MP3File mp3File, AudioMetadata songMetadata) {
		if (mp3File.hasID3v1Tag()) {
			ID3v1 id3v1 = mp3File.getID3v1Tag();
			songMetadata.putAudioTag("album", id3v1.getAlbum(), false);
			songMetadata.putAudioTag("author", id3v1.getArtist(),
					false);//folosit de winamp la salvare pe sony
			songMetadata.putAudioTag("title", id3v1.getTitle(), false);
			songMetadata.putAudioTag("id3v1.album", id3v1.getAlbum(), false);
			songMetadata.putAudioTag("id3v1.author", id3v1.getArtist(),
					false);//folosit de winamp la salvare pe sony
			songMetadata.putAudioTag("id3v1.title", id3v1.getTitle(), false);
			songMetadata.putAudioTag("backupDate", id3v1.getYear(), false);
			if (id3v1 instanceof ID3v1_1) {
				songMetadata.putAudioTag("id3v11.track", id3v1.getTrackNumberOnAlbum(), false);
			}
			songMetadata.putAudioTag("id3v11.genre", (String)
					TagConstant.genreIdToString.get(Long.valueOf(id3v1.getSongGenre())), false);
		}
		if (!mp3File.hasID3v2Tag()) {
			return;
		}
		for (Iterator<AbstractID3v2Frame> it1 = mp3File.getID3v2Tag().getFrameIterator(); it1.hasNext(); ) {
			AbstractID3v2Frame abstractID3v2Frame = it1.next();
			stringifyIdOf(abstractID3v2Frame).ifPresent(stringOfFrameId -> {
				String frameDescr;
				if (stringOfFrameId.startsWith(
						"Text: ") && stringOfFrameId.length() > "Text: ".length()) {
					frameDescr = stringOfFrameId.substring("Text: ".length());
				} else if (stringOfFrameId.startsWith(
						"URL: ") && stringOfFrameId.length() > "URL: ".length()) {
					frameDescr = stringOfFrameId.substring("URL: ".length());
				} else {
					return;
				}
				textOf(abstractID3v2Frame.getBody()).ifPresent(
						fragmentBodyText -> copyTags(frameDescr, fragmentBodyText, songMetadata));
			});
		}
	}

	private Optional<String> stringifyIdOf(AbstractID3v2Frame abstractID3v2Frame) {
		String identifier = identifierOf(abstractID3v2Frame);
		return switch (abstractID3v2Frame) {
			case ID3v2_4Frame v24 -> Optional.ofNullable(
					(String) TagConstant.id3v2_4FrameIdToString.get(identifier));
			case ID3v2_3Frame v23 -> Optional.ofNullable(
					(String) TagConstant.id3v2_3FrameIdToString.get(identifier));
			case ID3v2_2Frame v22 -> Optional.ofNullable(
					(String) TagConstant.id3v2_2FrameIdToString.get(identifier));
			default -> Optional.empty();
		};
	}

	private String identifierOf(AbstractID3v2Frame abstractID3v2Frame) {
		String identifier = abstractID3v2Frame.getIdentifier();
		if (identifier.length() > 4) {
			return identifier.substring(0, 4);
			// } else if (identifier.length() < 4) {
			//	log.debug("\nunder 4 chars v2Frame identifier: {}\n{}", identifier, song.getPath());
		}
		return identifier;
	}

	private void copyTags(String frameDescr, String fragmentBodyText,
			AudioMetadata songMetadata) {
		switch (frameDescr) {
			case "Band/orchestra/accompaniment" ->
					songMetadata.putAudioTag("mp3.id3tag.orchestra", fragmentBodyText, false);
			case "Year" -> songMetadata.putAudioTag("backupDate", fragmentBodyText, false);
			case "Original release year" ->
					songMetadata.putAudioTag("backupDate", fragmentBodyText, false);
			case "Date" -> songMetadata.putAudioTag("backupDate", fragmentBodyText, false);
			case "Album/Movie/Show title" -> {
				songMetadata.putAudioTag("album", fragmentBodyText, false);
				songMetadata.putAudioTag("id3v2.album", fragmentBodyText, false);
			}
			case "Content type" ->
					songMetadata.putAudioTag("mp3.id3tag.genre", fragmentBodyText, false);
			case "Title/songname/content description" -> {
				songMetadata.putAudioTag("title", fragmentBodyText, false);
				songMetadata.putAudioTag("id3v2.title", fragmentBodyText, false);
			}
			case "Track number/Position in set" ->
					songMetadata.putAudioTag("mp3.id3tag.track", fragmentBodyText, false);
			case "Composer" ->
					songMetadata.putAudioTag("mp3.id3tag.composer", fragmentBodyText, false);
			case "Lead performer(s)/Soloist(s)" -> {
				songMetadata.putAudioTag("author", fragmentBodyText, false);
				songMetadata.putAudioTag("id3v2.author", fragmentBodyText, false);
			}
			case "Publisher" ->
					songMetadata.putAudioTag("mp3.id3tag.publisher", fragmentBodyText, false);
			case "Comments" -> songMetadata.putAudioTag("comment", fragmentBodyText, false);
			case "Encoded by" ->
					songMetadata.putAudioTag("mp3.id3tag.encoded", fragmentBodyText, false);
			case "Part of a set" ->
					songMetadata.putAudioTag("mp3.id3tag.disc", fragmentBodyText, false);
			default -> songMetadata.putAudioTag("jid3." + frameDescr, fragmentBodyText, false);
		}
	}

	private Optional<MP3File> toMp3File(AudioMetadata songMetadata) {
		try {
			return fileMap(songMetadata, f -> new MP3File(f, false));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			log.error("\nFailed to create MP3File for:\n{}", songMetadata.uri());
		}
		return Optional.empty();
	}

	@SneakyThrows
	private Optional<String> textOf(AbstractMP3FragmentBody fragmentBody) {
		var it1 = fragmentBody.iterator();
		if (!it1.hasNext()) {
			return Optional.empty();
		}
		Object object = it1.next();
		if (object instanceof ObjectNumberHashMap objectNumberHashMap) {
			Long encodingId = (Long) objectNumberHashMap.getValue();
			String encoding = (String) objectNumberHashMap.getIdToString().get(encodingId);
			object = it1.next();
			if (object instanceof ObjectStringSizeTerminated stringSizeTerminated) {
				return Optional.of(new String(stringSizeTerminated.writeByteArray(), encoding));
			} else {
				while (it1.hasNext()) {
					object = it1.next();
					if (object instanceof ObjectStringSizeTerminated) {
						break;
					}
				}
				if (object instanceof ObjectStringSizeTerminated stringSizeTerminated) {
					return Optional.of(new String(stringSizeTerminated.writeByteArray(), encoding));
				}
			}
		}
		return Optional.empty();
	}
}
