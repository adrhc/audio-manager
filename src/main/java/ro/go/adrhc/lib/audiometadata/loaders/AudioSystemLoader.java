package ro.go.adrhc.lib.audiometadata.loaders;

import lombok.extern.slf4j.Slf4j;
import ro.go.adrhc.lib.audiometadata.AudioMetadataStreamLoader;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

@Slf4j
public class AudioSystemLoader extends AudioMetadataStreamLoader {
	public AudioSystemLoader(InputStream inputStream) {
		super(inputStream);
	}

	/**
	 * Standard parameters :
	 * - duration : [Long], duration in microseconds.
	 * - title : [String], Title of the stream.
	 * - author : [String], Name of the artist of the stream.
	 * - album : [String], Name of the album of the stream.
	 * - backupDate : [String], The backupDate (year) of the recording or release of the stream.
	 * - copyright : [String], Copyright message of the stream.
	 * - comment : [String], Comment of the stream.  Extended MP3 parameters :
	 * - mp3.version.mpeg : [String], mpeg version : 1,2 or 2.5
	 * - mp3.version.layer : [String], layer version 1, 2 or 3
	 * - mp3.version.encoding : [String], mpeg encoding : MPEG1, MPEG2-LSF, MPEG2.5-LSF
	 * - mp3.channels : [Integer], number of channels 1 : mono, 2 : stereo.
	 * - mp3.frequency.hz : [Integer], sampling rate in hz.
	 * - mp3.bitrate.nominal.bps : [Integer], nominal bitrate in bps.
	 * - mp3.length.bytes : [Integer], length in bytes.
	 * - mp3.length.frames : [Integer], length in frames.
	 * - mp3.framesize.bytes : [Integer], framesize of the first frame.
	 * framesize is not constant for VBR streams.
	 * - mp3.framerate.fps : [Float], framerate in frames per seconds.
	 * - mp3.header.pos : [Integer], position of first audio header (or ID3v2 size).
	 * - mp3.vbr : [Boolean], vbr flag.
	 * - mp3.vbr.scale : [Integer], vbr scale.
	 * - mp3.crc : [Boolean], crc flag.
	 * - mp3.original : [Boolean], original flag.
	 * - mp3.copyright : [Boolean], copyright flag.
	 * - mp3.padding : [Boolean], padding flag.
	 * - mp3.mode : [Integer], mode 0:STEREO 1:JOINT_STEREO 2:DUAL_CHANNEL 3:SINGLE_CHANNEL
	 * - mp3.id3tag.genre : [String], ID3 tag (v1 or v2) genre.
	 * - mp3.id3tag.track : [String], ID3 tag (v1 or v2) track info.
	 * - mp3.id3tag.v2 : [InputStream], ID3v2 frames.
	 * - mp3.shoutcast.metadata.key : [String], Shoutcast meta key with matching value.
	 * For instance :
	 * mp3.shoutcast.metadata.icy-irc=#shoutcast
	 * mp3.shoutcast.metadata.icy-metaint=8192
	 * mp3.shoutcast.metadata.icy-genre=Trance Techno Dance
	 * mp3.shoutcast.metadata.icy-url=http://www.di.fm
	 */
	@Override
	public void populate(AudioMetadata songMetadata) {
		audioFileFormat().ifPresent(audioFileFormat
				-> populateAudioMetadata(audioFileFormat, songMetadata));
			/*.ifPresentOrElse(
					audioFileFormat -> populateAudioMetadata(audioFileFormat, songMetadata),
					() -> log.error("\nAudioSystem failed to read properties of: {}", songMetadata.getPath()));*/
	}

	private void populateAudioMetadata(AudioFileFormat audioFileFormat,
			AudioMetadata songMetadata) {
		AudioFormat baseFormat = audioFileFormat.getFormat();
		songMetadata.addDuration(audioFileFormat.getFrameLength(), baseFormat.getFrameRate(),
				baseFormat.getChannels(), baseFormat.getFrameSize());
		Map<String, Object> properties = baseFormat.properties();
		songMetadata.putAudioTags(properties);
	}

	private Optional<AudioFileFormat> audioFileFormat() {
		try {
			return Optional.of(AudioSystem.getAudioFileFormat(inputStream));
		} catch (UnsupportedAudioFileException e) {
			// log.warn("\nFile of unsupported format: " + song.getFilePath());
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		return Optional.empty();
	}

	@FunctionalInterface
	private interface AudioFileFormatSupplier {
		AudioFileFormat get() throws UnsupportedAudioFileException, IOException;
	}
}
