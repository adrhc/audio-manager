package ro.go.adrhc.lib.audiometadata.loaders;

import lombok.extern.slf4j.Slf4j;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.springframework.util.StringUtils;
import org.xml.sax.ContentHandler;
import org.xml.sax.helpers.DefaultHandler;
import ro.go.adrhc.lib.audiometadata.AudioMetadataStreamLoader;
import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Optional;

@Slf4j
public class TikaMp3Loader extends AudioMetadataStreamLoader {
	public TikaMp3Loader(InputStream inputStream) {
		super(inputStream);
	}

	public void populate(AudioMetadata songMetadata) {
		metadataOf(inputStream)
				.ifPresentOrElse(metadata -> Arrays.stream(metadata.names())
						.filter(name -> StringUtils.hasText(metadata.get(name)) ||
								metadata.isMultiValued(name) &&
										(Arrays.stream(metadata.getValues(name)).anyMatch(
												StringUtils::hasText)))
						.forEach(name -> {
							String value = metadata.isMultiValued(name) ?
									String.join(" ", metadata.getValues(name)) : metadata.get(name);
							songMetadata.putAudioTag(name, value);
						}), () -> log.error("\nMp3Parser failed to parse: " + songMetadata.uri()));
	}

	private Optional<Metadata> metadataOf(InputStream inputStream) {
		try {
			ContentHandler handler = new DefaultHandler();
			Metadata metadata = new Metadata();
			Mp3Parser parser = new Mp3Parser();
			parser.parse(inputStream, handler, metadata, new ParseContext());
			return Optional.of(metadata);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return Optional.empty();
	}
}
