package ro.go.adrhc.lib.audiometadata.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ro.go.adrhc.util.stream.StreamAware;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Getter
public class AudioMetadataCollection implements StreamAware<AudioMetadata> {
	private final List<AudioMetadata> metadata;

	public static AudioMetadataCollection of(Stream<? extends AudioMetadata> stream) {
		return new AudioMetadataCollection(new ArrayList<>(stream.toList()));
	}

	public AudioMetadata getFirst() {
		return metadata.getFirst();
	}

	public boolean isEmpty() {
		return metadata.isEmpty();
	}

	public int size() {
		return metadata.size();
	}

	@Override
	public Stream<AudioMetadata> rawStream() {
		return metadata.stream();
	}
}
