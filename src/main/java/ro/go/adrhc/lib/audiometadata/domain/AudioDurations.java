package ro.go.adrhc.lib.audiometadata.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

@Getter
@Setter
@ToString
@Slf4j
public class AudioDurations {
	private final Set<Integer> durationsSet = new HashSet<>();

	public static AudioDurations of(String durations) {
		AudioDurations audioDurations = new AudioDurations();
		audioDurations.addDurations(durations);
		return audioDurations;
	}

	public static Stream<Integer> intStreamDurationsOf(String spaceSeparatedDurations) {
		// https://stackoverflow.com/questions/4731055/whitespace-matching-regex-java?answertab=active#tab-top
		return Arrays.stream(spaceSeparatedDurations.split("(?U)\\s+"))
				.map(Double::valueOf)
				.mapToInt(Double::intValue)
				.boxed();
	}

	public Integer getIfOne() {
		return durationsSet.size() == 1 ? durationsSet.iterator().next() : null;
	}

	public boolean hasDuration() {
		return !durationsSet.isEmpty();
	}

	public void addDurations(String spaceSeparatedDurations) {
		if (!StringUtils.hasText(spaceSeparatedDurations)) {
			return;
		}
		intStreamDurationsOf(spaceSeparatedDurations).forEach(this::addDuration);
	}

	/**
	 * Won't add negative or zero durations!
	 */
	public void addDuration(int frameLength, float frameRate, int channels, int frameSize) {
		if (frameRate <= 0f || channels <= 0 || frameSize <= 0) {
			return;
		}
		addDuration((int) (frameLength / frameRate / channels / frameSize));
	}

	protected void addDuration(int duration) {
		if (!DurationValidators.isValid(duration)) {
			return;
		}
		durationsSet.add(duration);
	}

	public boolean isEmpty() {
		return durationsSet.isEmpty();
	}
}
