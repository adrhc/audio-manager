package ro.go.adrhc.lib.audiometadata.domain;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import ro.go.adrhc.persistence.lucene.core.typed.Mergeable;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Slf4j
@Getter
@ToString
public class AudioTags implements Mergeable<AudioTags> {
	private final Map<String, String> tags = new TreeMap<>();

	public static AudioTags of(String key, String value) {
		AudioTags audioTags = new AudioTags();
		audioTags.putTag(key, value);
		return audioTags;
	}

	public Set<String> getSimilarTagNames(String key) {
		String keyLC = key.toLowerCase();
		return tags.keySet().stream().filter(it -> it.contains(keyLC)).collect(Collectors.toSet());
	}

	public String getTag(String key) {
		return tags.get(key.toLowerCase());
	}

	public void putTag(String key, String value) {
		putTag(key, value, true);
	}

	public void putTag(String key, String value, boolean overwrite) {
		String keyLC = key.toLowerCase();
		if (!StringUtils.hasText(value)) {
			if (overwrite) {
				log.warn("\nTag {} will be removed!", keyLC);
				tags.remove(keyLC);
			}
			return;
		}
		String valueLC = value.toLowerCase();
		if (getTag(keyLC) == null) {
			tags.put(keyLC, valueLC);
		} else if (overwrite) {
			log.warn("\nTag {} will be overwritten with {}!", key, valueLC);
			tags.put(keyLC, valueLC);
		}
	}

	public void putTags(Map<String, ?> tags) {
		tags.forEach((k, v) -> putTag(k, v == null ? null : v.toString()));
	}

	public boolean isEmpty() {
		return tags.isEmpty();
	}

	public AudioTags merge(AudioTags audioTags) {
		AudioTags result = new AudioTags();
		result.putTags(tags);
		result.putTags(audioTags.tags);
		return result;
	}
}
