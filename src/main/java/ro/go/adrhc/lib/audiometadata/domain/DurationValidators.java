package ro.go.adrhc.lib.audiometadata.domain;

public class DurationValidators {
	public static boolean isValid(Integer duration) {
		return duration != null && isValid((int) duration);
	}

	public static boolean isValid(int duration) {
		return duration > 0;
	}
}
