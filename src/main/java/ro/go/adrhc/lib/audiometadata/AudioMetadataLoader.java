package ro.go.adrhc.lib.audiometadata;

import ro.go.adrhc.lib.audiometadata.domain.AudioMetadata;

public interface AudioMetadataLoader {
	void populate(AudioMetadata songMetadata);
}
