# ERROR: feign.FeignException$Forbidden: [403 Forbidden] during [POST] to ...

It means that "YouTube Music" doesn't work! e.g. YouTube Music -> Liked Songs doesn't work and has nothing
to do with "YouTube"! e.g. it is NOT related to YouTube -> My YouTube playlists -> Likes Alternative

Solve by fixing music-curl-path: .../config/yt-music-liked-curl.txt.
Verify yt-music-liked-curl.txt validity with YtLikedMusicRestClientIT or with "fix-yt-music-auth" command.

Update using "CURL (bash)" of YouTube Music -> Library -> Your Likes then RESTART Mopidy!

```bash
./mvnw test -Dtest="YtLikedMusicParamsTest"
./mvnw test -Dtest="YtLikedMusicRestClientIT"
./mvnw test -Dtest="YtLikedMusicMetadataProviderIT#isYouTubeMusicAuthValid"
```

# libraries

https://www.sauronsoftware.it/projects/jave/download.php

# bash scripts

```bash
mvn install:install-file -DgroupId=it.sauronsoftware -DartifactId=jave -Dversion=1.0.2 -Dpackaging=jar -Dfile=/$HOME/temp/jave-1.0.2/jave-1.0.2.jar
mvn install:install-file -DgroupId=it.sauronsoftware -DartifactId=jave -Dversion=1.0.2 -Dpackaging=jar -Dfile=/$HOME/Downloads/jave-1.0.2.jar
```

# batch or powershell scripts

```bash
mvn install:install-file -DgroupId="it.sauronsoftware" -DartifactId=jave -Dversion="1.0.2" -Dpackaging=jar -Dfile="%homedrive%%homepath%\Projects-adrhc\jave-1.0.2\jave-1.0.2.jar"
mvn install:install-file -DgroupId="it.sauronsoftware" -DartifactId=jave -Dversion="1.0.2" -Dpackaging=jar -Dfile="jave-1.0.2.jar"
```

# Luke (Lucene)

https://github.com/DmitryKey/luke.git  
Binary releases: lucene-8.11.0.tgz  
https://www.apache.org/dyn/closer.lua/lucene/java/8.11.0/lucene-8.11.0.tgz -> lucene-8.11.0\lucene-8.11.0\luke\luke.sh

# jq

```bash
jq '.. | .videoId? | select(.)' youtube-music-likes.json
((JSONArray) JsonPath.read(json, "$..videoId")).stream().map(Object::toString).collect(Collectors.toSet());
```

# youtube-dl

https://github.com/ytdl-org/youtube-dl plugins:
Get cookies.txt LOCALLY (for Chrome) or cookies.txt (for Firefox)
`./mvnw test -Dtest="YoutubeDlIT"`

# Useful links

https://www.shazam.com/myshazam

# maven commands

mvn archetype:generate -DgroupId=ro.go.adrhc -DartifactId=playlist -DarchetypeArtifactId=maven-archetype-quickstart
mvn archetype:generate -DgroupId=ro.go.adrhc -DartifactId=deduplicator -DarchetypeArtifactId=maven-archety

# MacBook

keytool -importcert -file $HOME/Downloads/repo1.maven.org.cer -alias maven-central-repo1 -keystore
/usr/local/opt/sdkman-cli/libexec/candidates/java/current/lib/security/cacerts
keytool -importcert -file $HOME/Downloads/repo.maven.apache.org.cer -alias maven-central-repo.maven.apache -keystore
/usr/local/opt/sdkman-cli/libexec/candidates/java/current/lib/security/cacerts
keytool -importcert -file $HOME/Downloads/repo.spring.io.cer -alias maven-central-repo.spring.io -keystore
/usr/local/opt/sdkman-cli/libexec/candidates/java/current/lib/security/cacerts
keytool -importcert -file $HOME/Downloads/pyszny.vls.icm.edu.cer -alias maven-central-pyszny.vls.icm.edu -keystore
/usr/local/opt/sdkman-cli/libexec/candidates/java/current/lib/security/cacerts

# Various java tests

```bash
./mvnw test -Dtest="AppPathsStringifierTest"
```