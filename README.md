# m3u8 playlists manager

The playlists can contain references to:  
- local files
- YouTube videos or music
- online radios
- URLs

Features:  
- repair, sort, merge, de-duplicate, add YouTube references
- index and search local media
- change from YouTube references to local files
- create bash scripts to download missing YouTube media
- define playlist specifications describing directories and other playlists, local or YouTube, to incorporate 
- create playlists by specification 
- playlist files management (i.e. one doesn't have to exit to e.g. view, rename, etc a file)

# usage

## bash alias

```bash
alias fixmp3='java20 -jar $HOME/Projects-adrhc/audio-manager/m3u8-playlists-manager/target/playlist-0.0.1-SNAPSHOT.jar --spring.config.additional-location=$HOME/Projects-adrhc/audio-manager/m3u8-playlists-manager/config/'
```

### Fix yt-music-liked-curl.txt

Copy as "CURL (bash)" into
yt-music-liked-curl.txt the 1st called URL of YouTube Music -> Library -> Your Likes.
Use "fix-yt-music-auth" command to update /var/lib/mopidy/.config/auth.json then restart Mopidy (
Raspberry Pi too).

## video-curl-path: .../config/yt-video-liked-curl.txt

Go to https://www.youtube.com/playlist?list=LL then copy its bash CURL.

for the moment this is not used

# debugging

## run with spring boot

```bash
./mvnw -DskipTests clean package
./mvnw spring-boot:run --spring.config.additional-location=file:///home/...
```

## run the (spring boot fat) jar

```batch
%homedrive%%homepath%\scoop\apps\openjdk\17.0.1-12\bin\java --enable-preview -jar %homedrive%%homepath%\Projects-adrhc\audio-manager\audio-manager\m3u8-playlists-manager\target\playlist-0.0.1-SNAPSHOT.jar --spring.config.additional-location=file:///%homedrive%%homepath%/Projects-adrhc/audio-manager/m3u8-playlists-manager/config/
```

pe-quickstart